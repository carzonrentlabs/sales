﻿// JScript File
// AUTHOR      : NIRAJ CJANDRA jOSHI
// CREATED ON  : 21-FEB-09
// DESCRIPTION : CONTAINS SCRIPT FOR DISABLE BUTTON CONTROL WHILE CLICK ON SUBMIT BUTTON
//---------------------------------------------------------------------------------------
function CheckDetails(con)

{

//here I am checking whether page is validated. so you can use validation controls too...

if(Page_ClientValidate() == true)

{

//hide the original submit button

con.style.display = "none"; 

//set html button text to original button text

document.getElementById("btnhide").value = con.value; 

//show the html button instead of original button

document.getElementById("btnhide").style.display = "";

return true;

}

else

{

return false;

}

}

