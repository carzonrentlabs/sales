﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;
using System.IO;


public partial class Sales_WayPlan : System.Web.UI.Page
{
    private WayPlanReport objWayPlan = null;
    Dictionary<string, Int64> transDic = new Dictionary<string, Int64>();
    Dictionary<string, Int64> transAcqui = new Dictionary<string, Int64>();
    Dictionary<string, Int64> GrandTotalDic = new Dictionary<string, Int64>();
    Dictionary<string, double> AVGLTR = new Dictionary<string, double>();
    Dictionary<string, double> AVGSR = new Dictionary<string, double>();
    Dictionary<string, Int64> TargetDic = new Dictionary<string, Int64>();
    Dictionary<string, int> LTRDic = new Dictionary<string, int>();
    Dictionary<string, double> spotRentDic = new Dictionary<string, double>();
    protected void Page_Load(object sender, EventArgs e)
    {
        StringBuilder strData = new StringBuilder();
        if (!Page.IsPostBack)
        {
            BindCityName();
        }

    }

    private void BindCityName()
    {
        objWayPlan = new WayPlanReport();
        DataSet ds = new DataSet();
        ds = objWayPlan.GetCityName();
        if (ds.Tables[0].Rows.Count > 0)
        {
            ddlCity.DataSource = ds.Tables[0];
            ddlCity.DataTextField = "CityName";
            ddlCity.DataValueField = "CityID";
            ddlCity.DataBind();
            ddlCity.Items.Insert(0, new ListItem("-Select-", "0"));
        }
    }
    private void InitializedDic(Dictionary<string, Int64> strDic)
    {
        strDic.Add("1", 0);
        strDic.Add("2", 0);
        strDic.Add("3", 0);
        strDic.Add("4", 0);
        strDic.Add("5", 0);
        strDic.Add("6", 0);
        strDic.Add("7", 0);
        strDic.Add("8", 0);
        strDic.Add("9", 0);
        strDic.Add("10", 0);
        strDic.Add("11", 0);
        strDic.Add("12", 0);
    }
    private void InitializedDic(Dictionary<string, double> strDic)
    {
        strDic.Add("1", 0);
        strDic.Add("2", 0);
        strDic.Add("3", 0);
        strDic.Add("4", 0);
        strDic.Add("5", 0);
        strDic.Add("6", 0);
        strDic.Add("7", 0);
        strDic.Add("8", 0);
        strDic.Add("9", 0);
        strDic.Add("10", 0);
        strDic.Add("11", 0);
        strDic.Add("12", 0);
    }
    private void InitializedLTR(int cityID, Dictionary<string, int> strDic)
    {
        if (cityID == 4)
        {
            strDic.Add("1", 7);
            strDic.Add("2", 7);
            strDic.Add("3", 7);
            strDic.Add("4", 7);
            strDic.Add("5", 7);
            strDic.Add("6", 7);
            strDic.Add("7", 7);
            strDic.Add("8", 7);
            strDic.Add("9", 7);
            strDic.Add("10", 7);
            strDic.Add("11", 7);
            strDic.Add("12", 7);
        }
        else if (cityID == 2)
        {
            strDic.Add("1", 59);
            strDic.Add("2", 61);
            strDic.Add("3", 63);
            strDic.Add("4", 44);
            strDic.Add("5", 45);
            strDic.Add("6", 47);
            strDic.Add("7", 48);
            strDic.Add("8", 50);
            strDic.Add("9", 51);
            strDic.Add("10", 53);
            strDic.Add("11", 56);
            strDic.Add("12", 57);
        }
        else if (cityID == 1 || cityID == 6)
        {
            strDic.Add("1", 0);
            strDic.Add("2", 0);
            strDic.Add("3", 0);
            strDic.Add("4", 0);
            strDic.Add("5", 0);
            strDic.Add("6", 0);
            strDic.Add("7", 0);
            strDic.Add("8", 0);
            strDic.Add("9", 0);
            strDic.Add("10", 0);
            strDic.Add("11", 0);
            strDic.Add("12", 0);
        }
        else if (cityID == 7)
        {
            strDic.Add("1", 125);
            strDic.Add("2", 125);
            strDic.Add("3", 125);
            strDic.Add("4", 100);
            strDic.Add("5", 100);
            strDic.Add("6", 100);
            strDic.Add("7", 100);
            strDic.Add("8", 100);
            strDic.Add("9", 125);
            strDic.Add("10", 125);
            strDic.Add("11", 125);
            strDic.Add("12", 125);
        }
        else if (cityID == 8)
        {
            strDic.Add("1", 45);
            strDic.Add("2", 45);
            strDic.Add("3", 46);
            strDic.Add("4", 38);
            strDic.Add("5", 39);
            strDic.Add("6", 39);
            strDic.Add("7", 40);
            strDic.Add("8", 40);
            strDic.Add("9", 42);
            strDic.Add("10", 42);
            strDic.Add("11", 42);
            strDic.Add("12", 42);
        }
        else if (cityID == 6)
        {
            strDic.Add("1", 4);
            strDic.Add("2", 6);
            strDic.Add("3", 10);
            strDic.Add("4", 3);
            strDic.Add("5", 3);
            strDic.Add("6", 3);
            strDic.Add("7", 3);
            strDic.Add("8", 3);
            strDic.Add("9", 3);
            strDic.Add("10", 3);
            strDic.Add("11", 3);
            strDic.Add("12", 4);
        }
        else if (cityID == 15)
        {
            strDic.Add("1", 4);
            strDic.Add("2", 6);
            strDic.Add("3", 10);
            strDic.Add("4", 3);
            strDic.Add("5", 3);
            strDic.Add("6", 3);
            strDic.Add("7", 3);
            strDic.Add("8", 3);
            strDic.Add("9", 3);
            strDic.Add("10", 3);
            strDic.Add("11", 3);
            strDic.Add("12", 4);
        }
        else if (cityID == 5)
        {
            strDic.Add("1", 1);
            strDic.Add("2", 1);
            strDic.Add("3", 1);
            strDic.Add("4", 1);
            strDic.Add("5", 1);
            strDic.Add("6", 1);
            strDic.Add("7", 1);
            strDic.Add("8", 1);
            strDic.Add("9", 1);
            strDic.Add("10", 1);
            strDic.Add("11", 1);
            strDic.Add("12", 1);
        }
    }
    private StringBuilder BindExcelData(int cityId, string financialYear)
    {
        StringBuilder strData = new StringBuilder();
        StringBuilder strData1 = new StringBuilder();
        StringBuilder strData2 = new StringBuilder();
        objWayPlan = new WayPlanReport();
        DataSet dsGroupEnity = new DataSet();
        dsGroupEnity = objWayPlan.GetGroupEntity(cityId);
        string startYear = financialYear.Split('-')[0];
        string endYear = financialYear.Split('-')[1];

        if (dsGroupEnity.Tables[0].Rows.Count > 0)
        {
            strData.Append("<html xmlns:o='urn:schemas-microsoft-com:office:office'");
            strData.Append("xmlns:x='urn:schemas-microsoft-com:office:excel'");
            strData.Append("xmlns='http://www.w3.org/TR/REC-html40'>");
            strData.Append("<head>");
            strData.Append("<meta http-equiv=Content-Type content='text/html; charset=windows-1252'>");
            strData.Append("<meta name=ProgId content=Excel.Sheet>");
            strData.Append("<meta name=Generator content='Microsoft Excel 14'>");
            strData.Append("<link rel=File-List");
            strData.Append("href='Way%20Forward%20Plan%20for%2017-18%20-%20Mumbai_files/filelist.xml'>");
            strData.Append("<style id='Way Forward Plan for 17-18 - Mumbai_10558_Styles'>");
            strData.Append("<!--table");
            strData.Append("{mso-displayed-decimal-separator:';");
            strData.Append("mso-displayed-thousand-separator:';}");
            strData.Append(".xl6610558");
            strData.Append("{padding-top:1px;");
            strData.Append("padding-right:1px;");
            strData.Append("padding-left:1px;");
            strData.Append("mso-ignore:padding;");
            strData.Append("color:black;");
            strData.Append("font-size:11.0pt;");
            strData.Append("font-weight:400;");
            strData.Append("font-style:normal;");
            strData.Append("text-decoration:none;");
            strData.Append("font-family:Calibri, sans-serif;");
            strData.Append("mso-font-charset:0;");
            strData.Append("mso-number-format:General;");
            strData.Append("text-align:general;");
            strData.Append("vertical-align:bottom;");
            strData.Append("border:.5pt solid windowtext;");
            strData.Append("background:#FFC000;");
            strData.Append("mso-pattern:black none;");
            strData.Append("white-space:nowrap;}");
            strData.Append(".xl6710558");
            strData.Append("{padding-top:1px;");
            strData.Append("padding-right:1px;");
            strData.Append("padding-left:1px;");
            strData.Append("mso-ignore:padding;");
            strData.Append("color:black;");
            strData.Append("font-size:11.0pt;");
            strData.Append("font-weight:400;");
            strData.Append("font-style:normal;");
            strData.Append("text-decoration:none;");
            strData.Append("font-family:Calibri, sans-serif;");
            strData.Append("mso-font-charset:0;");
            strData.Append("mso-number-format:0;");
            strData.Append("text-align:general;");
            strData.Append("vertical-align:bottom;");
            strData.Append("border:.5pt solid windowtext;");
            strData.Append("background:#FFC000;");
            strData.Append("mso-pattern:black none;");
            strData.Append("white-space:nowrap;}");
            strData.Append(".xl6810558");
            strData.Append("{padding-top:1px;");
            strData.Append("padding-right:1px;");
            strData.Append("padding-left:1px;");
            strData.Append("mso-ignore:padding;");
            strData.Append("color:black;");
            strData.Append("font-size:11.0pt;");
            strData.Append("font-weight:400;");
            strData.Append("font-style:normal;");
            strData.Append("text-decoration:none;");
            strData.Append("font-family:Calibri, sans-serif;");
            strData.Append("mso-font-charset:0;");
            strData.Append("mso-number-format: ';");
            strData.Append("text-align:general;");
            strData.Append("vertical-align:bottom;");
            strData.Append("border:.5pt solid windowtext;");
            strData.Append("background:#FFC000;");
            strData.Append("mso-pattern:black none;");
            strData.Append("white-space:nowrap;}");
            strData.Append(".xl6910558");
            strData.Append("{padding-top:1px;");
            strData.Append("padding-right:1px;");
            strData.Append("padding-left:1px;");
            strData.Append("mso-ignore:padding;");
            strData.Append("color:black;");
            strData.Append("font-size:11.0pt;");
            strData.Append("font-weight:400;");
            strData.Append("font-style:normal;");
            strData.Append("text-decoration:none;");
            strData.Append("font-family:Calibri, sans-serif;");
            strData.Append("mso-font-charset:0;");
            strData.Append("mso-number-format:General;");
            strData.Append("text-align:center;");
            strData.Append("vertical-align:bottom;");
            strData.Append("mso-background-source:auto;");
            strData.Append("mso-pattern:auto;");
            strData.Append("white-space:normal;}");
            strData.Append(".xl7010558");
            strData.Append("{padding-top:1px;");
            strData.Append("padding-right:1px;");
            strData.Append("padding-left:1px;");
            strData.Append("mso-ignore:padding;");
            strData.Append("color:black;");
            strData.Append("font-size:11.0pt;");
            strData.Append("font-weight:400;");
            strData.Append("font-style:normal;");
            strData.Append("text-decoration:none;");
            strData.Append("font-family:Calibri, sans-serif;");
            strData.Append("mso-font-charset:0;");
            strData.Append("mso-number-format:0;");
            strData.Append("text-align:general;");
            strData.Append("vertical-align:bottom;");
            strData.Append("mso-background-source:auto;");
            strData.Append("mso-pattern:auto;");
            strData.Append("white-space:nowrap;}");
            strData.Append(".xl7110558");
            strData.Append("{padding-top:1px;");
            strData.Append("padding-right:1px;");
            strData.Append("padding-left:1px;");
            strData.Append("mso-ignore:padding;");
            strData.Append("color:black;");
            strData.Append("font-size:11.0pt;");
            strData.Append("font-weight:700;");
            strData.Append("font-style:normal;");
            strData.Append("text-decoration:none;");
            strData.Append("font-family:Calibri, sans-serif;");
            strData.Append("mso-font-charset:0;");
            strData.Append("mso-number-format:General;");
            strData.Append("text-align:center;");
            strData.Append("vertical-align:bottom;");
            strData.Append("mso-background-source:auto;");
            strData.Append("mso-pattern:auto;");
            strData.Append("white-space:nowrap;}");
            strData.Append(".xl7210558");
            strData.Append("{padding-top:1px;");
            strData.Append("padding-right:1px;");
            strData.Append("padding-left:1px;");
            strData.Append("mso-ignore:padding;");
            strData.Append("color:black;");
            strData.Append("font-size:11.0pt;");
            strData.Append("font-weight:700;");
            strData.Append("font-style:normal;");
            strData.Append("text-decoration:none;");
            strData.Append("font-family:Calibri, sans-serif;");
            strData.Append("mso-font-charset:0;");
            strData.Append("mso-number-format:';");
            strData.Append("text-align:general;");
            strData.Append("vertical-align:bottom;");
            strData.Append("border:.5pt solid windowtext;");
            strData.Append("background:#FFC000;");
            strData.Append("mso-pattern:black none;");
            strData.Append("white-space:nowrap;}");
            strData.Append(".xl7310558");
            strData.Append("{padding-top:1px;");
            strData.Append("padding-right:1px;");
            strData.Append("padding-left:1px;");
            strData.Append("mso-ignore:padding;");
            strData.Append("color:black;");
            strData.Append("font-size:11.0pt;");
            strData.Append("font-weight:700;");
            strData.Append("font-style:normal;");
            strData.Append("text-decoration:none;");
            strData.Append("font-family:Calibri, sans-serif;");
            strData.Append("mso-font-charset:0;");
            strData.Append("mso-number-format:0;");
            strData.Append("text-align:general;");
            strData.Append("vertical-align:bottom;");
            strData.Append("border:.5pt solid windowtext;");
            strData.Append("background:#FFC000;");
            strData.Append("mso-pattern:black none;");
            strData.Append("white-space:nowrap;}");
            strData.Append(".xl7410558");
            strData.Append("{padding-top:1px;");
            strData.Append("padding-right:1px;");
            strData.Append("padding-left:1px;");
            strData.Append("mso-ignore:padding;");
            strData.Append("color:black;");
            strData.Append("font-size:11.0pt;");
            strData.Append("font-weight:400;");
            strData.Append("font-style:normal;");
            strData.Append("text-decoration:none;");
            strData.Append("font-family:Calibri, sans-serif;");
            strData.Append("mso-font-charset:0;");
            strData.Append("mso-number-format:General;");
            strData.Append("text-align:general;");
            strData.Append("vertical-align:bottom;");
            strData.Append("mso-background-source:auto;");
            strData.Append("mso-pattern:auto;");
            strData.Append("white-space:normal;}");
            strData.Append(".xl7510558");
            strData.Append("{padding-top:1px;");
            strData.Append("padding-right:1px;");
            strData.Append("padding-left:1px;");
            strData.Append("mso-ignore:padding;");
            strData.Append("color:black;");
            strData.Append("font-size:11.0pt;");
            strData.Append("font-weight:700;");
            strData.Append("font-style:normal;");
            strData.Append("text-decoration:none;");
            strData.Append("font-family:Calibri, sans-serif;");
            strData.Append("mso-font-charset:0;");
            strData.Append("mso-number-format:General;");
            strData.Append("text-align:center;");
            strData.Append("vertical-align:bottom;");
            strData.Append("border:.5pt solid windowtext;");
            strData.Append("mso-background-source:auto;");
            strData.Append("mso-pattern:auto;");
            strData.Append("white-space:normal;}");
            strData.Append(".xl7610558");
            strData.Append("{padding-top:1px;");
            strData.Append("padding-right:1px;");
            strData.Append("padding-left:1px;");
            strData.Append("mso-ignore:padding;");
            strData.Append("color:black;");
            strData.Append("font-size:11.0pt;");
            strData.Append("font-weight:700;");
            strData.Append("font-style:normal;");
            strData.Append("text-decoration:none;");
            strData.Append("font-family:Calibri, sans-serif;");
            strData.Append("mso-font-charset:0;");
            strData.Append("mso-number-format:General;");
            strData.Append("text-align:general;");
            strData.Append("vertical-align:bottom;");
            strData.Append("border:.5pt solid windowtext;");
            strData.Append("background:#92D050;");
            strData.Append("mso-pattern:black none;");
            strData.Append("white-space:nowrap;}");
            strData.Append(".xl7710558");
            strData.Append("{padding-top:1px;");
            strData.Append("padding-right:1px;");
            strData.Append("padding-left:1px;");
            strData.Append("mso-ignore:padding;");
            strData.Append("color:black;");
            strData.Append("font-size:11.0pt;");
            strData.Append("font-weight:700;");
            strData.Append("font-style:normal;");
            strData.Append("text-decoration:none;");
            strData.Append("font-family:Calibri, sans-serif;");
            strData.Append("mso-font-charset:0;");
            strData.Append("mso-number-format:0;");
            strData.Append("text-align:center;");
            strData.Append("vertical-align:bottom;");
            strData.Append("border:.5pt solid windowtext;");
            strData.Append("background:#92D050;");
            strData.Append("mso-pattern:black none;");
            strData.Append("white-space:nowrap;}");
            strData.Append(".xl7810558");
            strData.Append("{padding-top:1px;");
            strData.Append("padding-right:1px;");
            strData.Append("padding-left:1px;");
            strData.Append("mso-ignore:padding;");
            strData.Append("color:black;");
            strData.Append("font-size:11.0pt;");
            strData.Append("font-weight:700;");
            strData.Append("font-style:normal;");
            strData.Append("text-decoration:none;");
            strData.Append("font-family:Calibri, sans-serif;");
            strData.Append("mso-font-charset:0;");
            strData.Append("mso-number-format:0;");
            strData.Append("text-align:center;");
            strData.Append("vertical-align:bottom;");
            strData.Append("border:.5pt solid windowtext;");
            strData.Append("mso-background-source:auto;");
            strData.Append("mso-pattern:auto;");
            strData.Append("white-space:normal;}");
            strData.Append(".xl7910558");
            strData.Append("{padding-top:1px;");
            strData.Append("padding-right:1px;");
            strData.Append("padding-left:1px;");
            strData.Append("mso-ignore:padding;");
            strData.Append("color:black;");
            strData.Append("font-size:11.0pt;");
            strData.Append("font-weight:700;");
            strData.Append("font-style:normal;");
            strData.Append("text-decoration:none;");
            strData.Append("font-family:Calibri, sans-serif;");
            strData.Append("mso-font-charset:0;");
            strData.Append("mso-number-format:General;");
            strData.Append("text-align:left;");
            strData.Append("vertical-align:bottom;");
            strData.Append("border:.5pt solid windowtext;");
            strData.Append("mso-background-source:auto;");
            strData.Append("mso-pattern:auto;");
            strData.Append("white-space:normal;}");
            strData.Append(".xl8010558");
            strData.Append("{padding-top:1px;");
            strData.Append("padding-right:1px;");
            strData.Append("padding-left:1px;");
            strData.Append("mso-ignore:padding;");
            strData.Append("color:black;");
            strData.Append("font-size:11.0pt;");
            strData.Append("font-weight:400;");
            strData.Append("font-style:normal;");
            strData.Append("text-decoration:none;");
            strData.Append("font-family:Calibri, sans-serif;");
            strData.Append("mso-font-charset:0;");
            strData.Append("mso-number-format:General;");
            strData.Append("text-align:general;");
            strData.Append("vertical-align:bottom;");
            strData.Append("border:.5pt solid windowtext;");
            strData.Append("mso-background-source:auto;");
            strData.Append("mso-pattern:auto;");
            strData.Append("white-space:nowrap;}");
            strData.Append(".xl8110558");
            strData.Append("{padding-top:1px;");
            strData.Append("padding-right:1px;");
            strData.Append("padding-left:1px;");
            strData.Append("mso-ignore:padding;");
            strData.Append("color:black;");
            strData.Append("font-size:11.0pt;");
            strData.Append("font-weight:700;");
            strData.Append("font-style:normal;");
            strData.Append("text-decoration:none;");
            strData.Append("font-family:Calibri, sans-serif;");
            strData.Append("mso-font-charset:0;");
            strData.Append("mso-number-format:General;");
            strData.Append("text-align:center;");
            strData.Append("vertical-align:bottom;");
            strData.Append("border:.5pt solid windowtext;");
            strData.Append("mso-background-source:auto;");
            strData.Append("mso-pattern:auto;");
            strData.Append("white-space:nowrap;}");
            strData.Append(".xl8210558");
            strData.Append("{padding-top:1px;");
            strData.Append("padding-right:1px;");
            strData.Append("padding-left:1px;");
            strData.Append("mso-ignore:padding;");
            strData.Append("color:black;");
            strData.Append("font-size:11.0pt;");
            strData.Append("font-weight:400;");
            strData.Append("font-style:normal;");
            strData.Append("text-decoration:none;");
            strData.Append("font-family:Calibri, sans-serif;");
            strData.Append("mso-font-charset:0;");
            strData.Append("mso-number-format:';");
            strData.Append("text-align:general;");
            strData.Append("vertical-align:bottom;");
            strData.Append("border:.5pt solid windowtext;");
            strData.Append("mso-background-source:auto;");
            strData.Append("mso-pattern:auto;");
            strData.Append("white-space:nowrap;}");
            strData.Append(".xl8310558");
            strData.Append("{padding-top:1px;");
            strData.Append("padding-right:1px;");
            strData.Append("padding-left:1px;");
            strData.Append("mso-ignore:padding;");
            strData.Append("color:black;");
            strData.Append("font-size:11.0pt;");
            strData.Append("font-weight:400;");
            strData.Append("font-style:normal;");
            strData.Append("text-decoration:none;");
            strData.Append("font-family:Calibri, sans-serif;");
            strData.Append("mso-font-charset:0;");
            strData.Append("mso-number-format:';");
            strData.Append("text-align:general;");
            strData.Append("vertical-align:bottom;");
            strData.Append("border:.5pt solid windowtext;");
            strData.Append("mso-background-source:auto;");
            strData.Append("mso-pattern:auto;");
            strData.Append("white-space:nowrap;}");
            strData.Append(".xl8410558");
            strData.Append("{padding-top:1px;");
            strData.Append("padding-right:1px;");
            strData.Append("padding-left:1px;");
            strData.Append("mso-ignore:padding;");
            strData.Append("color:black;");
            strData.Append("font-size:11.0pt;");
            strData.Append("font-weight:400;");
            strData.Append("font-style:normal;");
            strData.Append("text-decoration:none;");
            strData.Append("font-family:Calibri, sans-serif;");
            strData.Append("mso-font-charset:0;");
            strData.Append("mso-number-format:0;");
            strData.Append("text-align:general;");
            strData.Append("vertical-align:bottom;");
            strData.Append("border:.5pt solid windowtext;");
            strData.Append("mso-background-source:auto;");
            strData.Append("mso-pattern:auto;");
            strData.Append("white-space:nowrap;}");
            strData.Append(".xl8510558");
            strData.Append("{padding-top:1px;");
            strData.Append("padding-right:1px;");
            strData.Append("padding-left:1px;");
            strData.Append("mso-ignore:padding;");
            strData.Append("color:black;");
            strData.Append("font-size:11.0pt;");
            strData.Append("font-weight:700;");
            strData.Append("font-style:normal;");
            strData.Append("text-decoration:none;");
            strData.Append("font-family:Calibri, sans-serif;");
            strData.Append("mso-font-charset:0;");
            strData.Append("mso-number-format:';");
            strData.Append("text-align:general;");
            strData.Append("vertical-align:bottom;");
            strData.Append("border:.5pt solid windowtext;");
            strData.Append("mso-background-source:auto;");
            strData.Append("mso-pattern:auto;");
            strData.Append("white-space:nowrap;}");
            strData.Append(".xl8610558");
            strData.Append("{padding-top:1px;");
            strData.Append("padding-right:1px;");
            strData.Append("padding-left:1px;");
            strData.Append("mso-ignore:padding;");
            strData.Append("color:black;");
            strData.Append("font-size:11.0pt;");
            strData.Append("font-weight:700;");
            strData.Append("font-style:normal;");
            strData.Append("text-decoration:none;");
            strData.Append("font-family:Calibri, sans-serif;");
            strData.Append("mso-font-charset:0;");
            strData.Append("mso-number-format:General;");
            strData.Append("text-align:general;");
            strData.Append("vertical-align:bottom;");
            strData.Append("border:.5pt solid windowtext;");
            strData.Append("background:#FFC000;");
            strData.Append("mso-pattern:black none;");
            strData.Append("white-space:nowrap;}");
            strData.Append(".xl8710558");
            strData.Append("{padding-top:1px;");
            strData.Append("padding-right:1px;");
            strData.Append("padding-left:1px;");
            strData.Append("mso-ignore:padding;");
            strData.Append("color:black;");
            strData.Append("font-size:11.0pt;");
            strData.Append("font-weight:700;");
            strData.Append("font-style:normal;");
            strData.Append("text-decoration:none;");
            strData.Append("font-family:Calibri, sans-serif;");
            strData.Append("mso-font-charset:0;");
            strData.Append("mso-number-format:';");
            strData.Append("text-align:general;");
            strData.Append("vertical-align:bottom;");
            strData.Append("border:.5pt solid windowtext;");
            strData.Append("mso-background-source:auto;");
            strData.Append("mso-pattern:auto;");
            strData.Append("white-space:nowrap;}");
            strData.Append(".xl8810558");
            strData.Append("{padding-top:1px;");
            strData.Append("padding-right:1px;");
            strData.Append("padding-left:1px;");
            strData.Append("mso-ignore:padding;");
            strData.Append("color:black;");
            strData.Append("font-size:11.0pt;");
            strData.Append("font-weight:400;");
            strData.Append("font-style:normal;");
            strData.Append("text-decoration:none;");
            strData.Append("font-family:Calibri, sans-serif;");
            strData.Append("mso-font-charset:0;");
            strData.Append("mso-number-format:General;");
            strData.Append("text-align:center;");
            strData.Append("vertical-align:bottom;");
            strData.Append("border:.5pt solid windowtext;");
            strData.Append("mso-background-source:auto;");
            strData.Append("mso-pattern:auto;");
            strData.Append("white-space:normal;}");
            strData.Append(".xl8910558");
            strData.Append("{padding-top:1px;");
            strData.Append("padding-right:1px;");
            strData.Append("padding-left:1px;");
            strData.Append("mso-ignore:padding;");
            strData.Append("color:black;");
            strData.Append("font-size:11.0pt;");
            strData.Append("font-weight:400;");
            strData.Append("font-style:normal;");
            strData.Append("text-decoration:none;");
            strData.Append("font-family:Calibri, sans-serif;");
            strData.Append("mso-font-charset:0;");
            strData.Append("mso-number-format:General;");
            strData.Append("text-align:center;");
            strData.Append("vertical-align:bottom;");
            strData.Append("border:.5pt solid windowtext;");
            strData.Append("mso-background-source:auto;");
            strData.Append("mso-pattern:auto;");
            strData.Append("white-space:nowrap;}");
            strData.Append(".xl9010558");
            strData.Append("{padding-top:1px;");
            strData.Append("padding-right:1px;");
            strData.Append("padding-left:1px;");
            strData.Append("mso-ignore:padding;");
            strData.Append("color:black;");
            strData.Append("font-size:11.0pt;");
            strData.Append("font-weight:400;");
            strData.Append("font-style:normal;");
            strData.Append("text-decoration:none;");
            strData.Append("font-family:Calibri, sans-serif;");
            strData.Append("mso-font-charset:0;");
            strData.Append("mso-number-format:0;");
            strData.Append("text-align:center;");
            strData.Append("vertical-align:bottom;");
            strData.Append("border:.5pt solid windowtext;");
            strData.Append("mso-background-source:auto;");
            strData.Append("mso-pattern:auto;");
            strData.Append("white-space:normal;}");
            strData.Append(".xl9110558");
            strData.Append("{padding-top:1px;");
            strData.Append("padding-right:1px;");
            strData.Append("padding-left:1px;");
            strData.Append("mso-ignore:padding;");
            strData.Append("color:black;");
            strData.Append("font-size:11.0pt;");
            strData.Append("font-weight:400;");
            strData.Append("font-style:normal;");
            strData.Append("text-decoration:none;");
            strData.Append("font-family:Calibri, sans-serif;");
            strData.Append("mso-font-charset:0;");
            strData.Append("mso-number-format:0;");
            strData.Append("text-align:center;");
            strData.Append("vertical-align:bottom;");
            strData.Append("border:.5pt solid windowtext;");
            strData.Append("mso-background-source:auto;");
            strData.Append("mso-pattern:auto;");
            strData.Append("white-space:nowrap;}");
            strData.Append(".xl9210558");
            strData.Append("{padding-top:1px;");
            strData.Append("padding-right:1px;");
            strData.Append("padding-left:1px;");
            strData.Append("mso-ignore:padding;");
            strData.Append("color:windowtext;");
            strData.Append("font-size:11.0pt;");
            strData.Append("font-weight:700;");
            strData.Append("font-style:normal;");
            strData.Append("text-decoration:none;");
            strData.Append("font-family:Calibri, sans-serif;");
            strData.Append("mso-font-charset:0;");
            strData.Append("mso-number-format:General;");
            strData.Append("text-align:general;");
            strData.Append("vertical-align:bottom;");
            strData.Append("border:.5pt solid windowtext;");
            strData.Append("background:#92D050;");
            strData.Append("mso-pattern:black none;");
            strData.Append("white-space:nowrap;}");
            strData.Append(".xl9310558");
            strData.Append("{padding-top:1px;");
            strData.Append("padding-right:1px;");
            strData.Append("padding-left:1px;");
            strData.Append("mso-ignore:padding;");
            strData.Append("color:windowtext;");
            strData.Append("font-size:11.0pt;");
            strData.Append("font-weight:700;");
            strData.Append("font-style:normal;");
            strData.Append("text-decoration:none;");
            strData.Append("font-family:Calibri, sans-serif;");
            strData.Append("mso-font-charset:0;");
            strData.Append("mso-number-format:0;");
            strData.Append("text-align:center;");
            strData.Append("vertical-align:bottom;");
            strData.Append("border:.5pt solid windowtext;");
            strData.Append("background:#92D050;");
            strData.Append("mso-pattern:black none;");
            strData.Append("white-space:nowrap;}");
            strData.Append(".xl9410558");
            strData.Append("{padding-top:1px;");
            strData.Append("padding-right:1px;");
            strData.Append("padding-left:1px;");
            strData.Append("mso-ignore:padding;");
            strData.Append("color:black;");
            strData.Append("font-size:11.0pt;");
            strData.Append("font-weight:700;");
            strData.Append("font-style:normal;");
            strData.Append("text-decoration:none;");
            strData.Append("font-family:Calibri, sans-serif;");
            strData.Append("mso-font-charset:0;");
            strData.Append("mso-number-format:0;");
            strData.Append("text-align:center;");
            strData.Append("vertical-align:bottom;");
            strData.Append("border:.5pt solid windowtext;");
            strData.Append("background:#FFC000;");
            strData.Append("mso-pattern:black none;");
            strData.Append("white-space:nowrap;}");
            strData.Append("-->");
            strData.Append("</style>");
            strData.Append("</head>");
            strData.Append("<body>");
            strData.Append("<!--[if !excel]>&nbsp;&nbsp;<![endif]-->");
            strData.Append("<!--The following information was generated by Microsoft Excel's Publish as Web");
            strData.Append("Page wizard.-->");
            strData.Append("<!--If the same item is republished from Excel, all information between the DIV");
            strData.Append("tags will be replaced.-->");
            strData.Append("<!----------------------------->");
            strData.Append("<!--START OF OUTPUT FROM EXCEL PUBLISH AS WEB PAGE WIZARD -->");
            strData.Append("<!----------------------------->");
            strData.Append("<div id='Way Forward Plan for 17-18 - Mumbai_10558' align=center");
            strData.Append("x:publishsource='Excel'>");
            strData.Append("<table border=0 cellpadding=0 cellspacing=0 width=2049 class=xl6553510558 style='border-collapse:collapse;table-layout:fixed;width:1537pt'>");
            strData.Append("<col class=xl6553510558 width=19 style='mso-width-source:userset;mso-width-alt: 694;width:14pt'>");
            strData.Append("<col class=xl6553510558 width=89 style='mso-width-source:userset;mso-width-alt: 3254;width:67pt'>");
            strData.Append("<col class=xl6553510558 width=172 style='mso-width-source:userset;mso-width-alt: 6290;width:129pt'>");
            strData.Append("<col class=xl6553510558 width=302 style='mso-width-source:userset;mso-width-alt: 11044;width:227pt'>");
            strData.Append("<col class=xl6553510558 width=67 style='mso-width-source:userset;mso-width-alt: 2450;width:50pt'>");
            strData.Append("<col class=xl6553510558 width=96 style='mso-width-source:userset;mso-width-alt: 3510;width:72pt'>");
            strData.Append("<col class=xl6553510558 width=95 style='mso-width-source:userset;mso-width-alt: 3474;width:71pt'>");
            strData.Append("<col class=xl6553510558 width=94 style='mso-width-source:userset;mso-width-alt: 3437;width:71pt'>");
            strData.Append("<col class=xl6553510558 width=99 span=2 style='mso-width-source:userset; mso-width-alt:3620;width:74pt'>");
            strData.Append("<col class=xl6553510558 width=93 style='mso-width-source:userset;mso-width-alt: 3401;width:70pt'>");
            strData.Append("<col class=xl6553510558 width=91 style='mso-width-source:userset;mso-width-alt: 3328;width:68pt'>");
            strData.Append("<col class=xl6553510558 width=96 style='mso-width-source:userset;mso-width-alt: 3510;width:72pt'>");
            strData.Append("<col class=xl6553510558 width=91 style='mso-width-source:userset;mso-width-alt: 3328;width:68pt'>");
            strData.Append("<col class=xl6553510558 width=86 style='mso-width-source:userset;mso-width-alt: 3145;width:65pt'>");
            strData.Append("<col class=xl6553510558 width=90 style='mso-width-source:userset;mso-width-alt: 3291;width:68pt'>");
            strData.Append("<col class=xl6553510558 width=87 style='mso-width-source:userset;mso-width-alt: 3181;width:65pt'>");
            strData.Append("<col class=xl6553510558 width=91 style='mso-width-source:userset;mso-width-alt: 3328;width:68pt'>");
            strData.Append("<col class=xl6553510558 width=64 span=3 style='width:48pt'>");
            strData.Append("<tr height=20 style='height:15.0pt'>");
            strData.Append("<td height=20 class=xl6553510558 width=19 style='height:15.0pt;width:14pt'></td>");
            strData.Append("<td class=xl6553510558 width=89 style='width:67pt'></td>");
            strData.Append("<td class=xl6553510558 width=172 style='width:129pt'></td>");
            strData.Append("<td class=xl6553510558 width=302 style='width:227pt'></td>");
            strData.Append("<td class=xl6553510558 width=67 style='width:50pt'></td>");
            strData.Append("<td class=xl6553510558 width=96 style='width:72pt'></td>");
            strData.Append("<td class=xl6553510558 width=95 style='width:71pt'></td>");
            strData.Append("<td class=xl6553510558 width=94 style='width:71pt'></td>");
            strData.Append("<td class=xl6553510558 width=99 style='width:74pt'></td>");
            strData.Append("<td class=xl6553510558 width=99 style='width:74pt'></td>");
            strData.Append("<td class=xl6553510558 width=93 style='width:70pt'></td>");
            strData.Append("<td class=xl6553510558 width=91 style='width:68pt'></td>");
            strData.Append("<td class=xl6553510558 width=96 style='width:72pt'></td>");
            strData.Append("<td class=xl6553510558 width=91 style='width:68pt'></td>");
            strData.Append("<td class=xl6553510558 width=86 style='width:65pt'></td>");
            strData.Append("<td class=xl6553510558 width=90 style='width:68pt'></td>");
            strData.Append("<td class=xl6553510558 width=87 style='width:65pt'></td>");
            strData.Append("<td class=xl6553510558 width=91 style='width:68pt'></td>");
            strData.Append("<td class=xl6553510558 width=64 style='width:48pt'></td>");
            strData.Append("<td class=xl6553510558 width=64 style='width:48pt'></td>");
            strData.Append("<td class=xl6553510558 width=64 style='width:48pt'></td>");
            strData.Append("</tr>");
            strData.Append("<tr height=20 style='height:15.0pt'>");
            strData.Append("<td height=20 class=xl6553510558 style='height:15.0pt'></td>");
            strData.Append("<td class=xl8010558>&nbsp;</td>");
            strData.Append("<td class=xl8010558 style='border-left:none'>&nbsp;</td>");
            strData.Append("<td class=xl8010558 align=right style='border-left:none' colspan='2'></td>");
            //strData.Append("<td class=xl8010558 style='border-left:none'>&nbsp;</td>");
            strData.Append("<td class=xl8110558 style='border-left:none'>Apr'17</td>");
            strData.Append("<td class=xl8110558 style='border-left:none'>May'17</td>");
            strData.Append("<td class=xl8110558 style='border-left:none'>Jun'17</td>");
            strData.Append("<td class=xl8110558 style='border-left:none'>Jul'17</td>");
            strData.Append("<td class=xl8110558 style='border-left:none'>Aug'17</td>");
            strData.Append("<td class=xl8110558 style='border-left:none'>Sep'17</td>");
            strData.Append("<td class=xl8110558 style='border-left:none'>Oct'17</td>");
            strData.Append("<td class=xl8110558 style='border-left:none'>Nov'17</td>");
            strData.Append("<td class=xl8110558 style='border-left:none'>Dec'17</td>");
            strData.Append("<td class=xl8110558 style='border-left:none'>Jan'18</td>");
            strData.Append("<td class=xl8110558 style='border-left:none'>Feb'18</td>");
            strData.Append("<td class=xl8110558 style='border-left:none'>Mar'18</td>");
            strData.Append("<td class=xl8110558 style='border-left:none'>Total</td>");
            strData.Append("<td class=xl7110558></td>");
            strData.Append("<td class=xl7110558></td>");
            strData.Append("<td class=xl7110558></td>");
            strData.Append("</tr>");
            strData.Append("<tr height=20 style='height:15.0pt'>");
            strData.Append("<td height=20 class=xl6553510558 style='height:15.0pt'></td>");
            strData.Append("<td class=xl8010558 style='border-top:none'>&nbsp;</td>");
            strData.Append("<td class=xl8010558 style='border-top:none;border-left:none'>Target</td>");
            strData.Append("<td class=xl8010558 style='border-top:none;border-left:none' colspan='2'>&nbsp;</td>");
            // strData.Append("<td class=xl8010558 style='border-top:none;border-left:none'>&nbsp;</td>");

            DataSet dsTarget = new DataSet();
            dsTarget = objWayPlan.GetTarget(cityId, financialYear);
            double totalTarget = 0;
            if (dsTarget.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow drTar in dsTarget.Tables[0].Rows)
                {
                    for (int i = 4; i <= 12; i++)
                    {
                        if (Convert.ToInt32(drTar["cityID"]) == cityId && Convert.ToString(drTar["MonthYear"]) == Convert.ToString(i + "-" + startYear))
                        {
                            //strData.Append("<td class=xl8210558 style='border-top:none;border-left:none;text-align: right;'>" + Convert.ToInt32(drTar["TotalTransaction"])  + "</td>");
                            TargetDic[i.ToString()] = Convert.ToInt32(TargetDic[i.ToString()]) + Convert.ToInt32(drTar["TotalTransaction"]);
                        }
                        else
                        {
                            //strData.Append("<td class=xl8210558 style='border-top:none;border-left:none;text-align: right;'>0</td>");
                            TargetDic[i.ToString()] = Convert.ToInt32(TargetDic[i.ToString()]) + 0;
                        }
                        

                    }
                    for (int i = 1; i <= 3; i++)
                    {
                        if (Convert.ToInt32(drTar["cityID"]) == cityId && Convert.ToString(drTar["MonthYear"]) == Convert.ToString(i + "-" + endYear))
                        {
                            //strData.Append("<td class=xl8210558 style='border-top:none;border-left:none;text-align: right;'>" + Convert.ToInt32(drTar["TotalTransaction"]) + "</td>");
                            TargetDic[i.ToString()] = Convert.ToInt32(TargetDic[i.ToString()]) + Convert.ToInt32(drTar["TotalTransaction"]);
                        }
                        else
                        {
                            //strData.Append("<td class=xl8210558 style='border-top:none;border-left:none;text-align: right;'>0</td>");
                            TargetDic[i.ToString()] = Convert.ToInt32(TargetDic[i.ToString()]) + 0;
                        }
                        
                    }
                }
            }

            for (int i = 4; i <= 12; i++)
            {
                strData.Append("<td class=xl8210558 style='border-top:none;border-left:none;text-align: right;'>" + TargetDic[i.ToString()] + "</td>");
                totalTarget = totalTarget + TargetDic[i.ToString()];
            }
            for (int i = 1; i <= 3; i++)
            {
                strData.Append("<td class=xl8210558 style='border-top:none;border-left:none;text-align: right;'>" + TargetDic[i.ToString()] + "</td>");
                totalTarget = totalTarget + TargetDic[i.ToString()];
            }

            strData.Append("<td class=xl8210558 style='border-top:none;border-left:none'>" + totalTarget + "</td>");

            strData.Append("<td class=xl6553510558></td>");
            strData.Append("<td class=xl6553510558></td>");
            strData.Append("<td class=xl6553510558></td>");
            strData.Append("</tr>");
            strData.Append("<tr height=20 style='height:15.0pt'>");
            strData.Append("<td height=20 class=xl6553510558 style='height:15.0pt'></td>");
            strData.Append("<td class=xl8010558 style='border-top:none'>&nbsp;</td>");
            strData.Append("<td class=xl8010558 style='border-top:none;border-left:none'>Avg Ticket Size- LTR</td>");
            strData.Append("<td class=xl8410558 align=right style='border-top:none;border-left:none' colspan='2'>&nbsp;</td>");
            // strData.Append("<td class=xl8410558 style='border-top:none;border-left:none'>&nbsp;</td>");


            DataSet dsLTR = new DataSet();
            dsLTR = objWayPlan.GetAvgTicketSizeLTR(cityId, financialYear);
            if (dsLTR.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow drLTR in dsLTR.Tables[0].Rows)
                {
                    for (int i = 4; i <= 12; i++)
                    {
                        if (Convert.ToInt32(drLTR["cityID"]) == cityId && Convert.ToString(drLTR["MonthYear"]) == Convert.ToString(i + "-" + startYear))
                        {
                            //strData.Append("<td class=xl8410558 style='border-top:none;border-left:none'>" + drLTR["TotalTransaction"] + "</td>");
                            AVGLTR[i.ToString()] = Convert.ToInt32(AVGLTR[i.ToString()]) + Convert.ToInt32(drLTR["TotalTransaction"]);
                        }
                        else
                        {
                            //strData.Append("<td class=xl8410558 style='border-top:none;border-left:none'>0</td>");
                            AVGLTR[i.ToString()] = Convert.ToInt32(AVGLTR[i.ToString()]) + 0;
                        }

                    }
                    for (int i = 1; i <= 3; i++)
                    {
                        if (Convert.ToInt32(drLTR["cityID"]) == cityId && Convert.ToString(drLTR["MonthYear"]) == Convert.ToString(i + "-" + endYear))
                        {
                            //strData.Append("<td class=xl8410558 style='border-top:none;border-left:none'>" + drLTR["TotalTransaction"] + "</td>");
                            AVGLTR[i.ToString()] = Convert.ToInt32(AVGLTR[i.ToString()]) + Convert.ToInt32(drLTR["TotalTransaction"]);
                        }
                        else
                        {
                            //strData.Append("<td class=xl8410558 style='border-top:none;border-left:none'>0</td>");
                            AVGLTR[i.ToString()] = Convert.ToInt32(AVGLTR[i.ToString()]) + 0;
                        }
                    }
                }
            }
            for (int i = 4; i <= 12; i++)
            {
                strData.Append("<td class=xl8410558 style='border-top:none;border-left:none;text-align: right;'>" + AVGLTR[i.ToString()] + "</td>");
            }
            for (int i = 1; i <= 3; i++)
            {
                strData.Append("<td class=xl8410558 style='border-top:none;border-left:none;text-align: right;'>" + AVGLTR[i.ToString()] + "</td>");
            }
            //else
            //{

            //    strData.Append("<td class=xl8410558 align=right style='border-top:none;border-left:none'>0</td>");
            //    strData.Append("<td class=xl8410558 align=right style='border-top:none;border-left:none'>0</td>");
            //    strData.Append("<td class=xl8410558 align=right style='border-top:none;border-left:none'>0</td>");
            //    strData.Append("<td class=xl8410558 align=right style='border-top:none;border-left:none'>0</td>");
            //    strData.Append("<td class=xl8410558 align=right style='border-top:none;border-left:none'>0</td>");
            //    strData.Append("<td class=xl8410558 align=right style='border-top:none;border-left:none'>0</td>");
            //    strData.Append("<td class=xl8410558 align=right style='border-top:none;border-left:none'>0</td>");
            //    strData.Append("<td class=xl8410558 align=right style='border-top:none;border-left:none'>0</td>");
            //    strData.Append("<td class=xl8410558 align=right style='border-top:none;border-left:none'>0</td>");
            //    strData.Append("<td class=xl8410558 align=right style='border-top:none;border-left:none'>0</td>");
            //    strData.Append("<td class=xl8410558 align=right style='border-top:none;border-left:none'>0</td>");
            //    strData.Append("<td class=xl8410558 align=right style='border-top:none;border-left:none'>0</td>");
            //}

            strData.Append("<td class=xl8010558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData.Append("<td class=xl6553510558></td>");
            strData.Append("<td class=xl6553510558></td>");
            strData.Append("<td class=xl6553510558></td>");
            strData.Append("</tr>");

            strData.Append("<tr height=20 style='height:15.0pt'>");
            strData.Append("<td height=20 class=xl6553510558 style='height:15.0pt'></td>");
            strData.Append("<td class=xl8010558 style='border-top:none'>&nbsp;</td>");
            strData.Append("<td class=xl8010558 style='border-top:none;border-left:none'>Avg Ticket Size-Spot Rental</td>");
            strData.Append("<td class=xl8010558 align=right style='border-top:none;border-left:none' colspan='2'>&nbsp;</td>");
            //strData.Append("<td class=xl8010558 style='border-top:none;border-left:none'>&nbsp;</td>");

            DataSet dsSPR = new DataSet();
            dsSPR = objWayPlan.GetAVGTicketSizeSpotRental(cityId, financialYear);
            if (dsSPR.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow drSPR in dsSPR.Tables[0].Rows)
                {
                    for (int i = 4; i <= 12; i++)
                    {
                        if (Convert.ToInt32(drSPR["cityID"]) == cityId && Convert.ToString(drSPR["MonthYear"]) == Convert.ToString(i + "-" + startYear))
                        {
                           // strData.Append("<td class=xl8010558 align=right style='border-top:none;border-left:none'>" + drSPR["TotalTransaction"] + "</td>");
                            AVGSR[i.ToString()] = Convert.ToDouble(AVGSR[i.ToString()]) + Convert.ToDouble(drSPR["TotalTransaction"]);
                        }
                        else
                        {
                            //strData.Append("<td class=xl8010558 align=right style='border-top:none;border-left:none'>0</td>");
                            AVGSR[i.ToString()] = Convert.ToDouble(AVGSR[i.ToString()]) + 0;
                        }

                    }
                    for (int i = 1; i <= 3; i++)
                    {
                        if (Convert.ToInt32(drSPR["cityID"]) == cityId && Convert.ToString(drSPR["MonthYear"]) == Convert.ToString(i + "-" + endYear))
                        {
                            //strData.Append("<td class=xl8010558 align=right style='border-top:none;border-left:none'>" + drSPR["TotalTransaction"] + "</td>");
                            AVGSR[i.ToString()] = Convert.ToDouble(AVGSR[i.ToString()]) + Convert.ToDouble(drSPR["TotalTransaction"]);
                        }
                        else
                        {
                            //strData.Append("<td class=xl8010558 align=right style='border-top:none;border-left:none'>0</td>");
                            AVGSR[i.ToString()] = Convert.ToDouble(AVGSR[i.ToString()]) + 0;
                        }

                    }
                }
            }
            for (int i = 4; i <= 12; i++)
            {   
                strData.Append("<td class=xl8010558 align=right style='border-top:none;border-left:none'>" + AVGSR[i.ToString()] + "</td>");
            }
            for (int i = 1; i <= 3; i++)
            {  
                strData.Append("<td class=xl8010558 align=right style='border-top:none;border-left:none'>" + AVGSR[i.ToString()] + "</td>");
            }
            //else
            //{
            //    strData.Append("<td class=xl8010558 align=right style='border-top:none;border-left:none'>0</td>");
            //    strData.Append("<td class=xl8010558 align=right style='border-top:none;border-left:none'>0</td>");
            //    strData.Append("<td class=xl8010558 align=right style='border-top:none;border-left:none'>0</td>");
            //    strData.Append("<td class=xl8010558 align=right style='border-top:none;border-left:none'>0</td>");
            //    strData.Append("<td class=xl8010558 align=right style='border-top:none;border-left:none'>0</td>");
            //    strData.Append("<td class=xl8010558 align=right style='border-top:none;border-left:none'>0</td>");
            //    strData.Append("<td class=xl8010558 align=right style='border-top:none;border-left:none'>0</td>");
            //    strData.Append("<td class=xl8010558 align=right style='border-top:none;border-left:none'>0</td>");
            //    strData.Append("<td class=xl8010558 align=right style='border-top:none;border-left:none'>0</td>");
            //    strData.Append("<td class=xl8010558 align=right style='border-top:none;border-left:none'>0</td>");
            //    strData.Append("<td class=xl8010558 align=right style='border-top:none;border-left:none'>0</td>");
            //    strData.Append("<td class=xl8010558 align=right style='border-top:none;border-left:none'>0</td>");

            //}

            strData.Append("<td class=xl8010558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData.Append("<td class=xl6553510558></td>");
            strData.Append("<td class=xl6553510558></td>");
            strData.Append("<td class=xl6553510558></td>");
            strData.Append("</tr>");
            strData.Append("<tr height=20 style='height:15.0pt'>");
            strData.Append("<td height=20 class=xl6553510558 style='height:15.0pt'></td>");
            strData.Append("<td class=xl8010558 style='border-top:none'>&nbsp;</td>");
            strData.Append("<td class=xl8010558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData.Append("<td class=xl8010558 style='border-top:none;border-left:none' colspan='2'>&nbsp;</td>");
            //strData.Append("<td class=xl8010558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData.Append("<td class=xl8010558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData.Append("<td class=xl8010558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData.Append("<td class=xl8010558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData.Append("<td class=xl8010558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData.Append("<td class=xl8010558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData.Append("<td class=xl8010558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData.Append("<td class=xl8010558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData.Append("<td class=xl8010558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData.Append("<td class=xl8010558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData.Append("<td class=xl8010558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData.Append("<td class=xl8010558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData.Append("<td class=xl8010558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData.Append("<td class=xl8010558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData.Append("<td class=xl6553510558></td>");
            strData.Append("<td class=xl6553510558></td>");
            strData.Append("<td class=xl6553510558></td>");
            strData.Append("</tr>");
            strData.Append("<tr height=20 style='height:15.0pt'>");
            strData.Append("<td height=20 class=xl6553510558 style='height:15.0pt'></td>");
            strData.Append("<td class=xl8010558 style='border-top:none'>&nbsp;</td>");
            strData.Append("<td class=xl6610558 style='border-top:none;border-left:none'>LTR</td>");
            strData.Append("<td class=xl6610558 align=right style='border-top:none;border-left:none' colspan='2'>&nbsp;</td>");
            //strData.Append("<td class=xl6610558 style='border-top:none;border-left:none'>&nbsp;</td>");
            //for (int index = 0; index < LTRDic.Count; index++)
            // {
            //  var item = LTRDic.ElementAt(index);
            //  var itemKey = item.Key;
            //  var itemValue = item.Value;
            for (int i = 4; i <= 12; i++)
            {
                //if (Convert.ToInt16(itemKey)==i)
                //{
                strData.Append("<td class=xl6610558 align=right style='border-top:none;border-left:none'>" + LTRDic[i.ToString()] + "</td>");
                // }
            }
            for (int i = 1; i <= 3; i++)
            {
                ///if (Convert.ToInt16(itemKey) == i)
                //{
                strData.Append("<td class=xl6610558 align=right style='border-top:none;border-left:none'>" + LTRDic[i.ToString()] + "</td>");
                // }
            }
            // }

            strData.Append("<td class=xl8010558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData.Append("<td class=xl6553510558></td>");
            strData.Append("<td class=xl6553510558></td>");
            strData.Append("<td class=xl6553510558></td>");
            strData.Append("</tr>");
            strData.Append("<tr height=20 style='height:15.0pt'>");
            strData.Append("<td height=20 class=xl6553510558 style='height:15.0pt'></td>");
            strData.Append("<td class=xl8010558 style='border-top:none'>&nbsp;</td>");
            strData.Append("<td class=xl6610558 style='border-top:none;border-left:none'>Spot Rental</td>");
            strData.Append("<td class=xl6710558 align=right style='border-top:none;border-left:none' colspan='2'>&nbsp;</td>");
            //strData.Append("<td class=xl6710558 style='border-top:none;border-left:none'>&nbsp;</td>");
            //for (int index = 0; index < LTRDic.Count; index++)
            // {
            //    var item = LTRDic.ElementAt(index);
            //    var itemKey = item.Key;
            //    var itemValue = item.Value;
            double spotRental = 0;
            for (int i = 4; i <= 12; i++)
            {
                spotRental = 0;
                //if (Convert.ToInt16(itemKey) == i)
                //{                       
                try
                {
                    spotRental = (TargetDic[i.ToString()] - (LTRDic[i.ToString()] * AVGLTR[i.ToString()])) / AVGSR[i.ToString()];
                }
                catch (Exception)
                {
                    spotRental = 0;
                }
                if (double.IsNaN(spotRental) || Convert.ToString(spotRental) == "Infinity")
                {
                    spotRental = 0;
                }

                spotRentDic[i.ToString()] = Math.Round(spotRental, 2);
                strData.Append("<td class=xl6710558 align=right style='border-top:none;border-left:none'>" + Math.Round(spotRental, 2) + "</td>");
                // }
            }
            for (int i = 1; i <= 3; i++)
            {
                spotRental = 0;
                //if (Convert.ToInt16(itemKey) == i)
                // {
                try
                {
                    spotRental = (TargetDic[i.ToString()] - (LTRDic[i.ToString()] * AVGLTR[i.ToString()])) / AVGSR[i.ToString()];
                }
                catch (Exception)
                {
                    spotRental = 0;
                }
                if (double.IsNaN(spotRental) || Convert.ToString(spotRental) == "Infinity")
                {
                    spotRental = 0;
                }
                spotRentDic[i.ToString()] = Math.Round(spotRental, 2);
                strData.Append("<td class=xl6710558 align=right style='border-top:none;border-left:none'>" + Math.Round(spotRental) + "</td>");
                // }
            }
            //}


            strData.Append("<td class=xl8010558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData.Append("<td class=xl6553510558></td>");
            strData.Append("<td class=xl6553510558></td>");
            strData.Append("<td class=xl6553510558></td>");
            strData.Append("</tr>");
            strData.Append("<tr height=20 style='height:15.0pt'>");
            strData.Append("<td height=20 class=xl6553510558 style='height:15.0pt'></td>");
            strData.Append("<td class=xl8010558 style='border-top:none'>&nbsp;</td>");
            strData.Append("<td class=xl6610558 style='border-top:none;border-left:none'>Projection</td>");
            strData.Append("<td class=xl6710558 style='border-top:none;border-left:none' colspan='2'>&nbsp;</td>");
            //strData.Append("<td class=xl6710558 style='border-top:none;border-left:none'>&nbsp;</td>");
            
            // here add strdata1  in grand total

            strData2.Append("<td class=xl8010558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("</tr>");
            strData2.Append("<tr height=20 style='height:15.0pt'>");
            strData2.Append("<td height=20 class=xl6553510558 style='height:15.0pt'></td>");
            strData2.Append("<td class=xl8010558 style='border-top:none'>&nbsp;</td>");
            strData2.Append("<td class=xl6610558 style='border-top:none;border-left:none'>Actuals -  Closing</td>");
            strData2.Append("<td class=xl6810558 style='border-top:none;border-left:none' colspan='2'>&nbsp;</td>");
            //strData2.Append("<td class=xl6810558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl7310558 align=right style='border-top:none;border-left:none'>0</td>");
            strData2.Append("<td class=xl7310558 align=right style='border-top:none;border-left:none'>0</td>");
            strData2.Append("<td class=xl7310558 align=right style='border-top:none;border-left:none'>0</td>");
            strData2.Append("<td class=xl7310558 align=right style='border-top:none;border-left:none'>0</td>");
            strData2.Append("<td class=xl7310558 align=right style='border-top:none;border-left:none'>0</td>");
            strData2.Append("<td class=xl7310558 align=right style='border-top:none;border-left:none'>0</td>");
            strData2.Append("<td class=xl7310558 align=right style='border-top:none;border-left:none'>0</td>");
            strData2.Append("<td class=xl7310558 align=right style='border-top:none;border-left:none'>0</td>");
            strData2.Append("<td class=xl7310558 align=right style='border-top:none;border-left:none'>0</td>");
            strData2.Append("<td class=xl7310558 align=right style='border-top:none;border-left:none'>0</td>");
            strData2.Append("<td class=xl7310558 align=right style='border-top:none;border-left:none'>0</td>");
            strData2.Append("<td class=xl7310558 align=right style='border-top:none;border-left:none'>0</td>");
            strData2.Append("<td class=xl8010558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("</tr>");
            strData2.Append("<tr height=20 style='height:15.0pt'>");
            strData2.Append("<td height=20 class=xl6553510558 style='height:15.0pt'></td>");
            strData2.Append("<td class=xl8010558 style='border-top:none'>&nbsp;</td>");
            strData2.Append("<td class=xl6610558 style='border-top:none;border-left:none'>Difference  Transactions</td>");
            strData2.Append("<td class=xl6610558 style='border-top:none;border-left:none' colspan='2' >&nbsp;</td>");
            //strData2.Append("<td class=xl6610558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl6610558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl6610558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl6610558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl6610558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl6610558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl6610558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl6610558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl6610558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl6610558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl6610558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl6610558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl6610558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl8010558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("</tr>");
            strData2.Append("<tr height=20 style='height:15.0pt'>");
            strData2.Append("<td height=20 class=xl6553510558 style='height:15.0pt'></td>");
            strData2.Append("<td class=xl8010558 style='border-top:none'>&nbsp;</td>");
            strData2.Append("<td class=xl6610558 style='border-top:none;border-left:none'>Short/Fall</td>");
            strData2.Append("<td class=xl6610558 align=right style='border-top:none;border-left:none' colspan='2'>&nbsp;</td>");
            //strData2.Append("<td class=xl6610558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl6610558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl6610558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl6610558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl6610558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl6610558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl6610558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl6610558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl6610558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl6610558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl6610558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl6610558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl6610558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl8010558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("</tr>");
            strData2.Append("<tr height=20 style='height:15.0pt'>");
            strData2.Append("<td height=20 class=xl6553510558 style='height:15.0pt'></td>");
            strData2.Append("<td class=xl8010558 style='border-top:none'>&nbsp;</td>");
            strData2.Append("<td class=xl8010558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl8010558 style='border-top:none;border-left:none' colspan='2'>&nbsp;</td>");
            //strData2.Append("<td class=xl8010558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl8510558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl8010558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl8010558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl8010558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl8010558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl8010558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl8010558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl8010558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl8010558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl8010558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl8010558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl8010558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl8010558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("</tr>");
            strData2.Append("<tr height=20 style='height:15.0pt'>");
            strData2.Append("<td height=20 class=xl6553510558 style='height:15.0pt'></td>");
            strData2.Append("<td class=xl8010558 style='border-top:none'>&nbsp;</td>");
            strData2.Append("<td class=xl8010558 style='border-top:none;border-left:none'>LTR Revenue</td>");
            strData2.Append("<td class=xl6610558 style='border-top:none;border-left:none' colspan='2'>&nbsp;</td>");
            //strData2.Append("<td class=xl6610558 style='border-top:none;border-left:none'>&nbsp;</td>");

            double ltrRevenue;
            double totalLTRRevenue = 0;
            for (int i = 4; i <= 12; i++)
            {
                ltrRevenue = (LTRDic[i.ToString()] * AVGLTR[i.ToString()]);
                if (double.IsNaN(ltrRevenue))
                {
                    ltrRevenue = 0;
                }
                strData2.Append("<td class=xl6610558 style='border-top:none;border-left:none'>" + ltrRevenue + "</td>");
                totalLTRRevenue = totalLTRRevenue + ltrRevenue;
            }
            for (int i = 1; i <= 3; i++)
            {
                ltrRevenue = (TargetDic[i.ToString()] - (LTRDic[i.ToString()] * AVGLTR[i.ToString()])) / AVGSR[i.ToString()];
                if (double.IsNaN(ltrRevenue))
                {
                    ltrRevenue = 0;
                }
                strData2.Append("<td class=xl6610558 style='border-top:none;border-left:none'>" + Math.Round(ltrRevenue, 2) + "</td>");
                totalLTRRevenue = totalLTRRevenue + ltrRevenue;
            }

            strData2.Append("<td class=xl6610558 style='border-top:none;border-left:none'>" + totalLTRRevenue + "</td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("</tr>");
            strData2.Append("<tr height=20 style='height:15.0pt'>");
            strData2.Append("<td height=20 class=xl6553510558 style='height:15.0pt'></td>");
            strData2.Append("<td class=xl8010558 style='border-top:none'>&nbsp;</td>");
            strData2.Append("<td class=xl8010558 style='border-top:none;border-left:none'>Spot Rental Revenue</td>");
            strData2.Append("<td class=xl6610558 style='border-top:none;border-left:none'colspan='2' >&nbsp;</td>");
            // strData2.Append("<td class=xl6610558 style='border-top:none;border-left:none'>&nbsp;</td>");

            double sptRevenue;
            double totalSptRentalRevenue = 0;
            for (int i = 4; i <= 12; i++)
            {
                sptRevenue = (AVGSR[i.ToString()] * spotRentDic[i.ToString()]);
                strData2.Append("<td class=xl6610558 style='border-top:none;border-left:none'>" + Math.Round(sptRevenue, 2) + "</td>");
                totalSptRentalRevenue = totalSptRentalRevenue + sptRevenue;
            }
            for (int i = 1; i <= 3; i++)
            {
                sptRevenue = (AVGSR[i.ToString()] * spotRentDic[i.ToString()]);
                strData2.Append("<td class=xl6610558 style='border-top:none;border-left:none'>" + Math.Round(sptRevenue, 2) + "</td>");
                totalSptRentalRevenue = totalSptRentalRevenue + sptRevenue;
            }


            strData2.Append("<td class=xl6610558 style='border-top:none;border-left:none'>" + Math.Round(totalSptRentalRevenue, 2) + "</td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("</tr>");
            strData2.Append("<tr height=20 style='height:15.0pt'>");
            strData2.Append("<td height=20 class=xl6553510558 style='height:15.0pt'></td>");
            strData2.Append("<td colspan=5 class=xl7510558 width=726 style='width:545pt'>Existing  Customers</td>");
            strData2.Append("<td class=xl8010558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl8010558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl8010558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl8010558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl8010558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl8010558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl8010558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl8010558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl8010558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl8010558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl8010558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl8010558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("</tr>");
            strData2.Append("<tr class=xl6910558 height=40 style='height:30.0pt'>");
            strData2.Append("<td height=40 class=xl6910558 width=19 style='height:30.0pt;width:14pt'></td>");
            strData2.Append("<td class=xl7510558 width=89 style='border-top:none;width:67pt'>Transactions  (Per Day)</td>");
            strData2.Append("<td class=xl7510558 width=172 style='border-top:none;border-left:none; width:129pt'>Implant/KAM</td>");
            strData2.Append("<td class=xl7510558 width=302 style='border-top:none;border-left:none; width:227pt'  colspan='2'>Name of the Clients</td>");
            //strData2.Append("<td class=xl7510558 width=67 style='border-top:none;border-left:none; width:50pt'>Current</td>");
            strData2.Append("<td class=xl7510558 width=96 style='border-top:none;border-left:none; width:72pt'>Transactions (Monthly)</td>");
            strData2.Append("<td class=xl7510558 width=95 style='border-top:none;border-left:none; width:71pt'>Transactions (Monthly)</td>");
            strData2.Append("<td class=xl7510558 width=94 style='border-top:none;border-left:none; width:71pt'>Transactions (Monthly)</td>");
            strData2.Append("<td class=xl7510558 width=99 style='border-top:none;border-left:none; width:74pt'>Transactions (Monthly)</td>");
            strData2.Append("<td class=xl7510558 width=99 style='border-top:none;border-left:none; width:74pt'>Transactions (Monthly)</td>");
            strData2.Append("<td class=xl7510558 width=93 style='border-top:none;border-left:none; width:70pt'>Transactions (Monthly)</td>");
            strData2.Append("<td class=xl7510558 width=91 style='border-top:none;border-left:none; width:68pt'>Transactions (Monthly)</td>");
            strData2.Append("<td class=xl7510558 width=96 style='border-top:none;border-left:none; width:72pt'>Transactions (Monthly)</td>");
            strData2.Append("<td class=xl7510558 width=91 style='border-top:none;border-left:none; width:68pt'>Transactions (Monthly)</td>");
            strData2.Append("<td class=xl7510558 width=86 style='border-top:none;border-left:none; width:65pt'>Transactions (Monthly)</td>");
            strData2.Append("<td class=xl7510558 width=90 style='border-top:none;border-left:none; width:68pt'>Transactions (Monthly)</td>");
            strData2.Append("<td class=xl7510558 width=87 style='border-top:none;border-left:none; width:65pt'>Transactions (Monthly)</td>");
            strData2.Append("<td class=xl8810558 width=91 style='border-top:none;border-left:none; width:68pt'>&nbsp;</td>");
            strData2.Append("<td class=xl6910558 width=64 style='width:48pt'></td>");
            strData2.Append("<td class=xl6910558 width=64 style='width:48pt'></td>");
            strData2.Append("<td class=xl6910558 width=64 style='width:48pt'></td>");
            strData2.Append("</tr>");

            foreach (DataRow dr in dsGroupEnity.Tables[0].Rows)
            {

                strData2.Append("<tr class=xl6910558 height=20 style='height:15.0pt'>");
                strData2.Append("<td height=20 class=xl6910558 width=19 style='height:15.0pt;width:14pt'></td>");
                strData2.Append("<td class=xl7510558 width=89 style='border-top:none;width:67pt'>&nbsp;</td>");
                strData2.Append("<td class=xl8010558 style='border-top:none;border-left:none'>" + dr["ImplantName"] + "<span");
                strData2.Append("style='mso-spacerun:yes'></span></td>");
                strData2.Append("<td class=xl8010558 style='border-top:none;border-left:none' colspan='2' >" + dr["GroupEntity"] + "</td>");
                //strData2.Append("<td class=xl8910558 style='border-top:none;border-left:none'>910</td>");

                DataSet dsWayPlan = new DataSet();
                dsWayPlan = objWayPlan.GetWayPlanData(financialYear, Convert.ToInt32(dr["ClientCoCityID"]), Convert.ToString(dr["GroupEntity"]), Convert.ToInt32(dr["ClientCoID"]));
                if (dsWayPlan.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dry in dsWayPlan.Tables[0].Rows)
                    {

                        for (int i = 4; i <= 12; i++)
                        {
                            if (Convert.ToString(dry["GroupEntity"]) == Convert.ToString(dr["GroupEntity"]) && Convert.ToString(dry["MonthYear"]) == Convert.ToString(i + "-" + startYear))
                            {
                                strData2.Append("<td class=xl9010558 width=96 style='border-top:none;border-left:none; width:72pt'>" + dry["TotalTransaction"] + "</td>");
                                transDic[i.ToString()] = Convert.ToInt32(transDic[i.ToString()]) + Convert.ToInt32(dry["TotalTransaction"]);
                            }
                            else
                            {
                                strData2.Append("<td class=xl9010558 width=96 style='border-top:none;border-left:none; width:72pt'>0</td>");
                                transDic[i.ToString()] = Convert.ToInt32(transDic[i.ToString()]) + 0;
                            }

                        }
                        for (int i = 1; i <= 3; i++)
                        {
                            if (Convert.ToString(dry["GroupEntity"]) == Convert.ToString(dr["GroupEntity"]) && Convert.ToString(dry["MonthYear"]) == Convert.ToString(i + "-" + endYear))
                            {
                                strData2.Append("<td class=xl9010558 width=96 style='border-top:none;border-left:none; width:72pt'>" + dry["TotalTransaction"] + "</td>");
                                transDic[i.ToString()] = Convert.ToInt32(transDic[i.ToString()]) + Convert.ToInt32(dry["TotalTransaction"]);
                            }
                            else
                            {
                                strData2.Append("<td class=xl9010558 width=96 style='border-top:none;border-left:none; width:72pt'>0</td>");
                                transDic[i.ToString()] = Convert.ToInt32(transDic[i.ToString()]) + 0;
                            }

                        }
                    }
                }
                else
                {
                    strData2.Append("<td class=xl9010558 width=96 style='border-top:none;border-left:none; width:72pt'>0</td>");
                    strData2.Append("<td class=xl9010558 width=95 style='border-top:none;border-left:none; width:71pt'>0</td>");
                    strData2.Append("<td class=xl9010558 width=94 style='border-top:none;border-left:none; width:71pt'>0</td>");
                    strData2.Append("<td class=xl9010558 width=99 style='border-top:none;border-left:none; width:74pt'>0</td>");
                    strData2.Append("<td class=xl9010558 width=99 style='border-top:none;border-left:none; width:74pt'>0</td>");
                    strData2.Append("<td class=xl9010558 width=93 style='border-top:none;border-left:none; width:70pt'>0</td>");
                    strData2.Append("<td class=xl9010558 width=91 style='border-top:none;border-left:none; width:68pt'>0</td>");
                    strData2.Append("<td class=xl9010558 width=96 style='border-top:none;border-left:none; width:72pt'>0</td>");
                    strData2.Append("<td class=xl9010558 width=91 style='border-top:none;border-left:none; width:68pt'>0</td>");
                    strData2.Append("<td class=xl9010558 width=86 style='border-top:none;border-left:none; width:65pt'>0</td>");
                    strData2.Append("<td class=xl9010558 width=90 style='border-top:none;border-left:none; width:68pt'>0</td>");
                    strData2.Append("<td class=xl9010558 width=87 style='border-top:none;border-left:none; width:65pt'>0</td>");
                }

                strData2.Append("<td class=xl9010558 width=91 style='border-top:none;border-left:none; width:68pt'>&nbsp;</td>");
                strData2.Append("<td class=xl6910558 width=64 style='width:48pt'></td>");
                strData2.Append("<td class=xl6910558 width=64 style='width:48pt'></td>");
                strData2.Append("<td class=xl6910558 width=64 style='width:48pt'></td>");
                strData2.Append("</tr>");
            }

            strData2.Append("<tr height=20 style='height:15.0pt'>");
            strData2.Append("<td height=20 class=xl6553510558 style='height:15.0pt'></td>");
            strData2.Append("<td class=xl8010558 style='border-top:none'>&nbsp;</td>");
            strData2.Append("<td class=xl9210558 style='border-top:none;border-left:none'>Existing Total</td>");
            strData2.Append("<td class=xl9210558 style='border-top:none;border-left:none' colspan='2'>&nbsp;</td>");
            //strData2.Append("<td class=xl9310558 style='border-top:none;border-left:none'>3031</td>");
            for (int i = 4; i <= 12; i++)
            {
                strData2.Append("<td class=xl9310558 style='border-top:none;border-left:none'>" + transDic[i.ToString()] + "</td>");
            }
            for (int i = 1; i <= 3; i++)
            {
                strData2.Append("<td class=xl9310558 style='border-top:none;border-left:none'>" + transDic[i.ToString()] + "</td>");
            }

            strData2.Append("<td class=xl8010558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("</tr>");


            strData2.Append("<tr height=20 style='height:15.0pt'>");
            strData2.Append("<td height=20 class=xl6553510558 style='height:15.0pt'></td>");
            strData2.Append("<td class=xl8010558 style='border-top:none'>&nbsp;</td>");
            strData2.Append("<td class=xl8010558 style='border-top:none;border-left:none' >&nbsp;</td>");
            strData2.Append("<td class=xl8010558 style='border-top:none;border-left:none' colspan='2'>&nbsp;</td>");
            //strData2.Append("<td class=xl8010558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl8010558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl8010558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl8010558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl8010558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl8010558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl8010558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl8010558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl8010558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl8010558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl8010558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl8010558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl8010558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl8010558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("</tr>");
            strData2.Append("<tr height=20 style='height:15.0pt'>");
            strData2.Append("<td height=20 class=xl6553510558 style='height:15.0pt'></td>");
            strData2.Append("<td colspan=5 class=xl8110558>New Acquisitions</td>");
            strData2.Append("<td class=xl8010558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl8010558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl8010558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl8010558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl8010558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl8010558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl8010558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl8010558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl8010558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl8010558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl8010558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl8010558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("</tr>");
            strData2.Append("<tr height=40 style='height:30.0pt'>");
            strData2.Append("<td height=40 class=xl6553510558 style='height:30.0pt'></td>");
            strData2.Append("<td class=xl7510558 width=89 style='border-top:none;width:67pt'>Transactions  (Per Day)</td>");
            strData2.Append("<td class=xl7510558 width=172 style='border-top:none;border-left:none; width:129pt'>Implant/KAM</td>");
            strData2.Append("<td class=xl7510558 width=302 style='border-top:none;border-left:none; width:227pt' colspan='2'>Name of the Clients</td>");
            //strData2.Append("<td class=xl7510558 width=67 style='border-top:none;border-left:none;  width:50pt'>&nbsp;</td>");
            strData2.Append("<td class=xl7510558 width=96 style='border-top:none;border-left:none;  width:72pt'>Transactions (Monthly)</td>");
            strData2.Append("<td class=xl7510558 width=95 style='border-top:none;border-left:none;  width:71pt'>Transactions (Monthly)</td>");
            strData2.Append("<td class=xl7510558 width=94 style='border-top:none;border-left:none;  width:71pt'>Transactions (Monthly)</td>");
            strData2.Append("<td class=xl7510558 width=99 style='border-top:none;border-left:none;  width:74pt'>Transactions (Monthly)</td>");
            strData2.Append("<td class=xl7510558 width=99 style='border-top:none;border-left:none;  width:74pt'>Transactions (Monthly)</td>");
            strData2.Append("<td class=xl7510558 width=93 style='border-top:none;border-left:none;  width:70pt'>Transactions (Monthly)</td>");
            strData2.Append("<td class=xl7510558 width=91 style='border-top:none;border-left:none;  width:68pt'>Transactions (Monthly)</td>");
            strData2.Append("<td class=xl7510558 width=96 style='border-top:none;border-left:none;  width:72pt'>Transactions (Monthly)</td>");
            strData2.Append("<td class=xl7510558 width=91 style='border-top:none;border-left:none;  width:68pt'>Transactions (Monthly)</td>");
            strData2.Append("<td class=xl7510558 width=86 style='border-top:none;border-left:none;  width:65pt'>Transactions (Monthly)</td>");
            strData2.Append("<td class=xl7510558 width=90 style='border-top:none;border-left:none;  width:68pt'>Transactions (Monthly)</td>");
            strData2.Append("<td class=xl7510558 width=87 style='border-top:none;border-left:none;  width:65pt'>Transactions (Monthly)</td>");
            strData2.Append("<td class=xl8010558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("</tr>");

            DataSet dsAcqus = new DataSet();
            dsAcqus = objWayPlan.GetAcquisitionClient(cityId);
            if (dsAcqus.Tables[0].Rows.Count > 0)
            {

                foreach (DataRow drAcqui in dsAcqus.Tables[0].Rows)
                {
                    strData2.Append("<tr height=20 style='height:15.0pt'>");
                    strData2.Append("<td height=20 class=xl6553510558 style='height:15.0pt'></td>");
                    strData2.Append("<td class=xl7510558 width=89 style='border-top:none;width:67pt'>&nbsp;</td>");
                    strData2.Append("<td class=xl7510558 width=172 style='border-top:none;border-left:none; width:129pt'>&nbsp;</td>");
                    strData2.Append("<td class=xl7910558 width=302 style='border-top:none;border-left:none; width:227pt' colspan='2'>" + drAcqui["clientConame"] + "</td>");
                    //strData2.Append("<td class=xl7510558 width=67 style='border-top:none;border-left:none;  width:50pt'>0</td>");

                    DataSet dsAcquiWayPlan = new DataSet();
                    dsAcquiWayPlan = objWayPlan.GetAcquisitionWayPlanData(financialYear, cityId, Convert.ToString(drAcqui["clientConame"]));
                    if (dsAcquiWayPlan.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow drAcquiWay in dsAcquiWayPlan.Tables[0].Rows)
                        {

                            for (int i = 4; i <= 12; i++)
                            {
                                if (Convert.ToString(drAcquiWay["GroupEntity"]) == Convert.ToString(drAcqui["GroupEntity"]) && Convert.ToString(drAcquiWay["MonthYear"]) == Convert.ToString(i + "-" + startYear))
                                {
                                    strData2.Append("<td class=xl7510558 width=96 style='border-top:none;border-left:none;  width:72pt'>" + drAcquiWay["TotalTransaction"] + "</td>");
                                    transAcqui[i.ToString()] = Convert.ToInt32(transAcqui[i.ToString()]) + Convert.ToInt32(drAcquiWay["TotalTransaction"]);
                                }
                                else
                                {
                                    strData2.Append("<td class=xl7510558 width=96 style='border-top:none;border-left:none;  width:72pt'>0</td>");
                                    transAcqui[i.ToString()] = Convert.ToInt32(transAcqui[i.ToString()]) + 0;
                                }

                            }
                            for (int i = 1; i <= 3; i++)
                            {
                                if (Convert.ToString(drAcquiWay["GroupEntity"]) == Convert.ToString(drAcqui["GroupEntity"]) && Convert.ToString(drAcquiWay["MonthYear"]) == Convert.ToString(i + "-" + endYear))
                                {
                                    strData2.Append("<td class=xl7510558 width=96 style='border-top:none;border-left:none;  width:72pt'>" + drAcquiWay["TotalTransaction"] + "</td>");
                                    transAcqui[i.ToString()] = Convert.ToInt32(transAcqui[i.ToString()]) + Convert.ToInt32(drAcquiWay["TotalTransaction"]);
                                }
                                else
                                {
                                    strData2.Append("<td class=xl7510558 width=96 style='border-top:none;border-left:none;  width:72pt'>0</td>");
                                    transAcqui[i.ToString()] = Convert.ToInt32(transAcqui[i.ToString()]) + 0;
                                }

                            }
                        }
                    }
                    else
                    {
                        strData2.Append("<td class=xl7510558 width=96 style='border-top:none;border-left:none;  width:72pt'>0</td>");
                        strData2.Append("<td class=xl7510558 width=95 style='border-top:none;border-left:none;  width:71pt'>0</td>");
                        strData2.Append("<td class=xl7810558 width=94 style='border-top:none;border-left:none;  width:71pt'>0</td>");
                        strData2.Append("<td class=xl7510558 width=99 style='border-top:none;border-left:none;  width:74pt'>0</td>");
                        strData2.Append("<td class=xl7810558 width=99 style='border-top:none;border-left:none;  width:74pt'>0</td>");
                        strData2.Append("<td class=xl7810558 width=93 style='border-top:none;border-left:none;  width:70pt'>0</td>");
                        strData2.Append("<td class=xl7810558 width=91 style='border-top:none;border-left:none;  width:68pt'>0</td>");
                        strData2.Append("<td class=xl7810558 width=96 style='border-top:none;border-left:none;  width:72pt'>0</td>");
                        strData2.Append("<td class=xl7810558 width=91 style='border-top:none;border-left:none;  width:68pt'>0</td>");
                        strData2.Append("<td class=xl7810558 width=86 style='border-top:none;border-left:none;  width:65pt'>0</td>");
                        strData2.Append("<td class=xl7810558 width=90 style='border-top:none;border-left:none;  width:68pt'>0</td>");
                        strData2.Append("<td class=xl7810558 width=87 style='border-top:none;border-left:none;  width:65pt'>0</td>");
                    }
                    strData2.Append("<td class=xl8010558 style='border-top:none;border-left:none'>&nbsp;</td>");
                    strData2.Append("<td class=xl6553510558></td>");
                    strData2.Append("<td class=xl6553510558></td>");
                    strData2.Append("<td class=xl6553510558></td>");
                    strData2.Append("</tr>");
                }

            }

            strData2.Append("<tr height=20 style='height:15.0pt'>");
            strData2.Append("<td height=20 class=xl6553510558 style='height:15.0pt'></td>");
            strData2.Append("<td class=xl8010558 style='border-top:none'>&nbsp;</td>");
            strData2.Append("<td class=xl7610558 style='border-top:none;border-left:none'>New Accounts Total</td>");
            strData2.Append("<td class=xl7610558 style='border-top:none;border-left:none' colspan='2' >&nbsp;</td>");
            //strData2.Append("<td class=xl7710558 style='border-top:none;border-left:none'>0</td>");

            for (int i = 4; i <= 12; i++)
            {
                strData2.Append("<td class=xl7710558 style='border-top:none;border-left:none'>" + transAcqui[i.ToString()] + "</td>");
            }
            for (int i = 1; i <= 3; i++)
            {
                strData2.Append("<td class=xl7710558 style='border-top:none;border-left:none'>" + transAcqui[i.ToString()] + "</td>");

            }

            strData2.Append("<td class=xl8010558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("</tr>");
            strData2.Append("<tr height=20 style='height:15.0pt'>");
            strData2.Append("<td height=20 class=xl6553510558 style='height:15.0pt'></td>");
            strData2.Append("<td class=xl8010558 style='border-top:none'>&nbsp;</td>");
            strData2.Append("<td class=xl8010558 style='border-top:none;border-left:none'  >&nbsp;</td>");
            strData2.Append("<td class=xl8010558 style='border-top:none;border-left:none' colspan='2'>&nbsp;</td>");
            // strData2.Append("<td class=xl8010558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl8210558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl8210558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl8210558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl8210558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl8210558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl8210558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl8210558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl8210558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl8210558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl8210558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl8210558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl8210558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl8010558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("</tr>");
            strData2.Append("<tr height=20 style='height:15.0pt'>");
            strData2.Append("<td height=20 class=xl6553510558 style='height:15.0pt'></td>");
            strData2.Append("<td class=xl8010558 style='border-top:none'>&nbsp;</td>");
            strData2.Append("<td class=xl8010558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl8010558 style='border-top:none;border-left:none' colspan='2'>&nbsp;</td>");
            //strData2.Append("<td class=xl8010558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl8210558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl8210558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl8210558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl8210558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl8210558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl8210558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl8210558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl8210558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl8210558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl8210558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl8210558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl8210558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl8010558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("</tr>");
            strData2.Append("<tr height=20 style='height:15.0pt'>");
            strData2.Append("<td height=20 class=xl6553510558 style='height:15.0pt'></td>");
            strData2.Append("<td class=xl8010558 style='border-top:none'>&nbsp;</td>");
            strData2.Append("<td class=xl8010558 style='border-top:none;border-left:none' >&nbsp;</td>");
            strData2.Append("<td class=xl8010558 style='border-top:none;border-left:none' colspan='2'>&nbsp;</td>");
            //strData2.Append("<td class=xl8010558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl8210558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl8210558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl8210558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl8210558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl8210558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl8210558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl8210558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl8210558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl8210558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl8210558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl8210558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl8210558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl8010558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("</tr>");
            strData2.Append("<tr height=20 style='height:15.0pt'>");
            strData2.Append("<td height=20 class=xl6553510558 style='height:15.0pt'></td>");
            strData2.Append("<td class=xl8010558 style='border-top:none'>&nbsp;</td>");
            strData2.Append("<td class=xl8010558 style='border-top:none;border-left:none'>Grand Total</td>");
            strData2.Append("<td class=xl6610558 style='border-top:none;border-left:none' colspan='2'>&nbsp;</td>");
            // strData2.Append("<td class=xl9410558 style='border-top:none;border-left:none'>3031</td>");

            for (int i = 4; i <= 12; i++)
            {
                int GrandTotal = Convert.ToInt32(transAcqui[i.ToString()]) + Convert.ToInt32(transDic[i.ToString()]);
                //GrandTotalDic[i.ToString()] = GrandTotal;
                strData2.Append("<td class=xl9410558 style='border-top:none;border-left:none'>" + GrandTotal + "</td>");
                strData1.Append("<td class=xl6710558 align=right style='border-top:none;border-left:none'>" + GrandTotal + "</td>");
            }
            for (int i = 1; i <= 3; i++)
            {
                int GrandTotal = Convert.ToInt32(transAcqui[i.ToString()]) + Convert.ToInt32(transDic[i.ToString()]);
                // GrandTotalDic[i.ToString()] = GrandTotal;
                strData2.Append("<td class=xl9410558 style='border-top:none;border-left:none'>" + GrandTotal + "</td>");
                strData1.Append("<td class=xl6710558 align=right style='border-top:none;border-left:none'>" + GrandTotal + "</td>");
            }

            strData2.Append("<td class=xl8010558 style='border-top:none;border-left:none'>&nbsp;</td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("</tr>");
            strData2.Append("<tr height=20 style='height:15.0pt'>");
            strData2.Append("<td height=20 class=xl6553510558 style='height:15.0pt'></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("</tr>");
            strData2.Append("<tr height=20 style='height:15.0pt'>");
            strData2.Append("<td height=20 class=xl6553510558 style='height:15.0pt'></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("</tr>");
            strData2.Append("<tr height=20 style='height:15.0pt'>");
            strData2.Append("<td height=20 class=xl6553510558 style='height:15.0pt'></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl7010558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("</tr>");
            strData2.Append("<tr height=20 style='height:15.0pt'>");
            strData2.Append("<td height=20 class=xl6553510558 style='height:15.0pt'></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("</tr>");
            strData2.Append("<tr height=20 style='height:15.0pt'>");
            strData2.Append("<td height=20 class=xl6553510558 style='height:15.0pt'></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl7410558 width=67 style='width:50pt'></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("<td class=xl6553510558></td>");
            strData2.Append("</tr>");
            strData2.Append("<![if supportMisalignedColumns]>");
            strData2.Append("<tr height=0 style='display:none'>");
            strData2.Append("<td width=19 style='width:14pt'></td>");
            strData2.Append("<td width=89 style='width:67pt'></td>");
            strData2.Append("<td width=172 style='width:129pt'></td>");
            strData2.Append("<td width=302 style='width:227pt'></td>");
            strData2.Append("<td width=67 style='width:50pt'></td>");
            strData2.Append("<td width=96 style='width:72pt'></td>");
            strData2.Append("<td width=95 style='width:71pt'></td>");
            strData2.Append("<td width=94 style='width:71pt'></td>");
            strData2.Append("<td width=99 style='width:74pt'></td>");
            strData2.Append("<td width=99 style='width:74pt'></td>");
            strData2.Append("<td width=93 style='width:70pt'></td>");
            strData2.Append("<td width=91 style='width:68pt'></td>");
            strData2.Append("<td width=96 style='width:72pt'></td>");
            strData2.Append("<td width=91 style='width:68pt'></td>");
            strData2.Append("<td width=86 style='width:65pt'></td>");
            strData2.Append("<td width=90 style='width:68pt'></td>");
            strData2.Append("<td width=87 style='width:65pt'></td>");
            strData2.Append("<td width=91 style='width:68pt'></td>");
            strData2.Append("<td width=64 style='width:48pt'></td>");
            strData2.Append("<td width=64 style='width:48pt'></td>");
            strData2.Append("<td width=64 style='width:48pt'></td>");
            strData2.Append("</tr>");
            strData2.Append("<![endif]>");
            strData2.Append("</table>");
            strData2.Append("</div>");
            strData2.Append("<!----------------------------->");
            strData2.Append("<!--END OF OUTPUT FROM EXCEL PUBLISH AS WEB PAGE WIZARD-->");
            strData2.Append("<!----------------------------->");
            strData2.Append("</body>");
            strData2.Append("</html>");
        }
        else
        {
            lblMessage.Text = "Recoud not found";
            lblMessage.ForeColor = System.Drawing.Color.Red;
        }
        strData.Append(strData1);
        strData.Append(strData2);
        return strData;
    }
    protected void btnGet_Click(object sender, EventArgs e)
    {
        StringBuilder strData = new StringBuilder();
        int cityId = 0;
        string financialYear = string.Empty;
        if (ddlCity.SelectedIndex == 0)
        {
            lblMessage.Text = "Select city name";
            lblMessage.ForeColor = System.Drawing.Color.Red;
            return;
        }
        else if (ddlFinacialYear.SelectedItem.Text == "")
        {
            lblMessage.Text = "Select financial year";
            lblMessage.ForeColor = System.Drawing.Color.Red;
            return;
        }
        else
        {
            cityId = Convert.ToInt32(ddlCity.SelectedValue);
            financialYear = Convert.ToString(ddlFinacialYear.SelectedValue);

            InitializedDic(transDic);
            InitializedDic(transAcqui);
            InitializedDic(GrandTotalDic);
            InitializedDic(AVGLTR);
            InitializedDic(AVGSR);
            InitializedDic(TargetDic);
            InitializedLTR(cityId, LTRDic);
            InitializedDic(spotRentDic);

            strData = BindExcelData(cityId, financialYear);
            ltlSummary.Text = strData.ToString();
        }
    }
    protected void btnExport_Click(object sender, EventArgs e)
    {

        StringBuilder strData = new StringBuilder();
        int cityId = 0;
        string financialYear = string.Empty;
        if (ddlCity.SelectedIndex == 0)
        {
            lblMessage.Text = "Select city name";
            lblMessage.ForeColor = System.Drawing.Color.Red;
            return;
        }
        else if (ddlFinacialYear.SelectedItem.Text == "")
        {
            lblMessage.Text = "Select financial year";
            lblMessage.ForeColor = System.Drawing.Color.Red;
            return;
        }
        else
        {
            cityId = Convert.ToInt32(ddlCity.SelectedValue);
            financialYear = Convert.ToString(ddlFinacialYear.SelectedValue);

            InitializedDic(transDic);
            InitializedDic(transAcqui);
            InitializedDic(GrandTotalDic);
            InitializedDic(AVGLTR);
            InitializedDic(AVGSR);
            InitializedDic(TargetDic);
            InitializedLTR(cityId, LTRDic);
            InitializedDic(spotRentDic);

            strData = BindExcelData(cityId, financialYear);
            ltlSummary.Text = strData.ToString();
            Response.Clear();
            Response.Buffer = true;
            string strFileName = "Way Forward Plan Report";
            strFileName = strFileName.Replace("(", "");
            strFileName = strFileName.Replace(")", "");
            Response.AddHeader("content-disposition", "attachment;filename=" + strFileName.ToString() + ".xls");
            Response.Charset = "";
            Response.ContentType = "application/vnd.ms-excel";
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            ltlSummary.RenderControl(hw);
            Response.Output.Write(sw.ToString());
            Response.End();

        }


    }
}