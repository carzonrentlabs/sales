﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Salespage.master" AutoEventWireup="true"
    CodeFile="Acquisitions.aspx.cs" Inherits="Sales_Acquisitions" EnableEventValidation="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder_Page" runat="Server">
    <script src="../JQuery/jquery-1.4.min.js" type="text/javascript"></script>
    <script src="../JQuery/moment.js" type="text/javascript"></script>
    <script src="../JQuery/ui.core.js" type="text/javascript"></script>
    <script src="../JQuery/ui.datepicker.js" type="text/javascript"></script>
    <script src="../ScriptsReveal/jquery.reveal.js" type="text/javascript"></script>
    <script type="text/javascript">
        //$(function ()
        function pageLoad(sender, args) {
            $("#<%=txtLocalInfluencerMobile.ClientID%>").keyup(function () {
                //            alert("tere");

            });



            $("#<%=drpYear.ClientID%>").change(function () {
                // ShowProgress();
            });


            //});

            function ShowProgress() {
                setTimeout(function () {
                    var modal = $('<div />');
                    modal.addClass("modal");
                    $('body').append(modal);
                    var loading = $(".loading");
                    loading.show();
                    var top = Math.max($(window).height() / 2 - loading[0].offsetHeight / 2, 0);
                    var left = Math.max($(window).width() / 2 - loading[0].offsetWidth / 2, 0);
                    loading.css({ top: top, left: left });
                }, 200);
            }
        }
    </script>
    <style type="text/css">
        body
        {
            font-family: Calibri;
        }
        .gridcss
        {
            background: #df5015;
            font-weight: bold;
            color: White;
        }
    </style>
    <asp:UpdatePanel ID="UpdateSales" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <table cellpadding="0" cellspacing="0" style="width: 100%">
                <tr>
                    <td align="center">
                        <b style="font-size: medium;">New Acquisitions</b>&nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:Label ID="lblMsg" runat="server" ForeColor="red"></asp:Label>
                    </td>
                </tr>
            </table>
            <br />
            <center>
                <table cellpadding="10" cellspacing="0" border="1" width="60%">
                    <tr>
                        <td style="white-space: nowrap">
                            <b>City Name</b>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlHQ" runat="server" OnSelectedIndexChanged="ddlHQ_SelectedIndexChanged"
                                AutoPostBack="true">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlHQ"
                                ErrorMessage="*"  ForeColor="Green" Display="Static" InitialValue="0" ValidationGroup ="Client"></asp:RequiredFieldValidator>
                        </td>
                        <td style="white-space: nowrap">
                            <b>Company Name</b>
                        </td>
                        <td style="white-space: nowrap">
                            <asp:DropDownList ID="ddlClientName" runat="server" OnSelectedIndexChanged="ddlClientName_SelectedIndexChanged"
                                AutoPostBack="true">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="rfvClientName" runat="server" ControlToValidate="ddlClientName"
                                ErrorMessage="*" ForeColor="Green" Display="Static" InitialValue="0" ValidationGroup ="Client"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td style="white-space: nowrap">
                            <b>Account Type</b>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlAccountType" runat="server">
                                <asp:ListItem Text="--Select--" Value="0"></asp:ListItem>
                                <asp:ListItem Text="Centralized" Value="Centralized"></asp:ListItem>
                                <asp:ListItem Text="Dead" Value="Dead"></asp:ListItem>
                                <asp:ListItem Text="Decentralized" Value="Decentralized"></asp:ListItem>
                                <asp:ListItem Text="Follow-up Account" Value="Follow-up Account"></asp:ListItem>
                                <asp:ListItem Text="Key Centralized" Value="Key Centralized"></asp:ListItem>
                                <asp:ListItem Text="Key Decentralized" Value="Key Decentralized"></asp:ListItem>
                                <asp:ListItem Text="Key Local" Value="Key Local"></asp:ListItem>
                                <asp:ListItem Text="Low Producing" Value="Low Producing"></asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="rfvAccountType" runat="server" ControlToValidate="ddlAccountType"
                                ErrorMessage="*" ForeColor="Green" Display="Static" InitialValue="0" ValidationGroup ="Client"></asp:RequiredFieldValidator>
                        </td>
                        <td style="white-space: nowrap">
                            <b>Signed by</b>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlSigned" runat="server">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="rfvddlSigned" runat="server" ControlToValidate="ddlSigned"
                                ErrorMessage="*" ForeColor="Green" Display="Static" InitialValue="0" ValidationGroup ="Client"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td style="white-space: nowrap">
                            <b>Local Influencer</b>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlLocalInf" runat="server" OnSelectedIndexChanged="ddlLocalInf_SelectedIndexChanged"
                                AutoPostBack="true">
                                <asp:ListItem Text="--Select--" Value="0"></asp:ListItem>
                                <asp:ListItem Value="Yes" Text="Yes"></asp:ListItem>
                                <asp:ListItem Value="No" Text="No"></asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="rfvddlLocalInf" runat="server" ControlToValidate="ddlLocalInf"
                                ErrorMessage="*" ForeColor="Green" Display="Static" InitialValue="0" ValidationGroup ="Client"></asp:RequiredFieldValidator>
                        </td>
                        <td style="white-space: nowrap">
                            <b>Monthly Potential</b>
                        </td>
                        <td>
                            <asp:TextBox ID="txtMonthlyPotential" runat="Server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvMonthlyPotential" runat="server" ControlToValidate="txtMonthlyPotential"
                                ErrorMessage="*" ForeColor="Green" Display="Static" ValidationGroup ="Client"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ForeColor="Green" runat="server" ID="rexNumber" ControlToValidate="txtMonthlyPotential"
                                ValidationExpression="\d+" ErrorMessage="Please enter  number!" />
                        </td>
                    </tr>
                    <tr id="trLocalInfluencerName" runat="server" visible="false">
                        <td style="white-space: nowrap">
                            <b>Local Influencer Name</b>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtLocalInfluencerName"></asp:TextBox>
                        </td>
                        <td style="white-space: nowrap">
                            <b>Local Influencer Mobile No</b>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtLocalInfluencerMobile"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="white-space: nowrap">
                            <b>Local Account Mgr</b>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlLocalAccountManager" runat="server" Width="200px">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="rfvLocalAccountManager" runat="server" ControlToValidate="ddlLocalAccountManager"
                                ErrorMessage="*" ForeColor="Green" Display="Static" InitialValue="0" ValidationGroup ="Client"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td style="white-space: nowrap">
                            <b>Financial Year</b>
                            <td style="white-space: nowrap;">
                                <asp:DropDownList ID="drpYear" runat="server" AutoPostBack="True" OnSelectedIndexChanged="drpYear_SelectedIndexChanged">
                                    <asp:ListItem Value="0">--Select--</asp:ListItem>
                                    <asp:ListItem Value="2016-2017">2016-2017</asp:ListItem>
                                    <asp:ListItem Value="2017-2018">2017-2018</asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="rfvdrpYear" runat="server" ControlToValidate="drpYear"
                                    Display="Static" ErrorMessage="*" ForeColor="Red" InitialValue="0" ValidationGroup ="Client"></asp:RequiredFieldValidator>
                                <asp:UpdateProgress ID="updProgress" runat="server" AssociatedUpdatePanelID="UpdateSales"
                                    DynamicLayout="true">
                                    <ProgressTemplate>
                                        <img src="../Images/loader.gif" alt="Loading..." />
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                            </td>
                            <td style="white-space: nowrap">
                                <b>
                                    <asp:Label ID="lblFyAmt" runat="server" Text="FY Transaction Details" Visible="false"></asp:Label></b>
                            </td>
                            <td>
                                <asp:GridView ID="GvFinancialyearWiseAmount" HeaderStyle-BackColor="#3AC0F2" HeaderStyle-ForeColor="White"
                                    runat="server" AutoGenerateColumns="false" OnRowDataBound="GvFinancialyearWiseAmount_RowDataBound">
                                    <Columns>
                                        <asp:TemplateField HeaderText="ID" SortExpression="ID" Visible="false">
                                            <ItemTemplate>
                                                <center>
                                                    <asp:Label ID="lblID" Text='<%# Eval("ID")%>' runat="server"></asp:Label></center>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="MonthID" SortExpression="MonthID" Visible="false">
                                            <ItemTemplate>
                                                <center>
                                                    <asp:Label ID="lblMonthID" Text='<%# Eval("MonthID")%>' runat="server"></asp:Label>
                                                </center>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Month" SortExpression="Month">
                                            <ItemTemplate>
                                                <center>
                                                    <asp:Label ID="lblMonth" Text='<%# Eval("Month")%>' runat="server"></asp:Label></center>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Transactions">
                                            <ItemTemplate>
                                                <center>
                                                    <asp:TextBox ID="txtTransactions" Text='<%# Eval("Amount")%>' runat="server" Width="111px"
                                                        MaxLength="11"></asp:TextBox></center>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtTransactions"
                                                    ValidChars="1234567890.">
                                                </cc1:FilteredTextBoxExtender>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <%--<asp:BoundField HeaderText="Achieved Transaction" DataField="Trans" DataFormatString="{0:0.00}" />
                                        <asp:BoundField HeaderText="Avg. ticket Size" DataField="AvgticketSize" DataFormatString="{0:0.00}" />
                                        <asp:BoundField HeaderText="Target Rev" DataField="TargetRev" DataFormatString="{0:0.00}" />
                                        <asp:BoundField HeaderText="Achieved Rev" DataField="AchievedReve" DataFormatString="{0:0.00}" />--%>
                                    </Columns>
                                </asp:GridView>
                            </td>
                    </tr>
                </table>
                <br />
                <table cellpadding="5" cellspacing="0" border="0">
                    <tr>
                        <td style="white-space: nowrap">
                            <asp:Button ID="btnSubmit" runat="server" Text="Add / Edit" OnClick="btnSubmit_Click"
                                Visible="false"  ValidationGroup ="Client"/>
                        </td>
                        <%--<td>
                            <asp:Button ID="bntViewDetails" runat="server" Text="View Details" CausesValidation="false"
                                OnClick="bntViewDetails_Click" />
                        </td>
                        <td>
                            <asp:Button ID="btnExport" runat="server" Text="Export to excel" CausesValidation="false"
                                OnClick="btnExport_Click" />
                        </td>--%>
                    </tr>
                </table>
                <div id="detailShow">
                    <asp:GridView ID="gvShowDetails" runat="server" AutoGenerateColumns="false">
                        <Columns>
                            <asp:BoundField DataField="CompanyName" HeaderText="Company Name" />
                            <asp:BoundField DataField="AccountType" HeaderText="Account Type" />
                            <asp:BoundField DataField="CityName" HeaderText="City Name" />
                            <asp:BoundField DataField="Signedby" HeaderText="Signed by" />
                            <asp:BoundField DataField="LocalInfluencer" HeaderText="Local Influencer" />
                            <asp:BoundField DataField="MonthlyPotential" HeaderText="Monthly Potential" />
                            <asp:BoundField DataField="Target" HeaderText="Target" />
                            <asp:BoundField DataField="Period" HeaderText="Period" />
                            <asp:BoundField DataField="LocalInfluencerName" HeaderText="Local Influencer Name" />
                            <asp:BoundField DataField="LocalInfluencerMobileNo" HeaderText="Local Influencer Mobile No" />
                            <asp:BoundField DataField="LocalAccountMgr" HeaderText="Local Account Mgr" />
                            <asp:BoundField DataField="Implant" HeaderText="Implant" />
                            <asp:BoundField DataField="Competitor" HeaderText="Competitor" />
                        </Columns>
                    </asp:GridView>
                </div>
            </center>
        </ContentTemplate>
        <Triggers>
            <%--<asp:PostBackTrigger ControlID="btnExport" runat="server" />
            <ajax:PostBackTrigger ControlID="btnExport"></ajax:PostBackTrigger>
            <ajax:PostBackTrigger ControlID="btnExport"></ajax:PostBackTrigger>--%>
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
