﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Salespage.master" AutoEventWireup="true"
    CodeFile="WayPlan.aspx.cs" Inherits="Sales_WayPlan" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder_Page" runat="Server">
    <script src="../JQuery/jquery-1.4.1.min.js" type="text/javascript"></script>
    <script type="text/javascript">
    $(function () {
        $("#<%=btnGet.ClientID%>").click(function () {
            if ($("#<%=ddlCity.ClientID%>")[0].selectedIndex == 0)
            {   
              alert("Select city name");
              $("#<%=ddlCity.ClientID%>").focus();
              return false;
            }
            else 
            {
             ShowProgress();
            }

     });
     $("#<%=btnExport.ClientID%>").click(function () {
             if ($("#<%=ddlCity.ClientID%>")[0].selectedIndex == 0) {
                 alert("Select city name");
                 $("#<%=ddlCity.ClientID%>").focus();
                 return false;
             }
             else {
                 //ShowProgress();
             }

         });
    });

    function ShowProgress() {
        setTimeout(function () {
            var modal = $('<div />');
            modal.addClass("modal");
            $('body').append(modal);
            var loading = $(".loading");
            loading.show();
            var top = Math.max($(window).height() / 2 - loading[0].offsetHeight / 2, 0);
            var left = Math.max($(window).width() / 2 - loading[0].offsetWidth / 2, 0);
            loading.css({ top: top, left: left });
        }, 200);
    }
    </script>
    <center>
        <table cellpadding="0" cellspacing="0" border="0" width="50%">
            <tr>
                <td colspan="4">
                    <asp:Label ID="lblMessage" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <b>Select City Name</b>
                </td>
                <td>
                    <asp:DropDownList ID="ddlCity" runat="server">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="rvfddlCity" ControlToValidate="ddlCity" InitialValue="0"
                        Display="Static" ErrorMessage="*" ForeColor="Red" runat="server"></asp:RequiredFieldValidator>
                </td>
                <td>
                    <b>Select Financial Year</b>
                </td>
                <td>
                    <asp:DropDownList ID="ddlFinacialYear" runat="server">
                        <asp:ListItem Text="2017-2018" Value="2017-2018"></asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td>
                    <asp:Button ID="btnGet" runat="server" Text="Get It" OnClick="btnGet_Click" />&nbsp;&nbsp;
                    <asp:Button ID ="btnExport" runat ="server" Text ="Export" 
                        onclick="btnExport_Click" />

                </td>
            </tr>
        </table>
        <div class="loading" align="center">
            Loading. Please wait.<br />
            <br />
            <img src="../Images/loader.gif" alt="" />
        </div>
        <asp:Literal ID="ltlSummary" runat="server"></asp:Literal>
    </center>
</asp:Content>
