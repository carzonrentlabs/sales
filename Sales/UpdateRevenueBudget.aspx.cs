﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using Insta;

public partial class Sales_UpdateRevenueBudget : System.Web.UI.Page
{
    CRM_Insta objcrm = new CRM_Insta();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //Session["SysUserId"] = 3274;
            BindUnit();
        }
    }

    private void BindUnit()
    {
        DataSet dsUnitname = new DataSet();
        dsUnitname = objcrm.GetUnitName();
        ddlUnit.DataTextField = "UnitName";
        ddlUnit.DataValueField = "UnitID";
        ddlUnit.DataSource = dsUnitname;
        ddlUnit.DataBind();
        //ddlUnit.Items.Insert(0, "All");
    }

    public void ClearControl()
    {
        ddlBudgetDetails.SelectedIndex = 0;
        ddlUnit.SelectedIndex = 0;
        txtRemarks.Text = string.Empty;
        txtAmount.Text = string.Empty;
    }

    public void ClearControlU()
    {
        txtRemarks.Text = string.Empty;
        txtAmount.Text = string.Empty;
        ddlUnit.DataSource = null;
        ddlUnit.DataBind();
        ddlBudgetDetails.DataSource = null;
        ddlBudgetDetails.DataBind();
        ddlBudgetDetails.SelectedIndex = -1;
        ddlBudgetDetails.Items.Clear(); 
    }

    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        try
        {
            if (ddlUnit.SelectedIndex == 0)
            {
                lblMessage.Text = "Please Select Branch !";
                lblMessage.Visible = true;
                lblMessage.ForeColor = System.Drawing.Color.Red;
                return;
            }

            if (ddlBudgetDetails.SelectedIndex == -1)
            {
                lblMessage.Text = "Budget Date Not Exist For Selected Branch !";
                lblMessage.Visible = true;
                lblMessage.ForeColor = System.Drawing.Color.Red;
                return;
            }

            if (ddlBudgetDetails.SelectedIndex == 0)
            {
                lblMessage.Text = "Please Select Budget Date !";
                lblMessage.Visible = true;
                lblMessage.ForeColor = System.Drawing.Color.Red;
                return;
            }

            if (txtAmount.Text == "")
            {
                lblMessage.Text = "Please Enter Amount !";
                lblMessage.Visible = true;
                lblMessage.ForeColor = System.Drawing.Color.Red;
                return;
            }

            int rslt = objcrm.UpdateRevenueBudgetUnitWise(Convert.ToInt32(ddlUnit.SelectedValue),
                Convert.ToDateTime(ddlBudgetDetails.SelectedItem.Text),
                Convert.ToDouble(txtAmount.Text)
                , txtRemarks.Text.Trim(),
                Convert.ToInt32(Session["UserID"]));
            if (rslt == 0)
            {
                lblMessage.Text = "Record Update Successfully !";
                lblMessage.Visible = true;
                lblMessage.ForeColor = System.Drawing.Color.Red;
                ClearControl();
            }
            else if (rslt == -1)
            {
                lblMessage.Text = "Record already Present for the Unit and Budget Date !";
                lblMessage.Visible = true;
                lblMessage.ForeColor = System.Drawing.Color.Red;
            }
            else
            {
                lblMessage.Text = "Not Submitted Successfully !";
                lblMessage.Visible = true;
                lblMessage.ForeColor = System.Drawing.Color.Red;
            }
        }
        catch (Exception ex)
        {
            lblMessage.Text = ex.Message;
            lblMessage.Visible = true;
            lblMessage.ForeColor = System.Drawing.Color.Red;
        }
    }

    protected void ddlUnit_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataSet dsHQ = new DataSet();
        CRM_Insta objBudget = new CRM_Insta();
        dsHQ = objBudget.GetRevenueBudgetDateDetails(Convert.ToInt32(ddlUnit.SelectedValue));
        if (dsHQ.Tables[0].Rows.Count > 0)
        {
            lblMessage.Text = "";
            ddlBudgetDetails.DataSource = dsHQ.Tables[0];
            ddlBudgetDetails.DataTextField = "BudgetDate";
            ddlBudgetDetails.DataValueField = "BudgetId";
            ddlBudgetDetails.DataBind();
            ddlBudgetDetails.Items.Insert(0, new ListItem("--Select--", "0"));
        }
        else
        {
           
            ClearControlU(); 
            lblMessage.Text = "Budget Date Not Exist For Selected Branch !";
            lblMessage.Visible = true;
            lblMessage.ForeColor = System.Drawing.Color.Red;
        }
    }
    protected void ddlBudgetDetails_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataSet dsHQ = new DataSet();
        CRM_Insta objBudget = new CRM_Insta();
        dsHQ = objBudget.GetRevenueBudgetDetails(Convert.ToInt32(ddlUnit.SelectedValue), Convert.ToDateTime(ddlBudgetDetails.SelectedItem.Text));
        if (dsHQ.Tables[0].Rows.Count > 0)
        {

            txtAmount.Text = dsHQ.Tables[0].Rows[0]["Amount"].ToString();
            txtRemarks.Text = dsHQ.Tables[0].Rows[0]["Remarks"].ToString();
        }
        else
        {
            ddlUnit.DataSource = null;
            ddlUnit.DataBind();
        }
    }
   
    protected void BtnClear_Click(object sender, EventArgs e)
    {
        ClearControl();
        lblMessage.Text = ""; 
    }
}