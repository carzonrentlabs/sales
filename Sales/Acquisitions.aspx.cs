﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.IO;
using ExcelUtil;
using System.Globalization;


public partial class Sales_Acquisitions : System.Web.UI.Page
{
    CorporateModule objCorModule = null;
    SaleModule objSale = null;
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!Page.IsPostBack)
        {
            BindClient();
            BindCityName(ddlHQ);
            BindCityName(ddlSigned);
            BindAccountManager();
        }

    }

    private void BindClient()
    {
        DataSet ds = new DataSet();
        objCorModule = new CorporateModule();
        ds = objCorModule.GetProspects(0);
        if (ds.Tables[0].Rows.Count > 0)
        {
            ddlClientName.DataSource = ds.Tables[0];
            ddlClientName.DataTextField = "ClientCoName";
            ddlClientName.DataValueField = "ProspectId";
            ddlClientName.DataBind();
            ddlClientName.Items.Insert(0, new ListItem("--Select--", "0"));
        }

        else
        {
            ddlClientName.DataSource = null;
            ddlClientName.DataBind();
        }


    }

    private void BindAccountManager()
    {
        DataSet ds = new DataSet();
        objCorModule = new CorporateModule();
        ds = objCorModule.GetAccountManager();
        if (ds.Tables[0].Rows.Count > 0)
        {
            ddlLocalAccountManager.DataSource = ds.Tables[0];
            ddlLocalAccountManager.DataTextField = "LoginId";
            ddlLocalAccountManager.DataValueField = "Sysuserid";
            ddlLocalAccountManager.DataBind();
            ddlLocalAccountManager.Items.Insert(0, new ListItem("--Select--", "0"));
        }

        else
        {
            ddlLocalAccountManager.DataSource = null;
            ddlLocalAccountManager.DataBind();
        }


    }
    private void BindCityName(DropDownList ddlist)
    {
        DataSet dsCity = new DataSet();
        objCorModule = new CorporateModule();
        dsCity = objCorModule.GetCityName();
        if (dsCity.Tables[0].Rows.Count > 0)
        {
            ddlist.DataSource = dsCity.Tables[0];
            ddlist.DataTextField = "cityname";
            ddlist.DataValueField = "CityID";
            ddlist.DataBind();
            ddlist.Items.Insert(0, new ListItem("--Select--", "0"));
        }
        else
        {
            ddlist.DataSource = null;
            ddlist.DataBind();
        }

    }

    public void InitializeControls(Control objcontrol)
    {
        foreach (Control control in objcontrol.Controls)
        {
            if ((control.GetType() == typeof(TextBox)))
            {
                ((TextBox)(control)).Text = "";
            }

            //if ((control.GetType() == typeof(Label)))
            //{
            //    ((Label)(control)).Text = "";
            //}

            if ((control.GetType() == typeof(DropDownList)))
            {
                if (((DropDownList)(control)).Items.Count > 0)
                {
                    ((DropDownList)(control)).ClearSelection();
                    ((DropDownList)(control)).SelectedIndex = 0;
                }
            }
            if ((control.GetType() == typeof(ListBox)))
            {
                if (((ListBox)(control)).Items.Count > 0)
                {
                    ((ListBox)(control)).ClearSelection();
                    ((ListBox)(control)).SelectedIndex = 0;
                }
            }

            if ((control.GetType() == typeof(CheckBox)))
            {
                ((CheckBox)(control)).Checked = false;
            }

            if (control.HasControls() && control.GetType() != typeof(MultiView))
            {
                InitializeControls(control);
            }


            if ((control.GetType() == typeof(GridView)))
            {
                ((GridView)(control)).DataSource = null;
                ((GridView)(control)).DataBind();
            }
            lblFyAmt.Visible = false;
        }
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        int result = 0;
        objSale = new SaleModule();
        objCorModule = new CorporateModule();
        string multipleImplantId = string.Empty;
        string multipleCompetitor = string.Empty;
        try
        {

            if (Convert.ToInt32(ddlClientName.SelectedValue) == 0 || Convert.ToInt32(ddlHQ.SelectedValue) == 0)
            {
                lblMsg.Text = "City name and company name should not empty.";
                return;
            }
            else if (Convert.ToString(ddlAccountType.SelectedValue) == "0" || Convert.ToInt32(ddlSigned.SelectedValue) == 0)
            {
                lblMsg.Text = "Account type and Signedby should not empty.";
                return;
            }
            else
            {

                objSale.ClientCoId = 957;
                objSale.ProposalId = Convert.ToInt32(ddlClientName.SelectedValue);
                objSale.AccountType = ddlAccountType.SelectedValue;
                objSale.HQ = Convert.ToInt32(ddlHQ.SelectedValue);
                objSale.SignedBy = Convert.ToInt32(ddlSigned.SelectedValue);
                objSale.LocalInfluencer = ddlLocalInf.SelectedValue;
                objSale.MonthlyPotential = txtMonthlyPotential.Text.Trim();
                objSale.LocalAccountMgr = ddlLocalAccountManager.SelectedValue;
                objSale.SysUserID = Convert.ToInt32(Session["UserID"].ToString());
                objSale.LocalInfluencerName = txtLocalInfluencerName.Text;
                objSale.LocalInfluencerMobileNo = txtLocalInfluencerMobile.Text;
                objSale.Target_Amount_PanIndia = 0;
                objSale.Target_FinancialYear = drpYear.SelectedValue;

                foreach (GridViewRow row in GvFinancialyearWiseAmount.Rows)
                {

                    Label lblMonthID = (Label)row.FindControl("lblMonthID");
                    Label lblMonth = (Label)row.FindControl("lblMonth");
                    if (lblMonth.Text.ToString().Contains("16"))
                    {
                        objSale.Target_Year = 2016;
                    }
                    else if (lblMonth.Text.ToString().Contains("17"))
                    {
                        objSale.Target_Year = 2017;
                    }
                    else if (lblMonth.Text.ToString().Contains("18"))
                    {
                        objSale.Target_Year = 2018;
                    }
                    Label lblID = (Label)row.FindControl("lblID");
                    TextBox txtAmt = (TextBox)row.FindControl("txtTransactions");
                    HiddenField hdnMonthID = (HiddenField)row.FindControl("hdnMonthID");

                    double Amount = 0;
                    if (txtAmt.Text == "")
                    {
                        Amount = 0;
                    }
                    else
                    {
                        Amount = Convert.ToDouble(txtAmt.Text);
                    }

                    objSale.Target_FinancialMonthID = Convert.ToInt32(lblID.Text);
                    objSale.Target_Month = Convert.ToInt32(lblMonthID.Text);

                    objSale.Target_FinancialMonth = lblMonth.Text;
                    objSale.Target_Amount = Amount;

                    result = objCorModule.SaveCorintAcquistionCorporate(objSale);

                }

                if (result > 0)
                {
                    lblMsg.Text = "Record saved successfuly.";
                    InitializeControls(Form);
                }
                else
                {
                    lblMsg.Text = "Getting some issues to save data.";
                }
            }
        }
        catch (Exception Ex)
        {
            lblMsg.Text = Ex.Message;
        }



    }

    protected void ddlLocalInf_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlLocalInf.SelectedItem.Text.ToString() == "Yes")
        {
            trLocalInfluencerName.Visible = true;
        }
        else
        {
            trLocalInfluencerName.Visible = false;
        }
    }

    protected void ddlClientName_SelectedIndexChanged(object sender, EventArgs e)
    {

        DataSet ds = new DataSet();
        DataSet dsAccountType = new DataSet();
        DataSet dsHQ = new DataSet();
        DataSet dsSingedBy = new DataSet();
        GvFinancialyearWiseAmount.DataSource = null;
        GvFinancialyearWiseAmount.DataBind();
        objCorModule = new CorporateModule();
        lblMsg.Text = "";
        drpYear.SelectedIndex = 0;
        gvShowDetails.DataSource = null;
        gvShowDetails.DataBind();
        if (ddlClientName.SelectedIndex > 0)
        {
            //Account Type 
            dsAccountType = objCorModule.GetAccountTypeAcqisition(Convert.ToInt32(ddlClientName.SelectedValue), Convert.ToInt32(ddlHQ.SelectedValue));
            if (dsAccountType.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dr in dsAccountType.Tables[0].Rows)
                {

                    ddlAccountType.SelectedValue = Convert.ToString(dr["AccountType"]);
                }
            }
            else
            {
                ddlAccountType.SelectedValue = null;
            }


            //Singed By
            dsSingedBy = objCorModule.GetSingedByAcqisition(Convert.ToInt32(ddlClientName.SelectedValue), Convert.ToInt32(ddlHQ.SelectedValue));
            if (dsSingedBy.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dr in dsSingedBy.Tables[0].Rows)
                {

                    ddlSigned.SelectedValue = Convert.ToString(dr["CityId"]);

                }
            }
            else
            {
                ddlSigned.SelectedValue = null;
            }
            //End Singed By

            //Local Influencer
            dsSingedBy = objCorModule.GetLocalInfluencerAcqisition(Convert.ToInt32(ddlClientName.SelectedValue), Convert.ToInt32(ddlHQ.SelectedValue));
            if (dsSingedBy.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dr in dsSingedBy.Tables[0].Rows)
                {
                    ddlLocalInf.SelectedValue = Convert.ToString(dr["LocalInfluencer"]);
                    txtMonthlyPotential.Text = Convert.ToString(dr["MonthlyPotential"]);
                }
            }
            else
            {
                ddlLocalInf.SelectedValue = null;
                txtMonthlyPotential.Text = "";
            }



            ds = objCorModule.GetLocalAccountMgrAcqisition(Convert.ToInt32(ddlClientName.SelectedValue), Convert.ToInt32(ddlHQ.SelectedValue));
            if (ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    ddlLocalAccountManager.SelectedValue = Convert.ToString(dr["LocalAccountMgr"]);

                }
            }
            else
            {
                ddlLocalAccountManager.SelectedValue = null;
            }

            

        }

    }

    protected void ddlAccountType_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataSet dsHQ = new DataSet();
        objCorModule = new CorporateModule();
        dsHQ = objCorModule.GetSaleHeadQuarter(Convert.ToInt32(ddlClientName.SelectedValue), Convert.ToInt32(ddlAccountType.SelectedValue));
        if (dsHQ.Tables[0].Rows.Count > 0)
        {
            ddlHQ.DataSource = dsHQ.Tables[0];
            ddlHQ.DataTextField = "CityName";
            ddlHQ.DataValueField = "CityID";
            ddlHQ.DataBind();
            ddlHQ.Items.Insert(0, new ListItem("--Select--", "0"));
        }
        else
        {
            ddlHQ.DataSource = null;
            ddlHQ.DataBind();
        }
    }




    private void BindGrid()
    {
        DataSet ds = new DataSet();
        objCorModule = new CorporateModule();
        lblMsg.Visible = false;
        ds = objCorModule.EntryDetails(Convert.ToInt32(ddlClientName.SelectedValue));
        if (ds.Tables[0].Rows.Count > 0)
        {
            gvShowDetails.DataSource = ds.Tables[0];
            gvShowDetails.DataBind();
            WorkbookEngine.ExportDataSetToExcel(ds, "SalesCorporate.xls");
        }
        else
        {
            lblMsg.Visible = true;
            lblMsg.Text = "Record not available.";
            gvShowDetails.DataSource = null;
            gvShowDetails.DataBind();

        }

    }
    protected void bntViewDetails_Click(object sender, EventArgs e)
    {
        DataSet ds = new DataSet();
        objCorModule = new CorporateModule();
        if (ddlClientName.SelectedIndex > 0)
        {
            lblMsg.Visible = false;
            ds = objCorModule.EntryDetails(Convert.ToInt32(ddlClientName.SelectedValue));
            if (ds.Tables[0].Rows.Count > 0)
            {
                gvShowDetails.DataSource = ds.Tables[0];
                gvShowDetails.DataBind();
            }
            else
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Record not available.";
                gvShowDetails.DataSource = null;
                gvShowDetails.DataBind();

            }

        }
        else
        {
            lblMsg.Visible = true;
            lblMsg.Text = "Select client name first.";
        }


    }


    protected void btnExport_Click(object sender, EventArgs e)
    {
        BindGrid();
    }

    private void BindYear(DropDownList ddlYear)
    {
        try
        {
            for (int i = 2017; i <= 2017; i++)
            {
                ddlYear.Items.Add(new ListItem(i.ToString(), i.ToString()));
            }
            ddlYear.Items.Insert(0, new ListItem("--Select--", "0"));
        }
        catch (Exception)
        {

            throw new Exception("The method or operation is not implemented.");
        }
    }
    private void BindMonth(DropDownList ddlMonth)
    {
        objCorModule = new CorporateModule();
        try
        {
            DataTable dtFromMonth = new DataTable();
            dtFromMonth = objCorModule.GetMonthName();
            ddlMonth.DataTextField = "monthName";
            ddlMonth.DataValueField = "number";
            ddlMonth.DataSource = dtFromMonth;
            ddlMonth.DataBind();
            ddlMonth.Items.Insert(0, new ListItem("--Select--", "0"));
        }
        catch (Exception)
        {

            throw new Exception("The method or operation is not implemented.");
        }
    }

    protected void drpYear_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataSet ds = new DataSet();
        objCorModule = new CorporateModule();
        ds = objCorModule.GetMonthWiseAmountForAcquisition(Convert.ToInt32(ddlClientName.SelectedValue), Convert.ToInt32(ddlHQ.SelectedValue), drpYear.SelectedValue.ToString());
        if (ds.Tables[0].Rows.Count > 0)
        {
            DataTable dt = ds.Tables[0];
            GvFinancialyearWiseAmount.DataSource = dt;
            GvFinancialyearWiseAmount.DataBind();
            lblFyAmt.Visible = true;
            btnSubmit.Visible = true;
        }
        else
        {
            DataTable dt = new DataTable();
            dt.Columns.AddRange(new DataColumn[8] { new DataColumn("ID", typeof(string)), new DataColumn("MonthID", typeof(string)), new DataColumn("Month", typeof(string)), new DataColumn("Amount", typeof(double)), new DataColumn("Trans", typeof(double)), new DataColumn("AvgticketSize", typeof(double)), new DataColumn("TargetRev", typeof(double)), new DataColumn("AchievedReve", typeof(double)) });
            if (drpYear.SelectedValue.ToString() == "2017-2018")
            {
                dt.Rows.Add(1, 4, "April-17");
                dt.Rows.Add(2, 5, "May-17");
                dt.Rows.Add(3, 6, "June-17");
                dt.Rows.Add(4, 7, "July-17");
                dt.Rows.Add(5, 8, "August-17");
                dt.Rows.Add(6, 9, "September-17");
                dt.Rows.Add(7, 10, "October-17");
                dt.Rows.Add(8, 11, "November-17");
                dt.Rows.Add(9, 12, "December-17");
                dt.Rows.Add(10, 1, "January-18");
                dt.Rows.Add(11, 2, "February-18");
                dt.Rows.Add(12, 3, "March-18");
            }
            else if (drpYear.SelectedValue.ToString() == "2016-2017")
            {
                dt.Rows.Add(1, 4, "April-16");
                dt.Rows.Add(2, 5, "May-16");
                dt.Rows.Add(3, 6, "June-16");
                dt.Rows.Add(4, 7, "July-16");
                dt.Rows.Add(5, 8, "August-16");
                dt.Rows.Add(6, 9, "September-16");
                dt.Rows.Add(7, 10, "October-16");
                dt.Rows.Add(8, 11, "November-16");
                dt.Rows.Add(9, 12, "December-16");
                dt.Rows.Add(10, 1, "January-17");
                dt.Rows.Add(11, 2, "February-17");
                dt.Rows.Add(12, 3, "March-17");
            }
            GvFinancialyearWiseAmount.DataSource = dt;
            GvFinancialyearWiseAmount.DataBind();
            lblFyAmt.Visible = true;
            btnSubmit.Visible = true;
        }
    }

    public DataTable ConvertColumnsAsRows(DataTable dt)
    {
        DataTable dtnew = new DataTable();
        //Convert all the rows to columns
        for (int i = 0; i <= dt.Rows.Count; i++)
        {
            dtnew.Columns.Add(Convert.ToString(i));
        }
        DataRow dr;
        // Convert All the Columns to Rows
        for (int j = 0; j < dt.Columns.Count; j++)
        {
            dr = dtnew.NewRow();
            dr[0] = dt.Columns[j].ToString();
            for (int k = 1; k <= dt.Rows.Count; k++)
                dr[k] = dt.Rows[k - 1][j];
            dtnew.Rows.Add(dr);
        }
        return dtnew;
    }
    protected void GvFinancialyearWiseAmount_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        //if (e.Row.RowType == DataControlRowType.Header)
        //{
        //    e.Row.Cells[4].Text = "Achieved Transaction <br>" + DateTime.Today.AddDays(-1).Day.ToString() + "-" + DateTime.Today.AddDays(-1).ToString("MMM") + "-" + DateTime.Today.AddDays(-1).Year;
        //    e.Row.Cells[7].Text = "Achieved Rev <br>" + DateTime.Today.AddDays(-1).Day.ToString() + "-" + DateTime.Today.AddDays(-1).ToString("MMM") + "-" + DateTime.Today.AddDays(-1).Year;
        //}
    }
    protected void gvconverted_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        //if (e.Row.RowType == DataControlRowType.DataRow)
        //{
        //    e.Row.Cells[1].CssClass = "gridcss";
        //}
    }
    protected void ddlHQ_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataSet ds = new DataSet();
        objCorModule = new CorporateModule();
        ddlClientName.DataSource = null;
        ddlClientName.DataBind();

        ds = objCorModule.GetProspects(Convert.ToInt32(ddlHQ.SelectedValue));
        if (ds.Tables[0].Rows.Count > 0)
        {
            ddlClientName.DataSource = ds.Tables[0];
            ddlClientName.DataTextField = "ClientCoName";
            ddlClientName.DataValueField = "ProspectId";
            ddlClientName.DataBind();
            ddlClientName.Items.Insert(0, new ListItem("--Select--", "0"));
        }

        else
        {
            ddlClientName.DataSource = null;
            ddlClientName.DataBind();
        }
    }
}