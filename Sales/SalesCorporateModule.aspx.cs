using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.IO;
using ExcelUtil;
using System.Globalization;

public partial class SalesCorporateModule : System.Web.UI.Page
{
    CorporateModule objCorModule = null;
    SaleModule objSale = null;
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!Page.IsPostBack)
        {
            BindClient();
            BindCityName(ddlHQ);
            BindCityName(ddlSigned);
            BindAccountManager();
            BindImplant();
            BindCompetitor();
            GroupEntityClient();
            BindFinacialYear();
            //BindYear(ddlYear);
            //BindMonth(ddlMonth);
            //drpYear.Items.Insert(0, new ListItem("--Select--", "0"));
            //if (Session["UserID"].ToString() == "1891" || Session["UserID"].ToString()=="1463")
            //{
            //    chkPriority.Visible = true;
            //}
            //else
            //{ chkPriority.Visible = false; }


        }


    }

    private void BindCompetitor()
    {
        DataSet ds = new DataSet();
        objCorModule = new CorporateModule();
        ds = objCorModule.GetCompetitor();
        if (ds.Tables[0].Rows.Count > 0)
        {
            ddlCompetitor.DataSource = ds.Tables[0];
            ddlCompetitor.DataTextField = "Competitor";
            ddlCompetitor.DataValueField = "competitorID";
            ddlCompetitor.DataBind();
            ddlCompetitor.Items.Insert(0, new ListItem("--Add Competitor--", "0"));
            //ddlCompetitor.Items.Add("--Add Competitor--");

        }

        else
        {
            ddlCompetitor.DataSource = null;
            ddlCompetitor.DataBind();
        }
    }

    private void BindImplant()
    {
        DataSet ds = new DataSet();
        objCorModule = new CorporateModule();
        ds = objCorModule.GetImplant();
        if (ds.Tables[0].Rows.Count > 0)
        {
            ddlImplant.DataSource = ds.Tables[0];
            ddlImplant.DataTextField = "ImplantName";
            ddlImplant.DataValueField = "ImplantId";
            ddlImplant.DataBind();
            ddlImplant.Items.Insert(0, new ListItem("--Add Implant--", "0"));
            //ddlCompetitor.Items.Add("--Add Implant--");
        }

        else
        {
            ddlImplant.DataSource = null;
            ddlImplant.DataBind();
        }
    }

    private void BindClient()
    {
        DataSet ds = new DataSet();
        objCorModule = new CorporateModule();
        ds = objCorModule.GetClientName();
        if (ds.Tables[0].Rows.Count > 0)
        {
            ddlClientName.DataSource = ds.Tables[0];
            ddlClientName.DataTextField = "clientConame";
            ddlClientName.DataValueField = "clientCoId";
            ddlClientName.DataBind();
            ddlClientName.Items.Insert(0, new ListItem("--Select--", "0"));
        }

        else
        {
            ddlClientName.DataSource = null;
            ddlClientName.DataBind();
        }


    }

    private void GroupEntityClient()
    {
        DataSet ds = new DataSet();
        objCorModule = new CorporateModule();
        ds = objCorModule.GetClientName();
        if (ds.Tables[0].Rows.Count > 0)
        {
            ddlGroupEntity.DataSource = ds.Tables[0];
            ddlGroupEntity.DataTextField = "clientConame";
            ddlGroupEntity.DataValueField = "clientCoId";
            ddlGroupEntity.DataBind();
            ddlGroupEntity.Items.Insert(0, new ListItem("--Select--", "0"));
        }

        else
        {
            ddlGroupEntity.DataSource = null;
            ddlGroupEntity.DataBind();
        }


    }

    private void BindAccountManager()
    {
        DataSet ds = new DataSet();
        objCorModule = new CorporateModule();
        ds = objCorModule.GetAccountManager();
        if (ds.Tables[0].Rows.Count > 0)
        {
            ddlLocalAccountManager.DataSource = ds.Tables[0];
            ddlLocalAccountManager.DataTextField = "LoginId";
            ddlLocalAccountManager.DataValueField = "Sysuserid";
            ddlLocalAccountManager.DataBind();
            ddlLocalAccountManager.Items.Insert(0, new ListItem("--Select--", "0"));
        }

        else
        {
            ddlLocalAccountManager.DataSource = null;
            ddlLocalAccountManager.DataBind();
        }


    }
    private void BindCityName(DropDownList ddlist)
    {
        DataSet dsCity = new DataSet();
        objCorModule = new CorporateModule();
        dsCity = objCorModule.GetCityName();
        if (dsCity.Tables[0].Rows.Count > 0)
        {
            ddlist.DataSource = dsCity.Tables[0];
            ddlist.DataTextField = "cityname";
            ddlist.DataValueField = "CityID";
            ddlist.DataBind();
            ddlist.Items.Insert(0, new ListItem("--Select--", "0"));
        }
        else
        {
            ddlist.DataSource = null;
            ddlist.DataBind();
        }

    }

    public void InitializeControls(Control objcontrol)
    {
        foreach (Control control in objcontrol.Controls)
        {
            if ((control.GetType() == typeof(TextBox)))
            {
                ((TextBox)(control)).Text = "";
            }

            //if ((control.GetType() == typeof(Label)))
            //{
            //    ((Label)(control)).Text = "";
            //}

            if ((control.GetType() == typeof(DropDownList)))
            {
                if (((DropDownList)(control)).Items.Count > 0)
                {
                    ((DropDownList)(control)).ClearSelection();
                    ((DropDownList)(control)).SelectedIndex = 0;
                }
            }
            if ((control.GetType() == typeof(ListBox)))
            {
                if (((ListBox)(control)).Items.Count > 0)
                {
                    ((ListBox)(control)).ClearSelection();
                    ((ListBox)(control)).SelectedIndex = 0;
                }
            }

            if ((control.GetType() == typeof(CheckBox)))
            {
                ((CheckBox)(control)).Checked = false;
            }

            if (control.HasControls() && control.GetType() != typeof(MultiView))
            {
                InitializeControls(control);
            }


            if ((control.GetType() == typeof(GridView)))
            {
                ((GridView)(control)).DataSource = null;
                ((GridView)(control)).DataBind();
            }
            lblFyAmt.Visible = false;
        }
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        int result = 0;
        Int64 mobileNo;
        objSale = new SaleModule();
        objCorModule = new CorporateModule();
        string multipleImplantId = string.Empty;
        string multipleCompetitor = string.Empty;
        try
        {
            //if (Convert.ToString(ddlLocalInf.SelectedValue) == "Yes" && Int64.TryParse(txtLocalInfluencerMobile.Text, out mobileNo) == false)
            //{
            //    lblMsg.Text = "Mobile number should be numeric";
            //    return;
            //}
            //else if (Convert.ToString(ddlLocalInf.SelectedValue) == "Yes" && Int64.TryParse(txtLocalInfluencerMobile.Text, out mobileNo) == true && mobileNo.ToString().Length < 10)
            //{
            //    lblMsg.Text = "Mobile number should not be less than 10 digits";
            //    return;
            //}
            if (Convert.ToInt32(ddlClientName.SelectedValue) == 0 || Convert.ToInt32(ddlHQ.SelectedValue) == 0)
            {
                lblMsg.Text = "City name and company name should not empty.";
                return;
            }
            else if (Convert.ToString(ddlAccountType.SelectedValue) == "0" || Convert.ToInt32(ddlSigned.SelectedValue) == 0)
            {
                lblMsg.Text = "Account type and Signedby should not empty.";
                return;
            }
            else if (Convert.ToInt32(ddlLocalAccountManager.SelectedValue) == 0 || Convert.ToInt32(ddlGroupEntity.SelectedValue) == 0)
            {
                lblMsg.Text = "Local account mgr and Group entity should not empty.";
                return;
            }
            if (!string.IsNullOrEmpty(txtImplant.Text.Trim()))
            {
                DataSet ds = new DataSet();
                ds = objCorModule.CheckLoginValidate(txtImplant.Text.Trim());
                if (ds.Tables[0].Rows.Count > 0)
                {
                    objSale.ImplantSysUserId = Convert.ToInt32(ds.Tables[0].Rows[0]["sysUseriD"]);
                    objSale.NewImplantName = Convert.ToString(ds.Tables[0].Rows[0]["UserName"]);
                }
                else
                {
                    objSale.ImplantSysUserId = 0;
                    objSale.NewImplantName = string.Empty;
                    lblMsg.Text = "Enter correct loginid ";
                    return;
                }
            }
            else
            {
                objSale.ImplantSysUserId = 0;
                objSale.NewImplantName = string.Empty;
            }
            if (ddlImplant.SelectedItem != null && ddlImplant.SelectedItem.Text == "0" && string.IsNullOrEmpty(txtImplant.Text))
            {
                lblMsg.Text = "Enter implant name";
                return;
            }
            else if (ddlCompetitor.SelectedItem != null && ddlCompetitor.SelectedItem.Text == "0" && string.IsNullOrEmpty(txtCompetitor.Text))
            {
                lblMsg.Text = "Enter competitor name";
                return;
            }
            else if (drpYear.SelectedIndex == 0)
            {
                lblMsg.Text = "Select financial Year";
                return;
            }
            else
            {

                objSale.ClientCoId = Convert.ToInt32(ddlClientName.SelectedValue);
                objSale.AccountType = ddlAccountType.SelectedValue;
                objSale.HQ = Convert.ToInt32(ddlHQ.SelectedValue);
                objSale.SignedBy = Convert.ToInt32(ddlSigned.SelectedValue);
                objSale.LocalInfluencer = ddlLocalInf.SelectedValue;
                objSale.MonthlyPotential = txtMonthlyPotential.Text.Trim();
                objSale.LocalAccountMgr = ddlLocalAccountManager.SelectedValue;
                objSale.SysUserID = Convert.ToInt32(Session["UserID"].ToString());
                objSale.LocalInfluencerName = txtLocalInfluencerName.Text;
                objSale.LocalInfluencerMobileNo = txtLocalInfluencerMobile.Text;
                //objSale.Target_Year = Convert.ToInt32(ddlYear.SelectedValue);
                //objSale.Target_Amount_PanIndia = Convert.ToDouble(txtTargetAmountPanindia.Text);
                //string Target_Year= drpYear.SelectedValue.ToString();
                //string[]ar=Target_Year.Split('-');
                // objSale.Target_Year =Convert.ToInt32(ar[1]);
                objSale.Target_Amount_PanIndia = 0;
                objSale.Target_FinancialYear = drpYear.SelectedValue;
                if (chkPriority.Visible == true && chkPriority.Checked == true)
                {
                    objSale.IsPriority = true;
                }
                else
                {
                    objSale.IsPriority = false;
                }

                foreach (GridViewRow row in GvFinancialyearWiseAmount.Rows)
                {
                    //int Month = Convert.ToInt32(row.Cells[0].Text);

                    Label lblMonthID = (Label)row.FindControl("lblMonthID");
                    Label lblMonth = (Label)row.FindControl("lblMonth");
                    objSale.Target_Year = Convert.ToInt32(lblMonth.Text.ToString().Split('-')[1]);
                    //objSale.Target_Year = Convert.ToInt32(Convert.ToString("20") + Convert.ToString(lblMonth.Text.ToString().Split('-')[1]));
                    //if (lblMonth.Text.ToString().Contains("16"))
                    //{
                    //    objSale.Target_Year = 2016;
                    //}
                    //else if (lblMonth.Text.ToString().Contains("17"))
                    //{
                    //    objSale.Target_Year = 2017;
                    //}
                    //else if (lblMonth.Text.ToString().Contains("18"))
                    //{
                    //    objSale.Target_Year = 2018;
                    //}
                    Label lblID = (Label)row.FindControl("lblID");
                    TextBox txtAmt = (TextBox)row.FindControl("txtTransactions");
                    TextBox txtPipeLineRev = (TextBox)row.FindControl("txtPipelineRevenue");
                    HiddenField hdnMonthID = (HiddenField)row.FindControl("hdnMonthID");

                    double Amount = 0, pipeLineRev = 0;
                    if (txtAmt.Text == "")
                    {
                        Amount = 0;
                    }
                    else
                    {
                        Amount = Convert.ToDouble(txtAmt.Text);
                    }
                    if (!string.IsNullOrEmpty(txtPipeLineRev.Text))
                    {
                        pipeLineRev = Convert.ToDouble(txtPipeLineRev.Text);
                    }

                    //double Amount = Convert.ToDouble(txtAmt.Text);
                    objSale.Target_FinancialMonthID = Convert.ToInt32(lblID.Text);
                    objSale.Target_Month = Convert.ToInt32(lblMonthID.Text);
                    //objSale.Target_Month = Convert.ToInt32(hdnMonthID.Value);
                    objSale.Target_FinancialMonth = lblMonth.Text;
                    objSale.Target_Amount = Amount;
                    objSale.PipeLineRevenue = pipeLineRev;

                    //objSale.Target_Month = Convert.ToInt32(ddlMonth.SelectedValue);
                    //objSale.Target_Amount = Convert.ToDouble(txtTargetAmount.Text);

                    if (string.IsNullOrEmpty(ddlGroupEntity.SelectedItem.Text) || ddlGroupEntity.SelectedItem.Text == "--Select--" || ddlGroupEntity.SelectedValue == "0")
                    {
                        objSale.GroupEnityName = "";
                    }
                    else
                    {
                        objSale.GroupEnityName = ddlGroupEntity.SelectedItem.Text;
                    }

                    if (ddlImplant.SelectedItem != null && objSale.ImplantSysUserId == 0 && Convert.ToString(ddlImplant.SelectedItem.Text) != "0")
                    {

                        foreach (ListItem item in ddlImplant.Items)
                        {
                            if (item.Selected)
                            {
                                if (string.IsNullOrEmpty(multipleImplantId))
                                {
                                    multipleImplantId = item.Value;
                                }
                                else
                                {
                                    multipleImplantId = multipleImplantId + "," + item.Value;
                                }


                            }
                        }

                        //objSale.ImplantId = Convert.ToInt32(ddlImplant.SelectedValue);
                        objSale.ImplantId = multipleImplantId;
                    }
                    else
                    {
                        objSale.ImplantId = "0";
                    }

                    if (!string.IsNullOrEmpty(txtCompetitor.Text))
                    {
                        objSale.Competitor = txtCompetitor.Text.Trim();
                    }
                    else
                    {
                        foreach (ListItem item in ddlCompetitor.Items)
                        {
                            if (item.Selected)
                            {
                                if (string.IsNullOrEmpty(multipleCompetitor))
                                {
                                    multipleCompetitor = item.Text;
                                }
                                else
                                {
                                    multipleCompetitor = multipleCompetitor + "," + item.Text;
                                }
                            }
                        }
                        objSale.Competitor = multipleCompetitor;

                        if (string.IsNullOrEmpty(objSale.Competitor.ToString()))
                        {
                            objSale.Competitor = "0";
                        }
                        //objSale.Competitor = ddlCompetitor.SelectedItem.Text.Trim();
                    }

                    result = objCorModule.SaveCorintSalesCorporate(objSale);

                }

                if (result > 0)
                {
                    lblMsg.Text = "Record saved successfuly.";
                    InitializeControls(Form);
                }
                else
                {
                    lblMsg.Text = "Getting some issues to save data.";
                }
            }

        }
        catch (Exception Ex)
        {
            lblMsg.Text = Ex.Message;
        }



    }

    //public void ClearData()
    //{
    //  ddlClientName.SelectedValue="";
    //  ddlAccountType.SelectedValue="";
    //  ddlHQ.SelectedValue = "";
    //   ddlSigned.SelectedValue=""
    //    objSale.LocalInfluencer = ddlLocalInf.SelectedValue;
    //    objSale.MonthlyPotential = txtMonthlyPotential.Text.Trim();
    //    objSale.LocalAccountMgr = ddlLocalAccountManager.SelectedValue;
    //    objSale.SysUserID = Convert.ToInt32(Session["UserID"].ToString());
    //    objSale.LocalInfluencerName = txtLocalInfluencerName.Text;
    //    objSale.LocalInfluencerMobileNo = txtLocalInfluencerMobile.Text;
    //}

    protected void ddlLocalInf_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlLocalInf.SelectedItem.Text.ToString() == "Yes")
        {
            trLocalInfluencerName.Visible = true;
        }
        else
        {
            trLocalInfluencerName.Visible = false;
        }
    }
    protected void ddlCompetitor_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlCompetitor.Text == "0")
        {
            txtCompetitor.Visible = true;
        }
        else
        {
            txtCompetitor.Text = "";
            txtCompetitor.Visible = false;
        }
    }
    protected void ddlImplant_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlImplant.Text == "0")
        {
            txtImplant.Visible = true;
        }
        else
        {
            txtImplant.Text = "";
            txtImplant.Visible = false;

        }
    }
    protected void ddlClientName_SelectedIndexChanged(object sender, EventArgs e)
    {

        DataSet ds = new DataSet();
        DataSet dsAccountType = new DataSet();
        DataSet dsHQ = new DataSet();
        DataSet dsSingedBy = new DataSet();
        GvFinancialyearWiseAmount.DataSource = null;
        GvFinancialyearWiseAmount.DataBind();
        objCorModule = new CorporateModule();
        // drpYear.Items.Insert(0, new ListItem("--Select--", "0"));
        lblMsg.Text = "";
        drpYear.SelectedIndex = 0;
        gvShowDetails.DataSource = null;
        gvShowDetails.DataBind();
        if (ddlClientName.SelectedIndex > 0)
        {
            //Account Type 
            dsAccountType = objCorModule.GetAccountType(Convert.ToInt32(ddlClientName.SelectedValue), Convert.ToInt32(ddlHQ.SelectedValue));
            if (dsAccountType.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dr in dsAccountType.Tables[0].Rows)
                {

                    ddlAccountType.SelectedValue = Convert.ToString(dr["AccountType"]);
                }
            }
            else
            {
                ddlAccountType.SelectedValue = null;
            }


            //Singed By
            dsSingedBy = objCorModule.GetSingedBy(Convert.ToInt32(ddlClientName.SelectedValue), Convert.ToInt32(ddlHQ.SelectedValue));
            if (dsSingedBy.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dr in dsSingedBy.Tables[0].Rows)
                {

                    ddlSigned.SelectedValue = Convert.ToString(dr["CityId"]);

                }
            }
            else
            {
                ddlSigned.SelectedValue = null;
            }
            //End Singed By

            //Local Influencer
            dsSingedBy = objCorModule.GetLocalInfluencer(Convert.ToInt32(ddlClientName.SelectedValue), Convert.ToInt32(ddlHQ.SelectedValue));
            if (dsSingedBy.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dr in dsSingedBy.Tables[0].Rows)
                {
                    ddlLocalInf.SelectedValue = Convert.ToString(dr["LocalInfluencer"]);
                    txtMonthlyPotential.Text = Convert.ToString(dr["MonthlyPotential"]);
                }
            }
            else
            {
                ddlLocalInf.SelectedValue = null;
                txtMonthlyPotential.Text = "";
            }

            //Local Influencer

            //Implant 

            ds = objCorModule.GetImplantsClientBasis(Convert.ToInt32(ddlClientName.SelectedValue), Convert.ToInt32(ddlHQ.SelectedValue));
            if (ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    //ddlLocalInf.SelectedValue = Convert.ToString(dr["LocalInfluencer"]);
                }
            }
            else
            {
                //ddlLocalInf.SelectedValue = null;
            }

            //Competitore
            ds = objCorModule.GetLocalAccountMgr(Convert.ToInt32(ddlClientName.SelectedValue), Convert.ToInt32(ddlHQ.SelectedValue));
            if (ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    ddlLocalAccountManager.SelectedValue = Convert.ToString(dr["LocalAccountMgr"]);

                }
            }
            else
            {
                ddlLocalAccountManager.SelectedValue = null;
            }

            ds = objCorModule.GetCompetitorListClientBasis(Convert.ToInt32(ddlClientName.SelectedValue));
            if (ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dr in ds.Tables[0].Rows)
                {

                }
            }
            else
            {

            }

            ds = objCorModule.GetGroupEntityOfClienet(Convert.ToInt32(ddlClientName.SelectedValue));
            if (ds.Tables[0].Rows.Count > 0 && !string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["GroupEntity"])))
            {
                //ddlGroupEntity.SelectedItem.Text = Convert.ToString(ds.Tables[0].Rows[0]["GroupEntity"]);
                //ddlGroupEntity.Items.FindByText(Convert.ToString(ds.Tables[0].Rows[0]["GroupEntity"])).Selected= true;
                ddlGroupEntity.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["GroupEnityId"]);

            }
            else
            {
                //ddlGroupEntity.SelectedItem.Text = "--Select--";
                ddlGroupEntity.SelectedValue = "0";

            }

            if (Convert.ToBoolean(ds.Tables[0].Rows[0]["isprioritycorporate"]) == true)
            {
                chkPriority.Checked = true;
            }
            else
            {
                chkPriority.Checked = false;
            }
            //if (Session["UserID"].ToString() == "1891" || Session["UserID"].ToString() == "1463")
            //{
            //    chkPriority.Visible = true;
            //}
            //else
            //{ chkPriority.Visible = false; }
        }

    }

    protected void ddlAccountType_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataSet dsHQ = new DataSet();
        objCorModule = new CorporateModule();
        dsHQ = objCorModule.GetSaleHeadQuarter(Convert.ToInt32(ddlClientName.SelectedValue), Convert.ToInt32(ddlAccountType.SelectedValue));
        if (dsHQ.Tables[0].Rows.Count > 0)
        {
            ddlHQ.DataSource = dsHQ.Tables[0];
            ddlHQ.DataTextField = "CityName";
            ddlHQ.DataValueField = "CityID";
            ddlHQ.DataBind();
            ddlHQ.Items.Insert(0, new ListItem("--Select--", "0"));
        }
        else
        {
            ddlHQ.DataSource = null;
            ddlHQ.DataBind();
        }
    }




    private void BindGrid()
    {
        DataSet ds = new DataSet();
        objCorModule = new CorporateModule();
        lblMsg.Visible = false;
        ds = objCorModule.EntryDetails(Convert.ToInt32(ddlClientName.SelectedValue));
        if (ds.Tables[0].Rows.Count > 0)
        {
            gvShowDetails.DataSource = ds.Tables[0];
            gvShowDetails.DataBind();
            WorkbookEngine.ExportDataSetToExcel(ds, "SalesCorporate.xls");
        }
        else
        {
            lblMsg.Visible = true;
            lblMsg.Text = "Record not available.";
            gvShowDetails.DataSource = null;
            gvShowDetails.DataBind();

        }

    }
    protected void bntViewDetails_Click(object sender, EventArgs e)
    {
        DataSet ds = new DataSet();
        objCorModule = new CorporateModule();
        if (ddlClientName.SelectedIndex > 0)
        {
            lblMsg.Visible = false;
            ds = objCorModule.EntryDetails(Convert.ToInt32(ddlClientName.SelectedValue));
            if (ds.Tables[0].Rows.Count > 0)
            {
                gvShowDetails.DataSource = ds.Tables[0];
                gvShowDetails.DataBind();
            }
            else
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Record not available.";
                gvShowDetails.DataSource = null;
                gvShowDetails.DataBind();

            }

        }
        else
        {
            lblMsg.Visible = true;
            lblMsg.Text = "Select client name first.";
        }


    }


    protected void btnExport_Click(object sender, EventArgs e)
    {
        BindGrid();
    }
    private void BindFinacialYear()
    {
        try
        {
            for (int i = 2017; i <= DateTime.Now.Year || i == Convert.ToInt32(DateTime.Now.Year) + 1; i++)
            {
                drpYear.Items.Add(new ListItem((i - 1).ToString() + "-" + i.ToString(), (i - 1).ToString() + "-" + i.ToString()));
            }
            drpYear.Items.Insert(0, new ListItem("--Select--", "0"));
        }
        catch (Exception)
        {

            throw new Exception("The method or operation is not implemented.");
        }
    }

    private void BindYear(DropDownList ddlYear)
    {
        try
        {
            for (int i = 2017; i <= 2017; i++)
            {
                ddlYear.Items.Add(new ListItem(i.ToString(), i.ToString()));
            }
            ddlYear.Items.Insert(0, new ListItem("--Select--", "0"));
        }
        catch (Exception)
        {

            throw new Exception("The method or operation is not implemented.");
        }
    }
    private void BindMonth(DropDownList ddlMonth)
    {
        objCorModule = new CorporateModule();
        try
        {
            DataTable dtFromMonth = new DataTable();
            dtFromMonth = objCorModule.GetMonthName();
            ddlMonth.DataTextField = "monthName";
            ddlMonth.DataValueField = "number";
            ddlMonth.DataSource = dtFromMonth;
            ddlMonth.DataBind();
            ddlMonth.Items.Insert(0, new ListItem("--Select--", "0"));
        }
        catch (Exception)
        {

            throw new Exception("The method or operation is not implemented.");
        }
    }
    //protected void ddlMonth_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    //Get Target Amount
    //    DataSet ds = new DataSet();
    //    objCorModule = new CorporateModule();
    //    ds = objCorModule.GetTargetAmount(Convert.ToInt32(ddlClientName.SelectedValue), Convert.ToInt32(ddlHQ.SelectedValue),Convert.ToInt32(ddlYear.SelectedValue),Convert.ToInt32(ddlMonth.SelectedValue));
    //    if (ds.Tables[0].Rows.Count > 0)
    //    {
    //        ddlYear.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["Target_Year"]);
    //        ddlMonth.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["Target_Month"]);
    //        txtTargetAmount.Text = Convert.ToString(ds.Tables[0].Rows[0]["Target_Amount"]);
    //        txtTargetAmountPanindia.Text = Convert.ToString(ds.Tables[0].Rows[0]["Target_Amount_PanIndia"]);
    //    }
    //    else
    //    {
    //        ddlYear.SelectedValue = null;
    //        ddlMonth.SelectedValue = null;
    //        txtTargetAmount.Text = null;
    //        txtTargetAmountPanindia.Text = null;
    //    }
    //}
    protected void drpYear_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataSet ds = new DataSet();
        objCorModule = new CorporateModule();
        try
        {


            //string Target_Year= drpYear.SelectedValue.ToString();
            //string[]ar=Target_Year.Split('-');
            //if (Session["UserID"].ToString() == "1891" || Session["UserID"].ToString() == "1463")
            //{
            //    chkPriority.Visible = true;
            //}
            //else
            //{ chkPriority.Visible = false; }

            ds = objCorModule.GetMonthWiseAmount(Convert.ToInt32(ddlClientName.SelectedValue), Convert.ToInt32(ddlHQ.SelectedValue), drpYear.SelectedValue.ToString());
            if (ds.Tables[0].Rows.Count > 0)
            {
                DataTable dt = ds.Tables[0];


                GvFinancialyearWiseAmount.DataSource = dt;
                GvFinancialyearWiseAmount.DataBind();
                lblFyAmt.Visible = true;
                btnSubmit.Visible = true;
                //gvconverted.DataSource = ConvertColumnsAsRows(dt);
                //gvconverted.DataBind();
                //gvconverted.HeaderRow.Visible = false;
            }
            else
            {
                DataTable dt = new DataTable();
                int i, j = 4, k = 1;
                DateTime now = new DateTime(2020, 4, 1);

                dt.Columns.AddRange(new DataColumn[9]
                { new DataColumn("ID", typeof(string)),
              new DataColumn("MonthID", typeof(string)),
              new DataColumn("Month", typeof(string)),
              new DataColumn("Amount", typeof(double)), 
              new DataColumn("PipeLineRevenue", typeof(double)),
              new DataColumn("Trans", typeof(double)),
              new DataColumn("AvgticketSize", typeof(double)),
              new DataColumn("TargetRev", typeof(double)),
              new DataColumn("AchievedReve", typeof(double))
                });

                for (i = 1; i <= 12; i++)
                {
                    if (j >= 4 && j <= 12)
                    {
                        dt.Rows.Add(1, j, now.ToString("MMM") + "-" + drpYear.SelectedValue.ToString().Split('-')[0]);
                        now = now.AddMonths(1);
                        j++;
                    }
                    if (j > 12 && k <= 3)
                    {
                        dt.Rows.Add(1, j, now.ToString("MMM") + "-" + drpYear.SelectedValue.ToString().Split('-')[1]);
                        now = now.AddMonths(1);
                        k++;
                        j++;
                    }
                }



                /*
                if (drpYear.SelectedValue.ToString() == "2019-2020")
                {
                    dt.Rows.Add(1, 4, "April-19");
                    dt.Rows.Add(2, 5, "May-19");
                    dt.Rows.Add(3, 6, "June-19");
                    dt.Rows.Add(4, 7, "July-19");
                    dt.Rows.Add(5, 8, "August-19");
                    dt.Rows.Add(6, 9, "September-19");
                    dt.Rows.Add(7, 10, "October-19");
                    dt.Rows.Add(8, 11, "November-19");
                    dt.Rows.Add(9, 12, "December-19");
                    dt.Rows.Add(10, 1, "January-20");
                    dt.Rows.Add(11, 2, "February-20");
                    dt.Rows.Add(12, 3, "March-20");
                }
                else if (drpYear.SelectedValue.ToString() == "2018-2019")
                {
                    dt.Rows.Add(1, 4, "April-18");
                    dt.Rows.Add(2, 5, "May-18");
                    dt.Rows.Add(3, 6, "June-18");
                    dt.Rows.Add(4, 7, "July-18");
                    dt.Rows.Add(5, 8, "August-18");
                    dt.Rows.Add(6, 9, "September-18");
                    dt.Rows.Add(7, 10, "October-18");
                    dt.Rows.Add(8, 11, "November-18");
                    dt.Rows.Add(9, 12, "December-18");
                    dt.Rows.Add(10, 1, "January-19");
                    dt.Rows.Add(11, 2, "February-19");
                    dt.Rows.Add(12, 3, "March-19");
                }
                else if (drpYear.SelectedValue.ToString() == "2017-2018")
                {
                    dt.Rows.Add(1, 4, "April-17");
                    dt.Rows.Add(2, 5, "May-17");
                    dt.Rows.Add(3, 6, "June-17");
                    dt.Rows.Add(4, 7, "July-17");
                    dt.Rows.Add(5, 8, "August-17");
                    dt.Rows.Add(6, 9, "September-17");
                    dt.Rows.Add(7, 10, "October-17");
                    dt.Rows.Add(8, 11, "November-17");
                    dt.Rows.Add(9, 12, "December-17");
                    dt.Rows.Add(10, 1, "January-18");
                    dt.Rows.Add(11, 2, "February-18");
                    dt.Rows.Add(12, 3, "March-18");
                }
                else if (drpYear.SelectedValue.ToString() == "2016-2017")
                {
                    dt.Rows.Add(1, 4, "April-16");
                    dt.Rows.Add(2, 5, "May-16");
                    dt.Rows.Add(3, 6, "June-16");
                    dt.Rows.Add(4, 7, "July-16");
                    dt.Rows.Add(5, 8, "August-16");
                    dt.Rows.Add(6, 9, "September-16");
                    dt.Rows.Add(7, 10, "October-16");
                    dt.Rows.Add(8, 11, "November-16");
                    dt.Rows.Add(9, 12, "December-16");
                    dt.Rows.Add(10, 1, "January-17");
                    dt.Rows.Add(11, 2, "February-17");
                    dt.Rows.Add(12, 3, "March-17");
                }*/

                GvFinancialyearWiseAmount.DataSource = dt;
                GvFinancialyearWiseAmount.DataBind();
                lblFyAmt.Visible = true;
                if (drpYear.SelectedIndex > 0)
                {
                    btnSubmit.Visible = true;
                }
                else
                {
                    btnSubmit.Visible = false;
                }

            }
        }
        catch (Exception ex)
        {

            throw new Exception(ex.Message);
        }
        //gvconverted.DataSource = ConvertColumnsAsRows(dt);
        //gvconverted.DataBind();
        //gvconverted.HeaderRow.Visible = false;

  }

public DataTable ConvertColumnsAsRows(DataTable dt)
{
    DataTable dtnew = new DataTable();
    //Convert all the rows to columns
    for (int i = 0; i <= dt.Rows.Count; i++)
    {
        dtnew.Columns.Add(Convert.ToString(i));
    }
    DataRow dr;
    // Convert All the Columns to Rows
    for (int j = 0; j < dt.Columns.Count; j++)
    {
        dr = dtnew.NewRow();
        dr[0] = dt.Columns[j].ToString();
        for (int k = 1; k <= dt.Rows.Count; k++)
            dr[k] = dt.Rows[k - 1][j];
        dtnew.Rows.Add(dr);
    }
    return dtnew;
}
protected void GvFinancialyearWiseAmount_RowDataBound(object sender, GridViewRowEventArgs e)
{
    //if (e.Row.RowType == DataControlRowType.DataRow)
    //{
    // e.Row.Cells[1].CssClass = "gridcss";
    //}
    if (e.Row.RowType == DataControlRowType.Header)
    {
        e.Row.Cells[5].Text = "Achieved Transaction <br>" + DateTime.Today.AddDays(-1).Day.ToString() + "-" + DateTime.Today.AddDays(-1).ToString("MMM") + "-" + DateTime.Today.AddDays(-1).Year;
        e.Row.Cells[8].Text = "Achieved Rev <br>" + DateTime.Today.AddDays(-1).Day.ToString() + "-" + DateTime.Today.AddDays(-1).ToString("MMM") + "-" + DateTime.Today.AddDays(-1).Year;
    }
}
protected void gvconverted_RowDataBound(object sender, GridViewRowEventArgs e)
{
    //if (e.Row.RowType == DataControlRowType.DataRow)
    //{
    //    e.Row.Cells[1].CssClass = "gridcss";
    //}
}
}
