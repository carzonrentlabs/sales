﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Salespage.master" AutoEventWireup="true" CodeFile="UpdateRevenueBudget.aspx.cs" Inherits="Sales_UpdateRevenueBudget" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder_Page" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>

            <script src="../JQuery/jquery-1.4.1.min.js" type="text/javascript"></script>

            <script src="../JQuery/moment.js" type="text/javascript"></script>

            <script src="../JQuery/ui.core.js" type="text/javascript"></script>

            <script src="../JQuery/ui.datepicker.js" type="text/javascript"></script>

            <script src="../ScriptsReveal/jquery.reveal.js" type="text/javascript"></script>

            <script language="javascript" type="text/javascript">
                function CheckDate(sender, args) {
                    //                    alert(sender._selectedDate.getDate());
                    if (sender._selectedDate.getDate() != 1) {
                        alert("Only first date of the month are allowed.");
                        // Cancel selected the date
                        sender._textbox.set_Value("")
                    }
                }
            </script>

            <table cellpadding="0" cellspacing="0" style="width: 60%" align="center">
                <tr>
                    <td align="center">
                        <table cellpadding="0" cellspacing="0" border="1" style="width: 75%">
                            <tr>
                                <td colspan="4" align="center" style="background-color: #ccffff; height: 30px;">
                                    <b>Update Revenue Budget</b>&nbsp;
                                    <asp:Label ID="lblloginBy" Text="" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" align="center">
                                    &nbsp;</td>
                            </tr>
                            <tr>
                                <td align="center" colspan="4">
                                    <asp:Label ID="lblMessage" runat="server" 
                                        style="font-weight: 700; font-size: small" Visible="false"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" colspan="4">
                                    &nbsp;</td>
                            </tr>
                            <tr>
                                <td style="height: 40px; white-space: nowrap; padding: 0 2px 0 4px;" align="left">
                                     <b>Branch</b>
                                </td>
                                <td style="height: 40px; white-space: nowrap; padding: 0 2px 0 4px; text-align: left;">
                                    <asp:DropDownList ID="ddlUnit" runat="server" AutoPostBack="true" 
                                        ValidationGroup="FS" onselectedindexchanged="ddlUnit_SelectedIndexChanged" 
                                        >
                                       <%-- <asp:ListItem Text="Select" Value="0"></asp:ListItem>--%>
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="rvddlUnit" runat="server" 
                                        ControlToValidate="ddlUnit" Display="None" 
                                        ErrorMessage="Please Select Branch !" InitialValue="0" 
                                        SetFocusOnError="True" ValidationGroup="FS"></asp:RequiredFieldValidator>
                                </td>
                                <td style="height: 40px; white-space: nowrap; padding: 0 2px 0 4px;" align="left">
                                   <b>Budget Date</b>
                                </td>
                                <td style="height: 40px; white-space: nowrap; padding: 0 2px 0 4px; text-align: left;">
                                    <asp:DropDownList ID="ddlBudgetDetails" runat="server" AutoPostBack="true" 
                                        ValidationGroup="FS" 
                                        onselectedindexchanged="ddlBudgetDetails_SelectedIndexChanged">
                                        <%-- <asp:ListItem Text="Select" Value="0"></asp:ListItem>--%>
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="rvBudgetDate" runat="server" 
                                        ControlToValidate="ddlBudgetDetails" Display="None" 
                                        ErrorMessage="Please Select Budget Date !" SetFocusOnError="True" 
                                        ValidationGroup="FS"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 40px; white-space: nowrap; padding: 0 2px 0 4px;" align="left">
                                    <b>Amount</b>
                                </td>
                                <td style="height: 40px; white-space: nowrap; padding: 0 2px 0 4px;">
                                    <asp:TextBox ID="txtAmount" runat="server" ValidationGroup="FS"></asp:TextBox>
                                    &nbsp;<asp:RequiredFieldValidator ID="rvtxtAmount" SetFocusOnError="True" ValidationGroup="FS"
                                        runat="server" ErrorMessage="Please Enter Amount !" ControlToValidate="txtAmount"
                                        Display="None"></asp:RequiredFieldValidator>
                                         <cc1:filteredtextboxextender id="FilteredTextBoxExtender2" runat="server" targetcontrolid="txtAmount"
                               validchars="1234567890."></cc1:filteredtextboxextender>
                                </td>
                                <td style="height: 40px; white-space: nowrap; padding: 0 2px 0 4px;" align="left">
                                    <b>Remarks</b>
                                </td>
                                <td style="height: 40px; white-space: nowrap; padding: 0 2px 0 4px;">
                                    <asp:TextBox ID="txtRemarks" runat="server" Columns="25" Rows="5" 
                                        TextMode="MultiLine"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rvtxtRemarks" SetFocusOnError="True" ValidationGroup="FS"
                                        runat="server" ErrorMessage="Please Enter Remarks !" ControlToValidate="txtRemarks"
                                        Display="None"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" align="center" style="height: 40px; white-space: nowrap; padding: 0 2px 0 4px;">
                                    <asp:ValidationSummary ID="vs" runat="server" ShowMessageBox="True" ShowSummary="False"
                                        ValidationGroup="FS" />
                                    &nbsp;
                                    <asp:Button ID="btnUpdate" runat="server"
                                        Text="Update" onclick="btnUpdate_Click" />
                                    &nbsp;<asp:Button ID="BtnClear" runat="server"  
                                        Text="Reset" onclick="BtnClear_Click"  />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
    </asp:Content>
