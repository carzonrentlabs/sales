﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using Insta;

public partial class Sales_RevenueBudget : System.Web.UI.Page
{
    public CorDrive objCordrive = null;
    CRM_Insta objcrm = new CRM_Insta();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //Session["SysUserId"] = 3274;
            BindUnit();
        }
    }


    private void BindUnit()
    {
        objCordrive = new CorDrive();
        DataSet dsUnitname = new DataSet();
        dsUnitname = objcrm.GetUnitName();
        ddlUnit.DataTextField = "UnitName";
        ddlUnit.DataValueField = "UnitID";
        ddlUnit.DataSource = dsUnitname;
        ddlUnit.DataBind();
        //ddlUnit.Items.Insert(0, "All");
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            if (ddlUnit.SelectedIndex == 0)
            {
                lblMessage.Text = "Please Select Branch !";
                lblMessage.Visible = true;
                lblMessage.ForeColor = System.Drawing.Color.Red;
                return;
            }

            int rslt = objcrm.SubmitRevenueBudgetUnitWise(Convert.ToInt32(ddlUnit.SelectedValue),
                Convert.ToDateTime(txtBudgetDate.Text),
                Convert.ToDouble(txtAmount.Text)
                , txtRemarks.Text.Trim(),
                Convert.ToInt32(Session["UserID"]));
            if (rslt > 0)
            {
                lblMessage.Text = "Submitted Successfully";
                lblMessage.Visible = true;
                lblMessage.ForeColor = System.Drawing.Color.Red;
                ClearControl();
            }
            else if (rslt == -1)
            {
                lblMessage.Text = "Record already Present for the Unit and Budget Date !";
                lblMessage.Visible = true;
                lblMessage.ForeColor = System.Drawing.Color.Red;
            }
            else
            {
                lblMessage.Text = "Not Submitted Successfully !";
                lblMessage.Visible = true;
                lblMessage.ForeColor = System.Drawing.Color.Red;
            }
        }
        catch (Exception ex)
        {
            lblMessage.Text = ex.Message;
            lblMessage.Visible = true;
            lblMessage.ForeColor = System.Drawing.Color.Red;
        }
    }


    public void ClearControl()
    {
        txtBudgetDate.Text = string.Empty;
        ddlUnit.SelectedIndex = 0;
        txtRemarks.Text = string.Empty;
        txtAmount.Text = string.Empty;  
        
    }
   
    protected void btnClear_Click(object sender, EventArgs e)
    {
        ClearControl();
        lblMessage.Text = "";
    }
}