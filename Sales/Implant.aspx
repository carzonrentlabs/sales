﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Salespage.master" Theme ="TMSTheme" AutoEventWireup="true" CodeFile="Implant.aspx.cs" Inherits="CRM_Implant" validateRequest="false" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder_Page" Runat="Server">
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script> 
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.1/jquery-ui.min.js"></script> 
     
    <script src="../Scripts/gridviewScroll.min.js" type="text/javascript"></script>

    <script type="text/jscript" src="../CalendarControl.js" language="javascript"></script>
   
    <script type="text/javascript">
        $(document).ready(function () {
            gridviewScroll();
             $("#<%=txtFromDate.ClientID%>").datepicker();
            $("#<%=txtToDate.ClientID%>").datepicker();
        });

        function gridviewScroll() {
            $('#<%=gvImplantDetails.ClientID%>').gridviewScroll({
                width: 1150,
                height: 430
            });
        } 
        
        
</script>
<center>
<table width="70%" border="1" cellpadding="1" cellspacing="0">
        <tr><td colspan="6" align="center" bgcolor="#FF6600"><strong>Implant Performance</strong></td></tr>
                        <tr><td colspan="6" align="center"><asp:Label ID="lblMsg" runat="server" 
                                ForeColor="#CC0000" /></td></tr>
                        <tr>
                            <td align="right">

                                <strong>Company Name :</strong></td>
                            <td align="left">
                                <asp:DropDownList ID="ddlCmpName" runat ="server" AutoPostBack="True" 
                                    onselectedindexchanged="ddlCmpName_SelectedIndexChanged"></asp:DropDownList>

                            </td>
                            <td align="right"><strong>Implant Name :</strong></td>
                            <td align="left">
                                <asp:DropDownList ID="ddlImplantName" runat ="server" 
                                    onselectedindexchanged="ddlImplantName_SelectedIndexChanged" 
                                    AutoPostBack="True"> 
                                   
                                    </asp:DropDownList>
                            </td>
                             <td align="right" style="width: 101px"><strong> City Name :</strong></td>
                            <td align="left">
                                <asp:DropDownList ID="ddlCityName" runat ="server"> 
                                   
                                    </asp:DropDownList>
                            </td>
                        </tr>
                        
                        <tr>
                            <td align="right">

                                <strong>Date :</strong></td>
                            <td align="left">
                                <%-- <asp:TextBox ID="txtDate" runat="server" onfocus="showCalendarControl(this);" 
                                    AutoPostBack="True" ontextchanged="txtDate_TextChanged"/>--%>
                                     <asp:TextBox ID="txtDate" runat="server"
                                    AutoPostBack="True" ontextchanged="txtDate_TextChanged"/>
                                 <cc1:calendarextender ID="CE_txtComplainDate" runat="server" 
                                    Format="MM-dd-yyyy" TargetControlID="txtDate">
                        </cc1:calendarextender>
                            </td>
                           
                            <td align="right">
                                <strong>Booking taken By Implant :</strong></td>
                            <td align="left">
                                <asp:TextBox ID="txtBookingTakenByComplant" runat="server">0</asp:TextBox>
                            </td>
                             <td align="right" style="width: 101px"><strong>Competitor Name:</strong></td>
                            <td align="left">
                                <asp:DropDownList ID="ddlCompetitorName" runat ="server" 
                                    onselectedindexchanged="ddlCompetitorName_SelectedIndexChanged" 
                                    AutoPostBack="True"> 
                                 </asp:DropDownList>
                                <asp:Button ID="btnCompetitor" runat="server" BackColor="#0099FF" Visible="false"
                                    BorderColor="#000099" BorderStyle="Solid" Text="+" onclick="btnCompetitor_Click" 
                                   />
                                    <br />
                                <table id="tblAddComp" runat="server" visible="false"><tr><td><asp:TextBox ID="txtCompetitor" runat="server" /></td> <td>
                                    <asp:Button ID="btnAddCompetitor" runat="server" Text="Add" 
                                        onclick="btnAddCompetitor_Click1"  /> 
                                       </td></tr></table>
                            </td>
                        </tr>
                        
                        <tr>
                            <td align="right">
                                <strong>Competitor Bookings:</strong></td>
                            <td align="left">
                            <table>
                                <asp:Panel ID="pnlComp1" runat="server" Visible="false">
                                <tr><td><asp:Label ID="lblComBooking1" runat="server" /></td><td><asp:TextBox ID="txtCompetitorBooking1" runat="server"/></td><td> 
                                    <asp:Button ID="btnNext2" runat="server" Text="+"  BackColor="#0099FF" 
                         BorderColor="#000099" BorderStyle="Solid" onclick="btnNext2_Click"/></td>
                        <%--  <td><asp:Button ID="btnRemove1" runat="server" Text="-" BackColor="#0099FF" 
                         BorderColor="#000099" BorderStyle="Solid" onclick="btnRemove1_Click"/> </td>--%>
                         </tr>
                         </asp:Panel>

                         <asp:Panel ID="pnlComp2" runat="server" Visible="false">
                                <tr><td><asp:Label ID="lblComBooking2" runat="server" /></td><td><asp:TextBox ID="txtCompetitorBooking2" runat="server"/></td><td> 
                                    <asp:Button ID="btnNext3" runat="server" Text="+"  BackColor="#0099FF" 
                         BorderColor="#000099" BorderStyle="Solid" onclick="btnNext3_Click"/> </td>
                         <td><asp:Button ID="btnRemove2" runat="server" Text="-" BackColor="#0099FF" 
                         BorderColor="#000099" BorderStyle="Solid" onclick="btnRemove2_Click" /> </td>
                        </tr>
                         </asp:Panel>

                          <asp:Panel ID="pnlComp3" runat="server" Visible="false">
                                <tr><td><asp:Label ID="lblComBooking3" runat="server" /></td><td><asp:TextBox ID="txtCompetitorBooking3" runat="server"/></td><td> 
                                    <asp:Button ID="btnNext4" runat="server" Text="+"  BackColor="#0099FF" 
                         BorderColor="#000099" BorderStyle="Solid" onclick="btnNext4_Click"/></td>
                          <td><asp:Button ID="btnRemove3" runat="server" Text="-" BackColor="#0099FF" 
                         BorderColor="#000099" BorderStyle="Solid" onclick="btnRemove3_Click" /> </td>
                         </tr>
                         </asp:Panel>

                          <asp:Panel ID="pnlComp4" runat="server" Visible="false">
                                <tr><td><asp:Label ID="lblComBooking4" runat="server" /></td><td><asp:TextBox ID="txtCompetitorBooking4" runat="server"/></td><td> 
                                    <asp:Button ID="btnNext5" runat="server" Text="+"  BackColor="#0099FF" 
                         BorderColor="#000099" BorderStyle="Solid" onclick="btnNext5_Click"/></td>
                          <td><asp:Button ID="btnRemove4" runat="server" Text="-" BackColor="#0099FF" 
                         BorderColor="#000099" BorderStyle="Solid" onclick="btnRemove4_Click" /> </td>
                         </tr>
                         </asp:Panel>

                          <asp:Panel ID="pnlComp5" runat="server" Visible="false">
                                <tr><td><asp:Label ID="lblComBooking5" runat="server" /></td><td><asp:TextBox ID="txtCompetitorBooking5" runat="server"/></td><td> 
                                    <asp:Button ID="btnRemove5" runat="server" Text="-"  BackColor="#0099FF" 
                         BorderColor="#000099" BorderStyle="Solid" onclick="btnRemove5_Click"/></td></tr>
                         </asp:Panel>
                            </table>
                                                   
                                 
                            </td>
                             <td align="right">
                            <strong>COR Booking :</strong>
                                </td>
                            <td align="left">
                                <asp:TextBox ID="txtCorBooking" runat="server" ReadOnly="True">0</asp:TextBox>
                            </td>



                             <td align="right" style="width: 101px"><strong> Bookings MTD :</strong></td>
                            <td align="left">
                                <asp:TextBox ID="txtBookingMDT" runat="server" ReadOnly="True">0</asp:TextBox>
                            </td>
                        </tr>
                        
                        <tr>
                            <td align="right">
                               <strong> Complaint Count(CRM) :</strong></td>
                            <td align="left">
                                 <asp:TextBox ID="txtComplaintCount" runat="server" ReadOnly="True">0</asp:TextBox>
                            </td>
                            <td align="right"><strong>SMS/Feedback Count(Insta) :</strong></td>
                            <td align="left">
                                 <asp:TextBox ID="txtSMSFeedbackCount" runat="server" ReadOnly="True">0</asp:TextBox>
                            </td>
                             <td align="right" style="width: 101px"><strong>Implant Performance :(<asp:Label ID="lblBookingTakenByImplant" runat="server" />)</strong></td>
                            <td align="left">
                                 <asp:TextBox ID="txtPerformance" runat="server" ReadOnly="True">0</asp:TextBox>
                            </td>
                        </tr>
                        
                        <tr>
                            <td align="right">
                                <strong>Client Performance :(<asp:Label ID="lblClientPerformance" runat="server" />)</strong></td>
                            <td align="left">
                                 <asp:TextBox ID="txtClientPerformance" runat="server" ReadOnly="True">0</asp:TextBox>
                            </td>
                            <td align="right"><strong>Complaint/Feedback Count(CRM+Insta) :</strong></td>
                            <td align="left">
                                 <asp:TextBox ID="txtComplaintCountInstaCRM" runat="server" ReadOnly="True">0</asp:TextBox>

                            </td>
                             <td align="right" style="width: 101px"><strong>Feedback Type :</strong></td>
                            <td align="left">
                                <asp:Label ID="lblFeedback" runat="server" 
                                ForeColor="#CC0000" />

                            </td>
                        </tr>
  
                        
                        <tr>
                            <%--<td align="right">
                                &nbsp;</td>--%>
                            <td align="left" valign="top" colspan="2">
                                 <table><tr>
                            <td ><strong>Complaint Resolution</strong></td>
                            </tr>
                            <tr>
                            <td><asp:GridView ID="GvResolution" runat="server" AutoGenerateColumns="false" EmptyDataText="No Data Found">
                            <Columns>
                             <asp:BoundField HeaderText="BranchName" DataField="BranchName" />
                            <asp:BoundField HeaderText="BookingId" DataField="BookingID" />
                               <asp:TemplateField HeaderText="Description">
                                        <ItemTemplate>
                                            <asp:TextBox id="txtComplaintDescription" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"ComplaintDescription") %>' BackColor="#ffffcc"  Width="180px" ReadOnly="true"/>
                                            
                                        </ItemTemplate>
                                   </asp:TemplateField>    

                                        <%--   <asp:TemplateField HeaderText="ResolutionProvided">
                                        <ItemTemplate>
                                            <asp:TextBox id="txtResolution" runat="server" BackColor="#ffffcc" Width="180px"/>
                                        </ItemTemplate>
                                   </asp:TemplateField>   --%>
                                   
                                   <asp:TemplateField HeaderText="ResolutionStatus">
                                        <ItemTemplate>
                                             <asp:TextBox id="txtResolution" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Resolution") %>' BackColor="#ffffcc"  Width="180px" ReadOnly="true" />
                                        </ItemTemplate>
                                   </asp:TemplateField>    
                                   </Columns>
                            </asp:GridView> </td>
                            </tr>
                            
                            
                            </table></td>


                             <td align="left" valign="top" colspan="1">
                                 <table><tr>
                            <td ><strong>Action Taken to Improve transaction</strong></td>
                            </tr>
                            <tr>
                            <td>
                            <asp:TextBox ID="txtAction" runat="server" TextMode="MultiLine" Width="300" 
                                   Height="50px" />
                             </td>
                            </tr>
                            
                            
                            </table></td>

                            <td align="left" valign="top" colspan="2">
                            <table border="0" runat="server" visible="false" id="trFeedbackDetails">
                                <tr><td> <strong>Quality Of Cab :</strong></td> <td><asp:Label ID="lblQualityOfCab" runat="server" /> </td></tr>
                                <tr><td><strong>Chauffeur Complaint :</strong></td><td><asp:Label ID="lblChauffeurComplaint" runat="server" /> </td></tr>
                                <tr><td><strong>Service Failure :</strong></td><td><asp:Label ID="lblServiceFailure" runat="server" /> </td></tr>
                                 <tr><td><strong>SOS Complaint :</strong></td><td><asp:Label ID="lblSOSComplaint" runat="server" /></td></tr>
                                  <tr><td> <strong>Billing :</strong></td><td><asp:Label ID="lblBilling" runat="server" /></td></tr>
                                   
                                    
                            </table>
                                &nbsp;</td>
                             
                            <td align="left" valign="top">
                                <asp:GridView ID="GvFeedBack" runat="server" AutoGenerateColumns="false" EmptyDataText="No Data Found">
                                <Columns>
                                 <asp:BoundField DataField="CityName" HeaderText="CityName"/>
                                 <asp:BoundField DataField="TotalCount" HeaderText="TotalCount"/>
                                  <asp:BoundField DataField="FeedbackStatus" HeaderText="Status"/>
                                </Columns>
                                </asp:GridView>

                            </td>
                        </tr>
                        <tr>
                        <td valign="top" align="right">
                            <strong>WeekOff Day :</strong>
                        </td>
                        <td valign="top" align="left">
                        <asp:DropDownList ID="ddlWeekoff" runat="server">
                            <asp:ListItem Text="Sun" Value="Sun" />
                            <asp:ListItem Text="Sat" Value="Sat"/>
                            <asp:ListItem Text="Fri" Value="Fri"/>
                            
                            <asp:ListItem Text="Thu" Value="Thu" />
                            <asp:ListItem Text="Wed" Value="Wed"/>
                            <asp:ListItem Text="Tue" Value="Tue"/>
                            <asp:ListItem Text="Mon" Value="Mon"/>
                            
                        </asp:DropDownList>
                        </td>
                        </tr>
                        
                        <tr>
                            <td colspan="6" align="center">
                                <asp:Button ID="btnNewReg" runat="server"
                                    Text="Submit" Font-Bold="True" 
                                    ValidationGroup="NR" onclick="btnNewReg_Click" />
                            </td>
                        </tr>
                        <tr><td colspan="6" align="center" bgcolor="#FF6600"><strong>Implant Performance Report</strong></td></tr>
                              
                        <tr>
                            <td colspan="6" align="center">
                            <table>
                            <tr><td><strong>From Date :</strong></td><td>
                                 <asp:TextBox ID="txtFromDate" runat="server"></asp:TextBox>
                                <%-- <cc1:calendarextender ID="ClFromDate" runat="server" 
                                    Format="MM-dd-yyyy" TargetControlID="txtFromDate">
                        </cc1:calendarextender>--%>
                                </td><td><strong>To Date :</strong></td><td>
                                 <asp:TextBox ID="txtToDate" runat="server"></asp:TextBox>
                               <%--  <cc1:calendarextender ID="ClToDate" runat="server" 
                                    Format="MM-dd-yyyy" TargetControlID="txtToDate">
                        </cc1:calendarextender>--%>
                                </td><td><strong>Implant Name :</strong></td><td>
                                <asp:DropDownList ID="ddlImplant" runat ="server"> 
                                   
                                    </asp:DropDownList>
                                </td></tr>
                            <tr><td><strong>Company Name :</strong></td><td>
                                <asp:DropDownList ID="ddlReportCmp" runat ="server"> 
                                   
                                    </asp:DropDownList>
                                </td><td><strong>City Name :</strong></td><td>
                                <asp:DropDownList ID="ddlReportCity" runat ="server"> 
                                   
                                    </asp:DropDownList>
                                </td><td>&nbsp;<asp:Button ID="btnView" runat="server" Text="View" 
                                     Font-Bold="True" OnClick="btnView_Click" /></td><td>
                                <asp:Button ID="btnExport" runat="server" Text="Export To excel" 
                                    onclick="btnExport_Click" Font-Bold="True" />
                                </td></tr>
                                <tr><td colspan="6">
                                     
                                <asp:GridView ID="gvImplantDetails" runat="server" AutoGenerateColumns="false" Width="100%"> 
                                 <Columns> 
                                        <asp:BoundField HeaderText="S.No." DataField="S.No." /> 
                                        <asp:BoundField HeaderText="Date" DataField="Date" /> 
                                        <asp:BoundField HeaderText="CityName" DataField="CityName" /> 
                                        <asp:BoundField HeaderText="CreatedBy" DataField="CreatedBy" /> 
                                        <asp:BoundField HeaderText="ClientName" DataField="ClientName" /> 
                                        <asp:BoundField HeaderText="ImplantName" DataField="ImplantName" /> 
                                        <asp:BoundField HeaderText="CompetitorName" DataField="CompetitorName" /> 
                                        <asp:BoundField HeaderText="CorBooking" DataField="CorBooking" /> 
                                        <asp:BoundField HeaderText="BookingTakenByImplant" DataField="BookingTakenByImplant" /> 
                                        <asp:BoundField HeaderText="BookingMDT" DataField="BookingMDT" /> 
                                        <asp:BoundField HeaderText="CompetitorBooking" DataField="CompetitorBooking" /> 
                                        <asp:BoundField HeaderText="ComplaintCount" DataField="ComplaintCount" /> 
                                        
                                        <asp:BoundField HeaderText="SMSFeedbackCount" DataField="SMSFeedbackCount" /> 
                                        <asp:BoundField HeaderText="Performance" DataField="Performance" /> 
                                        <asp:BoundField HeaderText="ComplaintResolution" DataField="ComplaintResolution" /> 
                                        <asp:BoundField HeaderText="ComplaintCountIntsaCrm" DataField="ComplaintCountIntsaCrm" /> 
                                        
                                        <asp:BoundField HeaderText="FeedbackType" DataField="FeedbackType" /> 
                                        <asp:BoundField HeaderText="ActionTakenToimprove" DataField="ActionTakenToimprove" /> 
                                        <asp:BoundField HeaderText="QualityOfCab" DataField="QualityOfCab" /> 
                                        <asp:BoundField HeaderText="ChauffeurComplaint" DataField="ChauffeurComplaint" /> 
                                        <asp:BoundField HeaderText="ServiceFailure" DataField="ServiceFailure" /> 
                                        
                                        <asp:BoundField HeaderText="SOSComplaint" DataField="SOSComplaint" /> 
                                        <asp:BoundField HeaderText="Invoice_Billining" DataField="Invoice_Billining" /> 
                                        <asp:BoundField HeaderText="BookingIdComplaintResolution" DataField="BookingIdComplaintResolution" /> 
                                        
                                        <asp:BoundField HeaderText="CityWiseStatus" DataField="CityWiseStatus" /> 
                                        <asp:BoundField HeaderText="Create Date" DataField="Create Date" /> 
                                         
                                  </Columns>
                                   <HeaderStyle CssClass="GridviewScrollHeader" /> 
                                   <RowStyle CssClass="GridviewScrollItem" /> 
                                   <PagerStyle CssClass="GridviewScrollPager" /> 
                                                                   
                                </asp:GridView>
                               
                                </td></tr>
                            </table>
                                &nbsp;</td>
                        </tr>
                    </table>
                    </center>
</asp:Content>


