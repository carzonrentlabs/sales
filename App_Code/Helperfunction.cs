﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Web;

/// <summary>
/// Summary description for Helperfunction
/// </summary>
public class Helperfunction
{
	public Helperfunction()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public static void LogInfoDetails(string Info)
    {
        try
        {
            string path = System.Web.Hosting.HostingEnvironment.MapPath("~/InfoLogging/");
            // check if directory exists
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            path = path + DateTime.Today.ToString("dd-MMM-yy") + ".log";
            // check if file exist
            if (!File.Exists(path))
            {
                File.Create(path).Dispose();
            }
            // log the error now
            using (StreamWriter writer = File.AppendText(path))
            {
                string error = "Log written at : " + DateTime.Now.ToString() + "; " + " Info : " + Info;

                writer.WriteLine(error);
                writer.Flush();
                writer.Close();
            }
            //return userFriendlyError;
        }
        catch
        {
            // Do nothing
        }
    }
    
    public static void LogErrorDetails(string Error)
    {
        try
        {
            string path = System.Web.Hosting.HostingEnvironment.MapPath("~/ErrorLogging/");
            // check if directory exists
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            path = path + DateTime.Today.ToString("dd-MMM-yy-") + ".log";
            // check if file exist
            if (!File.Exists(path))
            {
                File.Create(path).Dispose();
            }
            // log the error now
            using (StreamWriter writer = File.AppendText(path))
            {
                string error = "Log written at : " + DateTime.Now.ToString() + "; " + " Error : " + Error;

                writer.WriteLine(error);
                writer.Flush();
                writer.Close();
            }
            //return userFriendlyError;
        }
        catch
        {
            // Do nothing
        }
    }
}