﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using Insta;

/// <summary>
/// Summary description for SQLPackage
/// </summary>
public class SQLPackage
{
    public DataSet GetApprovalPackageDetails(int userID,int clientID)
    {
        SqlParameter[] ObjParam = new SqlParameter[2];
        ObjParam[0] = new SqlParameter("@SysUserID", SqlDbType.Int);
        ObjParam[0].Value = userID;       
        ObjParam[1] = new SqlParameter("@ClientID", SqlDbType.Int);
        ObjParam[1].Value = clientID;
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_PackageApproval_New", ObjParam);
    }
    public DataSet GetUnApprovedClient(int userID)
    {
        SqlParameter[] ObjParam = new SqlParameter[1];
        ObjParam[0] = new SqlParameter("@SysUserID", SqlDbType.Int);
        ObjParam[0].Value = userID;
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_UnApprovedClientPackage", ObjParam);
    }

    public int ApproveRM(List<OL_PKG_Link> objPKGLink)
    {
        try
        {
            int count = 0;
            foreach (OL_PKG_Link objPKG in objPKGLink)
            {
                SqlParameter[] ObjParam = new SqlParameter[5];
                ObjParam[0] = new SqlParameter("@SysUserId", objPKG.ApprovedBy);
                ObjParam[1] = new SqlParameter("@PkgId", objPKG.PkgID);
                ObjParam[2] = new SqlParameter("@ClientCoId",objPKG.ClientID);
                ObjParam[3] = new SqlParameter("@RMRejectRemark",objPKG.Remark);
                ObjParam[4] = new SqlParameter("@RMApproveYN", objPKG.ApproveYN);
                object k = SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "ApproveChauffeurPackageBYRM", ObjParam);
                //count = Convert.ToInt32(k);
                count = count + 1;
            }
            return count;
        }
        catch (Exception)
        {
            return 0;
        }


    }


}