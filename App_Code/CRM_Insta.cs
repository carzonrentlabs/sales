using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Collections.Generic;

namespace Insta
{

    /// <summary>
    /// Summary description for CRM_Insta
    /// </summary>
    public class CRM_Insta
    {
        public CRM_Insta()
        {
            //
            // TODO: Add constructor logic here
            //
        }        
        
        public static object GetUserName(int SysuserID)
        {
            string ConString = System.Configuration.ConfigurationManager.ConnectionStrings["CRDMenu"].ToString();
            SqlConnection myconn = new SqlConnection(ConString);          
            SqlCommand Cmd = new SqlCommand();

            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@SysuserID", SysuserID);
            Cmd.CommandText="GetUserNameById";
            Cmd.CommandType= CommandType.StoredProcedure;
            Cmd.Parameters.AddRange(param);
            Cmd.Connection = myconn;
            myconn.Open();
            return Cmd.ExecuteScalar();
            myconn.Close();

        }

      public static  string RegisterComplaint
      (
        int BranchID
      , int JobLocationID
      , string JobID
      , DateTime JobDate
      , string Job_From
      , string Job_To
      , string CustomerName
      , string CustomerContact
      , string ContactDetails
      , string Remarks
      , string CabID
      , string ChauffeurName
      , string ChauffeurContact
      , int ComplaintSourceID
      , DateTime ComplaintDateTime
      , string ComplaintDescription
      , string RegisteredBy
      , DateTime Reg_DateTime
      , int ResolutionAt
      , int PriorityID
      , int ComplaintStatus
      , string CarCategory
      , int SubCategoryId
      , string txtComplaintRemark
      )
        {
            string ConString = System.Configuration.ConfigurationManager.ConnectionStrings["EasyCabsConnString"].ToString();
            SqlConnection myconn = new SqlConnection(ConString);
            SqlDataReader Myreader;
            string Message = "";
            try
            {
                if (Remarks == "")
                {
                    Remarks = "0";
                }



                if (ComplaintDescription == "")
                {
                    ComplaintDescription = "0";
                }

               

                SqlCommand Insertbulktransaction = new SqlCommand("BL_CRM_RegisterComplaints", myconn);
                Insertbulktransaction.Connection = myconn;
                Insertbulktransaction.CommandType = CommandType.StoredProcedure;


                Insertbulktransaction.Parameters.AddWithValue("@BranchID", SqlDbType.Int).Value = BranchID;
                Insertbulktransaction.Parameters.AddWithValue("@JobLocationID", SqlDbType.Int).Value = JobLocationID;
                Insertbulktransaction.Parameters.AddWithValue("@JobID", SqlDbType.VarChar).Value = JobID;
                Insertbulktransaction.Parameters.AddWithValue("@JobDate", SqlDbType.DateTime).Value = JobDate;
                Insertbulktransaction.Parameters.AddWithValue("@Job_From", SqlDbType.VarChar).Value = Job_From;
                Insertbulktransaction.Parameters.AddWithValue("@Job_To", SqlDbType.VarChar).Value = Job_To;
                Insertbulktransaction.Parameters.AddWithValue("@CustomerName", SqlDbType.VarChar).Value = CustomerName;
                Insertbulktransaction.Parameters.AddWithValue("@CustomerContact", SqlDbType.VarChar).Value = CustomerContact;
                Insertbulktransaction.Parameters.AddWithValue("@ContactDetails", SqlDbType.VarChar).Value = ContactDetails;
                Insertbulktransaction.Parameters.AddWithValue("@Remarks", SqlDbType.VarChar).Value = Remarks;
                Insertbulktransaction.Parameters.AddWithValue("@CabID", SqlDbType.VarChar).Value = CabID;
                Insertbulktransaction.Parameters.AddWithValue("@ChauffeurName", SqlDbType.VarChar).Value = ChauffeurName;
                Insertbulktransaction.Parameters.AddWithValue("@ChauffeurContact", SqlDbType.Decimal).Value = ChauffeurContact;
                Insertbulktransaction.Parameters.AddWithValue("@ComplaintSourceID", SqlDbType.Int).Value = ComplaintSourceID;
                Insertbulktransaction.Parameters.AddWithValue("@ComplaintDateTime", SqlDbType.DateTime).Value = ComplaintDateTime;
                Insertbulktransaction.Parameters.AddWithValue("@ComplaintDescription", SqlDbType.VarChar).Value = ComplaintDescription;
                Insertbulktransaction.Parameters.AddWithValue("@RegisteredBy", SqlDbType.VarChar).Value = RegisteredBy;
                Insertbulktransaction.Parameters.AddWithValue("@Reg_DateTime", SqlDbType.DateTime).Value = Reg_DateTime;
                Insertbulktransaction.Parameters.AddWithValue("@ResolutionAt", SqlDbType.Int).Value = ResolutionAt;
                Insertbulktransaction.Parameters.AddWithValue("@PriorityID", SqlDbType.Int).Value = PriorityID;
                Insertbulktransaction.Parameters.AddWithValue("@ComplaintStatus", SqlDbType.Int).Value = ComplaintStatus;
                Insertbulktransaction.Parameters.AddWithValue("@CarCategory", SqlDbType.Int).Value = CarCategory;
                Insertbulktransaction.Parameters.AddWithValue("@SubCategoryId", SqlDbType.Int).Value = SubCategoryId;
                Insertbulktransaction.Parameters.AddWithValue("@txtComplaintRemark", SqlDbType.VarChar).Value = txtComplaintRemark;

                myconn.Open();

                Myreader = Insertbulktransaction.ExecuteReader();

                if (Myreader.Read())
                {
                    Message = Myreader[0].ToString();
                }
                else
                {
                    Message = "Retry";
                }
            }
            catch (Exception ex)
            {
                Message = "CRM under Maintenance";
            }
            myconn.Close();
            return Message;

        }

        public static  DataSet ListCrmMonthlyReoport(int BranchID, DateTime FromDate, DateTime ToDate, int IsExport)
        {
            string SProc = "[BL_CRM_ListCrmMonthlyReoport]";

            SqlParameter[] myParams = new SqlParameter[4];

            myParams[0] = new SqlParameter("@BranchID", SqlDbType.Int);
            myParams[0].Value = BranchID;

            myParams[1] = new SqlParameter("@FromDate", SqlDbType.DateTime);
            myParams[1].Value = FromDate.ToShortDateString();

            myParams[2] = new SqlParameter("@ToDate", SqlDbType.DateTime);
            myParams[2].Value = ToDate.ToShortDateString();           
            myParams[3] = new SqlParameter("@IsExport", SqlDbType.Int);
            myParams[3].Value = IsExport;
            string ConString = System.Configuration.ConfigurationManager.ConnectionStrings["EasyCabsConnString"].ToString();
            SqlConnection myconn = new SqlConnection(ConString);
            myconn.Open();
            SqlCommand Cmd = new SqlCommand(SProc, myconn);
            Cmd.CommandType = CommandType.StoredProcedure;
            Cmd.Parameters.AddRange(myParams);
            SqlDataAdapter da = new SqlDataAdapter(Cmd); 
            DataSet ds = new DataSet();
            da.Fill(ds);
            myconn.Close();
            return ds;
        }


        public static DataSet CSAT_CSATSuggestionReport(int BranchID, DateTime FromDate, DateTime ToDate)
        {
            string SProc = "[BL_CRM_CSAT_CSATSuggestionReport]";            
            SqlParameter[] myParams = new SqlParameter[3];
            myParams[0] = new SqlParameter("@FromDate", SqlDbType.DateTime);
            myParams[0].Value = FromDate;

            myParams[1] = new SqlParameter("@ToDate", SqlDbType.DateTime);
            myParams[1].Value = ToDate;

            myParams[2] = new SqlParameter("@BranchID", SqlDbType.Int);
            myParams[2].Value = BranchID;          
            string ConString = System.Configuration.ConfigurationManager.ConnectionStrings["EasyCabsConnString"].ToString();
            SqlConnection myconn = new SqlConnection(ConString);
            myconn.Open();
            SqlCommand Cmd = new SqlCommand(SProc, myconn);
            Cmd.CommandType = CommandType.StoredProcedure;
            Cmd.Parameters.AddRange(myParams);
            SqlDataAdapter da = new SqlDataAdapter(Cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            myconn.Close();
            return ds;
        }

        public static DataSet CSAT_LimoSuggestionReport(int BranchID, DateTime FromDate, DateTime ToDate)
        {
            string SProc = "[BL_CRM_CSAT_LimoSuggestionReport]";
            SqlParameter[] myParams = new SqlParameter[3];
            myParams[0] = new SqlParameter("@FromDate", SqlDbType.DateTime);
            myParams[0].Value = FromDate;

            myParams[1] = new SqlParameter("@ToDate", SqlDbType.DateTime);
            myParams[1].Value = ToDate;

            myParams[2] = new SqlParameter("@BranchID", SqlDbType.Int);
            myParams[2].Value = BranchID;
            string ConString = System.Configuration.ConfigurationManager.ConnectionStrings["EasyCabsConnString"].ToString();
            SqlConnection myconn = new SqlConnection(ConString);
            myconn.Open();
            SqlCommand Cmd = new SqlCommand(SProc, myconn);
            Cmd.CommandType = CommandType.StoredProcedure;
            Cmd.Parameters.AddRange(myParams);
            SqlDataAdapter da = new SqlDataAdapter(Cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            myconn.Close();
            return ds;
        }

        public static DataSet CSAT_TripsSuggestionReport(int BranchID, DateTime FromDate, DateTime ToDate)
        {
            string SProc = "[BL_CRM_CSAT_TripsSuggestionReport]";
            SqlParameter[] myParams = new SqlParameter[3];
            myParams[0] = new SqlParameter("@FromDate", SqlDbType.DateTime);
            myParams[0].Value = FromDate;

            myParams[1] = new SqlParameter("@ToDate", SqlDbType.DateTime);
            myParams[1].Value = ToDate;

            myParams[2] = new SqlParameter("@BranchID", SqlDbType.Int);
            myParams[2].Value = BranchID;
            string ConString = System.Configuration.ConfigurationManager.ConnectionStrings["EasyCabsConnString"].ToString();
            SqlConnection myconn = new SqlConnection(ConString);
            myconn.Open();
            SqlCommand Cmd = new SqlCommand(SProc, myconn);
            Cmd.CommandType = CommandType.StoredProcedure;
            Cmd.Parameters.AddRange(myParams);
            SqlDataAdapter da = new SqlDataAdapter(Cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            myconn.Close();
            return ds;
        }

        public static  DataSet ListLIMO_CSAT_Report(int BranchID, DateTime StartDate, DateTime EndDate)
        {
            string SProc = "[BL_CRM_LIMO_CSAT_CSAT_Report]";

            SqlParameter[] myParams = new SqlParameter[3];

            myParams[0] = new SqlParameter("@BranchID", SqlDbType.Int);
            myParams[0].Value = BranchID;

            myParams[1] = new SqlParameter("@StartDate", SqlDbType.DateTime);
            myParams[1].Value = StartDate;

            myParams[2] = new SqlParameter("@EndDate", SqlDbType.DateTime);
            myParams[2].Value = EndDate;

            string ConString = System.Configuration.ConfigurationManager.ConnectionStrings["EasyCabsConnString"].ToString();
            SqlConnection myconn = new SqlConnection(ConString);
            myconn.Open();
            SqlCommand Cmd = new SqlCommand(SProc, myconn);
            Cmd.CommandType = CommandType.StoredProcedure;
            Cmd.Parameters.AddRange(myParams);
            SqlDataAdapter da = new SqlDataAdapter(Cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            myconn.Close();
            return ds;
            
        }

        public static DataSet ListTrips_CSAT_Report(int BranchID, DateTime StartDate, DateTime EndDate)
        {
            string SProc = "[BL_CRM_Trips_CSAT_CSAT_Report]";

            SqlParameter[] myParams = new SqlParameter[3];

            myParams[0] = new SqlParameter("@BranchID", SqlDbType.Int);
            myParams[0].Value = BranchID;

            myParams[1] = new SqlParameter("@StartDate", SqlDbType.DateTime);
            myParams[1].Value = StartDate;

            myParams[2] = new SqlParameter("@EndDate", SqlDbType.DateTime);
            myParams[2].Value = EndDate;

            string ConString = System.Configuration.ConfigurationManager.ConnectionStrings["EasyCabsConnString"].ToString();
            SqlConnection myconn = new SqlConnection(ConString);
            myconn.Open();
            SqlCommand Cmd = new SqlCommand(SProc, myconn);
            Cmd.CommandType = CommandType.StoredProcedure;
            Cmd.Parameters.AddRange(myParams);
            SqlDataAdapter da = new SqlDataAdapter(Cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            myconn.Close();
            return ds;

        }

        public static DataSet GetCRMData(int Month, int Year)
        {
            string SProc = "[BL_SP_GetCRMData]";

            SqlParameter[] myParams = new SqlParameter[2];

            myParams[0] = new SqlParameter("@Month", SqlDbType.Int);
            myParams[0].Value = Month;

            myParams[1] = new SqlParameter("@year", SqlDbType.Int);
            myParams[1].Value = Year;
            string ConString = System.Configuration.ConfigurationManager.ConnectionStrings["EasyCabsConnString"].ToString();
            SqlConnection myconn = new SqlConnection(ConString);
            myconn.Open();
            SqlCommand Cmd = new SqlCommand(SProc, myconn);
            Cmd.CommandType = CommandType.StoredProcedure;
            Cmd.Parameters.AddRange(myParams);
            SqlDataAdapter da = new SqlDataAdapter(Cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            myconn.Close();
            return ds;
        }

        public DataSet Limo_ListQuestionnaire()
        {
            string SProc = "[BL_CRM_CSAT_Limo_ListQuestionnaire]";
            string ConString = System.Configuration.ConfigurationManager.ConnectionStrings["EasyCabsConnString"].ToString();
            SqlConnection myconn = new SqlConnection(ConString);
            myconn.Open();
            SqlCommand Cmd = new SqlCommand(SProc, myconn);
            Cmd.CommandType = CommandType.StoredProcedure;           
            SqlDataAdapter da = new SqlDataAdapter(Cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            myconn.Close();
            return ds;
        }

        public static SqlDataReader LIMO_CsatSummary <I>(I BranchID)
        {
            string SProc = "[BL_CRM_LIMO_CSAT_Summary]";

            SqlParameter[] myParams = new SqlParameter[1];

            myParams[0] = new SqlParameter("@BranchID", SqlDbType.Int);
            myParams[0].Value = BranchID;
            string ConString = System.Configuration.ConfigurationManager.ConnectionStrings["EasyCabsConnString"].ToString();
            SqlConnection myconn = new SqlConnection(ConString);
            myconn.Open();
            SqlCommand Cmd = new SqlCommand(SProc, myconn);
            Cmd.CommandType = CommandType.StoredProcedure;
            Cmd.Parameters.AddRange(myParams);
            SqlDataReader dr = Cmd.ExecuteReader();
            return dr;
            
        }

        public static DataSet LimoReadRandomNextRecord<I>(I BranchID, I Division)
        {
            string SProc = "[BL_CRM_Limo_CSAT_ReadRandomRecord]";            
            SqlParameter[] myParams = new SqlParameter[2];

            myParams[0] = new SqlParameter("@BranchID", SqlDbType.Int);
            myParams[0].Value = BranchID;

            myParams[1] = new SqlParameter("@Division", SqlDbType.Int);
            myParams[1].Value = Division;

            string ConString = System.Configuration.ConfigurationManager.ConnectionStrings["EasyCabsConnString"].ToString();
            SqlConnection myconn = new SqlConnection(ConString);
            myconn.Open();
            SqlCommand Cmd = new SqlCommand(SProc, myconn);
            Cmd.CommandType = CommandType.StoredProcedure;
            Cmd.Parameters.AddRange(myParams);
            SqlDataAdapter da = new SqlDataAdapter(Cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            myconn.Close();
            return ds;
            
        }

        public static SqlDataReader TRIPS_CsatSummary<I>(I BranchID)
        {
            string SProc = "[BL_CRM_TRIPS_CSAT_Summary]";

            SqlParameter[] myParams = new SqlParameter[1];

            myParams[0] = new SqlParameter("@BranchID", SqlDbType.Int);
            myParams[0].Value = BranchID;
            string ConString = System.Configuration.ConfigurationManager.ConnectionStrings["EasyCabsConnString"].ToString();
            SqlConnection myconn = new SqlConnection(ConString);
            myconn.Open();
            SqlCommand Cmd = new SqlCommand(SProc, myconn);
            Cmd.CommandType = CommandType.StoredProcedure;
            Cmd.Parameters.AddRange(myParams);
            SqlDataReader dr = Cmd.ExecuteReader();
            return dr;

        }

        public static DataSet TripsReadRandomNextRecord<I>(I BranchID, I Division)
        {
            string SProc = "[BL_CRM_Trips_CSAT_ReadRandomRecord]";
            SqlParameter[] myParams = new SqlParameter[2];

            myParams[0] = new SqlParameter("@BranchID", SqlDbType.Int);
            myParams[0].Value = BranchID;

            myParams[1] = new SqlParameter("@Division", SqlDbType.Int);
            myParams[1].Value = Division;

            string ConString = System.Configuration.ConfigurationManager.ConnectionStrings["EasyCabsConnString"].ToString();
            SqlConnection myconn = new SqlConnection(ConString);
            myconn.Open();
            SqlCommand Cmd = new SqlCommand(SProc, myconn);
            Cmd.CommandType = CommandType.StoredProcedure;
            Cmd.Parameters.AddRange(myParams);
            SqlDataAdapter da = new SqlDataAdapter(Cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            myconn.Close();
            return ds;

        }

        public DataSet Trips_ListQuestionnaire()
        {
            string SProc = "[BL_CRM_CSAT_TRIPS_ListQuestionnaire]";
            string ConString = System.Configuration.ConfigurationManager.ConnectionStrings["EasyCabsConnString"].ToString();
            SqlConnection myconn = new SqlConnection(ConString);
            myconn.Open();
            SqlCommand Cmd = new SqlCommand(SProc, myconn);
            Cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(Cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            myconn.Close();
            return ds;
        }
        public DataSet GetSubComplaintSubType(int ComplainTypeID)
        {
            DataSet ds = new DataSet();
            try
            {   

                string SProc = "[Prc_GetSubComplaintype]";
                SqlParameter[] myParams = new SqlParameter[1];

                myParams[0] = new SqlParameter("@ComplainTypeId", SqlDbType.Int);
                myParams[0].Value = ComplainTypeID;

                string ConString = System.Configuration.ConfigurationManager.ConnectionStrings["EasyCabsConnString"].ToString();
                SqlConnection myconn = new SqlConnection(ConString);
                myconn.Open();
                SqlCommand Cmd = new SqlCommand(SProc, myconn);
                Cmd.CommandType = CommandType.StoredProcedure;
                Cmd.Parameters.AddRange(myParams);
                SqlDataAdapter da = new SqlDataAdapter(Cmd);
                
                da.Fill(ds);
                myconn.Close();
                return ds;

            }
            catch (Exception Ex)
            {
                ds = null;
                new Exception(Ex.Message);
            }
            return ds;
        }
        public int GetResolutionId(int ComplainTypeID)
        {
            DataSet ds = new DataSet();
            int ResolutionId = 0;
            string SProc = "[Prc_GetResolutionRequired]";
            SqlParameter[] myParams = new SqlParameter[1];

            myParams[0] = new SqlParameter("@ComplainTypeId", SqlDbType.Int);
            myParams[0].Value = ComplainTypeID;

            string ConString = System.Configuration.ConfigurationManager.ConnectionStrings["EasyCabsConnString"].ToString();
            SqlConnection myconn = new SqlConnection(ConString);
            myconn.Open();
            SqlCommand Cmd = new SqlCommand(SProc, myconn);
            Cmd.CommandType = CommandType.StoredProcedure;
            Cmd.Parameters.AddRange(myParams);
            SqlDataAdapter da = new SqlDataAdapter(Cmd);
            da.Fill(ds);
            myconn.Close();
            if (ds.Tables[0].Rows.Count > 0)
            {
                ResolutionId = Convert.ToInt32(ds.Tables[0].Rows[0]["EntityValue"].ToString());
            }
            return ResolutionId;
        
        }

        public DataSet GetUnitName()
        {
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "dbo.Bl_Sp_GetUnitName");
        }

        public int SubmitRevenueBudgetUnitWise(int UnitID, DateTime BudgetDate, double Amount, string Remarks, int User)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[5];

            ObjparamUser[0] = new SqlParameter("@UnitID", UnitID);
            ObjparamUser[1] = new SqlParameter("@BudgetDate", BudgetDate);
            ObjparamUser[2] = new SqlParameter("@Amount", Amount);
            ObjparamUser[3] = new SqlParameter("@Remarks", Remarks);
            ObjparamUser[4] = new SqlParameter("@User", User);

            int status = 0;
            //return SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "Sp_SaveSalesFollowUp", ObjparamUser);
            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "dbo.Sp_Save_RevenueBudgetUnitWise", ObjparamUser);
            if (ds.Tables[0].Rows.Count > 0)
            {
                status = Convert.ToInt32(ds.Tables[0].Rows[0]["ReturnID"]);
            }
            else
            {
                status = -1;
            }

            return status;
        }

        public DataSet GetRevenueBudgetDetails(int UnitID, DateTime BudgetDate)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[2];
            ObjparamUser[0] = new SqlParameter("@UnitID", UnitID);
            ObjparamUser[1] = new SqlParameter("@BudgetDate", BudgetDate);

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "dbo.bl_sp_GetRevenueBudgetDetails", ObjparamUser);
        }

        public DataSet GetRevenueBudgetDateDetails(int UnitID)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[1];
            ObjparamUser[0] = new SqlParameter("@UnitID", UnitID);

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "dbo.bl_sp_GetRevenueBudgetDateDetails", ObjparamUser);
        }


        public int UpdateRevenueBudgetUnitWise(int UnitID, DateTime BudgetDate, double Amount, string Remarks, int User)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[5];

            ObjparamUser[0] = new SqlParameter("@UnitID", UnitID);
            ObjparamUser[1] = new SqlParameter("@BudgetDate", BudgetDate);
            ObjparamUser[2] = new SqlParameter("@Amount", Amount);
            ObjparamUser[3] = new SqlParameter("@Remarks", Remarks);
            ObjparamUser[4] = new SqlParameter("@User", User);

            int status = 0;
            //return SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "Sp_SaveSalesFollowUp", ObjparamUser);
            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "dbo.Sp_Update_RevenueBudgetUnitWise", ObjparamUser);
            if (ds.Tables[0].Rows.Count > 0)
            {
                status = Convert.ToInt32(ds.Tables[0].Rows[0]["ReturnID"]);
            }
            else
            {
                status = -1;
            }

            return status;
        }

    }
}