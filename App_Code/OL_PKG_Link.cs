﻿using System;
using System.Collections.Generic;
using System.Web;
/// <summary>
/// Summary description for OL_PKG_Link
/// </summary>
public class OL_PKG_Link:OL_PKG 
{
    #region Variale Declaration
    private bool _approveYN;
    private int _approvedBy;
    private int _clientID;
    private string _remark; 
    #endregion


    #region Properties
    public bool ApproveYN
    {
        get { return _approveYN; }
        set { _approveYN = value; }
    }
    public int ApprovedBy
    {
        get { return _approvedBy; }
        set { _approvedBy = value; }
    }
    public int ClientID
    {
        get { return _clientID; }
        set { _clientID = value; }
    }
    public string Remark
    {
        get { return _remark; }
        set { _remark = value; }
    } 
    #endregion
}