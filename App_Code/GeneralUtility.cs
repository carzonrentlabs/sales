﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

/// <summary>
/// Summary description for GeneralUtility
/// </summary>
public class GeneralUtility
{
	public GeneralUtility()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public static void InitializeControls(Control objcontrol)
    {
        foreach (Control control in objcontrol.Controls)
        {
            if ((control.GetType() == typeof(TextBox)))
            {
                ((TextBox)(control)).Text = "";
            }
            if ((control.GetType() == typeof(DropDownList)))
            {
                if (control.ClientID.Contains("ddlClient") || control.ClientID.Contains("ddlProspectCategories"))
                { }
                else if (((DropDownList)(control)).Items.Count > 0)
                {
                    ((DropDownList)(control)).ClearSelection();
                    ((DropDownList)(control)).SelectedIndex = 0;
                }
            }
            if ((control.GetType() == typeof(ListBox)))
            {
                if (((ListBox)(control)).Items.Count > 0)
                {
                    ((ListBox)(control)).ClearSelection();
                    ((ListBox)(control)).SelectedIndex = 0;
                }
            }

            if ((control.GetType() == typeof(CheckBox)))
            {
                ((CheckBox)(control)).Checked = false;
            }

            if (control.HasControls() && control.GetType() != typeof(MultiView))
            {
                InitializeControls(control);
            }
        }
    }
}

public class PackageType
{
    public const string local = "1";
    public const string outstation = "2";
    public const string Airport = "3";
}
