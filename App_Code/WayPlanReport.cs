﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using Insta;

/// <summary>
/// Summary description for WayPlanReport
/// </summary>
public class WayPlanReport
{
	
    public DataSet GetWayPlanData(string financialYear,int cityId,string GroupEntity,int clientCoId )
    {
        SqlParameter[] ObjparamUser = new SqlParameter[4];
        ObjparamUser[0] = new SqlParameter("@FinancialYear", financialYear);
        ObjparamUser[1] = new SqlParameter("@CityId", cityId);
        ObjparamUser[2] = new SqlParameter("@GroupEnityName", GroupEntity);
        ObjparamUser[3] = new SqlParameter("@ClientCoID", clientCoId);
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_GetWayPlan", ObjparamUser);
    }
    public DataSet GetGroupEntity(int cityId)
    {
        SqlParameter[] ObjparamUser = new SqlParameter[1];
        ObjparamUser[0] = new SqlParameter("@CityId", cityId);
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_GetGroupEntity", ObjparamUser);
    }
    public DataSet GetAcquisitionClient(int cityID)
    {
        SqlParameter[] ObjparamUser = new SqlParameter[1];
        ObjparamUser[0] = new SqlParameter("@CityId", cityID);
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_GetAcquisitionsClient", ObjparamUser);
    }
    public DataSet GetAcquisitionWayPlanData(string financialYear, int cityId, string GroupEntity)
    {
        SqlParameter[] ObjparamUser = new SqlParameter[3];
        ObjparamUser[0] = new SqlParameter("@FinancialYear", financialYear);
        ObjparamUser[1] = new SqlParameter("@CityId", cityId);
        ObjparamUser[2] = new SqlParameter("@GroupEnityName", GroupEntity);
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_GetWayPlan_ForAcquisition", ObjparamUser);
    }
    public DataSet GetTarget(int cityId, string financialYear)
    {
        SqlParameter[] ObjparamUser = new SqlParameter[2];
        ObjparamUser[0] = new SqlParameter("@CityId", cityId);
        ObjparamUser[1] = new SqlParameter("@FinancialYear", financialYear);
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "prc_GetTargetAmount", ObjparamUser);
    }
    public DataSet GetAvgTicketSizeLTR(int cityId, string financialYear)
    {
        SqlParameter[] ObjparamUser = new SqlParameter[2];
        ObjparamUser[0] = new SqlParameter("@CityId", cityId);
        ObjparamUser[1] = new SqlParameter("@FinancialYear", financialYear);
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "prc_GetAVGTicketSize_LTR", ObjparamUser);
    }
    public DataSet GetAVGTicketSizeSpotRental(int cityId, string financialYear)
    {
        SqlParameter[] ObjparamUser = new SqlParameter[2];
        ObjparamUser[0] = new SqlParameter("@CityId", cityId);
        ObjparamUser[1] = new SqlParameter("@FinancialYear", financialYear);
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "prc_GetAVGTicketSize_SpotRental", ObjparamUser);
    }
    public DataSet GetCityName()
    {
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_GetCityForWayPlan");
    }
}