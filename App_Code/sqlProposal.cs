﻿using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Security.Cryptography;
using System.Text;
using System.IO;
using System.Xml;
using System.Collections;
using System;
using Insta;

/// <summary>
/// Summary description for sqlProposal
/// </summary>
public class sqlProposal
{

    public DataSet GetProspectMaster()
    {
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_ProspectsMaster");
    }

    public DataSet GetClientMaster()
    {
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_ClientMaster");
    }

    public DataSet GetTypeofProposal()
    {
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "GetBookingServiceType");
    }
    public DataSet GetCarCategory()
    {
        //return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_GetCarCategory");
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_GetProposalCarCategory");
    }
    public DataSet GetCarModel(int carCatId)
    {
        SqlParameter[] ObjParam = new SqlParameter[1];
        ObjParam[0] = new SqlParameter("@CarCatID", SqlDbType.Int);
        ObjParam[0].Value = carCatId;
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_GetModelOnBasisOfCarCategory", ObjParam);
    }
    public DataSet GetPackageType()
    {
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_GetPackageType");
    }
    public DataSet GetProposalDetails(OL_Proposal objProposalOL)
    {
        SqlParameter[] ObjParam = new SqlParameter[1];
        //ObjParam[0] = new SqlParameter("@ProspectId", SqlDbType.Int);
        ObjParam[0] = new SqlParameter("@ProspectId", SqlDbType.Int);
        ObjParam[0].Value = objProposalOL.ProspectID;
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_GetProposalDetailsClose", ObjParam);
        //return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_GetProposalDetails", ObjParam);
    }
    public DataSet GetProposalDetailsMail(int proposalId)
    {
        SqlParameter[] ObjParam = new SqlParameter[1];
        ObjParam[0] = new SqlParameter("@ProspectId", SqlDbType.Int);
        ObjParam[0].Value = proposalId;
        //return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_GetProposalDetails", ObjParam);
        //return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_GetProposalDetails_New", ObjParam);
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_GetProposalMailer", ObjParam);
    }

    public DataSet GetProposalEmailData(int proposalid, string emailid)
    {
        SqlParameter[] ObjParam = new SqlParameter[2];
        ObjParam[0] = new SqlParameter("@ProspectId", proposalid);
        ObjParam[1] = new SqlParameter("@EmailID", emailid);
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_GetMailerdata", ObjParam);
    }

    public DataSet GetProposalDetailsMail_New(int proposalId)
    {
        SqlParameter[] ObjParam = new SqlParameter[1];
        ObjParam[0] = new SqlParameter("@ProspectId", SqlDbType.Int);
        ObjParam[0].Value = proposalId;
        //return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_GetProposalDetails", ObjParam);
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_GetProposalDetails_New", ObjParam);
    }
    public DataSet GetProposalDetailsStandPackage(OL_Proposal objProposalOL)
    {
        SqlParameter[] ObjParam = new SqlParameter[1];
        ObjParam[0] = new SqlParameter("@ProspectId", SqlDbType.Int);
        ObjParam[0].Value = objProposalOL.ProspectID;

        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_GetDafaultProposalNew", ObjParam);


    }
    public int SaveProposalDetails(OL_Proposal objProposalOL)
    {
        int status = 0;
        int i = 0;

        string ss = "exec Prc_InsertProposalDetails " + objProposalOL.PkgTypeId + "," + objProposalOL.CityID + "," + objProposalOL.CarCatID + ","
                + objProposalOL.PKGRate + "," + objProposalOL.PkgKMs + "," + objProposalOL.PkgHrs + "," +
                objProposalOL.ExtraHrRate + "," + objProposalOL.ExtraKMRate + "," + objProposalOL.ThresholdExtraHr + "," +
                objProposalOL.ThresholdExtraKM + "," + objProposalOL.OutStationAllowance + "," + objProposalOL.NightStayAllowance + "," +
                objProposalOL.SysUserId + "," + objProposalOL.ProspectID + "," + objProposalOL.ProposalCityId + ",'" +
                objProposalOL.DateInitition + "'," + objProposalOL.ServiceType + "," + objProposalOL.ProposalFor + ",'" +
                objProposalOL.ManagerName + "'," + objProposalOL.ActionManagerID + ",'" + objProposalOL.BillingBasis + "'," +
                objProposalOL.Fgr + "," + objProposalOL.TempId + "," + objProposalOL.BaseRate + "," +
                objProposalOL.BaseHr + "," + objProposalOL.BaseKM + "," + objProposalOL.OriginalBaseRate + "," + objProposalOL.ClientCoId
                + "," + objProposalOL.SDPRateID + ",'" + objProposalOL.ProspectCategory + "', '" + objProposalOL.Remarks
                + "', " + objProposalOL.NotApproveYN + "," + objProposalOL.BillPeriodID;

        try
        {
            SqlParameter[] ObjParam = new SqlParameter[33];
            ObjParam[i] = new SqlParameter("@PkgTypeId", objProposalOL.PkgTypeId);
            ObjParam[++i] = new SqlParameter("@CityID", objProposalOL.CityID);
            ObjParam[++i] = new SqlParameter("@CarCatID", objProposalOL.CarCatID);
            //ObjParam[++i] = new SqlParameter("@CarModelID", objProposalOL.CarModelID);
            ObjParam[++i] = new SqlParameter("@PKGRate", objProposalOL.PKGRate);
            ObjParam[++i] = new SqlParameter("@PkgKMs", objProposalOL.PkgKMs);
            ObjParam[++i] = new SqlParameter("@PkgHrs", objProposalOL.PkgHrs);
            ObjParam[++i] = new SqlParameter("@ExtraHrRate", objProposalOL.ExtraHrRate);
            ObjParam[++i] = new SqlParameter("@ExtraKMRate", objProposalOL.ExtraKMRate);
            ObjParam[++i] = new SqlParameter("@ThresholdExtraHr", objProposalOL.ThresholdExtraHr);
            ObjParam[++i] = new SqlParameter("@ThresholdExtraKM", objProposalOL.ThresholdExtraKM);
            ObjParam[++i] = new SqlParameter("@OutStationAllowance", objProposalOL.OutStationAllowance);
            ObjParam[++i] = new SqlParameter("@NightStayAllowance", objProposalOL.NightStayAllowance);
            ObjParam[++i] = new SqlParameter("@CreatedBy", objProposalOL.SysUserId);
            ObjParam[++i] = new SqlParameter("@ProspectId", objProposalOL.ProspectID);
            ObjParam[++i] = new SqlParameter("@ProposalCity", objProposalOL.ProposalCityId);
            ObjParam[++i] = new SqlParameter("@DateInitition", objProposalOL.DateInitition);
            ObjParam[++i] = new SqlParameter("@ServiceTypeId", objProposalOL.ServiceType);
            ObjParam[++i] = new SqlParameter("@ProposalFor", objProposalOL.ProposalFor);
            ObjParam[++i] = new SqlParameter("@ActionManagerName", objProposalOL.ManagerName);
            ObjParam[++i] = new SqlParameter("@ActionManagerID", objProposalOL.ActionManagerID);
            ObjParam[++i] = new SqlParameter("@BillingBasis", objProposalOL.BillingBasis);
            ObjParam[++i] = new SqlParameter("@FGRAmount", objProposalOL.Fgr);
            ObjParam[++i] = new SqlParameter("@PDTempID", objProposalOL.TempId);
            ObjParam[++i] = new SqlParameter("@BaseRate", objProposalOL.BaseRate);
            ObjParam[++i] = new SqlParameter("@BaseHr", objProposalOL.BaseHr);
            ObjParam[++i] = new SqlParameter("@BaseKM", objProposalOL.BaseKM);
            ObjParam[++i] = new SqlParameter("@OriginalPKGRate", objProposalOL.OriginalBaseRate);
            ObjParam[++i] = new SqlParameter("@ClientCoID", objProposalOL.ClientCoId);
            ObjParam[++i] = new SqlParameter("@SDPRateID", objProposalOL.SDPRateID);
            ObjParam[++i] = new SqlParameter("@ProspectCategory", objProposalOL.ProspectCategory);
            ObjParam[++i] = new SqlParameter("@Remarks", objProposalOL.Remarks);
            ObjParam[++i] = new SqlParameter("@NotApproveYN", objProposalOL.NotApproveYN);
            ObjParam[++i] = new SqlParameter("@BillPeriodID", objProposalOL.BillPeriodID);
            
            object k = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "Prc_InsertProposalDetails", ObjParam);
            status = Convert.ToInt32(k);


            Helperfunction.LogInfoDetails("end SaveProposalDetails , result : " + ss);

        }
        catch (Exception execp)
        {
            status = -1;

            Helperfunction.LogInfoDetails("Error SaveProposalDetails , result : " + ss + ";Error-" + execp);

        }
        return status;
    }
    public int UpdateProposalDetails(OL_Proposal objProposalOL)
    {
        int status = 0;
        int i = 0;
        string ss = "exec Prc_UpdateProposalDetails " + objProposalOL.ProposalDetailId + "," + objProposalOL.PkgTypeId + "," + objProposalOL.CityID + ","
                + objProposalOL.CarCatID + "," + objProposalOL.PKGRate + "," + objProposalOL.PkgKMs + "," +
                objProposalOL.PkgHrs + "," + objProposalOL.ExtraHrRate + "," + objProposalOL.ExtraKMRate + "," +
                objProposalOL.ThresholdExtraHr + "," + objProposalOL.ThresholdExtraKM + "," + objProposalOL.OutStationAllowance + "," +
                objProposalOL.NightStayAllowance + "," + objProposalOL.SysUserId + ",'" + objProposalOL.BillingBasis + "'," +
                objProposalOL.Fgr + "," + objProposalOL.ProspectID + "," + objProposalOL.TempId + ",'" +
                objProposalOL.Remarks + "'," + objProposalOL.OriginalBaseRate + "," + objProposalOL.SDPRateID;
        try
        {
            Helperfunction.LogInfoDetails("Start UpdateProposalDetails");

            SqlParameter[] ObjParam = new SqlParameter[22];
            ObjParam[i] = new SqlParameter("@ProposalDetailId", objProposalOL.ProposalDetailId);
            ObjParam[++i] = new SqlParameter("@PkgTypeId", objProposalOL.PkgTypeId);
            ObjParam[++i] = new SqlParameter("@CityID", objProposalOL.CityID);
            ObjParam[++i] = new SqlParameter("@CarCatID", objProposalOL.CarCatID);
            //ObjParam[++i] = new SqlParameter("@CarModelID", objProposalOL.CarModelID);
            ObjParam[++i] = new SqlParameter("@PKGRate", objProposalOL.PKGRate);
            ObjParam[++i] = new SqlParameter("@PkgKMs", objProposalOL.PkgKMs);
            ObjParam[++i] = new SqlParameter("@PkgHrs", objProposalOL.PkgHrs);
            ObjParam[++i] = new SqlParameter("@ExtraHrRate", objProposalOL.ExtraHrRate);
            ObjParam[++i] = new SqlParameter("@ExtraKMRate", objProposalOL.ExtraKMRate);
            ObjParam[++i] = new SqlParameter("@ThresholdExtraHr", objProposalOL.ThresholdExtraHr);
            ObjParam[++i] = new SqlParameter("@ThresholdExtraKM", objProposalOL.ThresholdExtraKM);
            ObjParam[++i] = new SqlParameter("@OutStationAllowance", objProposalOL.OutStationAllowance);
            ObjParam[++i] = new SqlParameter("@NightStayAllowance", objProposalOL.NightStayAllowance);
            ObjParam[++i] = new SqlParameter("@ModifiedBy", objProposalOL.SysUserId);
            ObjParam[++i] = new SqlParameter("@BillingBasis", objProposalOL.BillingBasis);
            ObjParam[++i] = new SqlParameter("@FGRAmount", objProposalOL.Fgr);
            ObjParam[++i] = new SqlParameter("@ProspectID", objProposalOL.ProspectID);
            ObjParam[++i] = new SqlParameter("@PDTempID", objProposalOL.TempId);
            ObjParam[++i] = new SqlParameter("@Remarks", objProposalOL.Remarks);
            ObjParam[++i] = new SqlParameter("@OriginalPKGRate", objProposalOL.OriginalBaseRate);
            ObjParam[++i] = new SqlParameter("@ClientCoID", objProposalOL.ClientCoId);
            ObjParam[++i] = new SqlParameter("@SDPRateID", objProposalOL.SDPRateID);
            
            //object k = SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "Prc_UpdateProposalDetails", ObjParam);
            object k = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "Prc_UpdateProposalDetails", ObjParam);
            
            Helperfunction.LogInfoDetails("end UpdateProposalDetails , result : " + ss);

            status = Convert.ToInt32(k);

        }
        catch (Exception ee)
        {
            status = -1;
            Helperfunction.LogInfoDetails("end UpdateProposalDetails , result : " + ss);
            Helperfunction.LogErrorDetails(ee.ToString());
        }
        return status;
    }
    public int DeleteProposalDetails(OL_Proposal objProposalOL)
    {
        int status = 0;
        try
        {
            SqlParameter[] ObjParam = new SqlParameter[3];
            ObjParam[0] = new SqlParameter("@ProposalDetailId", objProposalOL.ProposalDetailId);
            ObjParam[1] = new SqlParameter("@PDTempID", objProposalOL.TempId);
            ObjParam[2] = new SqlParameter("@SysUserID", objProposalOL.SysUserId);
            object k = SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "Prc_DeleteProposalDetails", ObjParam);
            status = Convert.ToInt32(k);
        }
        catch (Exception)
        {

            status = -1;
        }
        return status;
    }
    public int SaveSubmitApproval(OL_Proposal objProposalOL)
    {
        int status = 0;
        int i = 0;
        try
        {
            SqlParameter[] ObjParam = new SqlParameter[16];
            ObjParam[i] = new SqlParameter("@ProspectID", objProposalOL.ProspectID);
            ObjParam[++i] = new SqlParameter("@PersonName", objProposalOL.ContactPersonName);
            ObjParam[++i] = new SqlParameter("@PersonEmailId", objProposalOL.ContactPersonEmail);
            ObjParam[++i] = new SqlParameter("@Remarks", objProposalOL.Remarks);
            ObjParam[++i] = new SqlParameter("@FileName", objProposalOL.FileName);
            ObjParam[++i] = new SqlParameter("@SysUserId", objProposalOL.SysUserId);
            ObjParam[++i] = new SqlParameter("@ProposalCity", objProposalOL.ProposalCityId);
            ObjParam[++i] = new SqlParameter("@DateInitition", objProposalOL.DateInitition);
            ObjParam[++i] = new SqlParameter("@ServiceTypeId", objProposalOL.ServiceType);
            ObjParam[++i] = new SqlParameter("@ProposalFor", objProposalOL.ProposalFor);
            ObjParam[++i] = new SqlParameter("@ActionManagerName", objProposalOL.ManagerName);
            ObjParam[++i] = new SqlParameter("@ActionManagerID", objProposalOL.ActionManagerID);
            ObjParam[++i] = new SqlParameter("@Address", objProposalOL.Address);
            ObjParam[++i] = new SqlParameter("@Designation", objProposalOL.Designation);
            ObjParam[++i] = new SqlParameter("@MobileNo", objProposalOL.MobileNo);
            ObjParam[++i] = new SqlParameter("@ClientCoID", objProposalOL.ClientCoId);
            object k = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "InsertProposalApproval", ObjParam);
            status = Convert.ToInt32(k);
        }
        catch (Exception)
        {
            status = -1;
            string ss = objProposalOL.ProspectID + "," + objProposalOL.ContactPersonName + "," + objProposalOL.ContactPersonEmail + ","
                + objProposalOL.Remarks + "," + objProposalOL.FileName + "," + objProposalOL.SysUserId + "," +
                objProposalOL.ProposalCityId + "," + objProposalOL.DateInitition + "," + objProposalOL.ServiceType + "," +
                objProposalOL.ProposalFor + "," + objProposalOL.ManagerName + "," + objProposalOL.ActionManagerID + "," +
                objProposalOL.Address + "," + objProposalOL.Designation + "," + objProposalOL.MobileNo + "," +
                objProposalOL.ClientCoId;

            Helperfunction.LogInfoDetails("end SaveSubmitApproval, result : " + ss);

        }
        return status;
    }
    public int ReplicateProposalDetails(OL_Proposal objProposalOL)
    {
        int status = 0;
        int i = 0;
        try
        {
            SqlParameter[] ObjParam = new SqlParameter[5];
            ObjParam[i] = new SqlParameter("@MultipleCityId", objProposalOL.MultipleCityId);
            ObjParam[++i] = new SqlParameter("@ProspectId", objProposalOL.ProspectID);
            ObjParam[++i] = new SqlParameter("@ProposalDetailId", objProposalOL.ProposalDetailId);
            ObjParam[++i] = new SqlParameter("@SysUserId", objProposalOL.SysUserId);
            ObjParam[++i] = new SqlParameter("@PDTempID", objProposalOL.TempId);
            object k = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "ReplicateProposalDetails", ObjParam);
            status = Convert.ToInt32(k);
        }
        catch (Exception)
        {

            status = -1;
        }
        return status;

    }
    public DataSet GetApprovalDetails(OL_Proposal objProposalOL)
    {
        Helperfunction.LogInfoDetails("Start GetApprovalDetails:" + objProposalOL.ProspectID + "," + objProposalOL.ApprovalStats + "," + objProposalOL.UserID);

        SqlParameter[] ObjParam = new SqlParameter[3];
        ObjParam[0] = new SqlParameter("@ProspectID", objProposalOL.ProspectID);
        ObjParam[1] = new SqlParameter("@ApprovalStats", objProposalOL.ApprovalStats);
        ObjParam[2] = new SqlParameter("@UserID", objProposalOL.UserID);
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_ProposalApproval_Pkg", ObjParam);
    }

    public DataSet GetApprovalDetails_View(OL_Proposal objProposalOL)
    {
        Helperfunction.LogInfoDetails("Start GetApprovalDetails_View:" + objProposalOL.ProspectID);

        
        SqlParameter[] ObjParam = new SqlParameter[1];
        ObjParam[0] = new SqlParameter("@ProspectID", objProposalOL.ProspectID);

        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_GetProposalApprovalDetail", ObjParam);
         
    }

    public DataSet GetApprovalDetails_NoLogin(int intCOOApproval)
    {
        Helperfunction.LogInfoDetails("Start GetApprovalDetails_NoLogin");

        SqlParameter[] ObjParam = new SqlParameter[1];
        ObjParam[0] = new SqlParameter("@intCOOApproval", intCOOApproval);

        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_GetProposalApprovalDetail_NoLogin", ObjParam);

    }

    public int ApprovedProposal_NoLogic(OL_Proposal objProposalOL)
    {
        int status = 0;
        int i = 0;
        try
        {
            
            SqlParameter[] ObjParam = new SqlParameter[3];
            ObjParam[i] = new SqlParameter("@ProposalID", objProposalOL.ProposalID);
            ObjParam[++i] = new SqlParameter("@Remarks", objProposalOL.ApprovalRemark);
            ObjParam[++i] = new SqlParameter("@SysUserID", objProposalOL.SysUserId);
            //object k = SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "Prc_ProposalApproval", ObjParam);
            object k = SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "Prc_ProposalApproval_NoLogic", ObjParam);
            status = (int)k;
        }
        catch (Exception)
        {

            status = -1;
        }
        return status;
    }

    public int ApprovedProposal(OL_Proposal objProposalOL)
    {
        int status = 0;
        int i = 0;
        try
        {
            SqlParameter[] ObjParam = new SqlParameter[4];
            ObjParam[i] = new SqlParameter("@ProposalDetailId", objProposalOL.ProposalDetailId);
            ObjParam[++i] = new SqlParameter("@ApprovalStatus", objProposalOL.ApprovalStatus);
            ObjParam[++i] = new SqlParameter("@Remarks", objProposalOL.ApprovalRemark);
            ObjParam[++i] = new SqlParameter("@UserID", objProposalOL.SysUserId);
            object k = SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "Prc_UpdateProposalApproval_PKG", ObjParam);
            status = (int)k;
        }
        catch (Exception)
        {

            status = -1;
        }
        return status;
    }

    public DataSet GetApprovalRejectionMailDetail(OL_Proposal objProposalOL)
    {
        SqlParameter[] ObjParam = new SqlParameter[1];
        ObjParam[0] = new SqlParameter("@ProposalID", objProposalOL.ProposalID);
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_GetProposalDetailsForRecheck", ObjParam);
    }

    public DataSet PackageInsertion(OL_Proposal objProposalOL)
    {
        SqlParameter[] ObjParam = new SqlParameter[5];
        ObjParam[0] = new SqlParameter("@ProspectId", objProposalOL.ProspectID);
        ObjParam[1] = new SqlParameter("@SysUserId", objProposalOL.SysUserId);
        ObjParam[2] = new SqlParameter("@CarcatId", objProposalOL.PackageCategoryID);
        ObjParam[3] = new SqlParameter("@ProposalFor", objProposalOL.ProposalFor);
        ObjParam[4] = new SqlParameter("@CommitmentID", objProposalOL.CommitmentID);
        
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "ProposalTempInsertion", ObjParam);
    }
    public DataSet GetPackageBaseRate(OL_Proposal objProposalOL)
    {
        SqlParameter[] ObjParam = new SqlParameter[3];
        ObjParam[0] = new SqlParameter("@PackageTypeId", objProposalOL.PkgTypeId);
        ObjParam[1] = new SqlParameter("@CityID", objProposalOL.CityID);
        ObjParam[2] = new SqlParameter("@CarCatID", objProposalOL.CarCatID);
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_GetPackageBaseRate", ObjParam);
    }
    public DataSet GetCityAccessBasis(int sysUserId)
    {

        SqlParameter[] ObjParam = new SqlParameter[1];
        ObjParam[0] = new SqlParameter("@UserID", sysUserId);
        //return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_GetCityAccessBasis", ObjParam);
        // return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_GetCityAccessBasis_PanIndia", ObjParam);
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_GetCityAccessBasis_WithOutPanIndia", ObjParam);
    }

    public DataSet GetCategory()
    {
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "ProposalGetcarCategoryList");
    }
    public DataSet GetClientCommitment()
    {
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "prc_GetClientCommitment");
    }
    public DataSet GetCreditPeriod()
    {
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "prc_GetCreditPeriod");
    }
    public int InsertStandardPackage(OL_PKG objPkg)
    {
        int status = 0;
        int i = 0;
        try
        {
            SqlParameter[] ObjParam = new SqlParameter[17];
            ObjParam[i] = new SqlParameter("@CarCatID", objPkg.CarCatID);
            ObjParam[++i] = new SqlParameter("@ExtraHrRate", objPkg.ExtraHrRate);
            ObjParam[++i] = new SqlParameter("@ExtraKMRate", objPkg.ExtraKMRate);
            ObjParam[++i] = new SqlParameter("@ThresholdExtraHr", objPkg.ThresholdExtraHr);
            ObjParam[++i] = new SqlParameter("@ThresholdExtraKM", objPkg.ThresholdExtraKM);
            ObjParam[++i] = new SqlParameter("@OutStationAllowance", objPkg.OutStationAllowance);
            ObjParam[++i] = new SqlParameter("@NightStayAllowance", objPkg.NightStayAllowance);
            ObjParam[++i] = new SqlParameter("@CreatedBy", objPkg.CreatedBy);
            ObjParam[++i] = new SqlParameter("@ProposalFor", objPkg.ProposalFor);
            ObjParam[++i] = new SqlParameter("@CityID", objPkg.CityID);
            ObjParam[++i] = new SqlParameter("@BaseRate", objPkg.BaseRate);
            ObjParam[++i] = new SqlParameter("@PkgHrs", objPkg.PkgHrs);
            ObjParam[++i] = new SqlParameter("@PkgKMs", objPkg.PkgKMs);
            ObjParam[++i] = new SqlParameter("@ProspectCategory", objPkg.ProspectCategory);
            ObjParam[++i] = new SqlParameter("@CommitmentId", objPkg.CommitmentId);
            ObjParam[++i] = new SqlParameter("@VendorPKGRate", objPkg.VendorPKGRate);
            ObjParam[++i] = new SqlParameter("@BillingBasis", objPkg.BillingBasis);
            object k = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "Prc_InsertStandardPackageRate", ObjParam);
            status = Convert.ToInt32(k);
        }
        catch (Exception)
        {
            status = -1;
        }
        return status;

    }
    public DataSet GetStandardPackageForEdit(OL_PKG objPkg)
    {
        SqlParameter[] ObjParam = new SqlParameter[4];
        ObjParam[0] = new SqlParameter("@CityID", objPkg.CityID);
        ObjParam[1] = new SqlParameter("@ProspectCategory", objPkg.ProspectCategory);
        ObjParam[2] = new SqlParameter("@CarCatID", objPkg.CarCatID);
        ObjParam[3] = new SqlParameter("@ProposalFor", objPkg.ProposalFor);
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_GetStandardPackageForEdit", ObjParam);
    }

    public OL_PKG GetStandardPackageDetails(OL_PKG objPkg)
    {
        DataSet ds = new DataSet();
        SqlParameter[] ObjParam = new SqlParameter[1];
        ObjParam[0] = new SqlParameter("@SDPRateId", objPkg.SDPRateId);
        ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_GetStandardPackageDetails", ObjParam);
        if (ds.Tables[0].Rows.Count > 0)
        {
            objPkg.CityID = Convert.ToInt32(ds.Tables[0].Rows[0]["CityID"]);
            objPkg.ProspectCategory = Convert.ToString(ds.Tables[0].Rows[0]["ProspectCategory"]);
            objPkg.CarCatID = Convert.ToInt32(ds.Tables[0].Rows[0]["CarCatID"]);
            objPkg.ProposalFor = Convert.ToInt32(ds.Tables[0].Rows[0]["ProposalFor"]);
            objPkg.BaseRate = Convert.ToDouble(ds.Tables[0].Rows[0]["BaseRate"]);
            objPkg.BaseHr = Convert.ToDouble(ds.Tables[0].Rows[0]["BaseHr"]);
            objPkg.BaseKM = Convert.ToDouble(ds.Tables[0].Rows[0]["BaseKM"]);

            objPkg.PkgHrs = Convert.ToDouble(ds.Tables[0].Rows[0]["PkgHrs"]);
            objPkg.PkgKMs = Convert.ToDouble(ds.Tables[0].Rows[0]["PkgKMs"]);

            objPkg.ExtraHrRate = Convert.ToDouble(ds.Tables[0].Rows[0]["ExtraHrRate"]);
            objPkg.ExtraKMRate = Convert.ToDouble(ds.Tables[0].Rows[0]["ExtraKMRate"]);
            objPkg.ThresholdExtraHr = Convert.ToInt32(ds.Tables[0].Rows[0]["ThresholdExtraHr"]);
            objPkg.ThresholdExtraKM = Convert.ToInt32(ds.Tables[0].Rows[0]["ThresholdExtraKM"]);
            objPkg.OutStationAllowance = Convert.ToDouble(ds.Tables[0].Rows[0]["OutStationAllowance"]);
            objPkg.NightStayAllowance = Convert.ToDouble(ds.Tables[0].Rows[0]["NightStayAllowance"]);
            objPkg.CommitmentId = Convert.ToInt32(ds.Tables[0].Rows[0]["CommitmentID"]);
            objPkg.VendorPKGRate = Convert.ToDouble(ds.Tables[0].Rows[0]["PurchasePKGRate"]);
            objPkg.BillingBasis = Convert.ToString(ds.Tables[0].Rows[0]["BillingBasis"]);
        }
        return objPkg;
    }

    public int UpdateStandardPackage(OL_PKG objPkg)
    {
        int status = 0;
        int i = 0;
        try
        {
            SqlParameter[] ObjParam = new SqlParameter[14];
            ObjParam[i] = new SqlParameter("@SDPRateId", objPkg.SDPRateId);
            ObjParam[++i] = new SqlParameter("@BaseRate", objPkg.BaseRate);
            ObjParam[++i] = new SqlParameter("@PkgHrs", objPkg.PkgHrs);
            ObjParam[++i] = new SqlParameter("@PkgKMs", objPkg.PkgKMs);
            ObjParam[++i] = new SqlParameter("@ExtraHrRate", objPkg.ExtraHrRate);
            ObjParam[++i] = new SqlParameter("@ExtraKMRate", objPkg.ExtraKMRate);
            ObjParam[++i] = new SqlParameter("@ThresholdExtraHr", objPkg.ThresholdExtraHr);
            ObjParam[++i] = new SqlParameter("@ThresholdExtraKM", objPkg.ThresholdExtraKM);
            ObjParam[++i] = new SqlParameter("@OutStationAllowance", objPkg.OutStationAllowance);
            ObjParam[++i] = new SqlParameter("@NightStayAllowance", objPkg.NightStayAllowance);
            ObjParam[++i] = new SqlParameter("@ModifiedBy", objPkg.ModifiedBy);
            ObjParam[++i] = new SqlParameter("@CommitmentId", objPkg.CommitmentId);
            ObjParam[++i] = new SqlParameter("@VendorPKGRate", objPkg.VendorPKGRate);
            ObjParam[++i] = new SqlParameter("@BillingBasis", objPkg.BillingBasis);
            status = SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "Prc_UpdateStandardPackage", ObjParam);
        }
        catch (Exception)
        {
            status = 0;
        }
        return status;
    }

    public DataSet PackageInsertionNew(OL_Proposal objProposalOL)
    {
        SqlParameter[] ObjParam = new SqlParameter[4];
        ObjParam[0] = new SqlParameter("@ProspectId", objProposalOL.ProspectID);
        ObjParam[1] = new SqlParameter("@SysUserId", objProposalOL.SysUserId);
        ObjParam[2] = new SqlParameter("@CarcatId", objProposalOL.PackageCategoryID);
        ObjParam[3] = new SqlParameter("@ProposalFor", objProposalOL.ProposalFor);
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "ProposalTempInsertion_Nedw", ObjParam);
    }

    public DataSet ProposalCityMapping(string ProposalMapping)
    {
        SqlParameter[] ObjParam = new SqlParameter[1];
        ObjParam[0] = new SqlParameter("@ProposalMapping", ProposalMapping);

        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "prc_GetProposalCityMapping", ObjParam);
    } 

}