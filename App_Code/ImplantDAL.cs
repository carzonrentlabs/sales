﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
/// <summary>
/// Summary description for DRMDAL
/// </summary>
public class ImplantDAL
{
    #region StaticFields
    static private string _connection;
    #endregion
    static ImplantDAL()
	{
        
         //_connection = "Data Source=103.11.85.78,1433;Initial Catalog=DRM;User ID=carzonrent;PWD=CarZ#58B9;Max Pool Size=7500";
        _connection = ConfigurationManager.ConnectionStrings["CRDMenu"].ConnectionString;
	}
     
    #region StaticMethods

    static public SqlDataReader ExecuteReader(string SProc, params SqlParameter[] Parameters)
    {
        SqlDataReader reader = null;
        SqlConnection mycon = new SqlConnection(_connection);

        mycon.Open();
        using (SqlCommand command = new SqlCommand(SProc, mycon))
        {
            command.CommandType = CommandType.StoredProcedure;
            command.CommandTimeout = 600;

            foreach (SqlParameter param in Parameters)
            {
                command.Parameters.Add(param);
            }
            reader = command.ExecuteReader(CommandBehavior.CloseConnection);
            command.Parameters.Clear();
        }
        return reader;


    }

    static public int ExecuteNonQuery(string SProc, params SqlParameter[] Parameters)
    {
        int result = 0;
        using (SqlConnection mycon = new SqlConnection(_connection))
        {
            mycon.Open();
            using (SqlCommand command = new SqlCommand(SProc, mycon))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandTimeout = 600;
                foreach (SqlParameter param in Parameters)
                {
                    command.Parameters.Add(param);
                }
                result = command.ExecuteNonQuery();
                command.Parameters.Clear();
            }

        }
        return result;

    }

    static public DataSet GetDataSet(string SProc)
    {
        DataSet ResultSet = new DataSet();

        using (SqlConnection mycon = new SqlConnection(_connection))
        {
            mycon.Open();
            using (SqlCommand command = new SqlCommand(SProc, mycon))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandTimeout = 600;

                SqlDataAdapter sda = new SqlDataAdapter();
                sda.SelectCommand = command;
                sda.Fill(ResultSet);
            }
        }
        return ResultSet;
    }

    static public DataSet GetDataSet(string SProc, params SqlParameter[] Parameters)
    {
        DataSet ResultSet = new DataSet();
        using (SqlConnection mycon = new SqlConnection(_connection))
        {
            mycon.Open();
            using (SqlCommand command = new SqlCommand(SProc, mycon))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandTimeout = 600;
                foreach (SqlParameter param in Parameters)
                {
                    command.Parameters.Add(param);
                }
                SqlDataAdapter sda = new SqlDataAdapter();
                sda.SelectCommand = command;
                sda.Fill(ResultSet);
                command.Parameters.Clear();
            }

        }
        return ResultSet;

    }

    static public object ExecuteScalar(string SProc, params SqlParameter[] Parameters)
    {
        object result = 0;
        using (SqlConnection mycon = new SqlConnection(_connection))
        {
            mycon.Open();
            using (SqlCommand command = new SqlCommand(SProc, mycon))
            {

                command.CommandType = CommandType.StoredProcedure;
                command.CommandTimeout = 600;

                foreach (SqlParameter param in Parameters)
                {
                    command.Parameters.Add(param);
                }
                result = command.ExecuteScalar();
                command.Parameters.Clear();
            }

        }
        return result;

    }

    #endregion 
}