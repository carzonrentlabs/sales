using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Insta;

public class CorporateModule
{
	public CorporateModule()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    
    public DataSet GetAccountManager()
    {
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_getUserList");
    }
    public DataSet GetImplant()
    {
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_GetImplant");
    }

    public DataSet GetClientName()
    {
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_GetClientList");
        //return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_GetSalesClientList");
    }
    public DataSet GetCityName()
    {
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "GetCityForSalesCorporate");
    }
   
    public DataSet GetCompetitor()
    {
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_GetComCompetitor");
    }
    public DataSet CheckLoginValidate(string loginId)
    {
        SqlParameter[] ObjparamUser = new SqlParameter[1];
        ObjparamUser[0] = new SqlParameter("@LoginId", loginId);
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "CheckLoginIdExist", ObjparamUser);
    }

    public DataSet GetAccountType(int ClientCoId,int CityID)
    {
        SqlParameter[] ObjparamUser = new SqlParameter[2];
        ObjparamUser[0] = new SqlParameter("@ClientCoId", ClientCoId);
        ObjparamUser[1] = new SqlParameter("@CityID", CityID);
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "GetSaleAccountType", ObjparamUser);
    }

    public DataSet GetSaleHeadQuarter(int ClientCoId,int AccountTypeId)
    {
        SqlParameter[] ObjparamUser = new SqlParameter[2];
        ObjparamUser[0] = new SqlParameter("@ClientCoId", ClientCoId);
        ObjparamUser[1] = new SqlParameter("@AccountTypeId", AccountTypeId);
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "GetSaleHeadQuarter", ObjparamUser);
    }
    //public DataSet GetSaleHeadQuarter(int ClientCoId, int CityID)
    //{
    //    SqlParameter[] ObjparamUser = new SqlParameter[2];
    //    ObjparamUser[0] = new SqlParameter("@ClientCoId", ClientCoId);
    //    ObjparamUser[1] = new SqlParameter("@CityID", CityID);
    //    return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "GetSaleHeadQuarter", ObjparamUser);
    //}
    public DataSet GetSingedBy(int ClientCoId, int CityID)
    {
        SqlParameter[] ObjparamUser = new SqlParameter[2];
        ObjparamUser[0] = new SqlParameter("@ClientCoId", ClientCoId);
        ObjparamUser[1] = new SqlParameter("@CityID", CityID);
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "GetSalesCorportateSingedBy", ObjparamUser);
    }

    public DataSet GetLocalAccountMgr(int ClientCoId, int CityID)
    {
        SqlParameter[] ObjparamUser = new SqlParameter[2];
        ObjparamUser[0] = new SqlParameter("@ClientCoId", ClientCoId);
        ObjparamUser[1] = new SqlParameter("@CityID", CityID);
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "GetLocalAccountMgr", ObjparamUser);
    }
    public DataSet GetTargetAmount(int ClientCoId, int CityID,int Year,int Month)
    {
        SqlParameter[] ObjparamUser = new SqlParameter[4];
        ObjparamUser[0] = new SqlParameter("@ClientCoId", ClientCoId);
        ObjparamUser[1] = new SqlParameter("@CityID", CityID);
        ObjparamUser[2] = new SqlParameter("@Year", Year);
        ObjparamUser[3] = new SqlParameter("@MonthId", Month);
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "GetGargetAmount", ObjparamUser);
    }
    public DataSet GetLocalInfluencer(int ClientCoId, int CityID)
    {
        SqlParameter[] ObjparamUser = new SqlParameter[2];
        ObjparamUser[0] = new SqlParameter("@ClientCoId", ClientCoId);
        ObjparamUser[1] = new SqlParameter("@CityID", CityID);
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "GetLocalInfluencer", ObjparamUser);
    }
    public DataSet GetCompetitorListClientBasis(int ClientCoId)
    {
        SqlParameter[] ObjparamUser = new SqlParameter[1];
        ObjparamUser[0] = new SqlParameter("@ClientCoId", ClientCoId);
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "GetSalesImplantCompetitor", ObjparamUser);
    }
    public DataSet GetGroupEntityOfClienet(int ClientCoId)
    {
        SqlParameter[] ObjparamUser = new SqlParameter[1];
        ObjparamUser[0] = new SqlParameter("@ClientCoId", ClientCoId);
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_GetClientGroupEntity", ObjparamUser);
    }

    public DataSet GetImplantsClientBasis(int ClientCoId, int CityID)
    {
        SqlParameter[] ObjparamUser = new SqlParameter[2];
        ObjparamUser[0] = new SqlParameter("@ClientCoId", ClientCoId);
        ObjparamUser[1] = new SqlParameter("@CityID", CityID);
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "GetSalesImplant", ObjparamUser);
    }

    public int SaveCorintSalesCorporate(SaleModule objSales)
    {
        int status = 0; 
        try
        {
            SqlParameter[] ObjparamUser = new SqlParameter[24];
            ObjparamUser[0] = new SqlParameter("@ClientCoID", objSales.ClientCoId);
            ObjparamUser[1] = new SqlParameter("@AccountType", objSales.AccountType);
            ObjparamUser[2] = new SqlParameter("@HeadQuarter", objSales.HQ);
            ObjparamUser[3] = new SqlParameter("@SignedBy", objSales.SignedBy);
            ObjparamUser[4] = new SqlParameter("@LocalInfluencer", objSales.LocalInfluencer);
            ObjparamUser[5] = new SqlParameter("@MonthlyPotential", objSales.MonthlyPotential);
            ObjparamUser[6] = new SqlParameter("@LocalAccountMgr", objSales.LocalAccountMgr);
            ObjparamUser[7] = new SqlParameter("@CreatedBy", objSales.SysUserID);
            ObjparamUser[8] = new SqlParameter("@LocalInfluencerName", objSales.LocalInfluencerName);
            ObjparamUser[9] = new SqlParameter("@LocalInfluencerMobileNo", objSales.LocalInfluencerMobileNo);
            ObjparamUser[10] = new SqlParameter("@ImplantId", objSales.ImplantId);
            ObjparamUser[11] = new SqlParameter("@Competitor", objSales.Competitor);
            ObjparamUser[12] = new SqlParameter("@ImplantSysUserId", objSales.ImplantSysUserId);
            ObjparamUser[13] = new SqlParameter("@NewImpantName", objSales.NewImplantName);
            ObjparamUser[14] = new SqlParameter("@GroupEnityName", objSales.GroupEnityName);
            ObjparamUser[15] = new SqlParameter("@Target_Year", objSales.Target_Year);
            ObjparamUser[16] = new SqlParameter("@Target_Month", objSales.Target_Month);
            ObjparamUser[17] = new SqlParameter("@Target_Amount", objSales.Target_Amount);
            ObjparamUser[18] = new SqlParameter("@Target_Amount_PanIndia", objSales.Target_Amount_PanIndia);
            ObjparamUser[19] = new SqlParameter("@Target_FinancialYear", objSales.Target_FinancialYear);
            ObjparamUser[20] = new SqlParameter("@Target_FinancialMonth", objSales.Target_FinancialMonth);
            ObjparamUser[21] = new SqlParameter("@Target_FinancialMonthID", objSales.Target_FinancialMonthID);
            ObjparamUser[22] = new SqlParameter("@isPriority", objSales.IsPriority);
            ObjparamUser[23] = new SqlParameter("@PipeLineRev", objSales.PipeLineRevenue);
            object k = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "Sp_CorintSalesCorporate_New_30_11_2016", ObjparamUser);
            status =Convert.ToInt32(k);
            //return SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "Sp_SaveProspects_New", ObjparamUser);
            
        }
        catch (Exception)
        {
            status = 0; 
        }
        
        return status;
    }


    public DataSet EntryDetails(int ClientCoId)
    {
        SqlParameter[] ObjparamUser = new SqlParameter[2];
        ObjparamUser[0] = new SqlParameter("@ClientCoID", ClientCoId);
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "GetSalesCorporateDetails", ObjparamUser);
    }
    //public DataSet EntryDetailsExport(int ClientCoId)
    //{
    //    SqlParameter[] ObjparamUser = new SqlParameter[2];
    //    ObjparamUser[0] = new SqlParameter("@ClientCoID", ClientCoId);
    //    return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "GetSalesCorporateDetailsExport", ObjparamUser);
    //}
    public DataTable GetMonthName()
    {
        return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "Get_MonthName");
    }

    public DataSet GetMonthWiseAmount(int ClientCoId, int HeadQuarter,string  TargetFinanncialYear)
    {
        SqlParameter[] ObjparamUser = new SqlParameter[3];
        ObjparamUser[0] = new SqlParameter("@ClientCoId", ClientCoId);
        ObjparamUser[1] = new SqlParameter("@HeadQuarter", HeadQuarter);
        ObjparamUser[2] = new SqlParameter("@TargetFinanncialYear", TargetFinanncialYear);
        //return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "dbo.getMonthwiseTransactionDetails", ObjparamUser);
        //return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "getMonthwiseTransactionDetails_New", ObjparamUser);
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "getMonthwiseTransactionDetails_BK", ObjparamUser);
    }

    public DataSet GetProspects(int cityId)
    {
        SqlParameter[] ObjparamUser = new SqlParameter[1];
        ObjparamUser[0] = new SqlParameter("@CityId", cityId);
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_GetProspectMaster", ObjparamUser);
    }

    public DataSet GetMonthWiseAmountForAcquisition(int ProspectID, int HeadQuarter, string TargetFinanncialYear)
    {
        SqlParameter[] ObjparamUser = new SqlParameter[3];
        ObjparamUser[0] = new SqlParameter("@ProspectID", ProspectID);
        ObjparamUser[1] = new SqlParameter("@HeadQuarter", HeadQuarter);
        ObjparamUser[2] = new SqlParameter("@TargetFinanncialYear", TargetFinanncialYear);
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_getMonthwiseTransactionAcquisition", ObjparamUser);
    }

    public DataSet GetAccountTypeAcqisition(int ProspectID, int CityID)
    {
        SqlParameter[] ObjparamUser = new SqlParameter[2];
        ObjparamUser[0] = new SqlParameter("@ProspectID", ProspectID);
        ObjparamUser[1] = new SqlParameter("@CityID", CityID);
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "GetSaleAccountTypeAcquisition", ObjparamUser);
    }
   
    public DataSet GetSingedByAcqisition(int ProspectID, int CityID)
    {
        SqlParameter[] ObjparamUser = new SqlParameter[2];
        ObjparamUser[0] = new SqlParameter("@ProspectID", ProspectID);
        ObjparamUser[1] = new SqlParameter("@CityID", CityID);
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "GetSalesCorportateSingedByAcquisition", ObjparamUser);
    }
    public DataSet GetLocalInfluencerAcqisition(int ProspectID, int CityID)
    {
        SqlParameter[] ObjparamUser = new SqlParameter[2];
        ObjparamUser[0] = new SqlParameter("@ProspectID", ProspectID);
        ObjparamUser[1] = new SqlParameter("@CityID", CityID);
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "GetLocalInfluencerAcquisition", ObjparamUser);
    }
    public DataSet GetLocalAccountMgrAcqisition(int ProspectID, int CityID)
    {
        SqlParameter[] ObjparamUser = new SqlParameter[2];
        ObjparamUser[0] = new SqlParameter("@ProspectID", ProspectID);
        ObjparamUser[1] = new SqlParameter("@CityID", CityID);
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "GetLocalAccountMgrAcquisition", ObjparamUser);
    }

    public int SaveCorintAcquistionCorporate(SaleModule objSales)
    {
        int status = 0;
        try
        {
            SqlParameter[] ObjparamUser = new SqlParameter[18];
            ObjparamUser[0] = new SqlParameter("@ClientCoID", objSales.ClientCoId);
            ObjparamUser[1] = new SqlParameter("@AccountType", objSales.AccountType);
            ObjparamUser[2] = new SqlParameter("@HeadQuarter", objSales.HQ);
            ObjparamUser[3] = new SqlParameter("@SignedBy", objSales.SignedBy);
            ObjparamUser[4] = new SqlParameter("@LocalInfluencer", objSales.LocalInfluencer);
            ObjparamUser[5] = new SqlParameter("@MonthlyPotential", objSales.MonthlyPotential);
            ObjparamUser[6] = new SqlParameter("@LocalAccountMgr", objSales.LocalAccountMgr);
            ObjparamUser[7] = new SqlParameter("@CreatedBy", objSales.SysUserID);
            ObjparamUser[8] = new SqlParameter("@LocalInfluencerName", objSales.LocalInfluencerName);
            ObjparamUser[9] = new SqlParameter("@LocalInfluencerMobileNo", objSales.LocalInfluencerMobileNo);
            ObjparamUser[10] = new SqlParameter("@Target_Year", objSales.Target_Year);
            ObjparamUser[11] = new SqlParameter("@Target_Month", objSales.Target_Month);
            ObjparamUser[12] = new SqlParameter("@Target_Amount", objSales.Target_Amount);
            ObjparamUser[13] = new SqlParameter("@Target_Amount_PanIndia", objSales.Target_Amount_PanIndia);
            ObjparamUser[14] = new SqlParameter("@Target_FinancialYear", objSales.Target_FinancialYear);
            ObjparamUser[15] = new SqlParameter("@Target_FinancialMonth", objSales.Target_FinancialMonth);
            ObjparamUser[16] = new SqlParameter("@Target_FinancialMonthID", objSales.Target_FinancialMonthID);
            ObjparamUser[17] = new SqlParameter("@ProspectID", objSales.ProposalId);
            object k = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "Prc_InsertAcquisitions", ObjparamUser);
            status = Convert.ToInt32(k);
        }
        catch (Exception)
        {
            status = 0;
        }

        return status;
    }
}
