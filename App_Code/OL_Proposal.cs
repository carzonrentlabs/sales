﻿using System;
using System.Collections.Generic;
using System.Web;

/// <summary>
/// Summary description for OL_Proposal
/// </summary>
public class OL_Proposal : OL_LoginUser
{

    #region Varialble

    private int pkgTypeId;
    private int cityID;
    private int carCatID;
    private int carModelID;
    private double pKGRate;
    private double pkgKMs;
    private double pkgHrs;
    private double extraHrRate;
    private double extraKMRate;
    private double thresholdExtraHr;
    private double thresholdExtraKM;
    private double outStationAllowance;
    private double nightStayAllowance;
    private DateTime? createDate;
    private int createdBy;
    private int _prospectID;
    private DateTime _dateInitition;
    private int _serviceType;
    private int _proposalFor;
    private string _ManagerName;
    private int _proposalDetailId;
    private int _proposalID;
    private string _contactPersonName;
    private string _contactPersonEmail;
    private string _remarks;
    private string _fileName;
    private string _multipleCityId;
    private int _proposalCityId;
    private string _proposalStatus;
    private string _approvalStatus;
    private string _approvalRemark;
    private int _actionManagerID;
    private string _billingBasis;
    private double? _fgr;
    

    
    private string _address;
    private string _designation;
    private string _mobileNo;
    private int _tempId;
    private string _packageCategoryID;
    private double _baseRate;
    private double baseHr;
    private double baseKM;
    private string prospectCategory;
    private double originalbaserate;
    private int _commitmentid;
    private int _userid;
    private int _clientcoid;
    private int _approvalstats;

    private int _sdprateid;
    private int _sourcecityid;
    private bool _notapproveyn;
    private int _billperiodid;
    #endregion

    public bool NotApproveYN
    {
        get { return _notapproveyn; }
        set { _notapproveyn = value; }
    }

    public int SourceCityID
    {
        get { return _sourcecityid; }
        set { _sourcecityid = value; }
    }

    public int SDPRateID
    {
        get { return _sdprateid; }
        set { _sdprateid = value; }
    }

    public int ClientCoId
    {
        get { return _clientcoid; }
        set { _clientcoid = value; }
    }

    public int ApprovalStats
    {
        get { return _approvalstats; }
        set { _approvalstats = value; }
    }

    public int UserID
    {
        get { return _userid; }
        set { _userid = value; }
    }

    public int CommitmentID
    {
        get { return _commitmentid; }
        set { _commitmentid = value; }
    }

    public double OriginalBaseRate
    {
        get { return originalbaserate; }
        set { originalbaserate = value; }
    }

    public double BaseKM
    {
        get { return baseKM; }
        set { baseKM = value; }
    }

    public double BaseHr
    {
        get { return baseHr; }
        set { baseHr = value; }
    }
    public double BaseRate
    {
        get { return _baseRate; }
        set { _baseRate = value; }
    }
    public string PackageCategoryID
    {
        get { return _packageCategoryID; }
        set { _packageCategoryID = value; }
    }
    public int TempId
    {
        get { return _tempId; }
        set { _tempId = value; }
    }
    public double? Fgr
    {
        get { return _fgr; }
        set { _fgr = value; }
    }
    public string MobileNo
    {
        get { return _mobileNo; }
        set { _mobileNo = value; }
    }
    public string Designation
    {
        get { return _designation; }
        set { _designation = value; }
    }
    public string Address
    {
        get { return _address; }
        set { _address = value; }
    }


    public string BillingBasis
    {
        get { return _billingBasis; }
        set { _billingBasis = value; }
    }

    public int ActionManagerID
    {
        get { return _actionManagerID; }
        set { _actionManagerID = value; }
    }

    public string ApprovalRemark
    {
        get { return _approvalRemark; }
        set { _approvalRemark = value; }
    }
    public string ApprovalStatus
    {
        get { return _approvalStatus; }
        set { _approvalStatus = value; }
    }
    public string ProposalStatus
    {
        get { return _proposalStatus; }
        set { _proposalStatus = value; }
    }
    public string ContactPersonEmail
    {
        get { return _contactPersonEmail; }
        set { _contactPersonEmail = value; }
    }
    public string MultipleCityId
    {
        get { return _multipleCityId; }
        set { _multipleCityId = value; }
    }

    public string FileName
    {
        get { return _fileName; }
        set { _fileName = value; }
    }

    public int ProposalDetailId
    {
        get { return _proposalDetailId; }
        set { _proposalDetailId = value; }
    }
    public string ContactPersonName
    {
        get { return _contactPersonName; }
        set { _contactPersonName = value; }
    }

    public string Remarks
    {
        get { return _remarks; }
        set { _remarks = value; }
    }
    public int ProposalID
    {
        get { return _proposalID; }
        set { _proposalID = value; }
    }
    public int PkgTypeId
    {
        get { return pkgTypeId; }
        set { pkgTypeId = value; }
    }


    public int CityID
    {
        get { return cityID; }
        set { cityID = value; }
    }


    public int CarCatID
    {
        get { return carCatID; }
        set { carCatID = value; }
    }


    public int CarModelID
    {
        get { return carModelID; }
        set { carModelID = value; }
    }


    public double PKGRate
    {
        get { return pKGRate; }
        set { pKGRate = value; }
    }


    public double PkgKMs
    {
        get { return pkgKMs; }
        set { pkgKMs = value; }
    }


    public double PkgHrs
    {
        get { return pkgHrs; }
        set { pkgHrs = value; }
    }


    public double ExtraHrRate
    {
        get { return extraHrRate; }
        set { extraHrRate = value; }
    }


    public double ExtraKMRate
    {
        get { return extraKMRate; }
        set { extraKMRate = value; }
    }


    public double ThresholdExtraHr
    {
        get { return thresholdExtraHr; }
        set { thresholdExtraHr = value; }
    }


    public double ThresholdExtraKM
    {
        get { return thresholdExtraKM; }
        set { thresholdExtraKM = value; }
    }


    public double OutStationAllowance
    {
        get { return outStationAllowance; }
        set { outStationAllowance = value; }
    }


    public double NightStayAllowance
    {
        get { return nightStayAllowance; }
        set { nightStayAllowance = value; }
    }


    //public DateTime CreateDate
    //{
    //    get { return createDate; }
    //    set { createDate = value; }
    //}
    public DateTime? CreateDate
    {
        get { return createDate; }
        set { createDate = value; }
    }


    public int CreatedBy
    {
        get { return createdBy; }
        set { createdBy = value; }
    }



    public int ProspectID
    {
        get { return _prospectID; }
        set { _prospectID = value; }
    }


    public int ProposalCityId
    {
        get { return _proposalCityId; }
        set { _proposalCityId = value; }
    }


    public DateTime DateInitition
    {
        get { return _dateInitition; }
        set { _dateInitition = value; }
    }


    public int ServiceType
    {
        get { return _serviceType; }
        set { _serviceType = value; }
    }


    public int ProposalFor
    {
        get { return _proposalFor; }
        set { _proposalFor = value; }
    }


    public string ManagerName
    {
        get { return _ManagerName; }
        set { _ManagerName = value; }
    }
    public string ProspectCategory
    {
        get { return prospectCategory; }
        set { prospectCategory = value; }
    }

    public int BillPeriodID
    {
        get { return _billperiodid; }
        set { _billperiodid = value; }
    }

    /*
    public int PkgTypeId {get;set;}
    public int CityID { get; set; }
    public int CarCatID { get; set; }
    public int CarModelID { get; set; }
    public double PKGRate { get; set; }
    public double PkgKMs { get; set; }
    public double PkgHrs { get; set; }
    public double ExtraHrRate { get; set; }
    public double ExtraKMRate { get; set; }
    public double ThresholdExtraHr { get; set; }
    public double ThresholdExtraKM { get; set; }
    public double OutStationAllowance { get; set; }
    public double NightStayAllowance { get; set; }
    public DateTime CreateDate { get; set; }
    public int CreatedBy { get; set; } 
     */


}