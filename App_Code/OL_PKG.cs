﻿using System;
using System.Collections.Generic;
using System.Web;

/// <summary>
/// Summary description for OL_PKG
/// </summary>
public class OL_PKG
{
    
    private int sDPRateId;
    private int carCatID;
    private int carModelID;
    private double pKGRate;
    private double pkgKMs;
    private double pkgHrs;
    private double extraHrRate;
    private double extraKMRate;
    private double thresholdExtraHr;
    private double thresholdExtraKM;
    private double outStationAllowance;
    private double nightStayAllowance;
    private DateTime createDate;
    private double baseRate;
    private int modifiedBy;
    private int createdBy;
    private int proposalFor;
    private int cityID;
    private DateTime modifyDate;
    private bool active;
    private double baseHr;
    private double baseKM;
    private string prospectCategory;
    private int pkgID;

    private int commitmentId;
    private double vendorpkgrate;
    private string _billingbasis;
    private int _sourcecityid;

    public int SourceCityID
    {
        get { return _sourcecityid; }
        set { _sourcecityid = value; }
    }

    public string BillingBasis
    {
        get { return _billingbasis; }
        set { _billingbasis = value; }
    }

    public int CommitmentId
    {
        get { return commitmentId; }
        set { commitmentId = value; }
    }

    public double VendorPKGRate
    {
        get { return vendorpkgrate; }
        set { vendorpkgrate = value; }
    }

    public int PkgID
    {
        get { return pkgID; }
        set { pkgID = value; }
    }
    

    public int SDPRateId
    {
        get { return sDPRateId; }
        set { sDPRateId = value; }
    }
    public int CarModelID
    {
        get { return carModelID; }
        set { carModelID = value; }
    }

    public double PKGRate
    {
        get { return pKGRate; }
        set { pKGRate = value; }
    }


    public double PkgKMs
    {
        get { return pkgKMs; }
        set { pkgKMs = value; }
    }


    public double PkgHrs
    {
        get { return pkgHrs; }
        set { pkgHrs = value; }
    }


    public double ExtraHrRate
    {
        get { return extraHrRate; }
        set { extraHrRate = value; }
    }


    public double ExtraKMRate
    {
        get { return extraKMRate; }
        set { extraKMRate = value; }
    }


    public double ThresholdExtraHr
    {
        get { return thresholdExtraHr; }
        set { thresholdExtraHr = value; }
    }


    public double ThresholdExtraKM
    {
        get { return thresholdExtraKM; }
        set { thresholdExtraKM = value; }
    }


    public double OutStationAllowance
    {
        get { return outStationAllowance; }
        set { outStationAllowance = value; }
    }


    public double NightStayAllowance
    {
        get { return nightStayAllowance; }
        set { nightStayAllowance = value; }
    }


    public DateTime CreateDate
    {
        get { return createDate; }
        set { createDate = value; }
    }


    public int CreatedBy
    {
        get { return createdBy; }
        set { createdBy = value; }
    }


    public DateTime ModifyDate
    {
        get { return modifyDate; }
        set { modifyDate = value; }
    }


    public int ModifiedBy
    {
        get { return modifiedBy; }
        set { modifiedBy = value; }
    }


    public int ProposalFor
    {
        get { return proposalFor; }
        set { proposalFor = value; }
    }


    public int CityID
    {
        get { return cityID; }
        set { cityID = value; }
    }


    public bool Active
    {
        get { return active; }
        set { active = value; }
    }


    public double BaseRate
    {
        get { return baseRate; }
        set { baseRate = value; }
    }


    public double BaseHr
    {
        get { return baseHr; }
        set { baseHr = value; }
    }


    public double BaseKM
    {
        get { return baseKM; }
        set { baseKM = value; }
    }


    public string ProspectCategory
    {
        get { return prospectCategory; }
        set { prospectCategory = value; }
    }


    public int CarCatID
    {
        get { return carCatID; }
        set { carCatID = value; }
    }




}