﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Insta;
using System.Configuration;

public partial class Proposal_RateCard : System.Web.UI.Page
{
    private sqlProposal objProposal = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        lblMessage.Visible = false;
        DataSet ds = new DataSet();
        objProposal = new sqlProposal();
        if (Request.QueryString.HasKeys())
        {
            int id = Convert.ToInt32(Request.QueryString["Id"].ToString());
            if (id > 0)
            {
                //ds = objProposal.GetProposalDetails(Convert.ToInt32(id));
                ds = objProposal.GetProposalDetailsMail_New(Convert.ToInt32(id));
                if (ds.Tables[0].Rows.Count > 0)
                {
                    grvRateCard.DataSource = ds.Tables[0];
                    grvRateCard.DataBind();
                }
                else
                {
                    grvRateCard.DataSource = null;
                    grvRateCard.DataBind();
                    lblMessage.Visible = true;
                    lblMessage.Text = "Record not available";
                    lblMessage.ForeColor = System.Drawing.Color.Red;
                }
            }
            else
            {
                grvRateCard.DataSource = null;
                grvRateCard.DataBind();
                lblMessage.Visible = true;
                lblMessage.Text = "Record not available";
                lblMessage.ForeColor = System.Drawing.Color.Red;
            }
        }
    }

    protected void grvRateCard_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            try
            {
                Label lblsalesrate = (Label)e.Row.FindControl("lblsalesrate");
                Label lblpurchaserate = (Label)e.Row.FindControl("lblpurchaserate");
                Label lblbillingbasis = (Label)e.Row.FindControl("lblbillingbasis");

                lblpurchaserate.Text = (Math.Round((Convert.ToDouble(lblsalesrate.Text) * 100)
                    / (100 + Convert.ToDouble(ConfigurationManager.AppSettings["PercentIncreaseInPurchase"])), 2)).ToString();

                if (lblbillingbasis.Text.Trim().ToLower() == "pp")
                {
                    lblbillingbasis.Text = "Point to Point";
                }
                else
                {
                    lblbillingbasis.Text = "Garage to Garage";
                }
            }
            catch (Exception ex)
            {
                lblMessage.Visible = true;
                lblMessage.Text = ex.Message;
                lblMessage.ForeColor = System.Drawing.Color.Red;
            }
        }
    }

}