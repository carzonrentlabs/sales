﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Salespage.master" AutoEventWireup="true"
    CodeFile="ProposalNew.aspx.cs" Inherits="Proposal_ProposalNew" EnableEventValidation="false" %>

<asp:Content ID="cntPlaceholder" ContentPlaceHolderID="ContentPlaceHolder_Page" runat="Server">
    <script type="text/javascript">
        var GB_ROOT_DIR = '<%= this.ResolveClientUrl("~/greybox/")%>';
    </script>
    <script type="text/javascript" src='<%= this.ResolveClientUrl("~/greybox/AJS.js") %>'></script>
    <script type="text/javascript" src='<%= this.ResolveClientUrl("~/greybox/AJS_fx.js") %>'></script>
    <script type="text/javascript" src='<%= this.ResolveClientUrl("~/greybox/gb_scripts.js") %>'></script>
    <script src="../JQuery/jquery-1.4.1.min.js" type="text/javascript"></script>
    <script src="../JQuery/ui.datepicker.js" type="text/javascript"></script>
    <script src="../JQuery/jquery.reveal.js"></script>
    <link href="../CSSReveal/revealNew.css" rel="stylesheet" />
    <script type="text/javascript">
        function pageLoad(sender, args) {
            var currentDate = new Date();
            $("#<%=txtDate.ClientID%>").datepicker({ dateFormat: "mm/dd/yy" }).datepicker("setDate", "0");

            $('a[data-reveal-id]').live('click', function (e) {
                e.preventDefault();
                var chkboxrowcount = $("#<%=grvProposal.ClientID%> input[id*='chkReplicate']:checkbox:checked").size();
                if (chkboxrowcount == 0) {
                    alert("Please select at least a record to replicate");
                    return false;
                }
                else {
                    var modalLocation = $(this).attr('data-reveal-id');
                    $('#' + modalLocation).reveal($(this).data());
                    return true;
                }

            });


            $("#<%=txtMobileNo.ClientID%>").keyup(function () {

                var strPass = $("#<%=txtMobileNo.ClientID%>").val();
                var strLength = strPass.length;
                var lchar = strPass.charAt((strLength) - 1);
                var cCode = CalcKeyCode(lchar);
                if (cCode < 46 || cCode > 57 || cCode == 47) {
                    var myNumber = strPass.substring(0, (strLength) - 1);
                    $("#<%=txtMobileNo.ClientID%>").val(myNumber);
                    alert("Enter only numeric value.")
                }
            });

            $("#<%=txtOtherDetails.ClientID%>,#<%=txtAddress.ClientID%>").keyup(function () {
                strId = "#" + this.id
                data = $(strId).val();
                var iChars = "!@@#$%^&*()+=-[]\';{}|:<>?\"";
                for (var i = 0; i < data.length; i++) {
                    if (iChars.indexOf(data.charAt(i)) != -1) {
                        alert("Your string has special characters.These are not allowed.");
                        var myString = $(strId).val().substring(0, (data.length) - 1);
                        $(strId).val(myString);
                        return false;
                    }
                }
            });

            $("#<%=grvProposal.ClientID%> a[id*='btnUpdate']").click(function () {
                var raiseId = $(this).attr("id");
                var ddlBillingBasis = "#" + raiseId.replace("btnUpdate", "ddlEditBillingBasis");
                var ddlFGR = "#" + raiseId.replace("btnUpdate", "ddlEditFGR");
                var ddlPackageType = "#" + raiseId.replace("btnUpdate", "ddlEditPackageType");
                var ddlPackageType = "#" + raiseId.replace("btnUpdate", "ddlEditPackageType");
                var ddlPackageHour = "#" + raiseId.replace("btnUpdate", "ddlEditPackageHour");

                if ($(ddlBillingBasis).val() == "PP" && $(ddlFGR).val() == 0) {
                    var r = confirm("Are you sure want to pass fix garage run rate.");
                    if (r == true) {

                        return true;
                    }
                    else {
                        alert("Select FGR value");
                        $(ddlFGR).focus();
                        return false;
                    }
                }
                else if ($(ddlPackageType).val() == "1" && $(ddlPackageHour).val() == 0) {
                    alert("Local package not allow for 0 'Zero' hours ");
                    $(ddlPackageHour).focus();
                    return false;
                }

            });
            $("#<%=grvProposal.ClientID%> input[id*='btnInsertRecord']").click(function () {
                var add = $(this).attr("id");
                var ddlFFPackageType = "#" + add.replace("btnInsertRecord", "ddlPackageType");
                var ddlFFPackageHour = "#" + add.replace("btnInsertRecord", "ddlPackageHour");
                if ($(ddlFFPackageType).val() == "1" && $(ddlFFPackageHour).val() == 0) {
                    alert("Local package not allow for 0 'Zero' hours ");
                    $(ddlPackageHour).focus();
                    return false;
                }
            });
            $("#<%=grvProposal.ClientID%> input[id*='btnDelete']").click(function () {
                var chkboxrowcount = $("#<%=grvProposal.ClientID%> input[id*='chkDel']:checkbox:checked").size();
                if (chkboxrowcount == 0) {
                    alert("Please select at least a record to delete");
                    return false;
                }
                else {
                    var r = confirm("You have selected only " + chkboxrowcount + " to delete \n Are you sure to delete this.");
                    if (r == true) {

                        return true;
                    }
                    else {
                        return false;
                    }
                }
            });

            $("#<%=grvProposal.ClientID%> input[id*='bntReplicatePackage']").click(function () {
                var chkboxrowcount = $("#<%=grvProposal.ClientID%> input[id*='chkReplicate']:checkbox:checked").size();
                if (chkboxrowcount == 0) {
                    alert("Please select at least a record to replicate");
                    return false;
                }
                else {
                    var r = confirm("You have selected  " + chkboxrowcount + " package to replicate \n Are you sure to delete this.");
                    if (r == true) {

                        return true;
                    }
                    else {
                        return false;
                    }
                }
            });

            $("#<%=bntGet.ClientID%>").click(function () {
                var selected = $("[id*=ddlPackageCategory] option:selected");


                if ($("#<%=ddlProspectCategories.ClientID%>")[0].selectedIndex == 0) {
                    alert("Select prospects categories.");
                    $("#<%=ddlProspectCategories.ClientID%>").focus();
                    return false;
                }
                else if ($("#<%=ddlClient.ClientID%>")[0].selectedIndex == 0) {
                    alert("Select client name");
                    $("#<%=ddlClient.ClientID%>").focus();
                    return false;
                }
                else if ($("#<%=ddlPackageCategory.ClientID%>").val() == 0 || $("#<%=ddlPackageCategory.ClientID%>").val() == null || $("#<%=ddlPackageCategory.ClientID%>").val() == "") {
                    alert("Select package category");
                    $("#<%=ddlPackageCategory.ClientID%>").focus();
                    return false;
                }
                else if ($("#<%=ddlProposalFor.ClientID%>")[0].selectedIndex == 0) {
                    alert("Select proposal for");
                    $("#<%=ddlProposalFor.ClientID%>").focus();
                    return false;
                }
                else {
                    $("#dvProposalDetails").show();
                    return true;
                }
            });
        }

        $(function () {
            $("#btnViewStandard").click(function () {
                $("#<%=ddlRplicateCity.ClientID%>").hide();
                var caption = "Standard Mailer";

                var url = "../DOC/StandardMailer.pdf"
                return GB_showFullScreen(caption, url)

            });
            $("#btnCorPresentation").click(function () {
                $("#<%=ddlRplicateCity.ClientID%>").hide();
                var caption = "Corporate Presentation"
                var url = "../DOC/corporatepresentation.pdf"
                return GB_showCenter(caption, url, 500, 700)
            });
            $("#btnCommitService").click(function () {
                $("#<%=ddlRplicateCity.ClientID%>").hide();
                var caption = "commitment to service";
                var url = "../DOC/CommitmenttoService.pdf"
                return GB_showCenter(caption, url, 500, 700)
            });

            $("#<%=btnSubmit.ClientID%>").click(function () {

                var gv = document.getElementById("<%=grvProposal.ClientID %>");
                var gvRowCount = gv.rows.length;
                var rwIndex = 1;
                for (rwIndex; rwIndex <= gvRowCount - 2; rwIndex++) {
                    var pkgType = gv.rows[rwIndex].cells[1].childNodes[1].innerHTML;
                    var cityId = gv.rows[rwIndex].cells[2].childNodes[1].innerHTML;
                    if (pkgType == 0 || cityId == 0) {
                        alert("package type and city name should not be blank");
                        return false;
                    }

                }


                if ($("#<%=ddlClient.ClientID%>")[0].selectedIndex == 0) {
                    alert("Select client name");
                    $("#<%=ddlClient.ClientID%>").focus();
                    return false;
                }
                else if ($("#<%=ddlProposalFor.ClientID%>")[0].selectedIndex == 0) {
                    alert("Select proposal for.");
                    $("#<%=ddlProposalFor.ClientID%>").focus();
                    return false;
                }
                else if ($("#<%=ddlPackageCategory.ClientID%>")[0].selectedIndex == 0) {
                    alert("Select car category.");
                    $("#<%=ddlPackageCategory.ClientID%>").focus();
                    return false;
                }
                else if ($("#<%=ddlCityName.ClientID%>")[0].selectedIndex == 0) {
                    alert("Select city name");
                    $("#<%=ddlCityName.ClientID%>").focus();
                    return false;
                }
                else if ($("#<%=ddlTypeProposal.ClientID%>")[0].selectedIndex == 0) {
                    alert("Select proposal type.");
                    $("#<%=ddlTypeProposal.ClientID%>").focus();
                    return false;
                }
                else if ($("#<%=txtDate.ClientID%>").val() == "") {
                    alert("Enter date of initition.");
                    $("#<%=txtDate.ClientID%>").focus();
                    return false;
                }

                else if ($("#<%=ddlActionMgrName.ClientID%>")[0].selectedIndex == 0) {
                    alert("Select action manager name.");
                    $("#<%=ddlActionMgrName.ClientID%>").focus();
                    return false;
                }

                else if ($("#<%=txtContactPersonName.ClientID%>").val() == "") {
                    alert("Enter contact person name.");
                    $("#<%=txtContactPersonName.ClientID%>").focus();
                    return false;
                }
                else if ($("#<%=txtConactPersonEmailId.ClientID%>").val() == "") {
                    alert("Enter contact person email id.");
                    $("#<%=txtConactPersonEmailId.ClientID%>").focus();
                    return false;
                }
                else if ($("#<%=fudCommercial.ClientID%>").val() == "") {
                    alert("Select commercial file to upload.");
                    $("#<%=fudCommercial.ClientID%>").focus();
                    return false;
                }
                else if ($("#<%=txtMobileNo.ClientID%>").val() != "" && $("#<%=txtMobileNo.ClientID%>").val().length < 10) {
                    alert("Enter 10 digit mobile number.");
                    $("#<%=txtMobileNo.ClientID%>").focus();
                    return false;
                }
                else if ($("#<%=txtMobileNo.ClientID%>").val() != "" && isNaN($("#<%=txtMobileNo.ClientID%>").val())) {
                    alert("Mobile number should numeric value only.");
                    $("#<%=txtMobileNo.ClientID%>").focus();
                    return false;
                }

                else {
                    ShowProgress();
                }
            });

        });

        function CalcKeyCode(aChar) {
            var character = aChar.substring(0, 1);
            var code = aChar.charCodeAt(0);
            return code;
        }
        //Callback function
        function callback_fn() {
            alert(script_loaded);
        }
        function ShowProgress() {
            setTimeout(function () {
                var modal = $('<div />');
                modal.addClass("modal");
                $('body').append(modal);
                var loading = $(".loading");
                loading.show();
                var top = Math.max($(window).height() / 2 - loading[0].offsetHeight / 2, 0);
                var left = Math.max($(window).width() / 2 - loading[0].offsetWidth / 2, 0);
                loading.css({ top: top, left: left });
            }, 200);
        }
    </script>
    <center>
        <asp:UpdatePanel ID="updDetails" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <table cellpadding="3" cellspacing="0" border="0" width="80%">
                    <caption>
                        <b>Build Proposal</b></caption>
                    <tr>
                        <td colspan="6" align="center">
                            <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>Prospects Categories</b>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlProspectCategories" runat="server">
                                <asp:ListItem Text="-Select-" Value="0"></asp:ListItem>
                                <asp:ListItem Text="Category A" Value="A"></asp:ListItem>
                                <asp:ListItem Text="Category B" Value="B"></asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="reqProspectCate" ValidationGroup="ValgrpCust" ControlToValidate="ddlProspectCategories"
                                runat="server" ErrorMessage="*" InitialValue="0" Display="Dynamic"></asp:RequiredFieldValidator>
                            <asp:RequiredFieldValidator ID="reqProspectCateUPDoc" ValidationGroup="upDoc" ControlToValidate="ddlProspectCategories"
                                runat="server" ErrorMessage="*" InitialValue="0" Display="Dynamic"></asp:RequiredFieldValidator>
                            <asp:RequiredFieldValidator ID="reqProspectCateGet" ValidationGroup="Get" ControlToValidate="ddlProspectCategories"
                                runat="server" ErrorMessage="*" InitialValue="0" Display="Dynamic"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <b>Client</b>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlClient" runat="server" Width="200px" OnSelectedIndexChanged="ddlClient_SelectedIndexChanged"
                                AutoPostBack="true">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="reqClient" ValidationGroup="ValgrpCust" ControlToValidate="ddlClient"
                                runat="server" ErrorMessage="*" InitialValue="0" Display="Dynamic"></asp:RequiredFieldValidator>
                            <asp:RequiredFieldValidator ID="reqClientUpDoc" ValidationGroup="upDoc" ControlToValidate="ddlClient"
                                runat="server" ErrorMessage="*" InitialValue="0" Display="Dynamic"></asp:RequiredFieldValidator>
                            <asp:RequiredFieldValidator ID="reqClientGet" ValidationGroup="Get" ControlToValidate="ddlClient"
                                runat="server" ErrorMessage="*" InitialValue="0" Display="Dynamic"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <b>Proposal For</b>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlProposalFor" runat="server">
                                <asp:ListItem Text="-Select-" Value="0"></asp:ListItem>
                                <asp:ListItem Text="Local" Value="1"></asp:ListItem>
                                <asp:ListItem Text="Outstation" Value="2"></asp:ListItem>
                                <asp:ListItem Text="Fixed Airport Transfer" Value="3"></asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="reqProposalForCust" ValidationGroup="ValgrpCust"
                                ControlToValidate="ddlProposalFor" runat="server" ErrorMessage="*" InitialValue="0"
                                Display="Dynamic"></asp:RequiredFieldValidator>
                            <asp:RequiredFieldValidator ID="reqProposalForGet" ValidationGroup="Get" ControlToValidate="ddlProposalFor"
                                runat="server" ErrorMessage="*" InitialValue="0" Display="Dynamic"></asp:RequiredFieldValidator>
                            <asp:RequiredFieldValidator ID="reqProposalForDoc" ValidationGroup="upDoc" ControlToValidate="ddlProposalFor"
                                runat="server" ErrorMessage="*" InitialValue="0" Display="Dynamic"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>Car Category</b>
                        </td>
                        <td valign="top">
                            <asp:ListBox ID="ddlPackageCategory" runat="server" Rows="5" SelectionMode="Multiple">
                            </asp:ListBox>
                            <asp:RequiredFieldValidator ID="reqPackageCategory" ValidationGroup="ValgrpCust"
                                ControlToValidate="ddlPackageCategory" runat="server" ErrorMessage="*" InitialValue="0"
                                Display="Dynamic"></asp:RequiredFieldValidator>
                            <asp:RequiredFieldValidator ID="reqPackageCategoryDoc" ValidationGroup="upDoc" ControlToValidate="ddlPackageCategory"
                                runat="server" ErrorMessage="*" InitialValue="0" Display="Dynamic"></asp:RequiredFieldValidator>
                            <asp:RequiredFieldValidator ID="reqPackageCategoryGet" ValidationGroup="Get" ControlToValidate="ddlPackageCategory"
                                runat="server" ErrorMessage="*" InitialValue="0" Display="Dynamic"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <b>Date of Initition</b>
                        </td>
                        <td>
                            <asp:TextBox ID="txtDate" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="reqtxtDate" ValidationGroup="ValgrpCust" ControlToValidate="txtDate"
                                runat="server" ErrorMessage="*" Display="Dynamic"></asp:RequiredFieldValidator>
                            <asp:RequiredFieldValidator ID="reqtxtDateDoc" ValidationGroup="upDoc" ControlToValidate="txtDate"
                                runat="server" ErrorMessage="*" Display="Dynamic"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <b>Type of Proposal</b>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlTypeProposal" runat="server">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="reqTypeProposal" ValidationGroup="ValgrpCust" ControlToValidate="ddlTypeProposal"
                                runat="server" ErrorMessage="*" InitialValue="0" Display="Dynamic"></asp:RequiredFieldValidator>
                            <asp:RequiredFieldValidator ID="reqTypeProposalDoc" ValidationGroup="upDoc" ControlToValidate="ddlTypeProposal"
                                runat="server" ErrorMessage="*" InitialValue="0" Display="Dynamic"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>Source City Name</b>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlCityName" runat="server">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="reqCityName" ValidationGroup="ValgrpCust" ControlToValidate="ddlCityName"
                                runat="server" ErrorMessage="*" InitialValue="0" Display="Dynamic"></asp:RequiredFieldValidator>
                            <asp:RequiredFieldValidator ID="reqCityNameDoc" ValidationGroup="upDoc" ControlToValidate="ddlCityName"
                                runat="server" ErrorMessage="*" InitialValue="0" Display="Dynamic"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <b>Action Manager Name</b>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlActionMgrName" runat="server" Width="200px">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="reqActionMgrName" ValidationGroup="ValgrpCust" ControlToValidate="ddlActionMgrName"
                                runat="server" ErrorMessage="*" InitialValue="0" Display="Dynamic"></asp:RequiredFieldValidator>
                            <asp:RequiredFieldValidator ID="reqActionMgrNameDoc" ValidationGroup="upDoc" ControlToValidate="ddlActionMgrName"
                                runat="server" ErrorMessage="*" InitialValue="0" Display="Dynamic"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:Button ID="bntGet" runat="server" Text="Get" ValidationGroup="Get" OnClick="bntGet_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6" style="text-align: center">
                            <asp:UpdateProgress ID="updProgress" runat="server" AssociatedUpdatePanelID="updDetails"
                                DynamicLayout="true">
                                <ProgressTemplate>
                                    <img src="../Images/loader.gif" alt="Loading..." width="40px" height="40px" />
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6" style="text-align: center">
                            <asp:GridView ID="grvProposal" runat="server" AutoGenerateColumns="false" ShowFooter="true"
                                OnRowCommand="grvProposal_RowCommand" OnRowDataBound="grvProposal_RowDataBound"
                                OnRowCancelingEdit="grvProposal_RowCancelingEdit" OnRowEditing="grvProposal_RowEditing"
                                OnRowDeleting="grvProposal_RowDeleting" OnRowUpdating="grvProposal_RowUpdating"
                                DataKeyNames="ProposalDetailId,TempID,BaseRate,BaseHr,BaseKM">
                                <Columns>
                                    <asp:TemplateField HeaderText="S.No">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSNo" runat="server" Text='<%# Container.DataItemIndex + 1%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Requirement Type" FooterStyle-Wrap="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPackagetype" runat="server" Text='<%#Eval("PackageType")%>'></asp:Label>
                                            <asp:Label ID="lblPackageID" runat="server" Text='<%#Eval("PkgTypeID")%>' Visible="false"></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:Label ID="lblPackagetype" runat="server" Text='<%#Eval("PackageType")%>' Visible="false"></asp:Label>
                                            <asp:Label ID="lblPackageID" runat="server" Text='<%#Eval("PkgTypeID")%>' Visible="false"></asp:Label>
                                            <asp:DropDownList runat="server" ID="ddlEditPackageType">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="reqEditPackageType" ValidationGroup="EditPackage"
                                                ControlToValidate="ddlEditPackageType" runat="server" ErrorMessage="*" InitialValue="0"
                                                Display="Dynamic"></asp:RequiredFieldValidator>
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:DropDownList runat="server" ID="ddlPackageType">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="reqPackageType" ValidationGroup="ValgrpCust" ControlToValidate="ddlPackageType"
                                                runat="server" ErrorMessage="*" InitialValue="0" Display="Dynamic"></asp:RequiredFieldValidator>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="City Name" FooterStyle-Wrap="true" FooterStyle-Width="50PX"
                                        ItemStyle-Width="50PX">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPckageCityName" runat="server" Text='<%#Eval("cityName")%>'></asp:Label>
                                            <asp:Label ID="lblPckageCityID" runat="server" Text='<%#Eval("CityID")%>' Visible="false"></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:Label ID="lblPckageCityName" runat="server" Text='<%#Eval("cityName")%>' Visible="false"></asp:Label>
                                            <asp:DropDownList runat="server" ID="ddlEditPackageCityName" Width="100Px">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="reqPckageCityName" ValidationGroup="EditPackage"
                                                ControlToValidate="ddlEditPackageCityName" runat="server" ErrorMessage="*" InitialValue="0"
                                                Display="Dynamic"></asp:RequiredFieldValidator>
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:DropDownList runat="server" ID="ddlPackageCityName" Width="110PX">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="reqPackageCityName" ValidationGroup="ValgrpCust"
                                                ControlToValidate="ddlPackageCityName" runat="server" ErrorMessage="*" InitialValue="0"
                                                Display="Dynamic"></asp:RequiredFieldValidator>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Vehicle Category" FooterStyle-Wrap="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblVehicleCategory" runat="server" Text='<%#Eval("CarCatName")%>'></asp:Label>
                                            <asp:Label ID="lblVehicleCategoryID" runat="server" Text='<%#Eval("CarCatID")%>'
                                                Visible="false"></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:Label ID="lblVehicleCategory" runat="server" Text='<%#Eval("CarCatName")%>'
                                                Visible="false"></asp:Label>
                                            <asp:DropDownList runat="server" ID="ddlEditVehicleCategory" OnSelectedIndexChanged="ddlEditVehicleCategory_SelectedIndexChanged"
                                                AutoPostBack="true" Width="100Px">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="reqEditVehicleCategory" ValidationGroup="EditPackage"
                                                ControlToValidate="ddlEditVehicleCategory" runat="server" ErrorMessage="*" InitialValue="0"
                                                Display="Dynamic"></asp:RequiredFieldValidator>
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:DropDownList runat="server" ID="ddlVehicleCategory" OnSelectedIndexChanged="ddlVehicleCategory_SelectedIndexChanged"
                                                AutoPostBack="true" Width="100Px">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="reqVehicleCategory" ValidationGroup="ValgrpCust"
                                                ControlToValidate="ddlVehicleCategory" runat="server" ErrorMessage="*" InitialValue="0"
                                                Display="Dynamic"></asp:RequiredFieldValidator>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Billing Basis" FooterStyle-Wrap="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblBillingBasis" runat="server" Text='<%#Eval("BillingBasis")%>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:Label ID="lblBillingBasis" runat="server" Text='<%#Eval("BillingBasis")%>' Visible="false"></asp:Label>
                                            <asp:DropDownList runat="server" ID="ddlEditBillingBasis" OnSelectedIndexChanged="ddlEditBillingBasis_SelectedIndexChanged"
                                                AutoPostBack="true" Width="50Px">
                                                <asp:ListItem Text="--Select--" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="Point to point" Value="PP"></asp:ListItem>
                                                <asp:ListItem Text="Garage to garage" Value="GG"></asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="reqEditBillingBasis" ValidationGroup="EditPackage"
                                                ControlToValidate="ddlEditBillingBasis" runat="server" ErrorMessage="*" InitialValue="0"
                                                Display="Dynamic"></asp:RequiredFieldValidator>
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:DropDownList runat="server" ID="ddlBillingBasis" OnSelectedIndexChanged="ddlBillingBasis_SelectedIndexChanged"
                                                AutoPostBack="true" Width="50Px">
                                                <asp:ListItem Text="Point to point" Value="PP"></asp:ListItem>
                                                <asp:ListItem Text="Garage to garage" Value="GG"></asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="reqBillingBasis" ValidationGroup="ValgrpCust" ControlToValidate="ddlBillingBasis"
                                                runat="server" ErrorMessage="*" InitialValue="0" Display="Dynamic"></asp:RequiredFieldValidator>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="FGR" FooterStyle-Wrap="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblFGR" runat="server" Text='<%#Eval("FGR")%>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:Label ID="lblFGR" runat="server" Text='<%#Eval("FGR")%>' Visible="false"></asp:Label>
                                            <asp:DropDownList runat="server" ID="ddlEditFGR">
                                            </asp:DropDownList>
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:DropDownList runat="server" ID="ddlFGR">
                                            </asp:DropDownList>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Pacakage Hour" FooterStyle-Wrap="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPackageHour" runat="server" Text='<%#Eval("PkgHrs")%>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:Label ID="lblPackageHour" runat="server" Text='<%#Eval("PkgHrs")%>' Visible="false"></asp:Label>
                                            <asp:DropDownList runat="server" ID="ddlEditPackageHour" OnSelectedIndexChanged="ddlEditPackageHour_SelectedIndexChanged"
                                                AutoPostBack="true">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="reqEditPackageHour" ValidationGroup="EditPackage"
                                                ControlToValidate="ddlEditPackageHour" runat="server" ErrorMessage="*" InitialValue="-1"
                                                Display="Dynamic"></asp:RequiredFieldValidator>
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:DropDownList runat="server" ID="ddlPackageHour" OnSelectedIndexChanged="ddlPackageHour_SelectedIndexChanged"
                                                AutoPostBack="true">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="reqPackageHour" ValidationGroup="ValgrpCust" ControlToValidate="ddlPackageHour"
                                                runat="server" ErrorMessage="*" InitialValue="-1" Display="Dynamic"></asp:RequiredFieldValidator>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Pacakage KM" FooterStyle-Wrap="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPackageKM" runat="server" Text='<%#Eval("PkgKMs")%>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:Label ID="lblPackageKM" runat="server" Text='<%#Eval("PkgKMs")%>' Visible="false"></asp:Label>
                                            <asp:DropDownList runat="server" ID="ddlEditPackageKM" OnSelectedIndexChanged="ddlEditPackageKM_SelectedIndexChanged"
                                                AutoPostBack="true">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="reqEditPackageKM" ValidationGroup="EditPackage" ControlToValidate="ddlEditPackageKM"
                                                runat="server" ErrorMessage="*" InitialValue="0" Display="Dynamic"></asp:RequiredFieldValidator>
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:DropDownList runat="server" ID="ddlPackageKM" OnSelectedIndexChanged="ddlPackageKM_SelectedIndexChanged"
                                                AutoPostBack="true">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="reqPackageKM" ValidationGroup="ValgrpCust" ControlToValidate="ddlPackageKM"
                                                runat="server" ErrorMessage="*" InitialValue="0" Display="Dynamic"></asp:RequiredFieldValidator>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Package Rate" FooterStyle-Wrap="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPackageRate" runat="server" Text='<%#Eval("PKGRate")%>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:Label ID="lblPackageRate" runat="server" Text='<%#Eval("PKGRate")%>' Visible="false"></asp:Label>
                                            <asp:DropDownList runat="server" ID="ddlEditPackageRate" Enabled="false">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="reqEditPackageRate" ValidationGroup="EditPackage"
                                                ControlToValidate="ddlEditPackageRate" runat="server" ErrorMessage="*" InitialValue="0"
                                                Display="Dynamic"></asp:RequiredFieldValidator>
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:DropDownList runat="server" ID="ddlPackageRate" Enabled="false">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="reqPackageRate" ValidationGroup="ValgrpCust" ControlToValidate="ddlPackageRate"
                                                runat="server" ErrorMessage="*" InitialValue="0" Display="Dynamic"></asp:RequiredFieldValidator>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Extra Hour Rate" FooterStyle-Wrap="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPackageExtraHr" runat="server" Text='<%#Eval("ExtraHrRate")%>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:Label ID="lblPackageExtraHr" runat="server" Text='<%#Eval("ExtraHrRate")%>'
                                                Visible="false"></asp:Label>
                                            <asp:DropDownList runat="server" ID="ddlEditPackageExtraHr">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="reqEditPackageExtraHr" ValidationGroup="EditPackage"
                                                ControlToValidate="ddlEditPackageExtraHr" runat="server" ErrorMessage="*" InitialValue="-1"
                                                Display="Dynamic"></asp:RequiredFieldValidator>
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:DropDownList runat="server" ID="ddlPackageExtraHr">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="reqPackageExtraHr" ValidationGroup="ValgrpCust" ControlToValidate="ddlPackageExtraHr"
                                                runat="server" ErrorMessage="*" InitialValue="-1" Display="Dynamic"></asp:RequiredFieldValidator>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Extra KM Rate" FooterStyle-Wrap="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPackageExtraKM" runat="server" Text='<%#Eval("ExtraKMRate")%>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:Label ID="lblPackageExtraKM" runat="server" Text='<%#Eval("ExtraKMRate")%>'
                                                Visible="false"></asp:Label>
                                            <asp:DropDownList runat="server" ID="ddlEditPackageExtraKM">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="reqEditPackageExtraKM" ValidationGroup="EditPackage"
                                                ControlToValidate="ddlEditPackageExtraKM" runat="server" ErrorMessage="*" InitialValue="-1"
                                                Display="Dynamic"></asp:RequiredFieldValidator>
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:DropDownList runat="server" ID="ddlPackageExtraKM">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="reqPackageExtraKM" ValidationGroup="ValgrpCust" ControlToValidate="ddlPackageExtraKM"
                                                runat="server" ErrorMessage="*" InitialValue="-1" Display="Dynamic"></asp:RequiredFieldValidator>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Threshold Extra Hr" FooterStyle-Wrap="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblthresholdExtraHr" runat="server" Text='<%#Eval("ThresholdExtraHr")%>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:Label ID="lblthresholdExtraHr" runat="server" Text='<%#Eval("ThresholdExtraHr")%>'
                                                Visible="false"></asp:Label>
                                            <asp:DropDownList runat="server" ID="ddlEditthresholdExtraHr">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="reqEditthresholdExtraHr" ValidationGroup="EditPackage"
                                                ControlToValidate="ddlEditthresholdExtraHr" runat="server" ErrorMessage="*" InitialValue="-1"
                                                Display="Dynamic"></asp:RequiredFieldValidator>
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:DropDownList runat="server" ID="ddlthresholdExtraHr">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="reqthresholdExtraHr" ValidationGroup="ValgrpCust"
                                                ControlToValidate="ddlthresholdExtraHr" runat="server" ErrorMessage="*" InitialValue="-1"
                                                Display="Dynamic"></asp:RequiredFieldValidator>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Threshold Extra KM" FooterStyle-Wrap="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblthresholdExtraKM" runat="server" Text='<%#Eval("ThresholdExtraKM")%>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:Label ID="lblthresholdExtraKM" runat="server" Text='<%#Eval("ThresholdExtraKM")%>'
                                                Visible="false"></asp:Label>
                                            <asp:DropDownList runat="server" ID="ddlEditthresholdExtraKM">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="reqEditthresholdExtraKM" ValidationGroup="EditPackage"
                                                ControlToValidate="ddlEditthresholdExtraKM" runat="server" ErrorMessage="*" InitialValue="-1"></asp:RequiredFieldValidator>
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:DropDownList runat="server" ID="ddlthresholdExtraKM">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="reqthresholdExtraKM" ValidationGroup="ValgrpCust"
                                                ControlToValidate="ddlthresholdExtraKM" runat="server" ErrorMessage="*" InitialValue="-1"></asp:RequiredFieldValidator>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Night Charges" FooterStyle-Width="50px" ControlStyle-Width="50px"
                                        FooterStyle-Wrap="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblNightCharges" runat="server" Text='<%#Eval("NightStayAllowance")%>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox runat="server" ID="txtEditNightCharges" Text='<%#Eval("NightStayAllowance")%>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox runat="server" ID="txtNightCharges" Width="50px"></asp:TextBox>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Chauffeur Charges" ControlStyle-Width="50px" FooterStyle-Width="50px">
                                        <ItemTemplate>
                                            <asp:Label ID="lblChauffeurCharges" runat="server" Text='<%#Eval("OutStationAllowance")%>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox runat="server" ID="txtEditChauffeurCharges" Text='<%#Eval("OutStationAllowance")%>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox runat="server" ID="txtChauffeurCharges" Width="50px"></asp:TextBox>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Edit" HeaderStyle-Width="15%" FooterStyle-Wrap="false">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="btnEdit" Text="Edit" runat="server" CommandName="Edit" />
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:LinkButton ID="btnUpdate" Text="Update" runat="server" CommandName="Update"
                                                ValidationGroup="EditPackage" />
                                            <asp:LinkButton ID="btnCancel" Text="Cancel" runat="server" CommandName="Cancel" />
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:Button ID="btnInsertRecord" runat="server" Text="Add" ValidationGroup="ValgrpCust"
                                                CommandName="Insert" />
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="RR">
                                        <HeaderTemplate>
                                            <asp:LinkButton ID="lkbtnReplicate" runat="server" class="big-link" data-reveal-id="myModal"
                                                Text="Replicate" CssClass="ButtonBKSharma"></asp:LinkButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkReplicate" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Button ID="btnDelete" runat="server" Text="Delete" OnClick="btnDelete_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkDel" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
        <br />
        <div id="dvProposalDetails" style="display: none;">
            <table cellpadding="5" cellspacing="0" border="1" width="30%">
                <tr>
                    <td colspan="4" style="text-align: center">
                        <b>Spot Rental (Proposal Type)</b>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: left; white-space: nowrap;">
                        Standard Mailer &nbsp;
                        <input type="button" id="btnViewStandard" value="View" />
                    </td>
                    <td style="text-align: left; white-space: nowrap;">
                        Corporate Presentation &nbsp;<input type="button" id="btnCorPresentation" value="View" />
                    </td>
                    <td style="text-align: left; white-space: nowrap;">
                        Commitment to Service &nbsp;<input type="button" id="btnCommitService" value="View" />
                    </td>
                    <td style="text-align: left; white-space: nowrap;">
                        Commercial
                        <asp:FileUpload ID="fudCommercial" runat="server" />
                        &nbsp;
                        <asp:RequiredFieldValidator ID="reqCommercial" ValidationGroup="upDoc" ControlToValidate="fudCommercial"
                            runat="server" ErrorMessage="*" Display="Dynamic"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: left; white-space: nowrap;">
                        Contact Person Name &nbsp;
                        <asp:TextBox ID="txtContactPersonName" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="reqContactPerson" ValidationGroup="upDoc" ControlToValidate="txtContactPersonName"
                            runat="server" ErrorMessage="*" Display="Dynamic"></asp:RequiredFieldValidator>
                    </td>
                    <td style="text-align: left; white-space: nowrap;">
                        Contact Person Email &nbsp;
                        <asp:TextBox ID="txtConactPersonEmailId" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="reqConactPersonEmailId" ValidationGroup="upDoc" ControlToValidate="txtConactPersonEmailId"
                            runat="server" ErrorMessage="*" Display="Dynamic"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="rfVConactPersonEmailId" runat="server" Display="Dynamic"
                            ErrorMessage="Please Enter Valid Email ID" ValidationGroup="upDoc" ControlToValidate="txtConactPersonEmailId"
                            CssClass="requiredFieldValidateStyle" ForeColor="Red" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">
                        </asp:RegularExpressionValidator>
                    </td>
                    <td style="text-align: left; white-space: nowrap;">
                        Contact Preson Address &nbsp;
                        <asp:TextBox ID="txtAddress" runat="server" TextMode="MultiLine" Rows="2" Columns="20"></asp:TextBox>
                    </td>
                    <td style="text-align: left; white-space: nowrap;">
                        Designation &nbsp;
                        <asp:TextBox ID="txtDesignation" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: left; vertical-align: middle;" colspan="2">
                        Other Details (If Any)&nbsp;
                        <asp:TextBox ID="txtOtherDetails" runat="server" TextMode="MultiLine" Rows="2" Columns="50"></asp:TextBox>
                    </td>
                    <td style="text-align: left" colspan="2">
                        Mobile No&nbsp;
                        <asp:TextBox ID="txtMobileNo" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="reqMobileNO" runat="server" ControlToValidate="txtMobileNo"
                            ErrorMessage="*" ValidationGroup="upDoc" ForeColor="red" Display="Dynamic"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator runat="server" ID="reqNumer" ControlToValidate="txtMobileNo"
                            ValidationExpression="[0-9]{10}" ErrorMessage="Please enter  number!">
                        </asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td colspan="4" align="center">
                        <asp:Button ID="btnSubmit" Text="Submit for Approval" runat="server" ValidationGroup="upDoc"
                            OnClick="btnSubmit_Click" />
                    </td>
                </tr>
            </table>
        </div>
        <div style="width: 400px; display: block; height: 150px;" id="myModal" class="reveal-modal">
            <fieldset style="border-color: Green">
                <legend><b>Replicate City</b></legend>
                <table>
                    <tr style="display: none">
                        <td colspan="1">
                            <asp:TextBox ID="txtProposalDetailId" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:ListBox runat="server" ID="ddlRplicateCity" SelectionMode="Multiple" Rows="7">
                            </asp:ListBox>
                            <asp:RequiredFieldValidator ID="reqddlRplicateCity" ValidationGroup="upCopy" ControlToValidate="ddlRplicateCity"
                                runat="server" ErrorMessage="*" Display="Dynamic"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Button ID="Button1" runat="server" Text="Replicate City" ValidationGroup="upCopy"
                                OnClick="btnRplicate_Click" />
                        </td>
                    </tr>
                </table>
            </fieldset>
            <a class="close-reveal-modal" title="click to close"><b>&#215;</b></a>
        </div>
        <div class="loading" align="center">
            Sending mail. Please wait.<br />
            <br />
            <img src="../Images/loader.gif" alt="" />
        </div>
    </center>
</asp:Content>
