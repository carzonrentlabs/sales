﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Salespage.master" AutoEventWireup="true"
    CodeFile="PKGApproval.aspx.cs" Inherits="Proposal_PKGApproval" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_Page" runat="Server">
    <asp:ScriptManagerProxy ID="scriptProxy" runat="server">
        <Scripts>
            <asp:ScriptReference Path="../Scripts/jquery-1.8.2.min.js" />
        </Scripts>
    </asp:ScriptManagerProxy>
    <script type="text/javascript">
        function Check_Approve_Click(objRef) {
            //Get the Row based on checkbox
            var row = objRef.parentNode.parentNode;
            if (objRef.checked) {
                //If checked change color to Aqua
                row.style.backgroundColor = "aqua";
            }
            else {
                //If not checked change back to original color
                if (row.rowIndex % 2 == 0) {
                    //Alternating Row Color
                    //row.style.backgroundColor = "#C2D69B";
                    row.style.backgroundColor = "white";
                }
                else {
                    row.style.backgroundColor = "white";
                }
            }

            //Get the reference of GridView
            var GridView = row.parentNode;

            //Get all input elements in Gridview
            var inputList = GridView.getElementsByTagName("input")[0];

            for (var i = 0; i < inputList.length; i++) {
                //The First element is the Header Checkbox
                var headerCheckBox = inputList[0];

                //Based on all or none checkboxes
                //are checked check/uncheck Header Checkbox
                var checked = true;
                if (inputList[i].type == "checkbox" && inputList[i] != headerCheckBox) {
                    if (!inputList[i].checked) {
                        checked = false;
                        break;
                    }
                }
            }
            headerCheckBox.checked = checked;

        }

        function Check_Reject_Click(objRef) {
            //Get the Row based on checkbox
            var row = objRef.parentNode.parentNode;
            if (objRef.checked) {
                //If checked change color to Aqua
                row.style.backgroundColor = "Red";
            }
            else {
                //If not checked change back to original color
                if (row.rowIndex % 2 == 0) {
                    //Alternating Row Color
                    //row.style.backgroundColor = "#C2D69B";
                    row.style.backgroundColor = "white";
                }
                else {
                    row.style.backgroundColor = "white";
                }
            }

            //Get the reference of GridView
            var GridView = row.parentNode;

            //Get all input elements in Gridview
            var inputList = GridView.getElementsByTagName("input")[1];

            for (var i = 0; i < inputList.length; i++) {
                //The First element is the Header Checkbox
                var headerCheckBox = inputList[0];

                //Based on all or none checkboxes
                //are checked check/uncheck Header Checkbox
                var checked = true;
                if (inputList[i].type == "checkbox" && inputList[i] != headerCheckBox) {
                    if (!inputList[i].checked) {
                        checked = false;
                        break;
                    }
                }
            }
            headerCheckBox.checked = checked;
        }

        function checkAllApproval(objRef) {
            var GridView = objRef.parentNode.parentNode.parentNode;
            var inputList = GridView.getElementsByTagName("input");
            var inputList = $("#<%=grvPackage.ClientID%> input[id*='chkAprrove']");
            var inputListR = $("#<%=grvPackage.ClientID%> input[id*='chkReject']");
            var chk = objRef.id
            var chkAll = chk.replace("checkAll", "RejectCheckAll")
            $("#" + chkAll).prop("checked", false);
            // $("[id$=RejectCheckAll]").prop("checked", false)
            for (var i = 0; i < inputListR.length; i++) {
                inputListR[i].checked = false;
            }

            for (var i = 0; i < inputList.length; i++) {
                //Get the Cell To find out ColumnIndex
                var row = inputList[i].parentNode.parentNode;
                if (inputList[i].type == "checkbox" && objRef != inputList[i]) {
                    if (objRef.checked) {
                        //If the header checkbox is checked
                        //check all checkboxes
                        //and highlight all rows
                        row.style.backgroundColor = "aqua";
                        inputList[i].checked = true;
                    }
                    else {
                        //If the header checkbox is checked
                        //uncheck all checkboxes
                        //and change rowcolor back to original
                        if (row.rowIndex % 2 == 0) {
                            //Alternating Row Color
                            //row.style.backgroundColor = "#C2D69B";
                            row.style.backgroundColor = "white";
                        }
                        else {
                            row.style.backgroundColor = "white";
                        }
                        inputList[i].checked = false;
                    }
                }
            }
        }

        function checkAllReject(objRef) {
            var GridView = objRef.parentNode.parentNode.parentNode;
            var inputList = GridView.getElementsByTagName("input");
            var inputList = $("#<%=grvPackage.ClientID%> input[id*='chkReject']");
            var inputListA = $("#<%=grvPackage.ClientID%> input[id*='chkAprrove']");
            var chk = objRef.id
            var chkAll = chk.replace("RejectCheckAll", "checkAll")
            $("#" + chkAll).prop("checked", false);
            // $("[id$=CheckAll]").prop("checked", false)
            for (var i = 0; i < inputListA.length; i++) {
                inputListA[i].checked = false;
            }

            for (var i = 0; i < inputList.length; i++) {
                //Get the Cell To find out ColumnIndex
                var row = inputList[i].parentNode.parentNode;
                if (inputList[i].type == "checkbox" && objRef != inputList[i]) {
                    if (objRef.checked) {
                        //If the header checkbox is checked
                        //check all checkboxes
                        //and highlight all rows
                        row.style.backgroundColor = "Red";
                        inputList[i].checked = true;
                    }
                    else {
                        //If the header checkbox is checked
                        //uncheck all checkboxes
                        //and change rowcolor back to original
                        if (row.rowIndex % 2 == 0) {
                            //Alternating Row Color
                            //row.style.backgroundColor = "#C2D69B";
                            row.style.backgroundColor = "white";
                        }
                        else {
                            row.style.backgroundColor = "white";
                        }
                        inputList[i].checked = false;
                    }
                }
            }
        }

        function MouseEvents(objRef, evt) {
            var checkbox = objRef.getElementsByTagName("input")[0];
            var checkboxR = objRef.getElementsByTagName("input")[1];
            if (evt.type == "mouseover") {
                objRef.style.backgroundColor = "orange";
            }
            else {
                if (checkbox.checked) {
                    if (checkboxR.checked) {
                        checkboxR.checked = false;
                    }
                    objRef.style.backgroundColor = "aqua";
                }
                else if (checkboxR.checked) {
                    if (checkbox.checked) {
                        checkbox.checked = false;
                    }
                    objRef.style.backgroundColor = "Red";
                }
                else if (evt.type == "mouseout") {
                    if (objRef.rowIndex % 2 == 0) {
                        //Alternating Row Color
                        // objRef.style.backgroundColor = "#C2D69B";
                        objRef.style.backgroundColor = "white";
                    }
                    else {
                        objRef.style.backgroundColor = "white";
                    }
                }
            }
        }
        //function pageLoad(sender, args)
        
        $(function () {
            $("#<%=btnApprove.ClientID%>").click(function () {
                var chkboxApprovalcount = $("#<%=grvPackage.ClientID%> input[id*='chkAprrove']:checkbox:checked").size();
                var chkboxRejectcount = $("#<%=grvPackage.ClientID%> input[id*='chkReject']:checkbox:checked").size();
                if (chkboxApprovalcount == 0 && chkboxRejectcount == 0) {
                    alert("Select at least one package for either approval or rejection");
                    return false;
                }
                else if (chkboxRejectcount > 0) {
                    var counter = 0;
                    $("#<%=grvPackage.ClientID%> input[id*='chkReject']:checkbox").each(function (index) {
                        var chk = this.id
                        var chkRemark = chk.replace("chkReject", "txtRejectRemark")
                        //alert(this.id)
                        if ($(this).is(':checked') == true && $("#" + chkRemark).val() == "") {
                            counter++;
                            alert("Enter reject reason.")
                            $("#" + chkRemark).focus();
                            return false;
                        }
                    });

                    if (counter == 0) {
                        return true;
                    }
                    else {
                        return false;
                    }
                }
                else {
                    return true;
                }
            });
            $("#<%=grvPackage.ClientID%> input[id*='txtRejectRemark']").keyup(function () {
                alert("Hello");
                controlId = "#" + this.id;
                data = $(controlId).val();
                var iChars = "!@#$%^&*()+=[]\';{}|:<>?\\";
                for (var i = 0; i < data.length; i++) {
                    if (iChars.indexOf(data.charAt(i)) != -1) {
                        alert("Your string has special characters.These are not allowed.");
                        var myString = $(controlId).val().substring(0, (data.length) - 1);
                        $(controlId).val(myString);
                        return false;
                    }
                }
            });
        });
       

    </script>
    <center>
       <%-- <asp:UpdatePanel ID="udpApproval" runat="server" UpdateMode="Conditional">
            <ContentTemplate>--%>
                <table cellpadding="2" cellspacing="0" border="0">
                    <caption>
                        <b>RM Package Approval</b>
                    </caption>
                    <tr>
                        <td style="text-align: center;">
                            <asp:Label ID="lblMessage" runat="server" Text="" ></asp:Label>
                          <%--  <asp:UpdateProgress AssociatedUpdatePanelID="udpApproval" runat="server" DynamicLayout="true">
                                <ProgressTemplate>
                                    <img src="../Images/wait.gif" alt="Loading..." />
                                </ProgressTemplate>
                            </asp:UpdateProgress>--%>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align:center;">
                            <b>Pending Client</b>
                            <asp:DropDownList ID="ddlClient" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlClient_SelectedIndexChanged" ></asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: center;">
                            <asp:GridView ID="grvPackage" runat="server" AutoGenerateColumns="false" DataKeyNames="PkgId,ClientId"
                                OnRowDataBound="grvPackage_RowDataBound" PageSize="500" AllowPaging="true" OnPageIndexChanging="grvPackage_PageIndexChanging" EmptyDataText="No Records Available">
                                <Columns>
                                    <asp:BoundField DataField="CompanyName" HeaderText="Company Name" />
                                    <asp:BoundField DataField="CarCatName" HeaderText="Category Name" />
                                    <asp:BoundField DataField="PkgHrs" HeaderText="Pkg Hrs" />
                                    <asp:BoundField DataField="PkgKMs" HeaderText="Pkg KMs" />
                                    <asp:BoundField DataField="Pkgrate" HeaderText="Pkg Rate" />
                                    <asp:BoundField DataField="ExtraHrRate" HeaderText="ExtraHr Rate" />
                                    <asp:BoundField DataField="ExtraKMRate" HeaderText="ExtraKM Rate" />
                                    <asp:BoundField DataField="ThresholdExtraHr" HeaderText="Threshold Extra Hr" />
                                    <asp:BoundField DataField="ThresholdExtraKM" HeaderText="Threshold Extra KM" />
                                    <asp:BoundField DataField="NightStayAllowance" HeaderText="Night Allowance" />
                                    <asp:BoundField DataField="OutStationAllowance" HeaderText="Outstation Allowance" />
                                    <%--<asp:BoundField DataField="TariffType" HeaderText="Tariff Type" />--%>
                                    <asp:BoundField DataField="CreateDate" HeaderText="Create Date" />
                                    <asp:BoundField DataField="AssignDate" HeaderText="Assign Date" />
                                    <asp:BoundField DataField="EnterdBy" HeaderText="Created By" />
                                    <asp:BoundField DataField="CityName" HeaderText="City Name" />
                                    <asp:BoundField DataField="PackageType" HeaderText="Package Type" />
                                    <asp:BoundField DataField="BillingBasis" HeaderText="Billing Basis" />
                                    <asp:BoundField DataField="PaymentMode" HeaderText="Payment Mode" />
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="checkAll" Text="Approve" runat="server" onclick="checkAllApproval(this);" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox runat="server" ID="chkAprrove" onclick="Check_Approve_Click(this)" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="RejectCheckAll" Text="Reject" runat="server" onclick="checkAllReject(this);" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox runat="server" ID="chkReject" onclick="Check_Reject_Click(this)" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Reject Remark">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtRejectRemark" runat="server" TextMode="MultiLine" Rows="2" Columns="5" Text='<%#Eval("RMRejectRemark")%>'></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            <br />
                            <asp:Button ID="btnApprove" runat="server" Text="Approve" OnClick="btnApprove_Click" />
                        </td>
                    </tr>
                </table>
            <%--</ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnApprove" />
            </Triggers>
        </asp:UpdatePanel>--%>
    </center>
</asp:Content>