﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Salespage.master" AutoEventWireup="true"
    CodeFile="EditStandardPackage.aspx.cs" Inherits="EditStandardPackage" %>

<asp:Content ID="cntPlaceholder" ContentPlaceHolderID="ContentPlaceHolder_Page" runat="Server">
    <script src="../JQuery/jquery-1.4.1.min.js" type="text/javascript"></script>
    <script type="text/javascript">
    </script>
    <center>
        <table cellpadding="0" cellspacing="0" border="0" width="80%">
            <caption>
                <b>Edit Standard Package</b><br />
                &nbsp;
            </caption>
            <tr>
                <td>
                    <b>City Name</b>
                </td>
                <td>
                    <asp:DropDownList ID="ddlCityName" runat="server">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="reqddlCityName" ValidationGroup="pkg" ControlToValidate="ddlCityName"
                        runat="server" ErrorMessage="*" InitialValue="-1" Display="Dynamic"></asp:RequiredFieldValidator>
                </td>
                <td>
                    <b>Prospects Categories</b>
                </td>
                <td>
                    <asp:DropDownList ID="ddlProspectCategories" runat="server">
                        <asp:ListItem Text="All" Value="0"></asp:ListItem>
                        <asp:ListItem Text="Category A" Value="A"></asp:ListItem>
                        <asp:ListItem Text="Category B" Value="B"></asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td>
                    <b>Car Category</b>
                </td>
                <td>
                    <asp:DropDownList ID="ddlCarCat" runat="server">
                    </asp:DropDownList>
                </td>
                <td>
                    <b>Proposal For</b>
                </td>
                <td>
                    <asp:DropDownList ID="ddlProposalFor" runat="server">
                        <asp:ListItem Text="All" Value="0"></asp:ListItem>
                        <asp:ListItem Text="Local" Value="1"></asp:ListItem>
                        <asp:ListItem Text="Outstation" Value="2"></asp:ListItem>
                        <asp:ListItem Text="Fixed Airport Transfer" Value="3"></asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td colspan="2" align="center">
                    <asp:Button ID="btnGet" runat="server" Text="Get" ValidationGroup="pkg" OnClick="btnGet_Click" />
                </td>
            </tr>
            <tr>
                <td colspan="9" align="center">&nbsp;
                    <asp:Label ID="lblMessage" runat="server"></asp:Label>
                </td>
            </tr>
        </table>
        <div>
            <asp:MultiView ID="MultiEditView" runat="server" ActiveViewIndex="0">
                <asp:View ID="viewEditList" runat="server">
                    <asp:GridView ID="grvEditPackage" runat="server" AutoGenerateColumns="false" DataKeyNames="SDPRateId"
                        EmptyDataText="No Record Found" OnSelectedIndexChanged="grvEditPackage_SelectedIndexChanged">
                        <Columns>
                            <asp:BoundField DataField="CityName" HeaderText="City Name" ItemStyle-HorizontalAlign="Center" />
                            <asp:BoundField DataField="ProspectCategory" HeaderText="Prospect Category" ItemStyle-HorizontalAlign="Center" />
                            <asp:BoundField DataField="carcatname" HeaderText="Car Category" ItemStyle-HorizontalAlign="Center" />
                            <asp:BoundField DataField="ProposalFor" HeaderText="Proposal For" ItemStyle-HorizontalAlign="Center" />
                            <asp:BoundField DataField="PurchasePKGRate" HeaderText="Purchase Rate" ItemStyle-HorizontalAlign="Center" />
                            <asp:BoundField DataField="BaseRate" HeaderText="Sale Rate" ItemStyle-HorizontalAlign="Center" />
                            <asp:BoundField DataField="BaseKM" HeaderText="Base KM" ItemStyle-HorizontalAlign="Center" />
                            <asp:BoundField DataField="BaseHr" HeaderText="Base Hours" ItemStyle-HorizontalAlign="Center" />
                            <asp:BoundField DataField="ExtraHrRate" HeaderText="Extra Hours Rate" ItemStyle-HorizontalAlign="Center" />
                            <asp:BoundField DataField="ExtraKMRate" HeaderText="Extra KM Hours" ItemStyle-HorizontalAlign="Center" />
                            <asp:BoundField DataField="ThresholdExtraHr" HeaderText="Threshold Hours" ItemStyle-HorizontalAlign="Center" />
                            <asp:BoundField DataField="ThresholdExtraKM" HeaderText="Threshold KM" ItemStyle-HorizontalAlign="Center" />
                            <asp:BoundField DataField="OutStationAllowance" HeaderText="Out Station Allowance"
                                ItemStyle-HorizontalAlign="Center" />
                            <asp:BoundField DataField="NightStayAllowance" HeaderText="Night Stay Allowance"
                                ItemStyle-HorizontalAlign="Center" />
                            <asp:CommandField ShowSelectButton="true" HeaderText="Edit" EditText="Edit" />
                        </Columns>
                    </asp:GridView>
                </asp:View>
                <asp:View ID="viewEditDetails" runat="server">
                    <table cellpadding="5" cellspacing="0" border="1" width="40%">
                        <tr>
                            <td>
                                <b>City Name</b>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlEditCityName" runat="server" Enabled="false" OnSelectedIndexChanged="ddlEditCityName_SelectedIndexChanged"
                                    AutoPostBack="true">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="rqvEditCityName" ValidationGroup="pkgEdit" ControlToValidate="ddlEditCityName"
                                    runat="server" ErrorMessage="*" InitialValue="0" Display="Dynamic"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td><b>Billing Basis</b></td>
                            <td>
                                <asp:DropDownList ID="ddlbillingbasis" runat="server">
                                    <asp:ListItem Text="-Select-" Value="" Selected="True"></asp:ListItem>
                                    <asp:ListItem Text="Point to Point" Value="pp"></asp:ListItem>
                                    <asp:ListItem Text="Garage to Garage" Value="gg"></asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="regddlbillingbasis" ValidationGroup="pkg" ControlToValidate="ddlbillingbasis"
                                    runat="server" ErrorMessage="*" InitialValue="0" Display="Dynamic"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Prospects Categories</b>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlEditProspectCategories" runat="server" Enabled="false">
                                    <asp:ListItem Text="-Select-" Value="0"></asp:ListItem>
                                    <asp:ListItem Text="Category A" Value="A"></asp:ListItem>
                                    <asp:ListItem Text="Category B" Value="B"></asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="reqddlEditProspectCategories" ValidationGroup="pkgEdit" ControlToValidate="ddlEditProspectCategories"
                                    runat="server" ErrorMessage="*" InitialValue="0" Display="Dynamic"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Car Category</b>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlEditCarCategory" runat="server" Enabled="false">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="reqddlEditCarCategory" ValidationGroup="pkgEdit" ControlToValidate="ddlEditCarCategory"
                                    runat="server" ErrorMessage="*" InitialValue="0" Display="Dynamic"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Client Commitment</b>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlcommitment" runat="server"></asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Proposal For</b>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlEditProposalFor" runat="server" Enabled="false" AutoPostBack="true" OnSelectedIndexChanged="ddlProposalFor_SelectedIndexChanged">
                                    <asp:ListItem Text="-Select-" Value="0"></asp:ListItem>
                                    <asp:ListItem Text="Local" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="Outstation" Value="2"></asp:ListItem>
                                    <asp:ListItem Text="Fixed Airport Transfer" Value="3"></asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="reqddlEditProposalFor" ValidationGroup="pkgEdit" ControlToValidate="ddlEditProposalFor"
                                    runat="server" ErrorMessage="*" InitialValue="0" Display="Dynamic"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Purchase Rate</b>
                            </td>
                            <td>
                                <asp:TextBox ID="txtPurchaseRate" runat="server" Width="50" MaxLength="10" placeholder="0"
                                    OnTextChanged="txtPurchaseRate_TextChanged" AutoPostBack="true"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="reqtxtPurchaseRate" runat="server" ValidationGroup="pkg"
                                    ControlToValidate="txtPurchaseRate" ErrorMessage="*" Display="Dynamic"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ForeColor="Red" runat="server" ID="rexNumber1" ControlToValidate="txtPurchaseRate"
                                    ValidationExpression="\d+" ErrorMessage="Please enter  number!" ValidationGroup="pkg" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Sale Rate</b>
                            </td>
                            <td>
                                <asp:TextBox ID="txtBaseRate" runat="server" Width="50" MaxLength="10" placeholder="0" ReadOnly="true"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Package Hours</b>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlPkgHours" runat="server">
                                    <asp:ListItem Text="-Select-" Value="0" Selected="True"></asp:ListItem>
                                    <asp:ListItem Text="1" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="2" Value="2"></asp:ListItem>
                                    <asp:ListItem Text="3" Value="3"></asp:ListItem>
                                    <asp:ListItem Text="4" Value="4"></asp:ListItem>
                                    <asp:ListItem Text="5" Value="5"></asp:ListItem>
                                    <asp:ListItem Text="6" Value="6"></asp:ListItem>
                                    <asp:ListItem Text="7" Value="7"></asp:ListItem>
                                    <asp:ListItem Text="8" Value="8"></asp:ListItem>
                                    <asp:ListItem Text="9" Value="9"></asp:ListItem>
                                    <asp:ListItem Text="10" Value="10"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Package KM</b>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlPkgKM" runat="server">
                                    <asp:ListItem Text="-Select-" Value="0" Selected="True"></asp:ListItem>
                                    <asp:ListItem Text="10" Value="10"></asp:ListItem>
                                    <asp:ListItem Text="20" Value="20"></asp:ListItem>
                                    <asp:ListItem Text="30" Value="30"></asp:ListItem>
                                    <asp:ListItem Text="40" Value="40"></asp:ListItem>
                                    <asp:ListItem Text="50" Value="50"></asp:ListItem>
                                    <asp:ListItem Text="60" Value="60"></asp:ListItem>
                                    <asp:ListItem Text="70" Value="70"></asp:ListItem>
                                    <asp:ListItem Text="80" Value="80"></asp:ListItem>
                                    <asp:ListItem Text="90" Value="90"></asp:ListItem>
                                    <asp:ListItem Text="100" Value="100"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Extra Hours Rate</b>
                            </td>
                            <td>
                                <asp:TextBox ID="txtExtraHoursRate" runat="server" Width="50" MaxLength="5" placeholder="0"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="reqExtraHoursRate" runat="server" ValidationGroup="pkgEdit"
                                    ControlToValidate="txtExtraHoursRate" ErrorMessage="*" Display="Dynamic"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ForeColor="Red" runat="server" ID="reguExpreExtraHoursRate"
                                    ControlToValidate="txtExtraHoursRate" ValidationExpression="\d+" ErrorMessage="Please enter  number!"
                                    ValidationGroup="pkgEdit" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Extra KM Rate</b>
                            </td>
                            <td>
                                <asp:TextBox ID="txtExtraKMRate" runat="server" Width="50" MaxLength="5" placeholder="0"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="reqExtraKMRate" runat="server" ValidationGroup="pkgEdit"
                                    ControlToValidate="txtExtraKMRate" ErrorMessage="*" Display="Dynamic"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ForeColor="Red" runat="server" ID="reqExpressExtraKMRate"
                                    ControlToValidate="txtExtraKMRate" ValidationExpression="\d+" ErrorMessage="Please enter  number!"
                                    ValidationGroup="pkgEdit" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Threshold Extra Hour</b>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlThreshouldExHr" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Threshold Extra KM</b>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlThreshouldExKM" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Outstation Allowance</b>
                            </td>
                            <td>
                                <asp:TextBox ID="txtOutstationAllowance" runat="server" Width="50" MaxLength="5"
                                    placeholder="0"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="reqtxtOutstationAllowance" runat="server" ValidationGroup="pkgEdit"
                                    ControlToValidate="txtOutstationAllowance" ErrorMessage="*" Display="Dynamic"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ForeColor="Red" runat="server" ID="reqExpOutstationAllowance"
                                    ControlToValidate="txtOutstationAllowance" ValidationExpression="\d+" ValidationGroup="pkgEdit"
                                    ErrorMessage="Please enter  number!" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Night Stay Allowance</b>
                            </td>
                            <td>
                                <asp:TextBox ID="txtNightStayAllowance" runat="server" Width="50" MaxLength="5" placeholder="0"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="reqNightStayAllowance" runat="server" ValidationGroup="pkgEdit"
                                    ControlToValidate="txtNightStayAllowance" ErrorMessage="*" Display="Dynamic"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ForeColor="Red" runat="server" ID="reqExprNightStayAllowance"
                                    ControlToValidate="txtNightStayAllowance" ValidationExpression="\d+" ValidationGroup="pkgEdit"
                                    ErrorMessage="Please enter  number!" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" align="center">
                                <asp:Button ID="btnUpdate" runat="server" Text="Update"
                                    ValidationGroup="pkgEdit" OnClick="btnUpdate_Click" />
                            </td>
                        </tr>
                    </table>
                </asp:View>
            </asp:MultiView>
        </div>
        <div class="loading" align="center">
            Sending mail. Please wait.<br />
            <br />
            <img src="../Images/loader.gif" alt="" />
        </div>
    </center>
</asp:Content>
