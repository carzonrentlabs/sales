﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Insta;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Net.Mail;
using System.Net;
using System.Configuration;

public partial class ShowProposal : System.Web.UI.Page
{
    private sqlProposal objProposal = null;
    private CorDrive objCordrive = null;
    private OL_Proposal objProposalOL = null;
    protected void Page_Load(object sender, EventArgs e)
    {
        lblMessage.Visible = false;
        if (!Page.IsPostBack)
        {
            BindClient();
        }
    }

    private void BindClient()
    {
        objProposal = new sqlProposal();
        DataSet dsProspect = new DataSet();
        try
        {
            dsProspect = objProposal.GetProspectMaster();
            if (dsProspect.Tables[0].Rows.Count > 0)
            {
                ddlClient.DataSource = dsProspect.Tables[0];
                ddlClient.DataTextField = "ClientCoName";
                ddlClient.DataValueField = "ID";
                ddlClient.DataBind();
                ddlClient.Items.Insert(0, new ListItem("All", "0"));
            }
            else
            {
                ddlClient.DataSource = null;
                ddlClient.DataBind();
                lblMessage.Visible = true;
                lblMessage.Text = "Record not available";
                lblMessage.ForeColor = Color.Red;
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void BindApprovalDetails()
    {
        objProposalOL = new OL_Proposal();
        objProposalOL.ProspectID = Convert.ToInt32(ddlClient.SelectedValue);

        objProposal = new sqlProposal();
        DataSet ds = new DataSet();
        lblMessage.Visible = false;
        try
        {
            ds = objProposal.GetApprovalDetails_View(objProposalOL);
            if (ds.Tables[0].Rows.Count > 0)
            {
                grvApproval.DataSource = ds.Tables[0];
                grvApproval.DataBind();
            }
            else
            {
                grvApproval.DataSource = null;
                grvApproval.DataBind();
                lblMessage.Visible = true;
                lblMessage.Text = "Record not available";
                lblMessage.ForeColor = Color.Red;
            }
        }
        catch (Exception Ex)
        {

            lblMessage.Visible = true;
            lblMessage.Text = Ex.Message;
            lblMessage.ForeColor = Color.Red;
        }
    }

    protected void btnGet_Click(object sender, EventArgs e)
    {
        BindApprovalDetails();
    }
}