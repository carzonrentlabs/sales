﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Salespage.master" AutoEventWireup="true" CodeFile="BuildProposal.aspx.cs" Inherits="Proposal_BuildProposal" %>

<asp:Content ID="cntPlaceholder" ContentPlaceHolderID="ContentPlaceHolder_Page" runat="Server">

    <script src="../JQuery/jquery-1.4.min.js" type="text/javascript"></script>

    <script src="../JQuery/moment.js" type="text/javascript"></script>

    <script src="../JQuery/ui.core.js" type="text/javascript"></script>

    <script src="../JQuery/ui.datepicker.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {
            $("#<%=txtDate.ClientID%>").datepicker();
        });

    </script>

    <center>
        <table cellpadding="5" cellspacing="0" border="0" width="80%">
            <caption>Build Proposal</caption>
            <tr>
                <td colspan="6">
                    <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
                </td>
            </tr>
            <tr>
                <td><b>Client</b></td>
                <td>
                    <asp:DropDownList ID="ddlClient" runat="server" Width="200px"></asp:DropDownList>
                </td>
                <td><b>City Name</b></td>
                <td>
                    <asp:DropDownList ID="ddlCityName" runat="server"></asp:DropDownList>
                </td>
                <td><b>Date of Initition</b></td>
                <td>
                    <asp:TextBox ID="txtDate" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td><b>Type of Proposal</b></td>
                <td>
                    <asp:DropDownList ID="ddlTypeProposal" runat="server">
                    </asp:DropDownList>

                </td>
                <td><b>Proposal For</b></td>
                <td>
                    <asp:DropDownList ID="ddlProposalFor" runat="server">
                        <asp:ListItem Text="All" Value="-1"></asp:ListItem>
                        <asp:ListItem Text="Pan India" Value="1"></asp:ListItem>
                        <asp:ListItem Text="Local" Value="2"></asp:ListItem>
                        <asp:ListItem Text="Fixed Airport Transfer" Value="3"></asp:ListItem>

                    </asp:DropDownList>
                </td>
                <td><b>Action Manager Name</b></td>
                <td>
                    <asp:TextBox ID="txtActionManageName" runat="server"></asp:TextBox>
                </td>

            </tr>
            <tr>
                <td colspan="6" style="text-align: center">
                    <asp:GridView ID="grvProposal" runat="server" AutoGenerateColumns="false" OnRowDataBound="grvProposal_RowDataBound" ShowFooter ="true" OnRowCreated="grvProposal_RowCreated">
                        <Columns>
                            <asp:BoundField DataField="RowNumber" HeaderText="SNo." />
                            <asp:TemplateField HeaderText="Pacakage Type">
                                <ItemTemplate>
                                    <asp:DropDownList runat="server" ID="ddlPackageType">
                                    </asp:DropDownList>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="City Name">
                                <ItemTemplate>
                                    <asp:DropDownList runat="server" ID="ddlPackageCityName">
                                    </asp:DropDownList>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Vehicle Category">
                                <ItemTemplate>
                                    <asp:DropDownList runat="server" ID="ddlVehicleCategory">
                                    </asp:DropDownList>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Vehcile Model">
                                <ItemTemplate>
                                    <asp:DropDownList runat="server" ID="ddlVehicleModel">
                                    </asp:DropDownList>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Package Rate">
                                <ItemTemplate>
                                    <asp:DropDownList runat="server" ID="ddlPackageRate">
                                    </asp:DropDownList>
                                </ItemTemplate>

                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Pacakage KM">
                                <ItemTemplate>
                                    <asp:DropDownList runat="server" ID="ddlPackageKM">
                                    </asp:DropDownList>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Pacakage Hour">
                                <ItemTemplate>
                                    <asp:DropDownList runat="server" ID="ddlPackageHour">
                                    </asp:DropDownList>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Extra KM">
                                <ItemTemplate>
                                    <asp:DropDownList runat="server" ID="ddlPackageExtraKM">
                                    </asp:DropDownList>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Extra Hour">
                                <ItemTemplate>
                                    <asp:DropDownList runat="server" ID="ddlPackageExtraHr">
                                    </asp:DropDownList>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Threshold Extra Hr">
                                <ItemTemplate>
                                    <asp:DropDownList runat="server" ID="ddlthresholdExtraHr">
                                    </asp:DropDownList>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Threshold Extra KM">
                                <ItemTemplate>
                                    <asp:DropDownList runat="server" ID="ddlthresholdExtraKM">
                                    </asp:DropDownList>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Night Charges"  ControlStyle-Width="50px">
                                <ItemTemplate>
                                    <asp:TextBox runat="server" ID="txtNightCharges"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Chauffeur Charges" ControlStyle-Width="50px">
                                <ItemTemplate>
                                    <asp:TextBox runat="server" ID="txtNightChauffeurCharges"></asp:TextBox>
                                </ItemTemplate>
                                
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Add/Remove">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkRemomve" runat="server" Text="Remove"  onclick="lnkRemomve_Click"></asp:LinkButton>
                                </ItemTemplate>
                                <FooterStyle HorizontalAlign="Right" />
                                <FooterTemplate>
                                    <asp:Button ID="btnAdd" runat="server"
                                        Text="Add New Row" onclick="btnAdd_Click"/>
                                </FooterTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </td>

            </tr>

        </table>
    </center>
</asp:Content>


