﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Drawing;
using Insta;

public partial class Proposal_BuildProposal : System.Web.UI.Page
{
    private sqlProposal objProposal = null;
    private CorDrive objCordrive = null;
    protected void Page_Load(object sender, EventArgs e)
    {
        lblMessage.Visible = false;
        if (!Page.IsPostBack)
        {
            BindProspectMaster();
            BindCity();
            BindServiceType();
            BindGridWithInitialValue();
        }

    }
    private void BindGridWithInitialValue()
    {
        
        DataTable dt = new DataTable();
        DataRow dr = null;
        dt.Columns.Add(new DataColumn("RowNumber", typeof(string)));
        dt.Columns.Add(new DataColumn("Column1", typeof(string)));//for TextBox value   
        dt.Columns.Add(new DataColumn("Column2", typeof(string)));//for TextBox value   
        dt.Columns.Add(new DataColumn("Column3", typeof(string)));//for DropDownList selected item   
        dt.Columns.Add(new DataColumn("Column4", typeof(string)));//for DropDownList selected item   
        dt.Columns.Add(new DataColumn("Column5", typeof(string)));//for DropDownList selected item   
        dt.Columns.Add(new DataColumn("Column6", typeof(string)));//for DropDownList selected item   
        dt.Columns.Add(new DataColumn("Column7", typeof(string)));//for DropDownList selected item   
        dt.Columns.Add(new DataColumn("Column8", typeof(string)));//for DropDownList selected item   
        dt.Columns.Add(new DataColumn("Column9", typeof(string)));//for DropDownList selected item   
        dt.Columns.Add(new DataColumn("Column10", typeof(string)));//for DropDownList selected item   
        dt.Columns.Add(new DataColumn("Column11", typeof(string)));//for DropDownList selected item   

        dr = dt.NewRow();
        dr["RowNumber"] = 1;
        dr["Column1"] = string.Empty;
        dr["Column2"] = string.Empty;
        dt.Rows.Add(dr);

        //Store the DataTable in ViewState for future reference   
        ViewState["CurrentTable"] = dt;

        //Bind the Gridview   
        grvProposal.DataSource = dt;
        grvProposal.DataBind();

    }
    private void BindServiceType()
    {
        objProposal = new sqlProposal();
        DataSet dsService = new DataSet();
        try
        {
            dsService = objProposal.GetTypeofProposal();
            if (dsService.Tables[0].Rows.Count > 0)
            {
                ddlTypeProposal.DataSource = dsService.Tables[0];
                ddlTypeProposal.DataTextField = "ServiceName";
                ddlTypeProposal.DataValueField = "serviceTypeId";
                ddlTypeProposal.DataBind();
            }
            else
            {
                ddlTypeProposal.DataSource = null;
                ddlTypeProposal.DataBind();
                lblMessage.Visible = true;
                lblMessage.Text = "Record not available";
                lblMessage.ForeColor = Color.Red;
            }

        }
        catch (Exception ex)
        {

            throw new Exception(ex.Message);
        }
    }
    private void BindProspectMaster()
    {
        objProposal = new sqlProposal();
        DataSet dsProspect = new DataSet();
        try
        {
            dsProspect = objProposal.GetProspectMaster();
            if (dsProspect.Tables[0].Rows.Count > 0)
            {
                ddlClient.DataSource = dsProspect.Tables[0];
                ddlClient.DataTextField = "ClientCoName";
                ddlClient.DataValueField = "ID";
                ddlClient.DataBind();
            }
            else
            {
                ddlClient.DataSource = null;
                ddlClient.DataBind();
                lblMessage.Visible = true;
                lblMessage.Text = "Record not available";
                lblMessage.ForeColor = Color.Red;
            }

        }
        catch (Exception ex)
        {

            throw new Exception(ex.Message);
        }
    }
    private void BindCity()
    {
        try
        {
            objCordrive = new CorDrive();
            DataSet dsCustName = new DataSet();
            dsCustName = objCordrive.GetCityName();
            if (dsCustName.Tables[0].Rows.Count > 0)
            {
                ddlCityName.DataTextField = "cityname";
                ddlCityName.DataValueField = "cityid";
                ddlCityName.DataSource = dsCustName;
                ddlCityName.DataBind();
                ddlCityName.Items.Insert(0, new ListItem("--Select--", "0"));
            }
        }
        catch (Exception Ex)
        {
            lblMessage.Visible = true;
            lblMessage.ForeColor = Color.Red;
            lblMessage.Text = Ex.Message;
        }
    }
    private void BindDropDownList(DropDownList ddlName, DataTable dtData, string dataTextField, string dataValuefield)
    {
        if (dtData != null && dtData.Rows.Count > 0)
        {
            ddlName.Items.Clear();
            ddlName.DataSource = dtData;
            ddlName.DataTextField = dataTextField;
            ddlName.DataValueField = dataValuefield;
            ddlName.DataBind();
            ddlName.Items.Insert(0, new ListItem("--Select--", "0"));
        }
        else
        {
            ddlName.Items.Clear();
            ddlName.Items.Insert(0, new ListItem("--Select--", "0"));
        }

    }
    private void BindDropDownList(DropDownList ddlName, int startValue, int endValue, int incrementvalue)
    {

        Dictionary<int, int> ddlSource = new Dictionary<int, int>();

        for (int i = startValue; i <= endValue; i = i + incrementvalue)
        {
            ddlSource.Add(i, i);
        }
        ddlName.Items.Clear();
        ddlName.DataSource = ddlSource;
        ddlName.DataTextField = "key"; //ddlSource.Keys.ToString();
        ddlName.DataValueField = "value"; //ddlSource.Values.ToString();
        ddlName.DataBind();
        ddlName.Items.Insert(0, new ListItem("--Select--", "0"));
    }
    protected void grvProposal_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        objProposal = new sqlProposal();
        objCordrive = new CorDrive();
        DataSet ds = new DataSet();
        try
        {

            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                DropDownList ddlPackageType = (DropDownList)e.Row.FindControl("ddlPackageType");
                ds = objProposal.GetPackageType();
                BindDropDownList(ddlPackageType, ds.Tables[0], "PackageType", "PKGTypeID");

                DropDownList ddlPackageCityName = (DropDownList)e.Row.FindControl("ddlPackageCityName");
                ds = objCordrive.GetCityName();
                BindDropDownList(ddlPackageCityName, ds.Tables[0], "CityName", "CityID");

                DropDownList ddlVehicleCategory = (DropDownList)e.Row.FindControl("ddlVehicleCategory");
                ds = objProposal.GetCarCategory();
                BindDropDownList(ddlVehicleCategory, ds.Tables[0], "CarCatName", "CarCatID");

                DropDownList ddlVehicleModel = (DropDownList)e.Row.FindControl("ddlVehicleModel");
                ds = objProposal.GetCarModel(5);
                BindDropDownList(ddlVehicleModel, ds.Tables[0], "CarModelName", "CarModelID");

                DropDownList ddlPackageRate = (DropDownList)e.Row.FindControl("ddlPackageRate");
                BindDropDownList(ddlPackageRate, 10, 5000, 1);

                DropDownList ddlPackageKM = (DropDownList)e.Row.FindControl("ddlPackageKM");
                BindDropDownList(ddlPackageKM, 10, 5000, 1);

                DropDownList ddlPackageHour = (DropDownList)e.Row.FindControl("ddlPackageHour");
                BindDropDownList(ddlPackageHour, 10, 5000, 1);

                DropDownList ddlPackageExtraKM = (DropDownList)e.Row.FindControl("ddlPackageExtraKM");
                BindDropDownList(ddlPackageExtraKM, 10, 5000, 1);

                DropDownList ddlPackageExtraHr = (DropDownList)e.Row.FindControl("ddlPackageExtraHr");
                BindDropDownList(ddlPackageExtraHr, 10, 5000, 1);

                DropDownList ddlthresholdExtraHr = (DropDownList)e.Row.FindControl("ddlthresholdExtraHr");
                BindDropDownList(ddlthresholdExtraHr, 10, 5000, 1);

                DropDownList ddlthresholdExtraKM = (DropDownList)e.Row.FindControl("ddlthresholdExtraKM");
                BindDropDownList(ddlthresholdExtraKM, 10, 5000, 1);


            }
        }
        catch (Exception Ex)
        {
            lblMessage.Visible = true;
            lblMessage.ForeColor = Color.Red;
            lblMessage.Text = Ex.Message;
        }
    }
    private void AddNewRowToGrid()
    {

        if (ViewState["CurrentTable"] != null)
        {

            DataTable dtCurrentTable = (DataTable)ViewState["CurrentTable"];
            DataRow drCurrentRow = null;

            if (dtCurrentTable.Rows.Count > 0)
            {
                drCurrentRow = dtCurrentTable.NewRow();
                drCurrentRow["RowNumber"] = dtCurrentTable.Rows.Count + 1;

                //add new row to DataTable   
                dtCurrentTable.Rows.Add(drCurrentRow);
                //Store the current data to ViewState for future reference   

                ViewState["CurrentTable"] = dtCurrentTable;

                for (int i = 0; i < dtCurrentTable.Rows.Count - 1; i++)
                {

                    DropDownList ddlPackageType = (DropDownList)grvProposal.Rows[i].Cells[1].FindControl("ddlPackageType");
                    DropDownList ddlPackageCityName = (DropDownList)grvProposal.Rows[i].Cells[2].FindControl("ddlPackageCityName");
                    DropDownList ddlVehicleCategory = (DropDownList)grvProposal.Rows[i].Cells[3].FindControl("ddlVehicleCategory");
                    DropDownList ddlVehicleModel = (DropDownList)grvProposal.Rows[i].Cells[4].FindControl("ddlVehicleModel");
                    DropDownList ddlPackageRate = (DropDownList)grvProposal.Rows[i].Cells[5].FindControl("ddlPackageRate");
                    DropDownList ddlPackageKM = (DropDownList)grvProposal.Rows[i].Cells[6].FindControl("ddlPackageKM");
                    DropDownList ddlPackageHour = (DropDownList)grvProposal.Rows[i].Cells[7].FindControl("ddlPackageHour");
                    DropDownList ddlPackageExtraKM = (DropDownList)grvProposal.Rows[i].Cells[8].FindControl("ddlPackageExtraKM");
                    DropDownList ddlPackageExtraHr = (DropDownList)grvProposal.Rows[i].Cells[9].FindControl("ddlPackageExtraHr");
                    DropDownList ddlthresholdExtraHr = (DropDownList)grvProposal.Rows[i].Cells[10].FindControl("ddlthresholdExtraHr");
                    DropDownList ddlthresholdExtraKM = (DropDownList)grvProposal.Rows[i].Cells[11].FindControl("ddlthresholdExtraKM");

                    /*
                    //extract the TextBox values   

                    TextBox box1 = (TextBox)grvProposal.Rows[i].Cells[1].FindControl("TextBox1");
                    TextBox box2 = (TextBox)grvProposal.Rows[i].Cells[2].FindControl("TextBox2");

                    dtCurrentTable.Rows[i]["Column1"] = box1.Text;
                    dtCurrentTable.Rows[i]["Column2"] = box2.Text;

                    //extract the DropDownList Selected Items   

                    DropDownList ddl1 = (DropDownList)grvProposal.Rows[i].Cells[3].FindControl("DropDownList1");
                    DropDownList ddl2 = (DropDownList)grvProposal.Rows[i].Cells[4].FindControl("DropDownList2");
                    */
                    // Update the DataRow with the DDL Selected Items   

                    dtCurrentTable.Rows[i]["Column1"] = ddlPackageType.SelectedItem.Text;
                    dtCurrentTable.Rows[i]["Column2"] = ddlPackageCityName.SelectedItem.Text;
                    dtCurrentTable.Rows[i]["Column3"] = ddlVehicleCategory.SelectedItem.Text;
                    dtCurrentTable.Rows[i]["Column4"] = ddlVehicleModel.SelectedItem.Text;
                    dtCurrentTable.Rows[i]["Column5"] = ddlPackageRate.SelectedItem.Text;
                    dtCurrentTable.Rows[i]["Column6"] = ddlPackageKM.SelectedItem.Text;
                    dtCurrentTable.Rows[i]["Column7"] = ddlPackageHour.SelectedItem.Text;
                    dtCurrentTable.Rows[i]["Column8"] = ddlPackageExtraKM.SelectedItem.Text;
                    dtCurrentTable.Rows[i]["Column9"] = ddlPackageExtraHr.SelectedItem.Text;
                    dtCurrentTable.Rows[i]["Column10"] = ddlthresholdExtraHr.SelectedItem.Text;
                    dtCurrentTable.Rows[i]["Column11"] = ddlthresholdExtraKM.SelectedItem.Text;


                }


                //Rebind the Grid with the current data to reflect changes   
                grvProposal.DataSource = dtCurrentTable;
                grvProposal.DataBind();
            }
        }
        else
        {
            Response.Write("ViewState is null");
        }
        //Set Previous Data on Postbacks   
        SetPreviousData();
    }
    private void SetPreviousData()
    {

        int rowIndex = 0;
        if (ViewState["CurrentTable"] != null)
        {

            DataTable dt = (DataTable)ViewState["CurrentTable"];
            if (dt.Rows.Count > 0)
            {

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    /*
                    TextBox box1 = (TextBox)grvProposal.Rows[i].Cells[1].FindControl("TextBox1");
                    TextBox box2 = (TextBox)grvProposal.Rows[i].Cells[2].FindControl("TextBox2");

                    DropDownList ddl1 = (DropDownList)grvProposal.Rows[rowIndex].Cells[3].FindControl("DropDownList1");
                    DropDownList ddl2 = (DropDownList)grvProposal.Rows[rowIndex].Cells[4].FindControl("DropDownList2");

                    //Fill the DropDownList with Data   
                    // FillDropDownList(ddl1);
                    // FillDropDownList(ddl2);
                    */

                    DropDownList ddlPackageType = (DropDownList)grvProposal.Rows[i].Cells[1].FindControl("ddlPackageType");
                    DropDownList ddlPackageCityName = (DropDownList)grvProposal.Rows[i].Cells[2].FindControl("ddlPackageCityName");
                    DropDownList ddlVehicleCategory = (DropDownList)grvProposal.Rows[i].Cells[3].FindControl("ddlVehicleCategory");
                    DropDownList ddlVehicleModel = (DropDownList)grvProposal.Rows[i].Cells[4].FindControl("ddlVehicleModel");
                    DropDownList ddlPackageRate = (DropDownList)grvProposal.Rows[i].Cells[5].FindControl("ddlPackageRate");
                    DropDownList ddlPackageKM = (DropDownList)grvProposal.Rows[i].Cells[6].FindControl("ddlPackageKM");
                    DropDownList ddlPackageHour = (DropDownList)grvProposal.Rows[i].Cells[7].FindControl("ddlPackageHour");
                    DropDownList ddlPackageExtraKM = (DropDownList)grvProposal.Rows[i].Cells[8].FindControl("ddlPackageExtraKM");
                    DropDownList ddlPackageExtraHr = (DropDownList)grvProposal.Rows[i].Cells[9].FindControl("ddlPackageExtraHr");
                    DropDownList ddlthresholdExtraHr = (DropDownList)grvProposal.Rows[i].Cells[10].FindControl("ddlthresholdExtraHr");
                    DropDownList ddlthresholdExtraKM = (DropDownList)grvProposal.Rows[i].Cells[11].FindControl("ddlthresholdExtraKM");

                    if (i < dt.Rows.Count - 1)
                    {

                        //Assign the value from DataTable to the TextBox   
                        // box1.Text = dt.Rows[i]["Column1"].ToString();
                        // box2.Text = dt.Rows[i]["Column2"].ToString();

                        //Set the Previous Selected Items on Each DropDownList  on Postbacks   
                        ddlPackageType.ClearSelection();
                        ddlPackageType.Items.FindByText(dt.Rows[i]["Column1"].ToString()).Selected = true;

                        ddlPackageCityName.ClearSelection();
                        ddlPackageCityName.Items.FindByText(dt.Rows[i]["Column2"].ToString()).Selected = true;

                        ddlVehicleCategory.ClearSelection();
                        ddlVehicleCategory.Items.FindByText(dt.Rows[i]["Column3"].ToString()).Selected = true;


                        ddlVehicleModel.ClearSelection();
                        ddlVehicleModel.Items.FindByText(dt.Rows[i]["Column4"].ToString()).Selected = true;

                        ddlPackageRate.ClearSelection();
                        ddlPackageRate.Items.FindByText(dt.Rows[i]["Column5"].ToString()).Selected = true;

                        ddlPackageKM.ClearSelection();
                        ddlPackageKM.Items.FindByText(dt.Rows[i]["Column6"].ToString()).Selected = true;

                        ddlPackageHour.ClearSelection();
                        ddlPackageHour.Items.FindByText(dt.Rows[i]["Column7"].ToString()).Selected = true;

                        ddlPackageExtraKM.ClearSelection();
                        ddlPackageExtraKM.Items.FindByText(dt.Rows[i]["Column8"].ToString()).Selected = true;

                        ddlPackageExtraHr.ClearSelection();
                        ddlPackageExtraHr.Items.FindByText(dt.Rows[i]["Column9"].ToString()).Selected = true;

                        ddlthresholdExtraHr.ClearSelection();
                        ddlthresholdExtraHr.Items.FindByText(dt.Rows[i]["Column10"].ToString()).Selected = true;

                        ddlthresholdExtraKM.ClearSelection();
                        ddlthresholdExtraKM.Items.FindByText(dt.Rows[i]["Column11"].ToString()).Selected = true;
                    }

                    rowIndex++;
                }
            }
        }
    }
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        AddNewRowToGrid();
    }

    protected void lnkRemomve_Click(object sender, EventArgs e)
    {
        LinkButton lb = (LinkButton)sender;
        GridViewRow gvRow = (GridViewRow)lb.NamingContainer;
        int rowID = gvRow.RowIndex;
        if (ViewState["CurrentTable"] != null)
        {

            DataTable dt = (DataTable)ViewState["CurrentTable"];
            if (dt.Rows.Count > 1)
            {
                if (gvRow.RowIndex < dt.Rows.Count - 1)
                {
                    //Remove the Selected Row data and reset row number  
                    dt.Rows.Remove(dt.Rows[rowID]);
                    ResetRowID(dt);
                }
            }

            //Store the current data in ViewState for future reference  
            ViewState["CurrentTable"] = dt;

            //Re bind the GridView for the updated data  
            grvProposal.DataSource = dt;
            grvProposal.DataBind();
        }

        //Set Previous Data on Postbacks  
        SetPreviousData();
    }

    private void ResetRowID(DataTable dt)
    {
        int rowNumber = 1;
        if (dt.Rows.Count > 0)
        {
            foreach (DataRow row in dt.Rows)
            {
                row[0] = rowNumber;
                rowNumber++;
            }
        }
    }
    protected void grvProposal_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataTable dt = (DataTable)ViewState["CurrentTable"];
            LinkButton lb = (LinkButton)e.Row.FindControl("lnkRemomve");
            if (lb != null)
            {
                if (dt.Rows.Count > 1)
                {
                    if (e.Row.RowIndex == dt.Rows.Count - 1)
                    {
                        lb.Visible = false;
                    }
                }
                else
                {
                    lb.Visible = false;
                }
            }
        }
    }
}