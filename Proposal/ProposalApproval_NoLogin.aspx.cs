﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Insta;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Net.Mail;
using System.Net;
using System.Configuration;

public partial class ProposalApproval_NoLogin : System.Web.UI.Page
{
    private sqlProposal objProposal = null;
    private OL_Proposal objProposalOL = null;
    protected void Page_Load(object sender, EventArgs e)
    {
        lblMessage.Visible = false;
        if (!Page.IsPostBack)
        {
            string str = Request.QueryString["str"];

            BindUnApprovalDetails(str);
        }
    }
    
    private void BindUnApprovalDetails(string COOApproval)
    {
        objProposal = new sqlProposal();
        DataSet ds = new DataSet();
        lblMessage.Visible = false;
        try
        {
            int intCOOApproval = 2;

            if (COOApproval == "CEOApproval")
            {
                intCOOApproval = 0;
            }

            if (COOApproval == "COOApproval")
            {
                intCOOApproval = 1;
            }

            ds = objProposal.GetApprovalDetails_NoLogin(intCOOApproval);
            if (ds.Tables[0].Rows.Count > 0)
            {
                grvApproval.DataSource = ds.Tables[0];
                grvApproval.DataBind();
            }
            else
            {
                grvApproval.DataSource = null;
                grvApproval.DataBind();
                lblMessage.Visible = true;
                lblMessage.Text = "Record not available";
                lblMessage.ForeColor = Color.Red;
            }
        }
        catch (Exception Ex)
        {

            lblMessage.Visible = true;
            lblMessage.Text = Ex.Message;
            lblMessage.ForeColor = Color.Red;
        }
    }

    protected void grvApproval_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        objProposal = new sqlProposal();
        objProposalOL = new OL_Proposal();
        int status = 0;
        if (e.CommandName == "Approval")
        {
            int index = Convert.ToInt16(e.CommandArgument);
            GridViewRow row = grvApproval.Rows[index];
            //DropDownList ddlProposalStatus = (DropDownList)grvApproval.Rows[index].FindControl("ddlProposalStatus");
            DropDownList ddlApprovalStatus = (DropDownList)grvApproval.Rows[index].FindControl("ddlApprovalStatus");
            TextBox txtReasonRecheck = (TextBox)grvApproval.Rows[index].FindControl("txtReasonRecheck");
            HiddenField hdnProposalDetailId = (HiddenField)grvApproval.Rows[index].FindControl("hdnProposalDetailId");



            objProposalOL.ProposalID = (int)grvApproval.DataKeys[index].Values[0];
            objProposalOL.ApprovalStatus = ddlApprovalStatus.SelectedValue.ToString();
            objProposalOL.ProposalDetailId = Convert.ToInt32(hdnProposalDetailId.Value);

            //if (string.IsNullOrEmpty(txtReasonRecheck.Text.Trim()))
            //{
            //    lblMessage.Visible = true;
            //    lblMessage.Text = "Please enter remarks.";
            //    lblMessage.ForeColor = Color.Red;
            //    txtReasonRecheck.Focus();
            //    return;
            //}

            objProposalOL.ApprovalRemark = txtReasonRecheck.Text.ToString();
            objProposalOL.SysUserId = 1; //Convert.ToInt32(Session["UserID"]);
            if (objProposalOL.ProposalID > 0)
            {
                status = objProposal.ApprovedProposal_NoLogic(objProposalOL);
                if (status > 0)
                {
                    //SendMail(objProposalOL);
                    lblMessage.Visible = true;
                    lblMessage.Text = "Approved successfully.";
                    lblMessage.ForeColor = Color.Red;
                    grvApproval.DataSource = null;
                    grvApproval.DataBind();

                    string str = Request.QueryString["str"];

                    BindUnApprovalDetails(str);
                }
                else
                {
                    lblMessage.Visible = true;
                    lblMessage.Text = "Getting save error.";
                    lblMessage.ForeColor = Color.Red;
                }
            }
        }
    }
    protected void grvApproval_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            try
            {
                DropDownList ddlApprovalStatus = (DropDownList)e.Row.FindControl("ddlApprovalStatus");
                /*
                Label lblpurchaserate = (Label)e.Row.FindControl("lblpurchaserate");
                Label lblsalesrate = (Label)e.Row.FindControl("lblsalesrate");

                Button bntSave = (Button)e.Row.FindControl("bntSave");

                lblpurchaserate.Text = (Math.Round((Convert.ToDouble(lblsalesrate.Text) * 100)
                    / (100 + Convert.ToDouble(ConfigurationManager.AppSettings["PercentIncreaseInPurchase"])), 2)).ToString();
                */

                ddlApprovalStatus.SelectedValue = "1";
                /*
                if (Convert.ToInt32(grvApproval.DataKeys[e.Row.RowIndex].Values[2]) == 0)
                {
                    ddlApprovalStatus.SelectedValue = "1";
                    bntSave.Enabled = true;
                    bntSave.Visible = true;
                    ddlApprovalStatus.Enabled = true;
                }
                else
                {
                    ddlApprovalStatus.SelectedValue = Convert.ToInt32(grvApproval.DataKeys[e.Row.RowIndex].Values[2]).ToString();
                    bntSave.Enabled = false;
                    bntSave.Visible = false;
                    ddlApprovalStatus.Enabled = false;
                }
                */
            }
            catch (Exception ex)
            {
                lblMessage.Visible = true;
                lblMessage.Text = ex.Message;
                lblMessage.ForeColor = Color.Red;
            }
        }
    }    
}