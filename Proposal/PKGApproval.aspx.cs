﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class Proposal_PKGApproval : System.Web.UI.Page
{
    private SQLPackage objSQLPkg = null;
    SendSMS.SendSMS objSendSMS = new SendSMS.SendSMS();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            GetUnApprovePackage(0);
            BindClient();
        }

    }

    private void BindClient()
    {
        objSQLPkg = new SQLPackage();
        DataSet dsClient = new DataSet();

        try
        {
            dsClient = objSQLPkg.GetUnApprovedClient(Convert.ToInt32(Session["UserID"]));
            if (dsClient.Tables[0].Rows.Count > 0)
            {
                ddlClient.DataSource = dsClient.Tables[0];
                ddlClient.DataTextField = "ClientCoName";
                ddlClient.DataValueField = "clientcoID";
                ddlClient.DataBind();
            }
            else
            {
                ddlClient.DataSource = null;
                ddlClient.DataBind();
               
            }
            ddlClient.Items.Insert(0, new ListItem("--Select--", "0"));
        }
        catch (Exception)
        {
            throw new Exception();
        }

    }

    private void GetUnApprovePackage(int clientID)
    {
        objSQLPkg = new SQLPackage();
        DataSet dsPkg = new DataSet();
       
        try
        {
            dsPkg = objSQLPkg.GetApprovalPackageDetails(Convert.ToInt32(Session["UserID"]), clientID);
            if (dsPkg.Tables[0].Rows.Count > 0)
            {
                grvPackage.DataSource = dsPkg.Tables[0];
                grvPackage.DataBind();
            }
            else
            {
                grvPackage.DataSource = null;
                grvPackage.DataBind();
                btnApprove.Style.Add("display","none");
            }

        }
        catch (Exception)
        {
            throw new Exception();
        }


    }

    protected void grvPackage_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Attributes.Add("onmouseover", "MouseEvents(this, event)");
            e.Row.Attributes.Add("onmouseout", "MouseEvents(this, event)");

        }
    }
    protected void btnApprove_Click(object sender, EventArgs e)
    {
        OL_PKG objPkg = new OL_PKG();
        OL_PKG_Link objPKGlink = null;
        objSQLPkg = new SQLPackage();
        List<OL_PKG_Link> objLinkList = new List<OL_PKG_Link>();
        try
        {

            foreach (GridViewRow row in grvPackage.Rows)
            {  
                CheckBox checkApprove = (CheckBox)row.FindControl("chkAprrove");
                CheckBox checkReject = (CheckBox)row.FindControl("chkReject");
                TextBox rejectRemark = (TextBox)row.FindControl("txtRejectRemark");
                if (checkApprove.Checked == true || checkReject.Checked == true)
                {
                    objPKGlink = new OL_PKG_Link();
                    objPKGlink.PkgID = (int)grvPackage.DataKeys[row.RowIndex].Values[0];
                    objPKGlink.ClientID = (int)grvPackage.DataKeys[row.RowIndex].Values[1];
                    objPKGlink.ApprovedBy = Convert.ToInt32(Session["UserID"]);
                    if (checkApprove.Checked == true)
                    {
                        objPKGlink.ApproveYN = true;
                    }
                    else if (checkReject.Checked == true && (string.IsNullOrEmpty(rejectRemark.Text) || rejectRemark.Text.Trim() == ""))
                    {
                        lblMessage.Text = "Enter Remark";
                        lblMessage.ForeColor = System.Drawing.Color.Red;
                        return;
                    }
                    else if (checkReject.Checked == true && !string.IsNullOrEmpty(rejectRemark.Text))
                    {
                        objPKGlink.ApproveYN = false;
                        objPKGlink.Remark = rejectRemark.Text;
                    }
                    objLinkList.Add(objPKGlink);
                }
            }
            if (objLinkList.Count > 0)
            {
                int status = objSQLPkg.ApproveRM(objLinkList);
                if (status > 0)
                {
                    lblMessage.Text = status + " Records updated successfully.";
                    lblMessage.ForeColor = System.Drawing.Color.Red;
                    string mailstatus = objSendSMS.RMPackageRejection(objPKGlink.ApprovedBy, objPKGlink.ClientID);
                    GetUnApprovePackage(Convert.ToInt32(ddlClient.SelectedItem.Value));
                }
                else
                {
                    lblMessage.Text = "Getting error while approve package.";
                    lblMessage.ForeColor = System.Drawing.Color.Red;
                }
            }
            else
            {
                lblMessage.Text = "Select at least one package for any action.";
                lblMessage.ForeColor = System.Drawing.Color.Red;
            }
        }
        catch (Exception Ex)
        {
            lblMessage.Text = Ex.Message;
            lblMessage.ForeColor = System.Drawing.Color.Red;
        }

    }
    protected void grvPackage_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grvPackage.PageIndex = e.NewPageIndex;
        GetUnApprovePackage(Convert.ToInt32(ddlClient.SelectedItem.Value));
    }

    protected void ddlClient_SelectedIndexChanged(object sender, EventArgs e)
    {
        int clientID =Convert.ToInt32(ddlClient.SelectedItem.Value);
        GetUnApprovePackage(clientID);
    }
}