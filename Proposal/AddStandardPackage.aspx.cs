﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;

public partial class AddStandardPackage : System.Web.UI.Page
{
    private sqlProposal objProposal = null;
    private OL_PKG objPkg = null;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            BindCity();
            BindExtraHours();
            BindExtraKM();
            BindCarCategory();
            BindClientCommitment();
        }
    }


    private void BindCity()
    {
        objProposal = new sqlProposal();
        DataSet ds = new DataSet();
        ds = objProposal.GetCityAccessBasis(Convert.ToInt32(Session["UserID"]));
        if (ds.Tables[0].Rows.Count > 0)
        {
            ddlCityName.DataSource = ds.Tables[0];
            ddlCityName.DataTextField = "CityName";
            ddlCityName.DataValueField = "CityID";
            ddlCityName.DataBind();
            ddlCityName.Items.Insert(0, new ListItem("-Select-", "0"));
            ddlCityName.Items.Insert(1, new ListItem("Tier 1", "900"));
            ddlCityName.Items.Insert(2, new ListItem("Tier 2", "901"));
            ddlCityName.Items.Insert(3, new ListItem("Tier 3", "902"));
            ddlCityName.Items.Insert(4, new ListItem("City A", "903"));
            ddlCityName.Items.Insert(5, new ListItem("City B", "904"));
        }
        else
        {
            ddlCityName.DataSource = null;
            ddlCityName.DataBind();
        }
    }

    private void BindExtraHours()
    {
        Dictionary<int, int> ddlSource = new Dictionary<int, int>();
        for (int i = 0; i <= 5; i++)
        {
            ddlSource.Add(i, i);
        }
        ddlThreshouldExHr.Items.Clear();
        ddlThreshouldExHr.DataSource = ddlSource;
        ddlThreshouldExHr.DataTextField = "key";
        ddlThreshouldExHr.DataValueField = "value";
        ddlThreshouldExHr.DataBind();
    }
    private void BindExtraKM()
    {
        Dictionary<int, int> ddlSource = new Dictionary<int, int>();
        for (int i = 0; i <= 10; i++)
        {
            ddlSource.Add(i, i);
        }
        ddlThreshouldExKM.Items.Clear();
        ddlThreshouldExKM.DataSource = ddlSource;
        ddlThreshouldExKM.DataTextField = "key";
        ddlThreshouldExKM.DataValueField = "value";
        ddlThreshouldExKM.DataBind();
    }
    private void BindCarCategory()
    {
        objProposal = new sqlProposal();
        DataSet ds = new DataSet();
        ds = objProposal.GetCategory();
        if (ds.Tables[0].Rows.Count > 0)
        {
            ddlCarCat.DataSource = ds.Tables[0];
            ddlCarCat.DataTextField = "CarCatName";
            ddlCarCat.DataValueField = "carcatid";
            ddlCarCat.DataBind();
            ddlCarCat.Items.Insert(0, new ListItem("-Select-", "0"));

        }
        else
        {
            ddlCarCat.DataSource = null;
            ddlCarCat.DataBind();
        }
    }

    private void BindClientCommitment()
    {
        objProposal = new sqlProposal();
        DataSet ds = new DataSet();
        ds = objProposal.GetClientCommitment();
        if (ds.Tables[0].Rows.Count > 0)
        {
            ddlcommitment.DataSource = ds.Tables[0];
            ddlcommitment.DataTextField = "DisplayName";
            ddlcommitment.DataValueField = "id";
            ddlcommitment.DataBind();
            ddlcommitment.Items.Insert(0, new ListItem("Any", "0"));

        }
        else
        {
            ddlcommitment.DataSource = null;
            ddlcommitment.DataBind();
        }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        objPkg = new OL_PKG();
        objProposal = new sqlProposal();
        lblMessage.Text = "";
        int status = 0;
        try
        {

            if (ddlCityName.SelectedValue == "0")
            {
                lblMessage.Text = "Please select City.";
                return;
            }
            else if (ddlProspectCategories.SelectedValue == "0")
            {
                lblMessage.Text = "Please select Prospect Category.";
                return;
            }
            else if (ddlCarCat.SelectedValue == "0")
            {
                lblMessage.Text = "Please select Category.";
                return;
            }
            else if (ddlProposalFor.SelectedValue == "0")
            {
                lblMessage.Text = "Please select Proposal.";
                return;
            }
            else if (string.IsNullOrEmpty(txtBaseRate.Text))
            {
                lblMessage.Text = "Base (Sales) Rate should not be blank";
                return;
            }
            else if (Convert.ToDouble(txtBaseRate.Text) <= 0)
            {
                lblMessage.Text = "Base (Sales) Rate should be greater than zero.";
                return;
            }
            //else if (ddlcommitment.SelectedIndex == 0)
            //{
            //    lblMessage.Text = "Please Select Client Commitment.";
            //    return;
            //}
            else
            {
                // btnSubmit.Attributes.Add("onclick", "ShowProgress();");
                // btnSubmit.Attributes.Add("onclick", "return ShowProgress()");
                objPkg.CityID = Convert.ToInt32(ddlCityName.SelectedValue);

                objPkg.CarCatID = Convert.ToInt32(ddlCarCat.SelectedValue);
                objPkg.ProposalFor = Convert.ToInt32(ddlProposalFor.SelectedValue);
                objPkg.VendorPKGRate = Convert.ToDouble(txtPurchaseRate.Text);
                objPkg.BaseRate = Convert.ToDouble(txtBaseRate.Text);

                objPkg.PkgHrs = Convert.ToInt32(ddlPkgHours.SelectedValue);
                objPkg.PkgKMs = Convert.ToInt32(ddlPkgKM.SelectedValue);
                objPkg.ExtraHrRate = Convert.ToDouble(txtExtraHoursRate.Text);
                objPkg.ExtraKMRate = Convert.ToDouble(txtExtraKMRate.Text);
                objPkg.ThresholdExtraHr = Convert.ToInt32(ddlThreshouldExHr.SelectedValue);
                objPkg.ThresholdExtraKM = Convert.ToInt32(ddlThreshouldExKM.SelectedValue);
                objPkg.OutStationAllowance = Convert.ToDouble(txtOutstationAllowance.Text);
                objPkg.NightStayAllowance = Convert.ToDouble(txtNightStayAllowance.Text);
                objPkg.CreatedBy = Convert.ToInt32(Session["UserID"]);
                objPkg.ProspectCategory = ddlProspectCategories.SelectedValue;
                objPkg.CommitmentId = Convert.ToInt32(ddlcommitment.SelectedValue);
                objPkg.BillingBasis = Convert.ToString(ddlbillingbasis.SelectedValue);

                status = objProposal.InsertStandardPackage(objPkg);

                if (status > 0)
                {
                    lblMessage.Text = "Package create successfully.";
                    lblMessage.ForeColor = System.Drawing.Color.Red;
                    InitializeControls(Form);
                    return;
                }
                else if (status == -2)
                {
                    lblMessage.Text = "Package already exist.";
                    lblMessage.ForeColor = System.Drawing.Color.Red;
                    return;
                }
                else
                {
                    lblMessage.Text = "Getting error while create package.";
                    lblMessage.ForeColor = System.Drawing.Color.Red;
                    return;
                }

            }
        }
        catch (Exception Ex)
        {
            lblMessage.Text = Ex.Message;
            lblMessage.ForeColor = System.Drawing.Color.Red;
        }
    }

    private void InitializeControls(Control objcontrol)
    {
        foreach (Control control in objcontrol.Controls)
        {
            if ((control.GetType() == typeof(TextBox)))
            {
                ((TextBox)(control)).Text = "";
            }

            if ((control.GetType() == typeof(DropDownList)))
            {
                if (((DropDownList)(control)).Items.Count > 0)
                {
                    ((DropDownList)(control)).ClearSelection();
                    ((DropDownList)(control)).SelectedIndex = 0;
                }
            }
            if ((control.GetType() == typeof(ListBox)))
            {
                if (((ListBox)(control)).Items.Count > 0)
                {
                    ((ListBox)(control)).ClearSelection();
                    ((ListBox)(control)).SelectedIndex = 0;
                }
            }

            if ((control.GetType() == typeof(CheckBox)))
            {
                ((CheckBox)(control)).Checked = false;
            }

            if (control.HasControls() && control.GetType() != typeof(MultiView))
            {
                InitializeControls(control);
            }

            if ((control.GetType() == typeof(GridView)))
            {
                ((GridView)(control)).DataSource = null;
                ((GridView)(control)).DataBind();
            }
        }
    }
    protected void txtPurchaseRate_TextChanged(object sender, EventArgs e)
    {
        if (txtPurchaseRate.Text != "")
        {
            txtBaseRate.Text = (Convert.ToDouble(txtPurchaseRate.Text) + ((Convert.ToDouble(txtPurchaseRate.Text) *
                Convert.ToDouble(ConfigurationManager.AppSettings["PercentIncreaseInPurchase"])) / 100)).ToString();
        }
    }
    protected void ddlCityName_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlProposalFor.Items.Clear();
        ListItem item;

        if (ddlCityName.SelectedItem.Text == "City A" || ddlCityName.SelectedItem.Text == "City B")
        {
            item = new ListItem("Fixed Airport Transfer", "3");
            ddlProposalFor.Items.Add(item);
        }
        else if (ddlCityName.SelectedItem.Text == "Tier 1" || ddlCityName.SelectedItem.Text == "Tier 2" || ddlCityName.SelectedItem.Text == "Tier 3")
        {
            item = new ListItem("-Select-", "0");
            ddlProposalFor.Items.Add(item);
            item = new ListItem("Local", "1");
            ddlProposalFor.Items.Add(item);
            item = new ListItem("Outstation", "2");
            ddlProposalFor.Items.Add(item);
            ddlProposalFor.SelectedValue = "0";
        }
        else
        {
            item = new ListItem("-Select-", "0");
            ddlProposalFor.Items.Add(item);
            item = new ListItem("Local", "1");
            ddlProposalFor.Items.Add(item);
            item = new ListItem("Outstation", "2");
            ddlProposalFor.Items.Add(item);
            item = new ListItem("Fixed Airport Transfer", "3");
            ddlProposalFor.Items.Add(item);
            ddlProposalFor.SelectedValue = "0";
        }

        ddlProposalFor_SelectedIndexChanged(sender, e);
    }

    protected void ddlProposalFor_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlProposalFor.SelectedValue == "2")
        {
            ddlbillingbasis.SelectedValue = "gg";
            ddlbillingbasis.Enabled = false;
        }
        else if (ddlProposalFor.SelectedValue == "3")
        {
            ddlbillingbasis.SelectedValue = "pp";
            ddlbillingbasis.Enabled = false;
        }
        else
        {
            ddlbillingbasis.Enabled = true;
        }
    }
}