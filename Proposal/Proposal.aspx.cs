﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Drawing;
using Insta;
using System.IO;
using System.Text;
using System.Net.Mail;
using System.Net;
using System.Configuration;



public partial class ProposalCreate : System.Web.UI.Page
{
    private sqlProposal objProposal = null;
    private CorDrive objCordrive = null;
    private OL_Proposal objProposalOL = null;
    protected void Page_Load(object sender, EventArgs e)
    {
        lblMessage.Visible = false;

        if (!Page.IsPostBack)
        {
            BindProspectMaster();
            BindCity();
            BindServiceType();
            ReplicateCityName();
            BindActionManager();
            BindPackageCategory();
            BindClientCommitment();
            BindCreditPriod();
        }
    }

    private void BindClientCommitment()
    {
        objProposal = new sqlProposal();
        DataSet ds = new DataSet();
        ds = objProposal.GetClientCommitment();
        if (ds.Tables[0].Rows.Count > 0)
        {
            ddlcommitment.DataSource = ds.Tables[0];
            ddlcommitment.DataTextField = "DisplayName";
            ddlcommitment.DataValueField = "id";
            ddlcommitment.DataBind();
            ddlcommitment.Items.Insert(0, new ListItem("Any", "0"));

        }
        else
        {
            ddlcommitment.DataSource = null;
            ddlcommitment.DataBind();
        }
    }

    private void BindCreditPriod()
    {
        objProposal = new sqlProposal();
        DataSet ds = new DataSet();
        ds = objProposal.GetCreditPeriod();
        if (ds.Tables[0].Rows.Count > 0)
        {
            ddlCreditPeriod.DataSource = ds.Tables[0];
            ddlCreditPeriod.DataTextField = "Name";
            ddlCreditPeriod.DataValueField = "ID";
            ddlCreditPeriod.DataBind();
            ddlCreditPeriod.Items.Insert(0, new ListItem("Any", "0"));
        }
        else
        {
            ddlCreditPeriod.DataSource = null;
            ddlCreditPeriod.DataBind();
        }
    }

    private void BindPackageCategory()
    {
        objProposal = new sqlProposal();
        DataSet ds = new DataSet();
        try
        {

            ds = objProposal.GetCarCategory();
            if (ds.Tables[0].Rows.Count > 0)
            {
                ddlPackageCategory.DataSource = ds.Tables[0];
                ddlPackageCategory.DataTextField = "CarCatName";
                ddlPackageCategory.DataValueField = "CarCatID";
                ddlPackageCategory.DataBind();
                ddlPackageCategory.Items.Insert(0, new ListItem("-Select-", "0"));
                ddlPackageCategory.SelectedIndex = 0;
            }
            else
            {
                ddlPackageCategory.DataSource = null;
                ddlPackageCategory.DataBind();

            }
        }
        catch (Exception Ex)
        {

            lblMessage.Visible = true;
            lblMessage.ForeColor = Color.Red;
            lblMessage.Text = Ex.Message;
        }


    }
    protected void Page_Init(object sender, EventArgs e)
    {
        // btnSubmit.Attributes.Add("onclick", "ShowProgress();");

    }
    private void BindGridWithInitialValue(OL_Proposal objProposalOL)
    {
        objProposal = new sqlProposal();
        DataSet ds = new DataSet();
        try
        {

            ds = objProposal.GetProposalDetails(objProposalOL);
            if (ds.Tables[0].Rows.Count > 0)
            {
                grvProposal.DataSource = ds.Tables[0];
                grvProposal.DataBind();
            }
            else
            {
                ds = objProposal.GetProposalDetailsStandPackage(objProposalOL);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    grvProposal.DataSource = ds.Tables[0];
                    grvProposal.DataBind();
                }

                else
                {
                    grvProposal.DataSource = Get_EmptyDataTable();
                    grvProposal.DataBind();
                }
            }

            // divProposalDetails.Style.Add("display", "block");
            //divProposalDetails.Attributes.Add("style", "display:block");
            // divProposalDetails.Style["display"] = "block";
            // divProposalDetails.Style.Remove("display");


        }
        catch (Exception Ex)
        {
            lblMessage.Visible = true;
            lblMessage.Text = Ex.Message;
            lblMessage.ForeColor = Color.Red;
            Helperfunction.LogErrorDetails(Ex.ToString());
        }
        finally
        {
            //pnProposalDetails.Visible = true;
        }

    }
    private DataTable Get_EmptyDataTable()
    {
        DataTable dtEmpty = new DataTable();
        //Here ensure that you have added all the column available in your gridview
        dtEmpty.Columns.Add("ProposalDetailId", typeof(string));
        dtEmpty.Columns.Add("RowNumber", typeof(string));
        dtEmpty.Columns.Add("PackageType", typeof(string));
        dtEmpty.Columns.Add("BaseRate", typeof(string));
        dtEmpty.Columns.Add("BaseKM", typeof(string));
        dtEmpty.Columns.Add("ProspectCategory", typeof(string));
        dtEmpty.Columns.Add("PkgTypeID", typeof(string));
        dtEmpty.Columns.Add("CityID", typeof(string));
        dtEmpty.Columns.Add("CarCatID", typeof(string));
        dtEmpty.Columns.Add("SDPRateID", typeof(string));
        
        dtEmpty.Columns.Add("BaseHr", typeof(string));
        dtEmpty.Columns.Add("cityName", typeof(string));
        dtEmpty.Columns.Add("CarCatName", typeof(string));
        dtEmpty.Columns.Add("BillingBasis", typeof(string));
        dtEmpty.Columns.Add("CarModelName", typeof(string));
        dtEmpty.Columns.Add("FGR", typeof(string));
        dtEmpty.Columns.Add("PKGRate", typeof(string));
        dtEmpty.Columns.Add("PkgKMs", typeof(string));
        dtEmpty.Columns.Add("PkgHrs", typeof(string));
        dtEmpty.Columns.Add("ExtraKMRate", typeof(string));
        dtEmpty.Columns.Add("ExtraHrRate", typeof(string));
        dtEmpty.Columns.Add("ThresholdExtraHr", typeof(string));
        dtEmpty.Columns.Add("ThresholdExtraKM", typeof(string));
        dtEmpty.Columns.Add("NightStayAllowance", typeof(string));
        dtEmpty.Columns.Add("OutStationAllowance", typeof(string));
        dtEmpty.Columns.Add("TempID", typeof(string));
        DataRow datatRow = dtEmpty.NewRow();
        //Inserting a new row,datatable .newrow creates a blank row
        dtEmpty.Rows.Add(datatRow);//adding row to the datatable
        return dtEmpty;
    }
    private void BindServiceType()
    {
        objProposal = new sqlProposal();
        DataSet dsService = new DataSet();
        try
        {
            dsService = objProposal.GetTypeofProposal();
            if (dsService.Tables[0].Rows.Count > 0)
            {
                ddlTypeProposal.DataSource = dsService.Tables[0];
                ddlTypeProposal.DataTextField = "ServiceName";
                ddlTypeProposal.DataValueField = "serviceTypeId";
                ddlTypeProposal.DataBind();
                ddlTypeProposal.Items.Insert(0, new ListItem("--Select--", "0"));
            }
            else
            {
                ddlTypeProposal.DataSource = null;
                ddlTypeProposal.DataBind();
                lblMessage.Visible = true;
                lblMessage.Text = "Record not available";
                lblMessage.ForeColor = Color.Red;
            }

        }
        catch (Exception ex)
        {

            throw new Exception(ex.Message);
        }
    }
    private void BindProspectMaster()
    {
        objProposal = new sqlProposal();
        DataSet dsProspect = new DataSet();
        try
        {
            dsProspect = objProposal.GetProspectMaster();
            if (dsProspect.Tables[0].Rows.Count > 0)
            {
                ddlClient.DataSource = dsProspect.Tables[0];
                ddlClient.DataTextField = "ClientCoName";
                ddlClient.DataValueField = "ID";
                ddlClient.DataBind();
                ddlClient.Items.Insert(0, new ListItem("--Select--", "0"));
            }
            else
            {
                ddlClient.DataSource = null;
                ddlClient.DataBind();
                lblMessage.Visible = true;
                lblMessage.Text = "Record not available";
                lblMessage.ForeColor = Color.Red;
            }

        }
        catch (Exception ex)
        {

            throw new Exception(ex.Message);
        }
    }
    private void BindCity()
    {
        try
        {
            objCordrive = new CorDrive();
            DataSet dsCustName = new DataSet();
            dsCustName = objCordrive.GetCityName();
            if (dsCustName.Tables[0].Rows.Count > 0)
            {
                ddlCityName.DataTextField = "cityname";
                ddlCityName.DataValueField = "cityid";
                ddlCityName.DataSource = dsCustName;
                ddlCityName.DataBind();
                ddlCityName.Items.Insert(0, new ListItem("--Select--", "0"));
            }
        }
        catch (Exception Ex)
        {
            lblMessage.Visible = true;
            lblMessage.ForeColor = Color.Red;
            lblMessage.Text = Ex.Message;
        }
    }
    private void BindActionManager()
    {
        try
        {
            objCordrive = new CorDrive();
            DataSet dsActionManager = new DataSet();
            dsActionManager = objCordrive.GetActionManagerName();
            if (dsActionManager.Tables[0].Rows.Count > 0)
            {
                ddlActionMgrName.DataTextField = "UserName";
                ddlActionMgrName.DataValueField = "SysUserID";
                ddlActionMgrName.DataSource = dsActionManager;
                ddlActionMgrName.DataBind();
                ddlActionMgrName.Items.Insert(0, new ListItem("--Select--", "0"));
            }


        }
        catch (Exception Ex)
        {
            lblMessage.Visible = true;
            lblMessage.ForeColor = System.Drawing.Color.Red;
            lblMessage.Text = Ex.Message;
        }
    }
    private void ReplicateCityName()
    {
        try
        {
            objCordrive = new CorDrive();
            DataSet dsCustName = new DataSet();
            dsCustName = objCordrive.GetCityName();
            if (dsCustName.Tables[0].Rows.Count > 0)
            {
                ddlRplicateCity.DataTextField = "cityname";
                ddlRplicateCity.DataValueField = "cityid";
                ddlRplicateCity.DataSource = dsCustName;
                ddlRplicateCity.DataBind();
            }
        }
        catch (Exception Ex)
        {
            lblMessage.Visible = true;
            lblMessage.ForeColor = Color.Red;
            lblMessage.Text = Ex.Message;
        }
    }
    protected void grvProposal_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        objProposal = new sqlProposal();
        objCordrive = new CorDrive();
        DataSet ds = new DataSet();
        try
        {

            if (e.Row.RowType == DataControlRowType.Footer)
            {

                DropDownList ddlPackageType = (DropDownList)e.Row.FindControl("ddlPackageType");
                ds = objProposal.GetPackageType();
                BindDropDownList(ddlPackageType, ds.Tables[0], "PackageType", "PKGTypeID", "0");

                DropDownList ddlPackageCityName = (DropDownList)e.Row.FindControl("ddlPackageCityName");
                ds = objCordrive.GetCityName();
                BindDropDownList(ddlPackageCityName, ds.Tables[0], "CityName", "CityID", "0");

                DropDownList ddlVehicleCategory = (DropDownList)e.Row.FindControl("ddlVehicleCategory");
                ds = objProposal.GetCarCategory();
                BindDropDownList(ddlVehicleCategory, ds.Tables[0], "CarCatName", "CarCatID", "0");

                //DropDownList ddlVehicleModel = (DropDownList)e.Row.FindControl("ddlVehicleModel");
                //ds = objProposal.GetCarModel(5);
                //BindDropDownList(ddlVehicleModel, ds.Tables[0], "CarModelName", "CarModelID", "0");

                TextBox txtFGR = (TextBox)e.Row.FindControl("txtFGR");
                txtFGR.Text = "0.00";

                //DropDownList ddlFGR = (DropDownList)e.Row.FindControl("ddlFGR");
                //BindDropDownList(ddlFGR, 0, 500, 1, "0");

                TextBox ddlPackageRate = (TextBox)e.Row.FindControl("ddlPackageRate");
                //BindDropDownList(ddlPackageRate, 10, 10000, 1, "0");
                ddlPackageRate.Text = "0.00";

                DropDownList ddlPackageKM = (DropDownList)e.Row.FindControl("ddlPackageKM");
                //BindDropDownList(ddlPackageKM, 1, 250, 1, "0");
                ddlPackageKM.SelectedValue = "0";

                DropDownList ddlPackageHour = (DropDownList)e.Row.FindControl("ddlPackageHour");
                //BindDropDownList(ddlPackageHour, 0, 24, 1, "0");
                ddlPackageHour.SelectedValue = "0";

                //Extra KM Rate
                //DropDownList ddlPackageExtraKM = (DropDownList)e.Row.FindControl("ddlPackageExtraKM");
                TextBox ddlPackageExtraKM = (TextBox)e.Row.FindControl("ddlPackageExtraKM");
                //BindDropDownList(ddlPackageExtraKM, 0, 50, 1, "0");
                ddlPackageExtraKM.Text = "0.00";
                //End of Extra Package KM Rate

                //Extra HR Rate
                //DropDownList ddlPackageExtraHr = (DropDownList)e.Row.FindControl("ddlPackageExtraHr");
                TextBox ddlPackageExtraHr = (TextBox)e.Row.FindControl("ddlPackageExtraHr");

                //BindDropDownList(ddlPackageExtraHr, 0, 200, 1, "0");
                ddlPackageExtraHr.Text = "0.00";
                //End Extra HR

                //DropDownList ddlthresholdExtraHr = (DropDownList)e.Row.FindControl("ddlthresholdExtraHr");
                //BindDropDownList(ddlthresholdExtraHr, 0, 5, 1, "0");

                //DropDownList ddlthresholdExtraKM = (DropDownList)e.Row.FindControl("ddlthresholdExtraKM");
                //BindDropDownList(ddlthresholdExtraKM, 0, 10, 1, "0");
            }
            //else if (grvProposal.EditIndex == e.Row.RowIndex && e.Row.RowType == DataControlRowType.DataRow)
            //{
            else if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label ddlEditPackageType = (Label)e.Row.FindControl("ddlEditPackageType");
                //ds = objProposal.GetPackageType();

                Label lblPackageType = (Label)e.Row.FindControl("lblPackagetype");
                HiddenField hdnPacakgetype = (HiddenField)e.Row.FindControl("hdnPacakgetype");

                //BindDropDownList(ddlEditPackageType, ds.Tables[0], "PackageType", "PKGTypeID", lblPackageType.Text.ToString());

                Label ddlEditPackageCityName = (Label)e.Row.FindControl("ddlEditPackageCityName");
                //ds = objCordrive.GetCityName();
                
                Label lblPackageCityName = (Label)e.Row.FindControl("lblPckageCityName");
                Label lblPckageCityID = (Label)e.Row.FindControl("lblPckageCityID");
                //BindDropDownList(ddlEditPackageCityName, ds.Tables[0], "CityName", "CityID", lblPckageCityID.Text.ToString());

                Label ddlEditVehicleCategory = (Label)e.Row.FindControl("ddlEditVehicleCategory");
                ds = objProposal.GetCarCategory();
                //HiddenField hdnVehicleCategoryID = (HiddenField)e.Row.FindControl("hdnVehicleCategoryID");
                //BindDropDownList(ddlEditVehicleCategory, ds.Tables[0], "CarCatName", "CarCatID", hdnVehicleCategoryID.Value.ToString());

                //DropDownList ddlEditVehicleModel = (DropDownList)e.Row.FindControl("ddlEditVehicleModel");
                //ds = objProposal.GetCarModel(5);
                //Label lblVehicleModel = (Label)e.Row.FindControl("lblVehicleModel");
                //BindDropDownList(ddlEditVehicleModel, ds.Tables[0], "CarModelName", "CarModelID", lblVehicleModel.Text.ToString());

                DropDownList ddlEditBillingBasis = (DropDownList)e.Row.FindControl("ddlEditBillingBasis");
                Label lblBillingBasis = (Label)e.Row.FindControl("lblBillingBasis");
                ddlEditBillingBasis.SelectedValue = Convert.ToString(lblBillingBasis.Text);

                //DropDownList ddlEditFGR = (DropDownList)e.Row.FindControl("ddlEditFGR");
                TextBox txtEditFGR = (TextBox)e.Row.FindControl("txtEditFGR");
                Label lblFGR = (Label)e.Row.FindControl("lblFGR");
                txtEditFGR.Text = lblFGR.Text;

                //BindDropDownList(ddlEditFGR, 0, 500, 1, lblFGR.Text.ToString());

                TextBox ddlEditPackageRate = (TextBox)e.Row.FindControl("ddlEditPackageRate");
                Label lblPackageRate = (Label)e.Row.FindControl("lblPackageRate");

                ddlEditPackageRate.Text = lblPackageRate.Text.ToString();

                //BindDropDownList(ddlEditPackageRate, 10, 10000, 1, lblPackageRate.Text.ToString());

                DropDownList ddlEditPackageKM = (DropDownList)e.Row.FindControl("ddlEditPackageKM");
                Label lblPackageKM = (Label)e.Row.FindControl("lblPackageKM");
                //BindDropDownList(ddlEditPackageKM, 1, 250, 1, lblPackageKM.Text.ToString());
                ddlEditPackageKM.SelectedValue = lblPackageKM.Text;

                DropDownList ddlEditPackageHour = (DropDownList)e.Row.FindControl("ddlEditPackageHour");
                Label lblPackageHour = (Label)e.Row.FindControl("lblPackageHour");
                //BindDropDownList(ddlEditPackageHour, 0, 24, 1, lblPackageHour.Text.ToString());
                ddlEditPackageHour.SelectedValue = lblPackageHour.Text;

                //Extra KM Rate
                //DropDownList ddlEditPackageExtraKM = (DropDownList)e.Row.FindControl("ddlEditPackageExtraKM");
                TextBox ddlEditPackageExtraKM = (TextBox)e.Row.FindControl("ddlEditPackageExtraKM");

                Label lblPackageExtraKM = (Label)e.Row.FindControl("lblPackageExtraKM");
                //BindDropDownList(ddlEditPackageExtraKM, 0, 50, 1, lblPackageExtraKM.Text.ToString());
                ddlEditPackageExtraKM.Text = lblPackageExtraKM.Text.ToString();
                //End Exra KM Rate

                //Extra Hr Rate
                //DropDownList ddlEditPackageExtraHr = (DropDownList)e.Row.FindControl("ddlEditPackageExtraHr");
                TextBox ddlEditPackageExtraHr = (TextBox)e.Row.FindControl("ddlEditPackageExtraHr");

                Label lblPackageExtraHr = (Label)e.Row.FindControl("lblPackageExtraHr");
                //BindDropDownList(ddlEditPackageExtraHr, 0, 200, 1, lblPackageExtraHr.Text.ToString());
                ddlEditPackageExtraHr.Text = lblPackageExtraHr.Text.ToString();
                //End Extra HR Rate

                //DropDownList ddlEditthresholdExtraHr = (DropDownList)e.Row.FindControl("ddlEditthresholdExtraHr");
                //Label lblthresholdExtraHr = (Label)e.Row.FindControl("lblthresholdExtraHr");
                //BindDropDownList(ddlEditthresholdExtraHr, 0, 5, 1, lblthresholdExtraHr.Text.ToString());

                //DropDownList ddlEditthresholdExtraKM = (DropDownList)e.Row.FindControl("ddlEditthresholdExtraKM");
                //Label lblthresholdExtraKM = (Label)e.Row.FindControl("lblthresholdExtraKM");
                //BindDropDownList(ddlEditthresholdExtraKM, 0, 10, 1, lblthresholdExtraKM.Text.ToString());
            }
            //}
        }
        catch (Exception Ex)
        {
            lblMessage.Visible = true;
            lblMessage.ForeColor = Color.Red;
            lblMessage.Text = Ex.ToString();
            Helperfunction.LogErrorDetails(Ex.ToString());
        }

    }
    private void BindDropDownList(DropDownList ddlName, DataTable dtData, string dataTextField, string dataValuefield, string selectedValue)
    {
        double number;
        if (dtData != null && dtData.Rows.Count > 0)
        {
            ddlName.Items.Clear();
            ddlName.DataSource = dtData;
            ddlName.DataTextField = dataTextField;
            ddlName.DataValueField = dataValuefield;
            ddlName.DataBind();
            ddlName.Items.Insert(0, new ListItem("-Select-", "0"));
            //if (ddlName.ClientID.Contains("CityName"))
            //{
            //    ddlName.Items.Insert(1, new ListItem("Pan India", "-1"));
            //}

            if (dataTextField == "CityName")
            {
                ddlName.Items.Insert(1, new ListItem("Tier 1", "900"));
                ddlName.Items.Insert(2, new ListItem("Tier 2", "901"));
                ddlName.Items.Insert(3, new ListItem("Tier 3", "902"));
                ddlName.Items.Insert(4, new ListItem("City A", "903"));
                ddlName.Items.Insert(5, new ListItem("City B", "904"));
            }
            if (selectedValue != "0")
            {
                if (double.TryParse(selectedValue, out number))
                {
                    try
                    {
                        ddlName.Items.FindByValue(Convert.ToInt32(number).ToString()).Selected = true;
                    }
                    catch (Exception)
                    {

                        ddlName.Items.FindByValue(Convert.ToInt32(number).ToString()).Selected = true;
                    }

                }
                else
                {
                    try
                    {
                        ddlName.Items.FindByValue(selectedValue).Selected = true;
                    }
                    catch (Exception)
                    {

                        ddlName.Items.FindByValue("0").Selected = true;
                    }

                }
            }
        }
        else
        {
            ddlName.Items.Clear();
            ddlName.Items.Insert(0, new ListItem("--Select--", "0"));
        }

    }
    private void BindDropDownList(DropDownList ddlName, int startValue, int endValue, int incrementvalue, string selectedValue)
    {
        Dictionary<int, int> ddlSource = new Dictionary<int, int>();
        double number;
        for (int i = startValue; i <= endValue; i = i + incrementvalue)
        {
            ddlSource.Add(i, i);
        }
        ddlName.Items.Clear();
        ddlName.DataSource = ddlSource;
        ddlName.DataTextField = "key"; //ddlSource.Keys.ToString();
        ddlName.DataValueField = "value"; //ddlSource.Values.ToString();
        ddlName.DataBind();
        if (startValue == 0)
        {
            ddlName.Items.Insert(0, new ListItem("-Select-", "-1"));
        }
        else
        {
            ddlName.Items.Insert(0, new ListItem("-Select-", "0"));
        }

        //if (ddlName.ClientID.Contains("thresholdExtraKM"))
        //{
        //    ddlName.Items.RemoveAt(0);
        //    ddlName.Items.Insert(0, new ListItem("0", "0"));
        //}

        if (selectedValue != "0")
        {
            if (double.TryParse(selectedValue, out number))
            {
                // ddlName.SelectedItem.Text = Convert.ToInt32(number).ToString();

                try
                {
                    ddlName.Items.FindByText(Convert.ToInt32(number).ToString()).Selected = true;
                }
                catch (Exception)
                {
                    ddlName.Items.FindByValue(Convert.ToInt32(number).ToString()).Selected = true;
                }

            }
            else
            {
                try
                {
                    ddlName.Items.FindByText(selectedValue).Selected = true;
                }
                catch (Exception)
                {

                    ddlName.Items.FindByValue("0").Selected = true;
                }

                //ddlName.SelectedItem.Text = selectedValue;
            }
        }

    }
    protected void grvProposal_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        objProposalOL = new OL_Proposal();
        objProposal = new sqlProposal();
        DataSet ds = new DataSet();
        if (e.CommandName.Equals("Insert"))
        {

            DropDownList ddlPackageType = (DropDownList)grvProposal.FooterRow.FindControl("ddlPackageType");
            DropDownList ddlPackageCityName = (DropDownList)grvProposal.FooterRow.FindControl("ddlPackageCityName");
            DropDownList ddlVehicleCategory = (DropDownList)grvProposal.FooterRow.FindControl("ddlVehicleCategory");
            // DropDownList ddlVehicleModel = (DropDownList)grvProposal.FooterRow.FindControl("ddlVehicleModel");
            DropDownList ddlBillingBasis = (DropDownList)grvProposal.FooterRow.FindControl("ddlBillingBasis");
            DropDownList ddlFGR = (DropDownList)grvProposal.FooterRow.FindControl("ddlFGR");
            TextBox ddlPackageRate = (TextBox)grvProposal.FooterRow.FindControl("ddlPackageRate");
            DropDownList ddlPackageKM = (DropDownList)grvProposal.FooterRow.FindControl("ddlPackageKM");
            DropDownList ddlPackageHour = (DropDownList)grvProposal.FooterRow.FindControl("ddlPackageHour");
            //DropDownList ddlPackageExtraKM = (DropDownList)grvProposal.FooterRow.FindControl("ddlPackageExtraKM");
            TextBox ddlPackageExtraKM = (TextBox)grvProposal.FooterRow.FindControl("ddlPackageExtraKM");
            //DropDownList ddlPackageExtraHr = (DropDownList)grvProposal.FooterRow.FindControl("ddlPackageExtraHr");
            TextBox ddlPackageExtraHr = (TextBox)grvProposal.FooterRow.FindControl("ddlPackageExtraHr");

            //DropDownList ddlthresholdExtraHr = (DropDownList)grvProposal.FooterRow.FindControl("ddlthresholdExtraHr");
            //DropDownList ddlthresholdExtraKM = (DropDownList)grvProposal.FooterRow.FindControl("ddlthresholdExtraKM");
            TextBox txtNightCharges = (TextBox)grvProposal.FooterRow.FindControl("txtNightCharges");
            TextBox txtChauffeurCharges = (TextBox)grvProposal.FooterRow.FindControl("txtChauffeurCharges");
            HiddenField hdnSDPRateID = (HiddenField)grvProposal.FooterRow.FindControl("hdnSDPRateID");

            objProposalOL.PkgTypeId = Convert.ToInt32(ddlPackageType.SelectedValue.ToString());
            objProposalOL.CityID = Convert.ToInt32(ddlPackageCityName.SelectedValue.ToString());
            objProposalOL.CarCatID = Convert.ToInt32(ddlVehicleCategory.SelectedValue.ToString());
            //objProposalOL.CarModelID = Convert.ToInt32(ddlVehicleModel.SelectedValue.ToString());
            objProposalOL.BillingBasis = Convert.ToString(ddlBillingBasis.SelectedValue);
            //if (objProposalOL.BillingBasis == "PP")
            //{
                objProposalOL.Fgr = Convert.ToDouble(ddlFGR.SelectedValue);
            //}
            //else
            //{
            //    objProposalOL.Fgr = null;
            //}

            objProposalOL.PKGRate = Convert.ToDouble(ddlPackageRate.Text); //Convert.ToDouble(ddlPackageRate.SelectedValue.ToString());
            objProposalOL.OriginalBaseRate = Convert.ToDouble(ddlPackageRate.Text);
            objProposalOL.PkgKMs = Convert.ToDouble(ddlPackageKM.SelectedValue.ToString());
            objProposalOL.PkgHrs = Convert.ToDouble(ddlPackageHour.SelectedValue.ToString());
            //objProposalOL.ExtraKMRate = Convert.ToInt32(ddlPackageExtraKM.SelectedValue.ToString());
            objProposalOL.ExtraKMRate = Convert.ToDouble(ddlPackageExtraKM.Text);
            //objProposalOL.ExtraHrRate = Convert.ToInt32(ddlPackageExtraHr.SelectedValue.ToString());
            objProposalOL.ExtraHrRate = Convert.ToDouble(ddlPackageExtraHr.Text);

            //objProposalOL.ThresholdExtraHr = Convert.ToInt32(ddlthresholdExtraHr.SelectedValue.ToString());
            //objProposalOL.ThresholdExtraKM = Convert.ToInt32(ddlthresholdExtraKM.SelectedValue.ToString());
            objProposalOL.SysUserId = Convert.ToInt32(Session["UserID"]);
            if (string.IsNullOrEmpty(txtNightCharges.Text))
            {
                objProposalOL.NightStayAllowance = 0;
            }
            else
            {
                objProposalOL.NightStayAllowance = Convert.ToDouble(txtNightCharges.Text.ToString());
            }

            if (string.IsNullOrEmpty(txtChauffeurCharges.Text))
            {
                objProposalOL.OutStationAllowance = 0;
            }
            else
            {
                objProposalOL.OutStationAllowance = Convert.ToDouble(txtChauffeurCharges.Text.ToString());
            }

            objProposalOL.ProspectCategory = "A";
            objProposalOL.ProspectID = Convert.ToInt32(ddlClient.SelectedValue.ToString());
            objProposalOL.ProposalCityId = Convert.ToInt32(ddlCityName.SelectedValue.ToString());
            objProposalOL.DateInitition = Convert.ToDateTime(txtDate.Text.ToString());
            objProposalOL.ServiceType = Convert.ToInt32(ddlTypeProposal.SelectedValue.ToString());
            objProposalOL.ProposalFor = Convert.ToInt32(ddlProposalFor.SelectedValue.ToString());
            //objProposalOL.ManagerName = txtActionManageName.Text.ToString();
            objProposalOL.ManagerName = ddlActionMgrName.SelectedItem.Text.ToString();
            objProposalOL.ActionManagerID = Convert.ToInt32(ddlActionMgrName.SelectedValue.ToString());
            objProposalOL.ClientCoId = Convert.ToInt32(ddlClient.SelectedValue);
            objProposalOL.TempId = 0;
            objProposalOL.SDPRateID = 0; //Convert.ToInt32(hdnSDPRateID.Value);

            //ds = objProposal.GetPackageBaseRate(objProposalOL);
            //if (ds.Tables[0].Rows.Count > 0)
            //{
            //    objProposalOL.BaseRate = Convert.ToDouble(ds.Tables[0].Rows[0]["BaseRate"]);
            //    objProposalOL.BaseHr = Convert.ToDouble(ds.Tables[0].Rows[0]["BaseHr"]);
            //    objProposalOL.BaseKM = Convert.ToDouble(ds.Tables[0].Rows[0]["BaseKM"]);
            //}
            //else
            //{
                objProposalOL.BaseRate = 0;
                objProposalOL.BaseHr = 0;
                objProposalOL.BaseKM = 0;
            //}
            int status = 0;
            objProposalOL.NotApproveYN = true;
            //int status = objProposal.SaveProposalDetails(objProposalOL);
            if (ddlPackageCityName.SelectedItem.Text == "Tier 1" || ddlPackageCityName.SelectedItem.Text == "Tier 2"
                   || ddlPackageCityName.SelectedItem.Text == "Tier 3" || ddlPackageCityName.SelectedItem.Text == "City A"
                   || ddlPackageCityName.SelectedItem.Text == "City B")
            {
                ds = new DataSet();
                ds = objProposal.ProposalCityMapping(ddlPackageCityName.SelectedItem.Text);

                for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
                {
                    objProposalOL.CityID = Convert.ToInt32(ds.Tables[0].Rows[i]["CityID"]);

                    status = objProposal.SaveProposalDetails(objProposalOL);
                }
            }
            else
            {
                status = objProposal.SaveProposalDetails(objProposalOL);
            }

            if (status > 0)
            {
                lblMessage.Visible = true;
                lblMessage.Text = "Record inserted successfully!";
                lblMessage.ForeColor = Color.Red;
            }
            else
            {
                lblMessage.Visible = true;
                lblMessage.Text = "Getting Error!";
                lblMessage.ForeColor = Color.Red;
            }

            grvProposal.EditIndex = -1;
            BasicBinding(objProposalOL);
            BindGridWithInitialValue(objProposalOL);

        }
    }
    protected void grvProposal_RowEditing(object sender, GridViewEditEventArgs e)
    {
        grvProposal.EditIndex = e.NewEditIndex;
        objProposalOL = new OL_Proposal();
        objProposalOL = BasicBinding(objProposalOL);
        BindGridWithInitialValue(objProposalOL);
    }
    protected void grvProposal_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        grvProposal.EditIndex = -1;
        objProposalOL = new OL_Proposal();
        objProposalOL = BasicBinding(objProposalOL);
        BindGridWithInitialValue(objProposalOL);

    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        objProposal = new sqlProposal();
        objProposalOL = new OL_Proposal();
        string filename, fileExtension, fileNamewithoutExtention, newFileName;
        int status = 0;
        string ProspectId = ddlClient.SelectedValue.ToString();
        // string path="D:\Upload\ProposalDOC\";
        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Script", "javascript:ShowProgress();", true);
        try
        {
            if (grvProposal.Rows.Count <= 0)
            {
                lblMessage.Visible = true;
                lblMessage.Text = "Please enter package details";
                lblMessage.ForeColor = Color.Red;
                return;
            }

            if (fudCommercial.HasFile)
            {
                filename = Path.GetFileName(fudCommercial.FileName);
                fileExtension = Path.GetExtension(filename);
                fileNamewithoutExtention = Path.GetFileNameWithoutExtension(fudCommercial.FileName);
                newFileName = ProspectId + fileExtension;
                fudCommercial.SaveAs(Server.MapPath("~/ProposalDOC/") + newFileName);
                //live code
                // fudCommercial.SaveAs(Server.MapPath(+ newFileName));
                // fudCommercial.SaveAs("D:\\Upload\\ProposalDOC\\"+newFileName);
                lblMessage.Visible = true;
                lblMessage.Text = "Upload status: File uploaded!";
            }
            else
            {
                filename = string.Empty;
                fileExtension = string.Empty;
                fileNamewithoutExtention = string.Empty;
                newFileName = string.Empty;
            }

            objProposalOL.ProspectID = Convert.ToInt32(ddlClient.SelectedValue.ToString());
            objProposalOL.ContactPersonEmail = txtConactPersonEmailId.Text.ToString();
            objProposalOL.ContactPersonName = txtContactPersonName.Text.ToString();
            objProposalOL.Remarks = txtOtherDetails.Text.ToString();
            objProposalOL.ProposalCityId = Convert.ToInt32(ddlCityName.SelectedValue.ToString());
            objProposalOL.FileName = newFileName.ToString();
            objProposalOL.SysUserId = Convert.ToInt32(Session["UserID"]);
            objProposalOL.DateInitition = Convert.ToDateTime(txtDate.Text.ToString());
            objProposalOL.ServiceType = Convert.ToInt32(ddlTypeProposal.SelectedValue.ToString());
            objProposalOL.ProposalFor = Convert.ToInt32(ddlProposalFor.SelectedValue.ToString());
            //objProposalOL.ManagerName = txtActionManageName.Text.ToString();
            objProposalOL.ManagerName = ddlActionMgrName.SelectedItem.Text.ToString();
            

            objProposalOL.ActionManagerID = Convert.ToInt32(ddlActionMgrName.SelectedValue.ToString());
            objProposalOL.Address = txtAddress.Text;
            objProposalOL.Designation = txtDesignation.Text;
            objProposalOL.MobileNo = txtMobileNo.Text;
            DataSet ds;

            foreach (GridViewRow row in grvProposal.Rows)
            {
                objProposalOL.ProposalDetailId = (int)grvProposal.DataKeys[row.RowIndex].Values[0];
                objProposalOL.TempId = (int)grvProposal.DataKeys[row.RowIndex].Values[1];
                objProposalOL.BaseRate = Convert.ToDouble(grvProposal.DataKeys[row.RowIndex].Values[2]);

                Label lblPackageRate1 = (Label)grvProposal.Rows[row.RowIndex].FindControl("lblPackageRate");
                TextBox ddlEditPackageRate1 = (TextBox)grvProposal.Rows[row.RowIndex].FindControl("ddlEditPackageRate");
                TextBox txtRemarks1 = (TextBox)grvProposal.Rows[row.RowIndex].FindControl("txtEditRemarks");

                if (Convert.ToDouble(ddlEditPackageRate1.Text) == 0)
                {
                    lblMessage.Visible = true;
                    lblMessage.Text = "Sales rate cannot be 0";
                    lblMessage.ForeColor = Color.Red;
                    ddlEditPackageRate1.Focus();
                    return;
                }

                if (Convert.ToDouble(ddlEditPackageRate1.Text) != Convert.ToDouble(lblPackageRate1.Text) && string.IsNullOrEmpty(txtRemarks1.Text))
                {
                    lblMessage.Visible = true;
                    lblMessage.Text = "Please enter remarks.";
                    lblMessage.ForeColor = Color.Red;
                    txtRemarks1.Focus();
                    return;
                }
            }


            foreach (GridViewRow row in grvProposal.Rows)
            {
                objProposalOL.ProposalDetailId = (int)grvProposal.DataKeys[row.RowIndex].Values[0];
                objProposalOL.TempId = (int)grvProposal.DataKeys[row.RowIndex].Values[1];
                objProposalOL.BaseRate = Convert.ToDouble(grvProposal.DataKeys[row.RowIndex].Values[2]);
                objProposalOL.BaseHr = Convert.ToDouble(grvProposal.DataKeys[row.RowIndex].Values[3]);
                objProposalOL.BaseKM = Convert.ToDouble(grvProposal.DataKeys[row.RowIndex].Values[4]);

                //if (objProposalOL.ProposalDetailId == 0)
                //{
                //Label lblPackageID = (Label)grvProposal.Rows[row.RowIndex].FindControl("lblPackageID");
                Label ddlEditPackageType = (Label)grvProposal.Rows[row.RowIndex].FindControl("ddlEditPackageType");

                //Label lblPckageCityID = (Label)grvProposal.Rows[row.RowIndex].FindControl("lblPckageCityID");
                Label ddlEditPackageCityName = (Label)grvProposal.Rows[row.RowIndex].FindControl("ddlEditPackageCityName");

                //Label lblVehicleCategoryID = (Label)grvProposal.Rows[row.RowIndex].FindControl("lblVehicleCategoryID");
                Label ddlEditVehicleCategory = (Label)grvProposal.Rows[row.RowIndex].FindControl("ddlEditVehicleCategory");

                //Label lblBillingBasis = (Label)grvProposal.Rows[row.RowIndex].FindControl("lblBillingBasis");
                DropDownList ddlEditBillingBasis = (DropDownList)grvProposal.Rows[row.RowIndex].FindControl("ddlEditBillingBasis");

                //Label lblFGR = (Label)grvProposal.Rows[row.RowIndex].FindControl("lblFGR");
                TextBox txtEditFGR = (TextBox)grvProposal.Rows[row.RowIndex].FindControl("txtEditFGR");

                //Label lblPackageHour = (Label)grvProposal.Rows[row.RowIndex].FindControl("lblPackageHour");
                DropDownList ddlEditPackageHour = (DropDownList)grvProposal.Rows[row.RowIndex].FindControl("ddlEditPackageHour");

                //Label lblPackageKM = (Label)grvProposal.Rows[row.RowIndex].FindControl("lblPackageKM");
                DropDownList ddlEditPackageKM = (DropDownList)grvProposal.Rows[row.RowIndex].FindControl("ddlEditPackageKM");

                Label lblPackageRate = (Label)grvProposal.Rows[row.RowIndex].FindControl("lblPackageRate");
                TextBox ddlEditPackageRate = (TextBox)grvProposal.Rows[row.RowIndex].FindControl("ddlEditPackageRate");

                //Label lblPackageExtraHrRate = (Label)grvProposal.Rows[row.RowIndex].FindControl("lblPackageExtraHr");
                TextBox ddlEditPackageExtraHr = (TextBox)grvProposal.Rows[row.RowIndex].FindControl("ddlEditPackageExtraHr");

                //Label lblPackageExtraKMRate = (Label)grvProposal.Rows[row.RowIndex].FindControl("lblPackageExtraKM");
                TextBox ddlEditPackageExtraKM = (TextBox)grvProposal.Rows[row.RowIndex].FindControl("ddlEditPackageExtraKM");

                //Label lblthresholdExtraHrRate = (Label)grvProposal.Rows[row.RowIndex].FindControl("lblthresholdExtraHr");
                DropDownList ddlEditthresholdExtraHr = (DropDownList)grvProposal.Rows[row.RowIndex].FindControl("ddlEditthresholdExtraHr");

                //Label lblthresholdExtraKMRate = (Label)grvProposal.Rows[row.RowIndex].FindControl("lblthresholdExtraKM");
                DropDownList ddlEditthresholdExtraKM = (DropDownList)grvProposal.Rows[row.RowIndex].FindControl("ddlEditthresholdExtraKM");

                //Label lblNightCharges = (Label)grvProposal.Rows[row.RowIndex].FindControl("lblNightCharges");
                TextBox txtEditNightCharges = (TextBox)grvProposal.Rows[row.RowIndex].FindControl("txtEditNightCharges");

                //Label lblChauffeurCharges = (Label)grvProposal.Rows[row.RowIndex].FindControl("lblChauffeurCharges");
                TextBox txtEditChauffeurCharges = (TextBox)grvProposal.Rows[row.RowIndex].FindControl("txtEditChauffeurCharges");

                HiddenField hdnSDPRateID = (HiddenField)grvProposal.Rows[row.RowIndex].FindControl("hdnSDPRateID");
                HiddenField hdnProspectCategory = (HiddenField)grvProposal.Rows[row.RowIndex].FindControl("hdnProspectCategory");
                HiddenField hdnPckageCityID = (HiddenField)grvProposal.Rows[row.RowIndex].FindControl("hdnPckageCityID");
                HiddenField hdnPackageID = (HiddenField)grvProposal.Rows[row.RowIndex].FindControl("hdnPackageID");
                TextBox txtRemarks = (TextBox)grvProposal.Rows[row.RowIndex].FindControl("txtEditRemarks");


                objProposalOL.PkgTypeId = Convert.ToInt32(hdnPackageID.Value);  //Convert.ToInt32(lblPackageID.Text);
                objProposalOL.CityID = Convert.ToInt32(hdnPckageCityID.Value); //Convert.ToInt32(lblPckageCityID.Text);
                objProposalOL.CarCatID = Convert.ToInt32(ddlEditVehicleCategory.Text); //Convert.ToInt32(lblVehicleCategoryID.Text);
                objProposalOL.BillingBasis = Convert.ToString(ddlEditBillingBasis.SelectedValue);  //Convert.ToString(lblBillingBasis.Text);

                objProposalOL.ProspectCategory = hdnProspectCategory.Value;

                //if (objProposalOL.BillingBasis == "PP")
                //{
                    objProposalOL.Fgr = Convert.ToDouble(txtEditFGR.Text); //Convert.ToDouble(lblFGR.Text);
                //}
                //else
                //{
                //    objProposalOL.Fgr = null;
                //}

                objProposalOL.PKGRate = Convert.ToDouble(ddlEditPackageRate.Text); //Convert.ToDouble(lblPackageRate.Text);
                objProposalOL.PkgKMs = Convert.ToDouble(ddlEditPackageKM.SelectedValue); //Convert.ToDouble(lblPackageKM.Text);
                objProposalOL.PkgHrs = Convert.ToDouble(ddlEditPackageHour.SelectedValue); //Convert.ToDouble(lblPackageHour.Text);
                objProposalOL.ExtraKMRate = Convert.ToDouble(ddlEditPackageExtraKM.Text); //Convert.ToDouble(lblPackageExtraKMRate.Text);
                objProposalOL.ExtraHrRate = Convert.ToDouble(ddlEditPackageExtraHr.Text); //Convert.ToDouble(lblPackageExtraHrRate.Text);
                objProposalOL.ThresholdExtraHr = Convert.ToDouble(ddlEditthresholdExtraHr.SelectedValue); //Convert.ToDouble(lblthresholdExtraHrRate.Text);
                objProposalOL.ThresholdExtraKM = Convert.ToDouble(ddlEditthresholdExtraKM.SelectedValue); //Convert.ToDouble(lblthresholdExtraKMRate.Text);
                objProposalOL.SysUserId = Convert.ToInt32(Session["UserID"]);
                objProposalOL.OriginalBaseRate = Convert.ToDouble(lblPackageRate.Text);
                objProposalOL.ClientCoId = Convert.ToInt32(ddlClient.SelectedValue);
                

                //if (string.IsNullOrEmpty(lblNightCharges.Text))
                if (string.IsNullOrEmpty(txtEditNightCharges.Text))
                {
                    objProposalOL.NightStayAllowance = 0;
                }
                else
                {
                    //objProposalOL.NightStayAllowance = Convert.ToDouble(lblNightCharges.Text.ToString());
                    objProposalOL.NightStayAllowance = Convert.ToDouble(txtEditNightCharges.Text.ToString());
                }

                //if (string.IsNullOrEmpty(lblChauffeurCharges.Text))
                if (string.IsNullOrEmpty(txtEditChauffeurCharges.Text))
                {
                    objProposalOL.OutStationAllowance = 0;
                }
                else
                {
                    //objProposalOL.OutStationAllowance = Convert.ToDouble(lblNightCharges.Text.ToString());
                    objProposalOL.OutStationAllowance = Convert.ToDouble(txtEditChauffeurCharges.Text.ToString());
                }

                objProposalOL.SDPRateID = Convert.ToInt32(hdnSDPRateID.Value);
                objProposalOL.Remarks = txtRemarks.Text;
                objProposalOL.NotApproveYN = false;

                if (ddlEditPackageCityName.Text == "Tier 1" || ddlEditPackageCityName.Text == "Tier 2"
                    || ddlEditPackageCityName.Text == "Tier 3" || ddlEditPackageCityName.Text == "City A"
                    || ddlEditPackageCityName.Text == "City B")
                {
                    ds = new DataSet();
                    ds = objProposal.ProposalCityMapping(ddlEditPackageCityName.Text);

                    for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
                    {
                        objProposalOL.CityID = Convert.ToInt32(ds.Tables[0].Rows[i]["CityID"]);

                        status = objProposal.SaveProposalDetails(objProposalOL);
                    }
                }
                else
                {
                    status = objProposal.SaveProposalDetails(objProposalOL);
                }
                //}
            }

            status = objProposal.SaveSubmitApproval(objProposalOL);
            if (status > 0)
            {
                SendMail(newFileName);
                lblMessage.Visible = true;
                lblMessage.Text = "Proposal created successfully.";
                lblMessage.ForeColor = Color.Red;
                BindGridWithInitialValue(objProposalOL);
            }
            else
            {
                lblMessage.Visible = true;
                lblMessage.Text = "Getting save Error";
                lblMessage.ForeColor = Color.Red;
            }
        }
        catch (Exception Ex)
        {
            lblMessage.Visible = true;
            lblMessage.Text = Ex.Message;
            lblMessage.ForeColor = Color.Red;
        }
    }

    protected void grvProposal_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        objProposalOL = new OL_Proposal();
        objProposal = new sqlProposal();
        try
        {
            DropDownList ddlPackageType = (DropDownList)grvProposal.Rows[e.RowIndex].FindControl("ddlEditPackageType");
            DropDownList ddlPackageCityName = (DropDownList)grvProposal.Rows[e.RowIndex].FindControl("ddlEditPackageCityName");
            DropDownList ddlVehicleCategory = (DropDownList)grvProposal.Rows[e.RowIndex].FindControl("ddlEditVehicleCategory");
            // DropDownList ddlVehicleModel = (DropDownList)grvProposal.Rows[e.RowIndex].FindControl("ddlEditVehicleModel");
            DropDownList ddlBillingBasis = (DropDownList)grvProposal.Rows[e.RowIndex].FindControl("ddlEditBillingBasis");
            DropDownList ddlFGR = (DropDownList)grvProposal.Rows[e.RowIndex].FindControl("ddlEditFGR");
            //DropDownList ddlPackageRate = (DropDownList)grvProposal.Rows[e.RowIndex].FindControl("ddlEditPackageRate");
            TextBox ddlPackageRate = (TextBox)grvProposal.Rows[e.RowIndex].FindControl("ddlEditPackageRate");

            DropDownList ddlPackageKM = (DropDownList)grvProposal.Rows[e.RowIndex].FindControl("ddlEditPackageKM");
            DropDownList ddlPackageHour = (DropDownList)grvProposal.Rows[e.RowIndex].FindControl("ddlEditPackageHour");
            //DropDownList ddlPackageExtraKM = (DropDownList)grvProposal.Rows[e.RowIndex].FindControl("ddlEditPackageExtraKM");
            TextBox ddlPackageExtraKM = (TextBox)grvProposal.Rows[e.RowIndex].FindControl("ddlEditPackageExtraKM");
            //DropDownList ddlPackageExtraHr = (DropDownList)grvProposal.Rows[e.RowIndex].FindControl("ddlEditPackageExtraHr");
            TextBox ddlPackageExtraHr = (TextBox)grvProposal.Rows[e.RowIndex].FindControl("ddlEditPackageExtraHr");

            //DropDownList ddlthresholdExtraHr = (DropDownList)grvProposal.Rows[e.RowIndex].FindControl("ddlEditthresholdExtraHr");
            //DropDownList ddlthresholdExtraKM = (DropDownList)grvProposal.Rows[e.RowIndex].FindControl("ddlEditthresholdExtraKM");
            TextBox txtNightCharges = (TextBox)grvProposal.Rows[e.RowIndex].FindControl("txtEditNightCharges");
            TextBox txtChauffeurCharges = (TextBox)grvProposal.Rows[e.RowIndex].FindControl("txtEditChauffeurCharges");
            TextBox txtRemarks = (TextBox)grvProposal.Rows[e.RowIndex].FindControl("txtEditRemarks");
            Label lblPackageRate = (Label)grvProposal.Rows[e.RowIndex].FindControl("lblPackageRate");
            HiddenField hdnSDPRateID = (HiddenField)grvProposal.Rows[e.RowIndex].FindControl("hdnSDPRateID");

            objProposalOL.ProposalDetailId = Convert.ToInt32(grvProposal.DataKeys[e.RowIndex].Values[0]);
            objProposalOL.PkgTypeId = Convert.ToInt32(ddlPackageType.SelectedValue.ToString());
            objProposalOL.CityID = Convert.ToInt32(ddlPackageCityName.SelectedValue.ToString());
            objProposalOL.CarCatID = Convert.ToInt32(ddlVehicleCategory.SelectedValue.ToString());
            //objProposalOL.CarModelID = Convert.ToInt32(ddlVehicleModel.SelectedValue.ToString());
            //objProposalOL.PKGRate = Convert.ToDouble(ddlPackageRate.SelectedValue.ToString());
            objProposalOL.PKGRate = Convert.ToDouble(ddlPackageRate.Text.ToString());

            objProposalOL.PkgKMs = Convert.ToInt32(ddlPackageKM.SelectedValue.ToString());
            objProposalOL.PkgHrs = Convert.ToInt32(ddlPackageHour.SelectedValue.ToString());
            //objProposalOL.ExtraKMRate = Convert.ToInt32(ddlPackageExtraKM.SelectedValue.ToString());
            objProposalOL.ExtraKMRate = Convert.ToInt32(ddlPackageExtraKM.Text.ToString());
            //objProposalOL.ExtraHrRate = Convert.ToInt32(ddlPackageExtraHr.SelectedValue.ToString());
            objProposalOL.ExtraHrRate = Convert.ToInt32(ddlPackageExtraHr.Text.ToString());

            //objProposalOL.ThresholdExtraHr = Convert.ToInt32(ddlthresholdExtraHr.SelectedValue.ToString());
            //objProposalOL.ThresholdExtraKM = Convert.ToInt32(ddlthresholdExtraKM.SelectedValue.ToString());
            objProposalOL.BillingBasis = ddlBillingBasis.SelectedValue.ToString();
            objProposalOL.ProspectID = Convert.ToInt32(ddlClient.SelectedValue);
            objProposalOL.TempId = Convert.ToInt32(grvProposal.DataKeys[e.RowIndex].Values[1]);
            objProposalOL.SDPRateID = Convert.ToInt32(hdnSDPRateID.Value);

            //if (objProposalOL.BillingBasis == "PP")
            //{
                objProposalOL.Fgr = Convert.ToInt32(ddlFGR.SelectedValue);
            //}
            //else
            //{
            //    objProposalOL.Fgr = null;
            //}
            objProposalOL.SysUserId = Convert.ToInt32(Session["UserID"]);
            if (string.IsNullOrEmpty(txtNightCharges.Text))
            {
                objProposalOL.NightStayAllowance = 0;
            }
            else
            {
                objProposalOL.NightStayAllowance = Convert.ToDouble(txtNightCharges.Text.ToString());
            }

            if (string.IsNullOrEmpty(txtChauffeurCharges.Text))
            {
                objProposalOL.OutStationAllowance = 0;
            }
            else
            {
                objProposalOL.OutStationAllowance = Convert.ToDouble(txtChauffeurCharges.Text.ToString());
            }

            objProposalOL.Remarks = txtRemarks.Text;

            if (string.IsNullOrEmpty(objProposalOL.Remarks.Trim()))
            {
                lblMessage.Visible = true;
                lblMessage.Text = "Please enter Remarks.";
                lblMessage.ForeColor = Color.Red;
                txtRemarks.Focus();
                return;
            }

            objProposalOL.OriginalBaseRate = Convert.ToDouble(lblPackageRate.Text);

            int status = objProposal.UpdateProposalDetails(objProposalOL);
            if (status > 0)
            {
                grvProposal.EditIndex = -1;
                objProposalOL = BasicBinding(objProposalOL);
                BindGridWithInitialValue(objProposalOL);
                lblMessage.Visible = true;
                lblMessage.Text = "Update successfully.";
                lblMessage.ForeColor = Color.Red;

            }
            else
            {
                lblMessage.Text = "Getting Error";
                lblMessage.ForeColor = Color.Red;
            }
        }
        catch (Exception Ex)
        {
            lblMessage.Visible = true;
            lblMessage.Text = Ex.Message;
            lblMessage.ForeColor = Color.Red;

            Helperfunction.LogErrorDetails(Ex.ToString());
        }
    }

    protected void grvProposal_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        objProposal = new sqlProposal();
        objProposalOL = new OL_Proposal();

        try
        {
            objProposalOL.ProposalDetailId = Convert.ToInt32(grvProposal.DataKeys[e.RowIndex].Values[0]);
            objProposalOL.TempId = Convert.ToInt32(grvProposal.DataKeys[e.RowIndex].Values[1]);
            objProposalOL.SysUserId = Convert.ToInt32(Session["UserID"]);

            int status = objProposal.DeleteProposalDetails(objProposalOL);
            if (status > 0)
            {
                lblMessage.Visible = true;
                lblMessage.Text = "Record delete successfully.";
                lblMessage.ForeColor = Color.Red;
                objProposalOL = BasicBinding(objProposalOL);
                BindGridWithInitialValue(objProposalOL);
            }
            else
            {
                lblMessage.Visible = true;
                lblMessage.Text = "Delete getting error.";
                lblMessage.ForeColor = Color.Red;
            }


        }
        catch (Exception Ex)
        {

            lblMessage.Visible = true;
            lblMessage.Text = Ex.Message;
            lblMessage.ForeColor = Color.Red;
        }


    }
    private void SendMail(string uploadFileName)
    {

        objProposal = new sqlProposal();
        DataSet ds = new DataSet();
        DataSet dsdetail = new DataSet();
        StringBuilder strBody = new StringBuilder();

        //Response.Clear();
        //Response.Buffer = true;
        //Response.ClearContent();
        //Response.ClearHeaders();
        //Response.Charset = "";
        //string FileName = "Vithal" + DateTime.Now + ".doc";
        //StringWriter strwritter = new StringWriter();
        //HtmlTextWriter htmltextwrtter = new HtmlTextWriter(strwritter);
        //Response.Cache.SetCacheability(HttpCacheability.NoCache);
        //Response.ContentType = "application/msword";
        //Response.AddHeader("Content-Disposition", "attachment;filename=" + FileName);
        //grvProposal.GridLines = GridLines.Both;
        //grvProposal.HeaderStyle.Font.Bold = true;
        //grvProposal.RenderControl(htmltextwrtter);
        //Response.Write(strwritter.ToString());
        //Response.End();      

        ds = objProposal.GetProposalDetailsMail(Convert.ToInt32(ddlClient.SelectedValue.ToString()));


        if (ds.Tables[0].Rows.Count > 0)
        {

            //strBody.Append("<html><body><table cellpadding='2' cellspacing='0' border='1'>");
            //strBody.Append("</table>");
            //strBody.Append("</body>");
            //strBody.Append("</html>");
            //strBody.Append("<tr>");
            //strBody.Append("<td><b>Pacakage Type</b></td>");
            //strBody.Append("<td><b>City Name</b></td>");
            //strBody.Append("<td><b>Vehicle Category</b></td>");
            //strBody.Append("<td><b>Billing Basis<b></td>");
            //strBody.Append("<td><b>FGR<b></td>");
            //strBody.Append("<td><b>Pacakage Hr Rate</b></td>");
            //strBody.Append("<td><b>Pacakage KM Rate</b></td>");
            //strBody.Append("<td><b>Package Rate</b></td>");
            //strBody.Append("<td><b>Extra KM </b></td>");
            //strBody.Append("<td><b>Extra Hour</b></td>");
            //strBody.Append("<td><b>Threshold Extra Hr</b></td>");
            //strBody.Append("<td><b>Threshold Extra KM</b></td>");
            //strBody.Append("<td><b>Night Charges</b></td>");
            //strBody.Append("<td><b>Chauffeur Charges</b></td>");
            //strBody.Append("</tr>");

            string emailid = "";

            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                emailid = dr["ClientContractApprovalEmailID"].ToString();

                StringBuilder mailbody = new StringBuilder();
                mailbody.Append("<html xmlns='http://www.w3.org/1999/xhtml'><body><table style='font-family:Arial; font-size:12px;' cellpadding='5' cellspacing='0'><tr><td>");
                mailbody.Append("Dear " + dr["ClientContractApprovalName"].ToString() + ",<br /><br/></td></tr>");
                //mailbody.Append("<tr><td>A new proposal has been submitted for your city, <a href='https://insta.carzonrent.com/login.asp'>Click here</a> to logic approve the proposal </td></tr>");
                mailbody.Append("<tr><td>A new proposal has been submitted for your city, <a href='http://192.168.2.10/corint/login.asp'>Click here</a> to approve the proposal </td></tr>");


                dsdetail = objProposal.GetProposalEmailData(Convert.ToInt32(ddlClient.SelectedValue.ToString()), emailid);

                if (dsdetail.Tables[0].Rows.Count > 0)
                {
                    mailbody.Append("<tr><td>&nbsp;</td></tr>");
                    mailbody.Append("<table cellpadding='2' cellspacing='0' border='1'>");
                    int i = 1;
                    foreach (DataRow drdetail in dsdetail.Tables[0].Rows)
                    {
                        if (i == 1)
                        {
                            mailbody.Append("<tr>");
                            mailbody.Append("<td><b>Pacakage Type</b></td>");
                            mailbody.Append("<td><b>City Name</b></td>");
                            mailbody.Append("<td><b>Vehicle Category</b></td>");
                            mailbody.Append("<td><b>Billing Basis<b></td>");
                            mailbody.Append("<td><b>FGR<b></td>");
                            mailbody.Append("<td><b>Pacakage Hr Rate</b></td>");
                            mailbody.Append("<td><b>Pacakage KM Rate</b></td>");
                            mailbody.Append("<td><b>New Purchase Rate</b></td>");
                            mailbody.Append("<td><b>Sales Rate</b></td>");
                            mailbody.Append("<td><b>Deviation</b></td>");
                            mailbody.Append("<td><b>Extra KM </b></td>");
                            mailbody.Append("<td><b>Extra Hour</b></td>");
                            mailbody.Append("<td><b>Threshold Extra Hr</b></td>");
                            mailbody.Append("<td><b>Threshold Extra KM</b></td>");
                            mailbody.Append("<td><b>Night Charges</b></td>");
                            mailbody.Append("<td><b>Chauffeur Charges</b></td>");
                            mailbody.Append("</tr>");
                        }

                        mailbody.Append("<tr>");
                        mailbody.Append("<td>" + drdetail["PackageType"].ToString() + "</td>");
                        mailbody.Append("<td>" + drdetail["cityName"].ToString() + "</td>");
                        mailbody.Append("<td>" + drdetail["CarCatName"].ToString() + "</td>");
                        mailbody.Append("<td>" + drdetail["BillingBasis"].ToString() + "</td>");
                        mailbody.Append("<td>" + drdetail["FGR"].ToString() + "</td>");
                        mailbody.Append("<td>" + drdetail["PkgHrs"].ToString() + "</td>");
                        mailbody.Append("<td>" + drdetail["PkgKMs"].ToString() + "</td>");

                        double newpurchaserate = Math.Round((Convert.ToDouble(drdetail["PKGRate"]) * 100)
                            / (100 + Convert.ToDouble(ConfigurationManager.AppSettings["PercentIncreaseInPurchase"])), 2);

                        mailbody.Append("<td style='color:red'>" + newpurchaserate.ToString() + "</td>");
                        mailbody.Append("<td style='color:red'>" + drdetail["PKGRate"].ToString() + "</td>");
                        mailbody.Append("<td style='color:red'>" + drdetail["diviation"].ToString() + "</td>");

                        mailbody.Append("<td>" + drdetail["ExtraKMRate"].ToString() + "</td>");
                        mailbody.Append("<td>" + drdetail["ExtraHrRate"].ToString() + "</td>");
                        mailbody.Append("<td>" + drdetail["ThresholdExtraHr"].ToString() + "</td>");
                        mailbody.Append("<td>" + drdetail["ThresholdExtraKM"].ToString() + "</td>");
                        mailbody.Append("<td>" + drdetail["NightStayAllowance"].ToString() + "</td>");
                        mailbody.Append("<td>" + drdetail["OutStationAllowance"].ToString() + "</td>");
                        mailbody.Append("</tr>");

                        i++;
                    }
                    mailbody.Append("</table>");
                }

                mailbody.Append("<br/><br/><table style='font-family:Arial; font-size:12px;'>");
                mailbody.Append("<tr><td>Regards,<br/><br/>Team Carzonrent</td></tr>");
                mailbody.Append("<tr><td><br/><br/>This is system generated mail, Please do not reply.</td></tr></table></body></html>");

                Mail(uploadFileName, mailbody, emailid);
            }

            /*
            string fileName = "MsWordSample.doc";

            // You can add whatever you want to add as the HTML and it will be generated as Ms Word docs
            Response.AppendHeader("Content-Type", "application/msword");
            Response.AppendHeader ("Content-disposition", "attachment; filename="+ fileName);
            Response.Charset = "";
            this.EnableViewState = false;
            System.IO.StringWriter oStringWriter = new System.IO.StringWriter();
            System.Web.UI.HtmlTextWriter oHtmlTextWriter = new System.Web.UI.HtmlTextWriter(oStringWriter);
            //Response.Write(strBody);
            ltlReport.RenderControl(oHtmlTextWriter);
            Response.Write(strBody.ToString());
            Response.End();
           */

            GeneralUtility.InitializeControls(Form);
        }
    }

    private void Mail(string uploadFileName, StringBuilder strBody, string ToEmailID)
    {
        System.Net.Mail.MailMessage message = new System.Net.Mail.MailMessage();
        //message.To.Add("rajesh@carzonrent.com");
        //message.To.Add("achint.rastogi@carzonrent.com");
        //message.CC.Add("arpita.ghosh@carzonrent.com");
        //message.Bcc.Add("balister.sharma@carzonrent.com");
        message.To.Add(ToEmailID);

        //if (!string.IsNullOrEmpty(txtConactPersonEmailId.Text.ToString()))
        //{
        //    // message.CC.Add(txtConactPersonEmailId.Text.ToString());
        //}

        //byte[] wordFile = System.Text.Encoding.UTF8.GetBytes(strBody.ToString());
        //MemoryStream ms = new MemoryStream();
        //ms.Write(wordFile, 0, wordFile.Length);
        //ms.Position = 0;
        //Attachment attachment;
        //attachment = new Attachment(ms, "RateCard.doc", "application/msword");
        //message.Attachments.Add(attachment);

        //Attachment doc = null;
        //if (ddlTypeProposal.SelectedValue.ToString() == "1")
        //{
        //    string filePath = Server.MapPath("../DOC/Commercial.doc");
        //    doc = new Attachment(filePath);
        //    message.Attachments.Add(doc);
        //}
        //else if (ddlTypeProposal.SelectedValue.ToString() == "3")
        //{
        //    string filePath = Server.MapPath("../DOC/LTRProposal.doc");
        //    doc = new Attachment(filePath);
        //    message.Attachments.Add(doc);
        //}
        ////View attachment
        //Attachment view;
        //string viewFile = Server.MapPath("../DOC/CommitmenttoService.pdf");
        //view = new Attachment(viewFile);
        //message.Attachments.Add(view);

        //viewFile = Server.MapPath("../DOC/corporatepresentation.pdf");
        //view = new Attachment(viewFile);
        //message.Attachments.Add(view);

        //viewFile = Server.MapPath("../DOC/StandardMailer.pdf");
        //view = new Attachment(viewFile);
        //message.Attachments.Add(view);

        //if (!string.IsNullOrEmpty(uploadFileName))
        //{
        //    viewFile = Server.MapPath("../ProposalDOC/" + uploadFileName.ToString());
        //    view = new Attachment(viewFile);
        //    message.Attachments.Add(view);
        //}
        //End of view attachment

        message.Subject = "Proposal Details";
        message.From = new System.Net.Mail.MailAddress("booking@carzonrent.com");
        message.IsBodyHtml = true;
        message.Body = strBody.ToString();
        message.Priority = MailPriority.High;
        //System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient("smtp.gmail.com", 587);
        System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient("smtp-relay.gmail.com", 587);
        smtp.EnableSsl = true;
        smtp.Credentials = new NetworkCredential("services@carzonrent.com", "Carz@1234");
        smtp.Send(message);
    }

    //public override void VerifyRenderingInServerForm(Control control)
    //{
    //    //required to avoid the runtime error "  
    //    //Control 'GridView1' of type 'GridView' must be placed inside a form tag with runat=server."  
    //}

    protected void ddlClient_SelectedIndexChanged(object sender, EventArgs e)
    {
        /*
        objProposal = new sqlProposal();
        objProposalOL = new OL_Proposal();
        DataSet ds = new DataSet();

        objProposalOL.ProspectID = Convert.ToInt32(ddlClient.SelectedValue);
        objProposalOL.SysUserId = Convert.ToInt32(Session["UserID"]);
        ds = objProposal.PackageInsertion(objProposalOL);
        BindGridWithInitialValue();
        GeneralUtility.InitializeControls(Form);
        ddlCityName.SelectedIndex = 0;
        ddlProposalFor.SelectedIndex = 0;
        ddlTypeProposal.SelectedIndex = 0;
        ddlActionMgrName.SelectedIndex = 0;
        txtDate.Text = string.Empty;
         */
        GeneralUtility.InitializeControls(Form);
        grvProposal.DataSource = null;
        grvProposal.DataBind();
    }
    protected void ddlVehicleCategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        //DataSet ds = new DataSet();
        //objProposal = new sqlProposal();
        //DropDownList ddlcategory = (DropDownList)grvProposal.FooterRow.FindControl("ddlVehicleCategory");
        //DropDownList ddlModel = (DropDownList)grvProposal.FooterRow.FindControl("ddlVehicleModel");
        //ds = objProposal.GetCarModel(Convert.ToInt32(ddlcategory.SelectedValue.ToString()));
        //BindDropDownList(ddlModel, ds.Tables[0], "CarModelName", "CarModelID", "0");

    }
    protected void ddlEditVehicleCategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        /*
        DataSet ds = new DataSet();
        objProposal = new sqlProposal();
        int k = grvProposal.SelectedIndex;
        int Index = ((GridViewRow)((sender as Control)).NamingContainer).RowIndex;

        DropDownList ddlcategory = (DropDownList)grvProposal.Rows[Index].FindControl("ddlEditVehicleCategory");
        DropDownList ddlModel = (DropDownList)grvProposal.Rows[Index].FindControl("ddlEditVehicleModel");
        ds = objProposal.GetCarModel(Convert.ToInt32(ddlcategory.SelectedValue.ToString()));
        BindDropDownList(ddlModel, ds.Tables[0], "CarModelName", "CarModelID", "0");
         */

    }
    protected void btnRplicate_Click(object sender, EventArgs e)
    {
        /* old logic
        string selectedValues = string.Empty;
        int status = 0;
        objProposal = new sqlProposal();
        objProposalOL = new OL_Proposal();
        try
        {

            objProposalOL.ProposalDetailId = Convert.ToInt32(txtProposalDetailId.Text.ToString());
            objProposalOL.ProspectID = Convert.ToInt32(ddlClient.SelectedValue.ToString());
            objProposalOL.SysUserId = Convert.ToInt32(Session["UserID"]);
            objProposalOL.TempId = 0;
            foreach (ListItem li in ddlRplicateCity.Items)
            {
                if (li.Selected == true)
                {
                    selectedValues += li.Value + ",";
                }
            }
            objProposalOL.MultipleCityId = selectedValues.ToString();
            if (string.IsNullOrEmpty(objProposalOL.MultipleCityId) || string.IsNullOrEmpty(objProposalOL.ProposalDetailId.ToString()) || string.IsNullOrEmpty(objProposalOL.ProspectID.ToString()))
            {
                lblMessage.Visible = true;
                lblMessage.Text = "Select all required flieds";
                lblMessage.ForeColor = Color.Red;
            }
            else
            {
                status = objProposal.ReplicateProposalDetails(objProposalOL);
                BasicBinding(objProposalOL);
                BindGridWithInitialValue(objProposalOL);
            }
        }
        catch (Exception Ex)
        {
            lblMessage.Visible = true;
            lblMessage.Text = Ex.Message;
            lblMessage.ForeColor = Color.Red;
        } End of old*/

        string selectedValues = string.Empty;
        int status = 0;
        objProposal = new sqlProposal();
        objProposalOL = new OL_Proposal();
        try
        {

            objProposalOL.ProspectID = Convert.ToInt32(ddlClient.SelectedValue.ToString());
            objProposalOL.SysUserId = Convert.ToInt32(Session["UserID"]);

            foreach (ListItem li in ddlRplicateCity.Items)
            {
                if (li.Selected == true)
                {
                    selectedValues += li.Value + ",";
                }
            }
            selectedValues = selectedValues.Remove(selectedValues.Length - 1, 1);
            objProposalOL.MultipleCityId = selectedValues;

            if (string.IsNullOrEmpty(objProposalOL.MultipleCityId) || string.IsNullOrEmpty(objProposalOL.ProspectID.ToString()))
            {
                lblMessage.Visible = true;
                lblMessage.Text = "Select all required flieds";
                lblMessage.ForeColor = Color.Red;
                return;
            }


            foreach (GridViewRow grow in grvProposal.Rows)
            {

                CheckBox chkReplicate = (CheckBox)grow.FindControl("chkReplicate");
                objProposalOL.ProposalDetailId = (int)grvProposal.DataKeys[grow.RowIndex].Values[0];
                objProposalOL.TempId = (int)grvProposal.DataKeys[grow.RowIndex].Values[1];
                if (chkReplicate.Checked == true && !string.IsNullOrEmpty(objProposalOL.MultipleCityId))
                {
                    status = objProposal.ReplicateProposalDetails(objProposalOL);
                }
            }

            BasicBinding(objProposalOL);
            BindGridWithInitialValue(objProposalOL);


        }
        catch (Exception Ex)
        {
            lblMessage.Visible = true;
            lblMessage.Text = Ex.Message;
            lblMessage.ForeColor = Color.Red;
        }
    }
    protected void ddlBillingBasis_SelectedIndexChanged(object sender, EventArgs e)
    {
        //DataSet ds = new DataSet();
        //objProposal = new sqlProposal();
        //DropDownList ddlcategory = (DropDownList)grvProposal.FooterRow.FindControl("ddlVehicleCategory");
        //DropDownList ddlModel = (DropDownList)grvProposal.FooterRow.FindControl("ddlVehicleModel");
        //ds = objProposal.GetCarModel(Convert.ToInt32(ddlcategory.SelectedValue.ToString()));
        //BindDropDownList(ddlModel, ds.Tables[0], "CarModelName", "CarModelID", "0");

        DropDownList ddlBillingBasis = (DropDownList)grvProposal.FooterRow.FindControl("ddlBillingBasis");
        DropDownList ddlFGR = (DropDownList)grvProposal.FooterRow.FindControl("ddlFGR");
        if (ddlBillingBasis.SelectedValue == "PP")
        {
            // grvProposal.Columns[5].Visible = true;
            // BindDropDownList(ddlFGR, 1, 500, 1, "0");
        }
        else
        {
            //grvProposal.Columns[5].Visible = false;
        }


    }
    protected void ddlEditBillingBasis_SelectedIndexChanged(object sender, EventArgs e)
    {
        int Index = ((GridViewRow)((sender as Control)).NamingContainer).RowIndex;

        DropDownList ddlEditBillingBasis = (DropDownList)grvProposal.Rows[Index].FindControl("ddlEditBillingBasis");
        DropDownList ddlFGR = (DropDownList)grvProposal.Rows[Index].FindControl("ddlEditFGR");
        if (ddlEditBillingBasis.SelectedValue == "PP")
        {
            grvProposal.Columns[5].Visible = true;
            BindDropDownList(ddlFGR, 1, 500, 1, "0");
        }
        else
        {
            grvProposal.Columns[5].Visible = false;
        }

    }
    protected void bntGet_Click(object sender, EventArgs e)
    {
        DataSet ds = new DataSet();
        objProposalOL = new OL_Proposal();
        objProposalOL = BasicBinding(objProposalOL);
        ds = objProposal.PackageInsertion(objProposalOL);
        BindGridWithInitialValue(objProposalOL);
    }
    private OL_Proposal BasicBinding(OL_Proposal objPrososalOL)
    {
        objProposal = new sqlProposal();
        string selectedValues = string.Empty;
        foreach (ListItem li in ddlPackageCategory.Items)
        {
            if (li.Selected == true)
            {
                selectedValues += li.Value + ",";
            }
        }
        selectedValues = selectedValues.Remove(selectedValues.Length - 1, 1);
        objProposalOL.PackageCategoryID = selectedValues;
        objProposalOL.ProspectID = Convert.ToInt32(ddlClient.SelectedValue);
        objProposalOL.SysUserId = Convert.ToInt32(Session["UserID"]);
        objProposalOL.ProposalFor = Convert.ToInt16(ddlProposalFor.SelectedValue);
        objProposalOL.CommitmentID = Convert.ToInt32(ddlcommitment.SelectedValue);

        return objPrososalOL;
    }
    protected void btnDelete_Click(object sender, EventArgs e)
    {
        objProposalOL = new OL_Proposal();
        objProposal = new sqlProposal();
        int status = 0;
        try
        {
            objProposalOL.SysUserId = Convert.ToInt32(Session["UserID"]);
            foreach (GridViewRow grow in grvProposal.Rows)
            {
                //Searching CheckBox("chkDel") in an individual row of Grid  
                CheckBox chkdel = (CheckBox)grow.FindControl("chkDel");
                objProposalOL.ProposalDetailId = (int)grvProposal.DataKeys[grow.RowIndex].Values[0];
                objProposalOL.TempId = (int)grvProposal.DataKeys[grow.RowIndex].Values[1];
                //If CheckBox is checked than delete the record with particular empid  
                if (chkdel.Checked)
                {
                    status = objProposal.DeleteProposalDetails(objProposalOL);
                }
            }

            if (status > 0)
            {
                lblMessage.Visible = true;
                lblMessage.Text = "Record delete successfully.";
                lblMessage.ForeColor = Color.Red;
                objProposalOL = BasicBinding(objProposalOL);
                BindGridWithInitialValue(objProposalOL);
            }
            else
            {
                lblMessage.Visible = true;
                lblMessage.Text = "Delete getting error.";
                lblMessage.ForeColor = Color.Red;
            }

        }
        catch (Exception Ex)
        {

            lblMessage.Visible = true;
            lblMessage.Text = Ex.Message;
            lblMessage.ForeColor = Color.Red;
			BindGridWithInitialValue(objProposalOL);
        }

        objProposalOL = BasicBinding(objProposalOL);
        BindGridWithInitialValue(objProposalOL);

    }
    protected void bntReplicatePackage_Click(object sender, EventArgs e)
    {
        string selectedValues = string.Empty;
        int status = 0;
        objProposal = new sqlProposal();
        objProposalOL = new OL_Proposal();
        try
        {

            objProposalOL.ProspectID = Convert.ToInt32(ddlClient.SelectedValue.ToString());
            objProposalOL.SysUserId = Convert.ToInt32(Session["UserID"]);

            foreach (ListItem li in ddlRplicateCity.Items)
            {
                if (li.Selected == true)
                {
                    selectedValues += li.Value + ",";
                }
            }
            selectedValues = selectedValues.Remove(selectedValues.Length - 1, 1);
            objProposalOL.MultipleCityId = selectedValues;
            if (string.IsNullOrEmpty(objProposalOL.MultipleCityId) || string.IsNullOrEmpty(objProposalOL.ProspectID.ToString()))
            {
                lblMessage.Visible = true;
                lblMessage.Text = "Select all required flieds";
                lblMessage.ForeColor = Color.Red;
                return;
            }


            foreach (GridViewRow grow in grvProposal.Rows)
            {

                CheckBox chkReplicate = (CheckBox)grow.FindControl("chkReplicate");
                objProposalOL.ProposalDetailId = (int)grvProposal.DataKeys[grow.RowIndex].Values[0];
                objProposalOL.TempId = (int)grvProposal.DataKeys[grow.RowIndex].Values[1];
                if (chkReplicate.Checked == true && !string.IsNullOrEmpty(objProposalOL.MultipleCityId))
                {
                    status = objProposal.ReplicateProposalDetails(objProposalOL);
                }
            }

            BasicBinding(objProposalOL);
            BindGridWithInitialValue(objProposalOL);


        }
        catch (Exception Ex)
        {
            lblMessage.Visible = true;
            lblMessage.Text = Ex.Message;
            lblMessage.ForeColor = Color.Red;
        }
    }
    protected void ddlEditPackageHour_SelectedIndexChanged(object sender, EventArgs e)
    {

        DataSet ds = new DataSet();
        objProposal = new sqlProposal();
        int k = grvProposal.SelectedIndex;
        Double baseRate, baseKM, baseHr;
        Double packageKM = 0, packageHr = 0, PackageRate = 0;
        int Index = ((GridViewRow)((sender as Control)).NamingContainer).RowIndex;

        baseRate = Convert.ToDouble(grvProposal.DataKeys[Index].Values[2]);
        baseHr = Convert.ToDouble(grvProposal.DataKeys[Index].Values[3]);
        baseKM = Convert.ToDouble(grvProposal.DataKeys[Index].Values[4]);

        DropDownList ddlEditPackageHour = (DropDownList)grvProposal.Rows[Index].FindControl("ddlEditPackageHour");
        DropDownList ddlEditPackageKM = (DropDownList)grvProposal.Rows[Index].FindControl("ddlEditPackageKM");
        //DropDownList ddlEditPackageRate = (DropDownList)grvProposal.Rows[Index].FindControl("ddlEditPackageRate");
        TextBox ddlEditPackageRate = (TextBox)grvProposal.Rows[Index].FindControl("ddlEditPackageRate");
        Label lblPackageTypeId = (Label)grvProposal.Rows[Index].FindControl("lblPackageID");

        if (lblPackageTypeId.Text == PackageType.local)
        {
            packageKM = baseKM * Convert.ToDouble(ddlEditPackageHour.SelectedValue);
            PackageRate = baseRate * Convert.ToDouble(ddlEditPackageHour.SelectedValue);
        }
        else if (lblPackageTypeId.Text == PackageType.outstation)
        {
            packageKM = 0;
            PackageRate = baseRate;// *Convert.ToDouble(ddlEditPackageHour.SelectedValue);
        }
        else if (lblPackageTypeId.Text == PackageType.Airport)
        {
            packageKM = 0;
            PackageRate = baseRate;
        }

        //BindDropDownList(ddlEditPackageKM, 1, 250, 1, Convert.ToString(packageKM));
        //BindDropDownList(ddlEditPackageRate, 1, 10000, 1, Convert.ToString(PackageRate));
        ddlEditPackageRate.Text = Convert.ToString(PackageRate);

    }
    protected void ddlPackageHour_SelectedIndexChanged(object sender, EventArgs e)
    {

        DataSet ds = new DataSet();
        objProposal = new sqlProposal();
        objProposalOL = new OL_Proposal();
        int k = grvProposal.SelectedIndex;
        Double baseRate, baseKM, baseHr;
        Double packageKM = 0, packageHr = 0, PackageRate = 0;
        int Index = ((GridViewRow)((sender as Control)).NamingContainer).RowIndex;
        lblMessage.Visible = false;
        DropDownList ddlPackageHour = (DropDownList)grvProposal.FooterRow.FindControl("ddlPackageHour");
        DropDownList ddlPackageKM = (DropDownList)grvProposal.FooterRow.FindControl("ddlPackageKM");
        //DropDownList ddlPackageRate = (DropDownList)grvProposal.FooterRow.FindControl("ddlPackageRate");
        TextBox ddlPackageRate = (TextBox)grvProposal.FooterRow.FindControl("ddlPackageRate");

        DropDownList ddlPackageType = (DropDownList)grvProposal.FooterRow.FindControl("ddlPackageType");
        DropDownList ddlVehicleCategory = (DropDownList)grvProposal.FooterRow.FindControl("ddlVehicleCategory");
        DropDownList ddlPackageCityName = (DropDownList)grvProposal.FooterRow.FindControl("ddlPackageCityName");

        objProposalOL.PkgTypeId = Convert.ToInt32(ddlPackageType.SelectedValue);
        objProposalOL.CarCatID = Convert.ToInt32(ddlVehicleCategory.SelectedValue);
        objProposalOL.CityID = Convert.ToInt32(ddlPackageCityName.SelectedValue);
        ds = objProposal.GetPackageBaseRate(objProposalOL);
        if (ds.Tables[0].Rows.Count > 0)
        {
            baseRate = Convert.ToDouble(ds.Tables[0].Rows[0]["BaseRate"]);
            baseHr = Convert.ToDouble(ds.Tables[0].Rows[0]["BaseHr"]);
            baseKM = Convert.ToDouble(ds.Tables[0].Rows[0]["BaseKM"]);
        }
        else
        {
            baseRate = 0;
            baseHr = 0;
            baseKM = 0;
        }
        if (ddlPackageType.SelectedValue == PackageType.local && Convert.ToDouble(ddlPackageHour.SelectedValue) == 0)
        {

            ddlPackageHour.SelectedValue = "1";
            lblMessage.Visible = true;
            lblMessage.Text = "Zeor hours package not available for the local";
            lblMessage.ForeColor = Color.Red;

        }
        else if (ddlPackageType.SelectedValue == PackageType.local)
        {
            packageKM = baseKM * Convert.ToDouble(ddlPackageHour.SelectedValue);
            PackageRate = baseRate * Convert.ToDouble(ddlPackageHour.SelectedValue);
        }
        else if (ddlPackageType.SelectedValue == PackageType.outstation)
        {
            packageKM = 0;
            PackageRate = baseRate * Convert.ToDouble(ddlPackageHour.SelectedValue);
        }
        else if (ddlPackageType.SelectedValue == PackageType.Airport)
        {
            packageKM = 0;
            PackageRate = baseRate;
        }

        //BindDropDownList(ddlPackageKM, 1, 250, 1, Convert.ToString(packageKM));
        //BindDropDownList(ddlPackageRate, 1, 10000, 1, Convert.ToString(PackageRate));
        ddlPackageRate.Text = Convert.ToString(PackageRate);

    }
    protected void ddlEditPackageKM_SelectedIndexChanged(object sender, EventArgs e)
    {

        DataSet ds = new DataSet();
        objProposal = new sqlProposal();
        int k = grvProposal.SelectedIndex;
        Double baseRate, baseKM, baseHr;
        Double packageKM = 0, packageHr = 0, PackageRate = 0;
        int Index = ((GridViewRow)((sender as Control)).NamingContainer).RowIndex;

        baseRate = Convert.ToDouble(grvProposal.DataKeys[Index].Values[2]);
        baseHr = Convert.ToDouble(grvProposal.DataKeys[Index].Values[3]);
        baseKM = Convert.ToDouble(grvProposal.DataKeys[Index].Values[4]);

        DropDownList ddlEditPackageHour = (DropDownList)grvProposal.Rows[Index].FindControl("ddlEditPackageHour");
        DropDownList ddlEditPackageKM = (DropDownList)grvProposal.Rows[Index].FindControl("ddlEditPackageKM");
        //DropDownList ddlEditPackageRate = (DropDownList)grvProposal.Rows[Index].FindControl("ddlEditPackageRate");
        TextBox ddlEditPackageRate = (TextBox)grvProposal.Rows[Index].FindControl("ddlEditPackageRate");
        Label lblPackageTypeId = (Label)grvProposal.Rows[Index].FindControl("lblPackageID");

        if (lblPackageTypeId.Text == PackageType.outstation)
        {
            packageKM = Convert.ToDouble(ddlEditPackageKM.SelectedValue);
            PackageRate = baseRate * Convert.ToDouble(ddlEditPackageKM.SelectedValue);
        }

        //BindDropDownList(ddlEditPackageKM, 1, 250, 1, Convert.ToString(packageKM));
        //BindDropDownList(ddlEditPackageRate, 1, 10000, 1, Convert.ToString(PackageRate));
        ddlEditPackageRate.Text = Convert.ToString(PackageRate);
    }
    protected void ddlPackageKM_SelectedIndexChanged(object sender, EventArgs e)
    {

        DataSet ds = new DataSet();
        objProposal = new sqlProposal();
        objProposalOL = new OL_Proposal();
        int k = grvProposal.SelectedIndex;
        Double baseRate, baseKM, baseHr;
        Double packageKM = 0, packageHr = 0, PackageRate = 0;
        int Index = ((GridViewRow)((sender as Control)).NamingContainer).RowIndex;

        DropDownList ddlPackageHour = (DropDownList)grvProposal.FooterRow.FindControl("ddlPackageHour");
        DropDownList ddlPackageKM = (DropDownList)grvProposal.FooterRow.FindControl("ddlPackageKM");
        //DropDownList ddlPackageRate = (DropDownList)grvProposal.FooterRow.FindControl("ddlPackageRate");
        TextBox ddlPackageRate = (TextBox)grvProposal.FooterRow.FindControl("ddlPackageRate");

        DropDownList ddlPackageType = (DropDownList)grvProposal.FooterRow.FindControl("ddlPackageType");
        DropDownList ddlVehicleCategory = (DropDownList)grvProposal.FooterRow.FindControl("ddlVehicleCategory");
        DropDownList ddlPackageCityName = (DropDownList)grvProposal.FooterRow.FindControl("ddlPackageCityName");

        objProposalOL.PkgTypeId = Convert.ToInt32(ddlPackageType.SelectedValue);
        objProposalOL.CarCatID = Convert.ToInt32(ddlVehicleCategory.SelectedValue);
        objProposalOL.CityID = Convert.ToInt32(ddlPackageCityName.SelectedValue);
        ds = objProposal.GetPackageBaseRate(objProposalOL);
        if (ds.Tables[0].Rows.Count > 0)
        {
            baseRate = Convert.ToDouble(ds.Tables[0].Rows[0]["BaseRate"]);
            baseHr = Convert.ToDouble(ds.Tables[0].Rows[0]["BaseHr"]);
            baseKM = Convert.ToDouble(ds.Tables[0].Rows[0]["BaseKM"]);
        }
        else
        {
            baseRate = 0;
            baseHr = 0;
            baseKM = 0;
        }

        if (ddlPackageType.SelectedValue == PackageType.outstation)
        {
            packageKM = Convert.ToDouble(ddlPackageKM.SelectedValue);
            PackageRate = baseRate * Convert.ToDouble(ddlPackageKM.SelectedValue);
        }

        //BindDropDownList(ddlPackageKM, 1, 250, 1, Convert.ToString(packageKM));
        //BindDropDownList(ddlPackageRate, 1, 10000, 1, Convert.ToString(PackageRate));
        ddlPackageRate.Text = Convert.ToString(PackageRate);
    }

    protected void ddlEditPackageCityName_SelectedIndexChanged(object sender, EventArgs e)
    {
        GridViewRow gvr = (GridViewRow)(((Control)sender).NamingContainer);

        // Get the reference of this DropDownlist
        DropDownList ddlEditPackageCityName = (DropDownList)gvr.FindControl("ddlEditPackageCityName");
        DropDownList ddlEditPackageType = (DropDownList)gvr.FindControl("ddlEditPackageType");


        ddlEditPackageType.Items.Clear();
        ListItem item;

        if (ddlEditPackageCityName.SelectedItem.Text == "City A" || ddlEditPackageCityName.SelectedItem.Text == "City B")
        {
            item = new ListItem("Fixed Airport Transfer", "3");
            ddlEditPackageType.Items.Add(item);
        }
        else if (ddlEditPackageCityName.SelectedItem.Text == "Tier 1" || ddlEditPackageCityName.SelectedItem.Text == "Tier 2" || ddlEditPackageCityName.SelectedItem.Text == "Tier 3")
        {
            item = new ListItem("-Select-", "0");
            ddlEditPackageType.Items.Add(item);
            item = new ListItem("Local", "1");
            ddlEditPackageType.Items.Add(item);
            item = new ListItem("Outstation", "2");
            ddlEditPackageType.Items.Add(item);
            ddlEditPackageType.SelectedValue = "0";
        }
        else
        {
            item = new ListItem("-Select-", "0");
            ddlEditPackageType.Items.Add(item);
            item = new ListItem("Local", "1");
            ddlEditPackageType.Items.Add(item);
            item = new ListItem("Outstation", "2");
            ddlEditPackageType.Items.Add(item);
            item = new ListItem("Fixed Airport Transfer", "3");
            ddlEditPackageType.Items.Add(item);
            ddlEditPackageType.SelectedValue = "0";
        }
    }

    protected void ddlPackageCityName_SelectedIndexChanged(object sender, EventArgs e)
    {
        GridViewRow gvr = (GridViewRow)(((Control)sender).NamingContainer);

        // Get the reference of this DropDownlist
        DropDownList ddlPackageCityName = (DropDownList)gvr.FindControl("ddlPackageCityName");
        DropDownList ddlPackageType = (DropDownList)gvr.FindControl("ddlPackageType");


        ddlPackageType.Items.Clear();
        ListItem item;

        if (ddlPackageCityName.SelectedItem.Text == "City A" || ddlPackageCityName.SelectedItem.Text == "City B")
        {
            item = new ListItem("Fixed Airport Transfer", "3");
            ddlPackageType.Items.Add(item);
        }
        else if (ddlPackageCityName.SelectedItem.Text == "Tier 1" || ddlPackageCityName.SelectedItem.Text == "Tier 2" || ddlPackageCityName.SelectedItem.Text == "Tier 3")
        {
            item = new ListItem("-Select-", "0");
            ddlPackageType.Items.Add(item);
            item = new ListItem("Local", "1");
            ddlPackageType.Items.Add(item);
            item = new ListItem("Outstation", "2");
            ddlPackageType.Items.Add(item);
            ddlPackageType.SelectedValue = "0";
        }
        else
        {
            item = new ListItem("-Select-", "0");
            ddlPackageType.Items.Add(item);
            item = new ListItem("Local", "1");
            ddlPackageType.Items.Add(item);
            item = new ListItem("Outstation", "2");
            ddlPackageType.Items.Add(item);
            item = new ListItem("Fixed Airport Transfer", "3");
            ddlPackageType.Items.Add(item);
            ddlPackageType.SelectedValue = "0";
        }
    }

    protected void ddlPackageType_SelectedIndexChanged(object sender, EventArgs e)
    {
        GridViewRow gvr = (GridViewRow)(((Control)sender).NamingContainer);

        // Get the reference of this DropDownlist
        DropDownList ddlPackageType = (DropDownList)gvr.FindControl("ddlPackageType");
        DropDownList ddlbillingbasis = (DropDownList)gvr.FindControl("ddlbillingbasis");


        if (ddlPackageType.SelectedValue == "2")
        {
            ddlbillingbasis.SelectedValue = "gg";
            ddlbillingbasis.Enabled = false;
        }
        else if (ddlProposalFor.SelectedValue == "3")
        {
            ddlbillingbasis.SelectedValue = "pp";
            ddlbillingbasis.Enabled = false;
        }
        else
        {
            ddlbillingbasis.Enabled = true;
        }
    }
}
