﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="RateCard.aspx.cs" Inherits="Proposal_RateCard" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div style="text-align: center">
            <div>
                <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
            </div>
            <asp:GridView ID="grvRateCard" runat="server" AutoGenerateColumns="False" CellPadding="4" OnRowDataBound="grvRateCard_RowDataBound" ForeColor="#333333" GridLines="None">
                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                <Columns>
                    <asp:TemplateField HeaderText="S.No">
                        <ItemTemplate>
                            <asp:Label ID="lblSNo" runat="server" Text='<%# Container.DataItemIndex + 1%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField HeaderText="Pacakage Type" DataField="PackageType" />
                    <asp:BoundField HeaderText="City Name" DataField="cityName" />
                    <asp:BoundField HeaderText="Vehicle Category" DataField="CarCatName" />
                    <asp:TemplateField HeaderText="Billing Basis">
                        <ItemTemplate>
                            <asp:Label ID="lblbillingbasis" runat="server" Text='<%#Eval("BillingBasis") %>' ForeColor="Red"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:BoundField HeaderText="FGR" DataField="FGR" />
                    <%--<asp:BoundField HeaderText="Purchase Rate" DataField="PKGRate" />--%>
                    <asp:TemplateField HeaderText="New Purchase Rate">
                        <ItemTemplate>
                            <asp:Label ID="lblpurchaserate" runat="server" Text='<%#Eval("PKGRate") %>' ForeColor="Red"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Sales Rate">
                        <ItemTemplate>
                            <asp:Label ID="lblsalesrate" runat="server" Text='<%#Eval("PKGRate") %>' ForeColor="Red"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField HeaderText="Deviation" DataField="diff" />
                    <%--<asp:BoundField HeaderText="Sales Rate" DataField="PKGRate" />--%>
                    <asp:BoundField HeaderText="Pacakage KM Rate" DataField="PkgKMs" />
                    <asp:BoundField HeaderText="Pacakage Hr Rate" DataField="PkgHrs" />
                    <asp:BoundField HeaderText="Extra KM" DataField="ExtraKMRate" />
                    <asp:BoundField HeaderText="Extra Hour" DataField="ExtraHrRate" />
                    <asp:BoundField HeaderText="Threshold Extra Hr" DataField="ThresholdExtraHr" />
                    <asp:BoundField HeaderText="Threshold Extra KM" DataField="ThresholdExtraKM" />
                    <asp:BoundField HeaderText="Night Charges" DataField="NightStayAllowance" />
                    <asp:BoundField HeaderText="Chauffeur Charges" DataField="OutStationAllowance" />
                    <asp:BoundField HeaderText="Remarks" DataField="Remarks" />
                    
                </Columns>
                <EditRowStyle BackColor="#999999" />
                <FooterStyle BackColor="#5D7B9D" ForeColor="White" Font-Bold="True" />
                <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                <SortedAscendingCellStyle BackColor="#E9E7E2" />
                <SortedAscendingHeaderStyle BackColor="#506C8C" />
                <SortedDescendingCellStyle BackColor="#FFFDF8" />
                <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
            </asp:GridView>
        </div>
    </form>
</body>
</html>
