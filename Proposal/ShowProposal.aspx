﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Salespage.master" AutoEventWireup="true"
    CodeFile="ShowProposal.aspx.cs" Inherits="ShowProposal" %>

<asp:Content ID="cntPlaceholder" ContentPlaceHolderID="ContentPlaceHolder_Page" runat="Server">
    <script type="text/javascript">
        var GB_ROOT_DIR = '<%= this.ResolveClientUrl("~/greybox/")%>';
    </script>
    <script type="text/javascript" src='<%= this.ResolveClientUrl("~/greybox/AJS.js") %>'></script>
    <script type="text/javascript" src='<%= this.ResolveClientUrl("~/greybox/AJS_fx.js") %>'></script>
    <script type="text/javascript" src='<%= this.ResolveClientUrl("~/greybox/gb_scripts.js") %>'></script>
    <script src="../JQuery/jquery-1.4.1.min.js" type="text/javascript"></script>
    <script src="../JQuery/ui.datepicker.js" type="text/javascript"></script>
    <script src="../JQuery/jquery.reveal.js"></script>
    <link href="../CSSReveal/reveal.css" rel="stylesheet" />
    <script type="text/javascript">
        function pageLoad(sender, args) {
            $("#<%=grvApproval.ClientID%> a[id*='lblViewRateCard']").click(function () {
                var raiseId = $(this).attr("id");
                var id = $(this).attr("bksharma");
                var caption = "Proposal Details"
                var url = "../Proposal/RateCard.aspx?Id=" + id
                //return GB_showCenter(caption, url, 500, 700)
                GB_showFullScreen(caption, url)
            });
            $("#<%=grvApproval.ClientID%> a[id*='lblStandardMailView']").click(function () {
                var caption = "Standard Mailer";
                var url = "../DOC/StandardMailer.pdf"
                return GB_showFullScreen(caption, url)
            });
            $("#<%=grvApproval.ClientID%> a[id*='lblCorporatPresentation']").click(function () {
                var caption = "Corporate Presentation"
                var url = "../DOC/corporatepresentation.pdf"
                return GB_showCenter(caption, url, 500, 700)
            });
            $("#<%=grvApproval.ClientID%> a[id*='lblCommitmenttoService']").click(function () {
                var caption = "commitment to service";
                var url = "../DOC/CommitmenttoService.pdf"
                return GB_showCenter(caption, url, 500, 700)
            });
            $("#<%=grvApproval.ClientID%> a[id*='lblCommercial']").click(function () {
                var id = $(this).attr("bksharma");
                //alert(id);
                var caption = "Commercial";
                var url = "../ProposalDOC/" + id;
                return GB_showCenter(caption, url, 500, 700)
            });
        }
    </script>
    <center>
        <asp:UpdatePanel ID="updApproval" runat="server">
            <ContentTemplate>
                <table cellpadding="5" cellspacing="0" border="0">

                    <caption><br />
                        <b>View Proposal</b></caption>
                    <tr>
                        <td colspan="7" align="center">
                            <b>Client Name</b>&nbsp;&nbsp;
                            <asp:DropDownList ID="ddlClient" runat="server" Width="300px"></asp:DropDownList>
                            &nbsp;&nbsp;
                            <asp:Button ID="btnGet" runat="server" Text="Get It" OnClick="btnGet_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="7" align="center">
                            <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="7" style="text-align: center">
                            <asp:UpdateProgress ID="updProgress" runat="server" AssociatedUpdatePanelID="updApproval"
                                DynamicLayout="true">
                                <ProgressTemplate>
                                    <img src="../Images/loader.gif" alt="Loading..." width="40px" height="40px" />
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="7" style="text-align: center">
                            <asp:GridView ID="grvApproval" runat="server" AutoGenerateColumns="false">
                                <Columns>
                                    <asp:TemplateField HeaderText="S.No">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSNo" runat="server" Text='<%# Container.DataItemIndex + 1%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField HeaderText="Source City Name" DataField="CityName" />
                                    <asp:BoundField HeaderText="Corporate Name" DataField="ClientCoName" />
                                    <asp:BoundField HeaderText="Type of Proposal" DataField="ServiceName" />
                                    <asp:BoundField HeaderText="Date of Initiation" DataField="DateInitition" />
                                    <asp:BoundField HeaderText="Action Manager Name" DataField="ActionManagerName" />
                                    <%--<asp:TemplateField HeaderText="New Purchase Rate">
                                        <ItemTemplate>
                                            <asp:Label ID="lblpurchaserate" runat="server" Text='<%#Eval("OriginalPKGRate") %>' ForeColor="Red"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Sales Rate">
                                        <ItemTemplate>
                                            <asp:Label ID="lblsalesrate" runat="server" Text='<%#Eval("PKGRate") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Deviation">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDeviation" runat="server" Text='<%#Eval("diff") %>' ForeColor="Red"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>--%>
                                    <asp:TemplateField HeaderText="View Proposal">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lblViewRateCard" runat="server" Text="View" bksharma='<%#Eval("ProspectID")%>'></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Standard Mailer">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lblStandardMailView" runat="server" Text="View" bksharma='<%#Eval("ProposalId")%>'></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Corporate Presentation">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lblCorporatPresentation" runat="server" Text="View" bksharma='<%#Eval("ProposalId")%>'></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Commitment to Service">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lblCommitmenttoService" runat="server" Text="View" bksharma='<%#Eval("ProposalId")%>'></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Commercial">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lblCommercial" runat="server" Text="View" bksharma='<%#Eval("FileName")%>'></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <%--<asp:TemplateField HeaderText="Approval Status">
                                        <ItemTemplate>
                                            <asp:DropDownList ID="ddlApprovalStatus" runat="server">
                                                <asp:ListItem Text="Approve" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="Reject" Value="0"></asp:ListItem>
                                            </asp:DropDownList>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Remarks">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtReasonRecheck" runat="server" Text='<%#Eval("ReasonforRecheck")%>'></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Submit">
                                        <ItemTemplate>
                                            <asp:Button ID="bntSave" runat="server" Text="Submit" CommandName="Approval" CommandArgument='<%#((GridViewRow)Container).RowIndex %>'
                                                ValidationGroup="appValidate" />
                                        </ItemTemplate>
                                    </asp:TemplateField>--%>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </center>
</asp:Content>
