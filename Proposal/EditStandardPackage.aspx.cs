﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;

public partial class EditStandardPackage : System.Web.UI.Page
{
    private sqlProposal objProposal = null;
    private OL_PKG objPkg = null;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            BindCity(ddlCityName);
            BindCarCategory(ddlCarCat);
            BindClientCommitment(ddlcommitment);
        }
    }

    private void BindCity(DropDownList ddlCity)
    {
        objProposal = new sqlProposal();
        DataSet ds = new DataSet();
        ds = objProposal.GetCityAccessBasis(Convert.ToInt32(Session["UserID"]));
        if (ds.Tables[0].Rows.Count > 0)
        {
            ddlCity.DataSource = ds.Tables[0];
            ddlCity.DataTextField = "CityName";
            ddlCity.DataValueField = "CityID";
            ddlCity.DataBind();
            ddlCity.Items.Insert(0, new ListItem("-Select-", "0"));
            ddlCity.Items.Insert(1, new ListItem("Tier 1", "900"));
            ddlCity.Items.Insert(2, new ListItem("Tier 2", "901"));
            ddlCity.Items.Insert(3, new ListItem("Tier 3", "902"));
            ddlCity.Items.Insert(4, new ListItem("City A", "903"));
            ddlCity.Items.Insert(5, new ListItem("City B", "904"));
        }
        else
        {
            ddlCity.DataSource = null;
            ddlCity.DataBind();
        }

    }
    private void BindCarCategory(DropDownList ddlCat)
    {
        objProposal = new sqlProposal();
        DataSet ds = new DataSet();
        ds = objProposal.GetCategory();
        if (ds.Tables[0].Rows.Count > 0)
        {
            ddlCat.DataSource = ds.Tables[0];
            ddlCat.DataTextField = "CarCatName";
            ddlCat.DataValueField = "carcatid";
            ddlCat.DataBind();
            ddlCat.Items.Insert(0, new ListItem("-Select-", "0"));

        }
        else
        {
            ddlCat.DataSource = null;
            ddlCat.DataBind();
        }

    }
    private void BindClientCommitment(DropDownList ddlcommitment)
    {
        objProposal = new sqlProposal();
        DataSet ds = new DataSet();
        ds = objProposal.GetClientCommitment();
        if (ds.Tables[0].Rows.Count > 0)
        {
            ddlcommitment.DataSource = ds.Tables[0];
            ddlcommitment.DataTextField = "DisplayName";
            ddlcommitment.DataValueField = "id";
            ddlcommitment.DataBind();
            ddlcommitment.Items.Insert(0, new ListItem("Any", "0"));

        }
        else
        {
            ddlcommitment.DataSource = null;
            ddlcommitment.DataBind();
        }
    }
    protected void btnGet_Click(object sender, EventArgs e)
    {
        DisplayPackageList();
    }
    private void DisplayPackageList()
    {
        objProposal = new sqlProposal();
        OL_PKG objPkg = new OL_PKG();
        DataSet ds = new DataSet();
        MultiEditView.ActiveViewIndex = 0;
        objPkg.CityID = Convert.ToInt32(ddlCityName.SelectedValue);
        objPkg.ProspectCategory = ddlProspectCategories.SelectedValue;
        objPkg.CarCatID = Convert.ToInt32(ddlCarCat.SelectedValue);
        objPkg.ProposalFor = Convert.ToInt32(ddlProposalFor.SelectedValue);
        objPkg.CommitmentId = Convert.ToInt32(ddlcommitment.SelectedValue);

        ds = objProposal.GetStandardPackageForEdit(objPkg);
        if (ds.Tables[0].Rows.Count > 0)
        {
            grvEditPackage.DataSource = ds.Tables[0];
            grvEditPackage.DataBind();
        }
        else
        {
            grvEditPackage.DataSource = null;
            grvEditPackage.DataBind();
        }
    }

    private void DisplayEditForm(int editIndex)
    {
        objProposal = new sqlProposal();
        OL_PKG objPkg = new OL_PKG();
        ViewState["PkgId"] = 0;
        objPkg.SDPRateId = Convert.ToInt32(grvEditPackage.DataKeys[editIndex].Value);
        if (objPkg.SDPRateId > 0)
        {
            ViewState["PkgId"] = objPkg.SDPRateId;
        }
        MultiEditView.ActiveViewIndex = 1;
        objPkg = objProposal.GetStandardPackageDetails(objPkg);
        BindCity(ddlEditCityName);
        ddlEditCityName.SelectedValue = Convert.ToString(objPkg.CityID);
        ddlEditProspectCategories.SelectedValue = objPkg.ProspectCategory;
        BindCarCategory(ddlEditCarCategory);
        ddlEditCarCategory.SelectedValue = Convert.ToString(objPkg.CarCatID);

        ddlbillingbasis.SelectedValue = Convert.ToString(objPkg.BillingBasis);

        BindClientCommitment(ddlcommitment);
        ddlcommitment.SelectedValue = Convert.ToString(objPkg.CommitmentId);

        ddlPkgHours.SelectedValue = Convert.ToString(objPkg.PkgHrs);
        ddlPkgKM.SelectedValue = Convert.ToString(objPkg.PkgKMs);

        ddlEditProposalFor.SelectedValue = Convert.ToString(objPkg.ProposalFor);
        txtBaseRate.Text = Convert.ToString(objPkg.BaseRate);
        txtExtraHoursRate.Text = Convert.ToString(objPkg.ExtraHrRate);
        txtExtraKMRate.Text = Convert.ToString(objPkg.ExtraKMRate);
        BindExtraHours();
        ddlThreshouldExHr.SelectedValue = Convert.ToString(objPkg.ThresholdExtraHr);
        BindExtraKM();
        ddlThreshouldExKM.SelectedValue = Convert.ToString(objPkg.ThresholdExtraKM);
        txtOutstationAllowance.Text = Convert.ToString(objPkg.OutStationAllowance);
        txtNightStayAllowance.Text = Convert.ToString(objPkg.NightStayAllowance);
        txtPurchaseRate.Text = Convert.ToString(objPkg.VendorPKGRate);


    }
    protected void grvEditPackage_SelectedIndexChanged(object sender, EventArgs e)
    {
        DisplayEditForm(grvEditPackage.SelectedIndex);
    }
    private void BindExtraHours()
    {
        Dictionary<int, int> ddlSource = new Dictionary<int, int>();
        for (int i = 0; i <= 5; i++)
        {
            ddlSource.Add(i, i);
        }
        ddlThreshouldExHr.Items.Clear();
        ddlThreshouldExHr.DataSource = ddlSource;
        ddlThreshouldExHr.DataTextField = "key";
        ddlThreshouldExHr.DataValueField = "value";
        ddlThreshouldExHr.DataBind();
    }
    private void BindExtraKM()
    {
        Dictionary<int, int> ddlSource = new Dictionary<int, int>();
        for (int i = 0; i <= 10; i++)
        {
            ddlSource.Add(i, i);
        }
        ddlThreshouldExKM.Items.Clear();
        ddlThreshouldExKM.DataSource = ddlSource;
        ddlThreshouldExKM.DataTextField = "key";
        ddlThreshouldExKM.DataValueField = "value";
        ddlThreshouldExKM.DataBind();
    }
    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        objProposal = new sqlProposal();
        OL_PKG objPkg = new OL_PKG();
        int status = 0;
        lblMessage.Text = "";
        try
        {
            objPkg.SDPRateId = Convert.ToInt32(ViewState["PkgId"]);
            if (ddlEditCityName.SelectedValue == "0")
            {
                lblMessage.Text = "City should not be blank";
                return;
            }
            else if (ddlEditProspectCategories.SelectedValue == "0")
            {
                lblMessage.Text = "Prospect category should not be blank";
                return;
            }
            else if (ddlEditCarCategory.SelectedValue == "0")
            {
                lblMessage.Text = "Category should not be blank";
                return;
            }
            else if (ddlEditProposalFor.SelectedValue == "0")
            {
                lblMessage.Text = "Proposal should not be blank";
                return;
            }
            else if (string.IsNullOrEmpty(txtBaseRate.Text))
            {
                lblMessage.Text = "Sales Rate should not be blank";
                return;
            }
            else if (Convert.ToDouble(txtBaseRate.Text) <= 0)
            {
                lblMessage.Text = "Sales Rate should be greater than zero.";
                return;
            }
            else
            {
                objPkg.CityID = Convert.ToInt32(ddlCityName.SelectedValue);
                objPkg.CarCatID = Convert.ToInt32(ddlCarCat.SelectedValue);
                objPkg.ProposalFor = Convert.ToInt32(ddlProposalFor.SelectedValue);
                objPkg.BaseRate = Convert.ToDouble(txtBaseRate.Text);
                objPkg.PkgHrs = Convert.ToInt32(ddlPkgHours.SelectedValue);
                objPkg.PkgKMs = Convert.ToInt32(ddlPkgKM.SelectedValue);
                objPkg.ExtraHrRate = Convert.ToDouble(txtExtraHoursRate.Text);
                objPkg.ExtraKMRate = Convert.ToDouble(txtExtraKMRate.Text);
                objPkg.ThresholdExtraHr = Convert.ToInt32(ddlThreshouldExHr.SelectedValue);
                objPkg.ThresholdExtraKM = Convert.ToInt32(ddlThreshouldExKM.SelectedValue);
                objPkg.OutStationAllowance = Convert.ToDouble(txtOutstationAllowance.Text);
                objPkg.NightStayAllowance = Convert.ToDouble(txtNightStayAllowance.Text);
                objPkg.ModifiedBy = Convert.ToInt32(Session["UserID"]);
                objPkg.ProspectCategory = ddlProspectCategories.SelectedValue;
                objPkg.CommitmentId = Convert.ToInt32(ddlcommitment.SelectedValue);
                objPkg.VendorPKGRate = Convert.ToDouble(txtPurchaseRate.Text);
                objPkg.BillingBasis = Convert.ToString(ddlbillingbasis.SelectedValue);
                status = objProposal.UpdateStandardPackage(objPkg);
                if (status > 0)
                {
                    lblMessage.Text = "Successfully updated package.";
                    lblMessage.ForeColor = System.Drawing.Color.Red;
                    DisplayPackageList();
                }
                else
                {
                    lblMessage.Text = "Getting error while updating package.";
                    lblMessage.ForeColor = System.Drawing.Color.Red;
                }
            }
        }
        catch (Exception Ex)
        {
            lblMessage.Text = Ex.Message;
            lblMessage.ForeColor = System.Drawing.Color.Red;
        }
    }
    protected void txtPurchaseRate_TextChanged(object sender, EventArgs e)
    {
        if (txtPurchaseRate.Text != "")
        {
            txtBaseRate.Text = (Convert.ToDouble(txtPurchaseRate.Text) + ((Convert.ToDouble(txtPurchaseRate.Text) *
                Convert.ToDouble(ConfigurationManager.AppSettings["PercentIncreaseInPurchase"])) / 100)).ToString();
        }
    }
    protected void ddlEditCityName_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlProposalFor.Items.Clear();
        ListItem item;

        if (ddlEditCityName.SelectedItem.Text == "City A" || ddlEditCityName.SelectedItem.Text == "City B")
        {
            item = new ListItem("Fixed Airport Transfer", "3");
            ddlProposalFor.Items.Add(item);
        }
        else if (ddlEditCityName.SelectedItem.Text == "Tier 1" || ddlEditCityName.SelectedItem.Text == "Tier 2" || ddlEditCityName.SelectedItem.Text == "Tier 3")
        {
            item = new ListItem("-Select-", "0");
            ddlProposalFor.Items.Add(item);
            item = new ListItem("Local", "1");
            ddlProposalFor.Items.Add(item);
            item = new ListItem("Outstation", "2");
            ddlProposalFor.Items.Add(item);
            ddlProposalFor.SelectedValue = "0";
        }
        else
        {
            item = new ListItem("-Select-", "0");
            ddlProposalFor.Items.Add(item);
            item = new ListItem("Local", "1");
            ddlProposalFor.Items.Add(item);
            item = new ListItem("Outstation", "2");
            ddlProposalFor.Items.Add(item);
            item = new ListItem("Fixed Airport Transfer", "3");
            ddlProposalFor.Items.Add(item);
            ddlProposalFor.SelectedValue = "0";
        }
        ddlProposalFor_SelectedIndexChanged(sender, e);
    }

    protected void ddlProposalFor_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlProposalFor.SelectedValue == "2")
        {
            ddlbillingbasis.SelectedValue = "gg";
            ddlbillingbasis.Enabled = false;
        }
        else if (ddlProposalFor.SelectedValue == "3")
        {
            ddlbillingbasis.SelectedValue = "pp";
            ddlbillingbasis.Enabled = false;
        }
        else
        {
            ddlbillingbasis.Enabled = true;
        }
    }
}