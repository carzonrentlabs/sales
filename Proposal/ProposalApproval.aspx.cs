﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Insta;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Net.Mail;
using System.Net;
using System.Configuration;

public partial class Proposal_ProposalApproval : System.Web.UI.Page
{
    private sqlProposal objProposal = null;
    private CorDrive objCordrive = null;
    private OL_Proposal objProposalOL = null;
    protected void Page_Load(object sender, EventArgs e)
    {
        lblMessage.Visible = false;
        if (!Page.IsPostBack)
        {
            BindClient();
            //BindCity();
            //BindServiceType();
            //BindApprovalDetails();
            
        }
    }

    private void BindClient()
    {
        objProposal = new sqlProposal();
        DataSet dsProspect = new DataSet();
        try
        {
            dsProspect = objProposal.GetProspectMaster();
            if (dsProspect.Tables[0].Rows.Count > 0)
            {
                ddlClient.DataSource = dsProspect.Tables[0];
                ddlClient.DataTextField = "ClientCoName";
                ddlClient.DataValueField = "ID";
                ddlClient.DataBind();
                ddlClient.Items.Insert(0, new ListItem("All", "0"));
            }
            else
            {
                ddlClient.DataSource = null;
                ddlClient.DataBind();
                lblMessage.Visible = true;
                lblMessage.Text = "Record not available";
                lblMessage.ForeColor = Color.Red;
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void BindCity()
    {
        /*
        try
        {
           
            objCordrive = new CorDrive();
            DataSet dsCustName = new DataSet();
            dsCustName = objCordrive.GetCityName();
            if (dsCustName.Tables[0].Rows.Count > 0)
            {
                ddlCityName.DataTextField = "cityname";
                ddlCityName.DataValueField = "cityid";
                ddlCityName.DataSource = dsCustName;
                ddlCityName.DataBind();
                ddlCityName.Items.Insert(0, new ListItem("--Select--", "0"));
            }
             
        }
        catch (Exception Ex)
        {
            lblMessage.Visible = true;
            lblMessage.ForeColor = Color.Red;
            lblMessage.Text = Ex.Message;
        }
         */
    }
    private void BindServiceType()
    {
        /*
        objProposal = new sqlProposal();
        DataSet dsService = new DataSet();
        try
        {
           
            dsService = objProposal.GetTypeofProposal();
            if (dsService.Tables[0].Rows.Count > 0)
            {
                ddlTypeProposal.DataSource = dsService.Tables[0];
                ddlTypeProposal.DataTextField = "ServiceName";
                ddlTypeProposal.DataValueField = "serviceTypeId";
                ddlTypeProposal.DataBind();
                ddlTypeProposal.Items.Insert(0, new ListItem("--Select--", "0"));
            }
            else
            {
                ddlTypeProposal.DataSource = null;
                ddlTypeProposal.DataBind();
                lblMessage.Visible = true;
                lblMessage.Text = "Record not available";
                lblMessage.ForeColor = Color.Red;
            }
            
        }
        catch (Exception ex)
        {

            throw new Exception(ex.Message);
        }
          */
    }
    private void BindApprovalDetails()
    {
        objProposalOL = new OL_Proposal();

        objProposalOL.UserID = Convert.ToInt32(Session["UserID"]);
        objProposalOL.ApprovalStats = Convert.ToInt32(ddlApprovalstatus.SelectedValue);
        objProposalOL.ProspectID = Convert.ToInt32(ddlClient.SelectedValue);

        objProposal = new sqlProposal();
        DataSet ds = new DataSet();
        lblMessage.Visible = false;
        try
        {
            ds = objProposal.GetApprovalDetails(objProposalOL);
            if (ds.Tables[0].Rows.Count > 0)
            {
                lblnewmsg.Visible = true;
                grvApproval.DataSource = ds.Tables[0];
                grvApproval.DataBind();
            }
            else
            {
                grvApproval.DataSource = null;
                grvApproval.DataBind();
                lblMessage.Visible = true;
                lblMessage.Text = "Record not available";
                lblMessage.ForeColor = Color.Red;
            }
        }
        catch (Exception Ex)
        {

            lblMessage.Visible = true;
            lblMessage.Text = Ex.Message;
            lblMessage.ForeColor = Color.Red;
        }
    }

    protected void btnGet_Click(object sender, EventArgs e)
    {
        BindApprovalDetails();
    }

    protected void grvApproval_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        objProposal = new sqlProposal();
        objProposalOL = new OL_Proposal();
        int status = 0;
        if (e.CommandName == "Approval")
        {
            int index = Convert.ToInt16(e.CommandArgument);
            GridViewRow row = grvApproval.Rows[index];
            //DropDownList ddlProposalStatus = (DropDownList)grvApproval.Rows[index].FindControl("ddlProposalStatus");
            DropDownList ddlApprovalStatus = (DropDownList)grvApproval.Rows[index].FindControl("ddlApprovalStatus");
            TextBox txtReasonRecheck = (TextBox)grvApproval.Rows[index].FindControl("txtReasonRecheck");
            HiddenField hdnProposalDetailId = (HiddenField)grvApproval.Rows[index].FindControl("hdnProposalDetailId");



            objProposalOL.ProposalID = (int)grvApproval.DataKeys[index].Values[0];
            objProposalOL.ApprovalStatus = ddlApprovalStatus.SelectedValue.ToString();
            objProposalOL.ProposalDetailId = Convert.ToInt32(hdnProposalDetailId.Value);

            if (string.IsNullOrEmpty(txtReasonRecheck.Text.Trim()))
            {
                lblMessage.Visible = true;
                lblMessage.Text = "Please enter remarks.";
                lblMessage.ForeColor = Color.Red;
                txtReasonRecheck.Focus();
                return;
            }

            //if (objProposalOL.ApprovalStatus == "Approve")
            //{
            //    objProposalOL.ProposalStatus = "Closed"; //ddlProposalStatus.SelectedValue.ToString();
            //}
            //else
            //{
            //    objProposalOL.ProposalStatus = "Open"; //ddlProposalStatus.SelectedValue.ToString();
            //}

            objProposalOL.ApprovalRemark = txtReasonRecheck.Text.ToString();
            objProposalOL.SysUserId = Convert.ToInt32(Session["UserID"]);
            if (objProposalOL.ProposalID > 0)
            {
                status = objProposal.ApprovedProposal(objProposalOL);
                if (status > 0)
                {
                    //SendMail(objProposalOL);
                    lblMessage.Visible = true;
                    lblMessage.Text = "Save successfully.";
                    lblMessage.ForeColor = Color.Red;
                    grvApproval.DataSource = null;
                    grvApproval.DataBind();
                }
                else
                {
                    lblMessage.Visible = true;
                    lblMessage.Text = "Getting save error.";
                    lblMessage.ForeColor = Color.Red;
                }
            }
        }
    }
    protected void grvApproval_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            try
            {
                //if (Convert.ToString(grvApproval.DataKeys[e.Row.RowIndex].Values[2]) == "Recheck" && !string.IsNullOrEmpty(Convert.ToString(grvApproval.DataKeys[e.Row.RowIndex].Values[3])))
                //{
                //    // grvApproval.Rows[e.Row.RowIndex].BackColor = Color.Red;
                //    e.Row.Attributes["style"] = "background-color: #28b779";
                //}
                //else if (Convert.ToInt16(grvApproval.DataKeys[e.Row.RowIndex].Values[4]) >= 2 && string.IsNullOrEmpty(Convert.ToString(grvApproval.DataKeys[e.Row.RowIndex].Values[3])))
                //{
                //    e.Row.Attributes["style"] = "background-color: #da5554";
                //}
                //DropDownList ddlProposalStatus = (DropDownList)e.Row.FindControl("ddlProposalStatus");
                DropDownList ddlApprovalStatus = (DropDownList)e.Row.FindControl("ddlApprovalStatus");

                Label lblpurchaserate = (Label)e.Row.FindControl("lblpurchaserate");
                Label lblsalesrate = (Label)e.Row.FindControl("lblsalesrate");
                
                Button bntSave = (Button)e.Row.FindControl("bntSave");

                lblpurchaserate.Text = (Math.Round((Convert.ToDouble(lblsalesrate.Text) * 100)
                    / (100 + Convert.ToDouble(ConfigurationManager.AppSettings["PercentIncreaseInPurchase"])), 2)).ToString();


                if (Convert.ToInt32(grvApproval.DataKeys[e.Row.RowIndex].Values[2]) == 0)
                {
                    //txtBaseRate.Text = (Convert.ToDouble(txtPurchaseRate.Text) + ((Convert.ToDouble(txtPurchaseRate.Text) *
                    //Convert.ToDouble(ConfigurationManager.AppSettings["PercentIncreaseInPurchase"])) / 100)).ToString();

                    ddlApprovalStatus.SelectedValue = "1";
                    bntSave.Enabled = true;
                    bntSave.Visible = true;
                    ddlApprovalStatus.Enabled = true;
                }
                else
                {
                    ddlApprovalStatus.SelectedValue = Convert.ToInt32(grvApproval.DataKeys[e.Row.RowIndex].Values[2]).ToString();
                    bntSave.Enabled = false;
                    bntSave.Visible = false;
                    ddlApprovalStatus.Enabled = false;
                }
                //ddlProposalStatus.SelectedValue = Convert.ToString(grvApproval.DataKeys[e.Row.RowIndex].Values[5]);

            }
            catch (Exception ex)
            {
                lblMessage.Visible = true;
                lblMessage.Text = ex.Message;
                lblMessage.ForeColor = Color.Red;
            }
        }
    }
    private void SendMail(OL_Proposal objProposalOL)
    {

        objProposal = new sqlProposal();
        DataSet ds = new DataSet();
        StringBuilder strBody = new StringBuilder();
        StringBuilder mailbody = new StringBuilder();
        ds = objProposal.GetApprovalRejectionMailDetail(objProposalOL);
        if (ds.Tables[0].Rows.Count > 0)
        {

            if (objProposalOL.ApprovalStatus == "Recheck")
            {
                mailbody.Append("<html xmlns='http://www.w3.org/1999/xhtml'><body><table style='font-family:Arial; font-size:12px;' cellpadding='5' cellspacing='0'><tr><td>");
                mailbody.Append("Dear " + ds.Tables[0].Rows[0]["ActionManagerName1"].ToString() + ",<br /><br/></td></tr>");
                mailbody.Append("<tr><td>Your proposal for the client <b>" + ds.Tables[0].Rows[0]["ClientCoName"].ToString() + "</b> has been rejected due to " + ds.Tables[0].Rows[0]["ReasonforRecheck"].ToString() + "  </td></tr>");
                mailbody.Append("<tr><td>Kindly rechek and submit. </td></tr>");
                mailbody.Append("</table>");

                mailbody.Append("<br/><br/><table style='font-family:Arial; font-size:12px;'>");
                mailbody.Append("<tr><td>Regards,<br/><br/>Team Carzonrent</td></tr>");
                mailbody.Append("<tr><td><br/><br/>This is system generated mail, Please do not reply.</td></tr></table></body></html>");
            }
            else if (objProposalOL.ApprovalStatus == "Approve")
            {
                mailbody.Append("<html xmlns='http://www.w3.org/1999/xhtml'><body><table style='font-family:Arial; font-size:12px;' cellpadding='5' cellspacing='0'><tr><td>");
                mailbody.Append("Dear Sir/Madam,</td></tr>");
                mailbody.Append("<tr><td>Greetings from Carzonrent India.!! </td></tr>");
                mailbody.Append("<tr><td>It’s a pleasure to introduce “Carzonrent India Pvt. Ltd.” to your company. This note is to further</td></tr>");
                mailbody.Append("<tr><td>explore a mutually beneficial business opportunity of your “Transportation & Car Rental” requirement.</td></tr>");
                mailbody.Append("<tr><td>&nbsp;</td></tr>");
                mailbody.Append("<tr><td>We at Carzonrent India Private Limited would like to take the privilege of sharing our profile as the</td></tr>");
                mailbody.Append("<tr><td>market leader in the organized car rental industry in India. Established in 2000, Carzonrent is amongst</td></tr>");
                mailbody.Append("<tr><td>the largest personal ground transportation company in India.</td></tr>");
                mailbody.Append("<tr><td>Carzonrent now operates a fleet of almost 6500 cars in Car Rental, Limousine and Radio Cabs (EasyCabs)</td></tr>");
                mailbody.Append("<tr><td>businesses with annual revenue run rate of over INR 300 Crores and has built significant strengths in</td></tr>");
                mailbody.Append("<tr><td>terms of:</td></tr>");
                mailbody.Append("<tr><td><b>1. Largest national network.</b></td></tr>");
                mailbody.Append("<tr><td><b>2. Capacity to service biggest global clients.</b></td></tr>");
                mailbody.Append("<tr><td><b>3. Huge corporate and retail customer base.</b></td></tr>");
                mailbody.Append("<tr><td><b>4. Robust technology platform.</b></td></tr>");
                mailbody.Append("<tr><td><b>5. Service delivery capabilities to offer a taxi for an airport transfer to a lease for the</b></td></tr>");
                mailbody.Append("<tr><td><b>entire life cycle of the car thus encompassing the entire range of services in the Personal</b></td></tr>");
                mailbody.Append("<tr><td><b>Ground Transportation space.<br/></b></td></tr>");
                mailbody.Append("<tr><td>We appreciate and thank you for your valuable time & it will be our privilege to jointly explore our value</td></tr>");
                mailbody.Append("<tr><td>proposition to have you as our valued corporate customer.</td></tr>");
                mailbody.Append("<tr><td>For more details, please refer to our website www.carzonrent.com or please feel free to call me on +919899332576.</td></tr>");
                mailbody.Append("<tr><td>To assist you with a better understanding about our Organization and Services, given below are few bullets:</td></tr>");
                mailbody.Append("<tr><td>&nbsp;<br/></td></tr>");
                mailbody.Append("<tr><td>Carzonrent Group had been in car rental business since 2000.<br/></td></tr>");
                mailbody.Append("<tr><td>Carzonrent is ranked as the India no 1, Car Rental Company in the country and holds 20%+</td></tr>");
                mailbody.Append("<tr><td>market share of the organized transportation sector.<br/></td></tr>");
                mailbody.Append("<tr><td>India’s largest fleet operator with 6500+ vehicles and executing over 18000+ trips per day.<br/></td></tr>");
                mailbody.Append("<tr><td>Operating with national presence in 19cities including all major metros like</td></tr>");
                mailbody.Append("<tr><td>Mumbai, Delhi, Hyderabad, Bangalore, Chennai and Pune; and airports.<br/></td></tr>");
                mailbody.Append("<tr><td>Fleet availability in different car categories like compact, intermediate, standard,</td></tr>");
                mailbody.Append("<tr><td>van, premium and luxury segment.<br/></td></tr>");
                mailbody.Append("<tr><td>Currently serving over 600+ clients from diverse industries/ organizations like</td></tr>");
                mailbody.Append("<tr><td>MNC’s, aviation, hotel, expatriates, embassies, PSU’s, Govt. organizations etc.<br/></td></tr>");
                mailbody.Append("<tr><td>Operates on monthly rental, car rental, spot rental and packages modal based on</td></tr>");
                mailbody.Append("<tr><td>end user’s requirements.<br/></td></tr>");
                mailbody.Append("<tr><td>Out fleet travels intercity & intra-city, with all India tourist taxi permit.<br/></td></tr>");
                mailbody.Append("<tr><td>State-of-the-art technology platform.<br/></td></tr>");
                mailbody.Append("<tr><td>Highly process oriented operations with global practices.<br/></td></tr>");
                mailbody.Append("<tr><td>Cab can be booked through phone, email or by logging on to our website</td></tr>");
                mailbody.Append("<tr><td><http://www.carzonrent.com/> www.carzonrent.com.<br/></td></tr>");
                mailbody.Append("<tr><td>Centralized Call center number 0888 222 2222.<br/></td></tr>");
                mailbody.Append("<tr><td>Well maintained vehicles fitted the latest GPS and GPRS Tracking device.<br/></td></tr>");
                mailbody.Append("<tr><td>Transparent billing mechanism with multiple check points for smooth execution of</td></tr>");
                mailbody.Append("<tr><td>every trip.<br/></td></tr>");
                mailbody.Append("<tr><td>Scrupulously trained chauffeurs after a meticulous background check.<br/></td></tr>");
                mailbody.Append("<tr><td24X7 Services Support: Ensures highly responsive customer service through dedicated & skilled managers.<br/></td></tr>");
                mailbody.Append("<tr><td>&nbsp;</td></tr>");
                mailbody.Append("<tr><td>Major USP’s of associating with Carzonrent are our Network, Fleet Strength, Trained</td></tr>");
                mailbody.Append("<tr><td>Manpower, Airport Presence, Technology, Hotel/ Airlines Relationships, 24/7 Availability and</td></tr>");
                mailbody.Append("<tr><td>Key Account Management.<br/></td></tr>");
                mailbody.Append("<tr><td>All Carzonrent cabs come with the following features:<br/></td></tr>");
                mailbody.Append("<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>.</b> Modern, brand-new, air-conditioned luxury cabs <br/></td></tr>");
                mailbody.Append("<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>.</b> Flexibility of payment modal- Credit Card, Cash, Advance and BTC<br/></td></tr>");
                mailbody.Append("<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>.</b> Customizable fleet management process and procedures as per customer requirements <br/></td></tr>");
                mailbody.Append("<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>.</b> Round the clock availability of cabs backed by multiple hubs across the city - based</td></tr>");
                mailbody.Append("<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; dispatching system and a 24x7 Customer Service Center. This means a quicker pickup</td></tr>");
                mailbody.Append("<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; and faster response time to your call for a cab<br/></td></tr>");
                mailbody.Append("<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>.</b> Assured Service for corporate clients <br/></td></tr>");
                mailbody.Append("<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>.</b> Alternate modes of booking of cabs like mail and online booking apart from call center</br></td></tr>");
                mailbody.Append("<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>.</b> Thoroughly trained Chauffeurs in a smart Carzonrent uniform<br/></td></tr>");
                mailbody.Append("<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>.</b> Ability to track the location of the cab ‘real-time’ through the GPS technology -</td></tr>");
                mailbody.Append("<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; provides security in case of an emergency<br/></td></tr>");
                mailbody.Append("<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>.</b>Routine checks by our operation teams to ensure highest standards of service for both our fleet and chauffer’s</br></td></tr>");
                mailbody.Append("<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>.</b>Exclusive pick-ups at all airports country wide.</br><br/></td></tr>");
                mailbody.Append("<tr><td>&nbsp;</td></tr>");
                mailbody.Append("<tr><td>We would look forward to work with your esteem organization. Assuring you the best of service at all times.<br/></td></tr>");
                mailbody.Append("<tr><td>&nbsp;</td></tr>");
                mailbody.Append("<tr><td>We would like to meet you to understand your requirements and to take this discussion further.<br/></td></tr>");
                mailbody.Append("<tr><td>&nbsp;</td></tr>");
                mailbody.Append("<tr><td>Looking forward for a positive reply.<br/></td></tr>");
                mailbody.Append("</table>");

                mailbody.Append("<br/><br/><table style='font-family:Arial; font-size:12px;'>");
                mailbody.Append("<tr><td><b>Thanks & Regards</b><br/><br/><b>Team Carzonrent</b></td></tr>");
                mailbody.Append("<tr><td>&nbsp;</td></tr>");
                mailbody.Append("<tr><td><img src='../Images/Logo.png' alt='' height='42' width='42'></td></tr>");
                mailbody.Append("<tr><td>Leadership award in Leisure and Travel at The greatest corporate leader of India 2014</td></tr>");
                mailbody.Append("<tr><td>Awarded as Tourist Transport Operator(Category 1) at National Tourism Awards 2013</td></tr>");
                mailbody.Append("<tr><td>Entrepreneur of the year at Entrepreneur India Awards 2013</td></tr>");
                mailbody.Append("<tr><td><br/><br/>This is system generated mail, Please do not reply.</td></tr></table></body></html>");
            }


            System.Net.Mail.MailMessage message = new System.Net.Mail.MailMessage();
            if (objProposalOL.ApprovalStatus == "Recheck" && !string.IsNullOrEmpty(ds.Tables[0].Rows[0]["EmailID"].ToString()))
            {
                message.To.Add(ds.Tables[0].Rows[0]["EmailID"].ToString());
            }
            else if (objProposalOL.ApprovalStatus == "Approve" && !string.IsNullOrEmpty(ds.Tables[0].Rows[0]["ContactPersonEmailId"].ToString()))
            {
                message.To.Add(ds.Tables[0].Rows[0]["ContactPersonEmailId"].ToString());
            }
            message.CC.Add("arpita.ghosh@carzonrent.com");
            message.Bcc.Add("balister.sharma@carzonrent.com");
            message.Subject = "Proposal Approval";
            message.From = new System.Net.Mail.MailAddress("booking@carzonrent.com");
            message.IsBodyHtml = true;
            message.Body = mailbody.ToString();
            message.Priority = MailPriority.High;
            System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient("smtp.gmail.com", 587);
            smtp.EnableSsl = true;
            smtp.Credentials = new NetworkCredential("booking@carzonrent.com", "Carz@1212");
            smtp.Send(message);
            BindApprovalDetails();
        }
    }
}