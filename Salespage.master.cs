using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

public partial class Salespage : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserID"] != null && Convert.ToInt32(Session["UserID"]) > 0)
        {
            PopulateMenu();
            GetUserName();
        }
        else
        {
            Response.Redirect("http://insta.carzonrent.com");
        } 
    }

   //private  DataSet MenuItems()
   // {
   //     SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CRDMenu"].ToString());
   //     SqlDataAdapter MainMenu = new SqlDataAdapter("select corintModuleMaster.moduleID ,corintModuleMaster.ModuleName from corintModuleMaster INNER JOIN   CorintModuleFunctionMaster on corintModuleMaster.moduleID=CorintModuleFunctionMaster.ModuleID INNER JOIN CorintSysUsersModAccessMaster On CorintModuleFunctionMaster.FunctionID = CorintSysUsersModAccessMaster.ModFuncID where corintModuleMaster.ModuleID=126 and CorintModuleFunctionMaster.active=1 and CorintSysUsersModAccessMaster.SysUserID = " + Convert.ToInt32(Session["UserID"]) + " group by corintModuleMaster.ModuleID ,ModuleName", con);
   //     SqlDataAdapter SubMenu = new SqlDataAdapter("select CorintModuleFunctionMaster.moduleID,CorintModuleFunctionMaster.FunctionName,CorintModuleFunctionMaster.Link from corintModuleMaster INNER JOIN   CorintModuleFunctionMaster on corintModuleMaster.moduleID=CorintModuleFunctionMaster.ModuleID INNER JOIN CorintSysUsersModAccessMaster On CorintModuleFunctionMaster.FunctionID = CorintSysUsersModAccessMaster.ModFuncID where corintModuleMaster.ModuleID=126 and CorintModuleFunctionMaster.active=1  and CorintSysUsersModAccessMaster.SysUserID =" + Convert.ToInt32(Session["UserID"]) + " order by FunctionOrderId  ", con);

   
   //     DataSet ds = new DataSet();
   //     MainMenu.Fill(ds, "corintModuleMaster");
   //     SubMenu.Fill(ds, "CorintModuleFunctionMaster");
   //     ds.Relations.Add("SubMenu", ds.Tables["corintModuleMaster"].Columns["ModuleID"], ds.Tables["CorintModuleFunctionMaster"].Columns["ModuleID"]);
   //     return ds;
   // }

   //private void PopulateMenu()
   // {
   //     DataSet ds = MenuItems();

   //     if (ds.Tables["CorintModuleFunctionMaster"].Rows.Count > 0)
   //     {

   //         Menu menu = new Menu();
   //         menu.Orientation = Orientation.Horizontal;
   //         menu.StaticTopSeparatorImageUrl = "~/App_Images/separator.gif";
   //         menu.EnableTheming = true;
   //         menu.SkipLinkText = " ";
   //         menu.Width = 543;
   //         menu.CssClass = "topnav";
   //         menu.StaticMenuStyle.CssClass = "orngCopy";
   //         menu.StaticMenuItemStyle.CssClass = "orngCopy";
   //         foreach (DataRow MainMenu in ds.Tables["corintModuleMaster"].Rows)
   //         {
   //             //MenuItem CategoryItem = new MenuItem((string)MainMenu["ModuleName"]);
   //             //menu.Items.Add(CategoryItem);

   //             foreach (DataRow subMenu in MainMenu.GetChildRows("SubMenu"))
   //             {
   //                 MenuItem SubCategoryItem = new MenuItem((string)subMenu["FunctionName"].ToString());
   //                 SubCategoryItem.NavigateUrl = subMenu["Link"].ToString();
   //                 menu.Items.Add(SubCategoryItem);
   //                 //CategoryItem.ChildItems.Add(SubCategoryItem );
   //             }
   //         }

   //         ContentPlaceHolder1.Controls.Add(menu);
   //         //Panel1.Controls.Add(menu);
   //         //Panel1.DataBind();

   //     }
   //     else
   //     {
   //         ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Message", "alert('sorry! you are not authorized to CRM  access.');location.href='http://insta.carzonrent.com'", true);
            
   //     }
   // }

    private string GetUserName()
    {
        string UserName = "";

        SqlConnection mycon = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"].ToString());
        SqlDataAdapter adusername = new SqlDataAdapter("select  top 1 loginid from dbo.corintsysusersmaster where sysuserid = " + int.Parse(Session["Userid"].ToString()), mycon);
        DataSet dbusername = new DataSet();
        adusername.Fill(dbusername);

        if (dbusername.Tables[0].Rows.Count > 0)
        {
            lblUserID.Text = "Logged In : " + Convert.ToString(dbusername.Tables[0].Rows[0]["loginid"]);
        }

        return UserName;
    }

    private DataSet GetDataSetForMenu()
    {
       // SqlConnection myConnection = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["ConnectionString"].ToString());
        SqlConnection myConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CRDMenu"].ToString());
        SqlDataAdapter adCat = new SqlDataAdapter("exec prc_GetModule_Master_Sales " + int.Parse(Session["Userid"].ToString()), myConnection);
        SqlDataAdapter adProd = new SqlDataAdapter("exec prc_GetModule_MenuMaster_Sales " + int.Parse(Session["Userid"].ToString()), myConnection);
        DataSet ds = new DataSet();
        adCat.Fill(ds, "CORIntModuleMaster");
        adProd.Fill(ds, "CORIntModuleFunctionMaster");
        ds.Relations.Add("Children",
        ds.Tables["CORIntModuleMaster"].Columns["Moduleid"],
        ds.Tables["CORIntModuleFunctionMaster"].Columns["Moduleid"]);
        return ds;
    }

    private void PopulateMenu()
    {
        DataSet ds = GetDataSetForMenu();
        Menu menu = new Menu();

        foreach (DataRow parentItem in ds.Tables["CORIntModuleMaster"].Rows)
        {
            MenuItem categoryItem = new MenuItem((string)parentItem["ModuleName"]);
            mnuTop.Items.Add(categoryItem);

            //menu.Orientation = "Horizontal";
            foreach (DataRow childItem in parentItem.GetChildRows("Children"))
            {
                MenuItem childrenItem = new MenuItem((string)childItem["FunctionName"]);
                childrenItem.NavigateUrl = childItem["Link"].ToString();
                categoryItem.ChildItems.Add(childrenItem);
            }
        }

        Panel1.Controls.Add(mnuTop);
        Panel1.DataBind();
    }

}
