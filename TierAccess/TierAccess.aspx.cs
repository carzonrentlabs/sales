﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

public partial class TierAccess_TierAccess : System.Web.UI.Page
{
    string connectionString = ConfigurationManager.ConnectionStrings["CRDMenu"].ToString();
    SqlConnection myConnection = null;
    SqlCommand cmd = null;
    SqlDataAdapter adp = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            BindUser();
            BindCityName();
            //BindModuleName();
        }


    }
    private void BindUser()
    {
        DataSet dsUser = new DataSet();
        using (myConnection = new SqlConnection(connectionString))
        {
            adp = new SqlDataAdapter("select fname+' '+mname+' '+lname as username,sysuserid from CORIntSysUsersMaster order by fname+' '+mname+' '+lname ", myConnection);
            adp.Fill(dsUser);
            ddlUserName.DataSource = dsUser.Tables[0];
            ddlUserName.DataTextField = "username";
            ddlUserName.DataValueField = "sysuserid";
            ddlUserName.DataBind();
            ddlUserName.Items.Insert(0, new ListItem("--Select--", "0"));
        }

    }
    private void BindCityName()
    {
        DataSet dsUser = new DataSet();
        using (myConnection = new SqlConnection(connectionString))
        {
            adp = new SqlDataAdapter("select cityId,cityName from corintcityMaster", myConnection);
            adp.Fill(dsUser);
            ddlCityName.DataSource = dsUser.Tables[0];
            ddlCityName.DataTextField = "cityName";
            ddlCityName.DataValueField = "cityId";
            ddlCityName.DataBind();
            ddlCityName.Items.Insert(0, new ListItem("--Select--", "0"));
        }

    }
    private void BindModuleName()
    {
        //DataSet dsUser = new DataSet();
        //using (myConnection = new SqlConnection(connectionString))
        //{
        //    adp = new SqlDataAdapter("select FunctionID,FunctionName  from CORIntModuleFunctionMaster where FunctionID in (63,64,65,67,888,953)", myConnection);
        //    adp.Fill(dsUser);
        //    ddlModuleName.DataSource = dsUser.Tables[0];
        //    ddlModuleName.DataTextField = "FunctionName";
        //    ddlModuleName.DataValueField = "FunctionID";
        //    ddlModuleName.DataBind();
        //    ddlModuleName.Items.Insert(0, new ListItem("--Select--", "0"));
        //}
    }

    protected void btnGet_Click(object sender, EventArgs e)
    {
        BindGridData();
    }
    private void BindGridData()
    {

        DataSet ds = new DataSet();
        lblMessage.Text = "";
        if (ddlUserName.SelectedIndex == 0)
        {
            lblMessage.Visible = true;
            lblMessage.Text = "Select user name";
            lblMessage.ForeColor = System.Drawing.Color.Red;
        }
        else
        {
            try
            {

                using (myConnection = new SqlConnection(connectionString))
                {
                    myConnection.Open();
                    cmd = new SqlCommand();
                    cmd.CommandText = "Prc_Get2TierCityAccessRights";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@SysUserId", ddlUserName.SelectedValue);
                    cmd.Parameters.AddWithValue("@cityID", ddlCityName.SelectedValue);
                    cmd.Connection = myConnection;
                    adp = new SqlDataAdapter(cmd);
                    adp.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        grvUserAccess.DataSource = ds.Tables[0];
                        grvUserAccess.DataBind();
                    }
                    else
                    {
                        grvUserAccess.DataSource = null;
                        grvUserAccess.DataBind();
                        lblMessage.Visible = true;
                        lblMessage.Text = "No Record Available";
                        lblMessage.ForeColor = System.Drawing.Color.Red;
                    }

                }

            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }
    }

    protected void grvUserAccess_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        DataSet ds = new DataSet();
       
        try
        {
            if (e.Row.RowType == DataControlRowType.Footer)
            {
                string  userName = Convert.ToString(ddlUserName.SelectedItem.Text);

                DropDownList ddlUserNameg = (DropDownList)e.Row.FindControl("ddlUserNameAdd");
                BindDropDownList(ddlUserNameg, "useName", userName);
                DropDownList ddlFuncationName = (DropDownList)e.Row.FindControl("ddlFunctionNameAdd");
                BindDropDownList(ddlFuncationName, "functionName", "-1");
                DropDownList ddlCityName = (DropDownList)e.Row.FindControl("ddlCityNameAdd");
                BindDropDownList(ddlCityName, "cityName", "-1");
            }
            else if (grvUserAccess.EditIndex == e.Row.RowIndex && e.Row.RowType == DataControlRowType.DataRow)
            {
                DropDownList ddlUserNamee = (DropDownList)e.Row.FindControl("ddlUserNameEdit");
                Label lbluserName = (Label)e.Row.FindControl("lblUserNameEdit");
                BindDropDownList(ddlUserNamee, "useName", lbluserName.Text.ToString());
                DropDownList ddlFuncationName = (DropDownList)e.Row.FindControl("ddlFunctionNameEdit");
                Label lblFunctionName = (Label)e.Row.FindControl("lblFunctionNameEdit");
                BindDropDownList(ddlFuncationName, "functionName", lblFunctionName.Text.ToString());
                DropDownList ddlCityName = (DropDownList)e.Row.FindControl("ddlCityNameEdit");
                Label lblCityName = (Label)e.Row.FindControl("lblCityNameEdit");
                BindDropDownList(ddlCityName, "cityName", lblCityName.Text.ToString());
                DropDownList ddlStatus = (DropDownList)e.Row.FindControl("ddlStatusEdit");
                Label lblStatus = (Label)e.Row.FindControl("lblStatusEdit");
                BindDropDownList(ddlStatus, "status", lblStatus.Text.ToString());
            }
        }
        catch (Exception Ex)
        {
            throw new Exception(Ex.Message);
        }
    }

    private void BindDropDownList(DropDownList ddlName, string bindControl, string selectedValue)
    {
        DataSet ds = new DataSet();
        string dataTextField = string.Empty;
        string dataValueField = string.Empty;
        string QueryString = string.Empty;

        if (ddlName != null)
        {

            if (bindControl == "useName")
            {
                QueryString = "select fname+' '+mname+' '+lname as username,sysuserid from CORIntSysUsersMaster order by fname+' '+mname+' '+lname ";
                dataTextField = "username";
                dataValueField = "sysuserid";

            }

            else if (bindControl == "functionName")
            {
                QueryString = "select FunctionID,FunctionName  from CORIntModuleFunctionMaster where FunctionID in (63,64,65,67,888,953,1039,259)";
                dataTextField = "FunctionName";
                dataValueField = "FunctionID";
            }
            else if (bindControl == "cityName")
            {
                QueryString = "select cityId,cityName from corintcityMaster";
                dataTextField = "cityName";
                dataValueField = "cityId";
            }
            if (bindControl != "status")
            {

                using (myConnection = new SqlConnection(connectionString))
                {
                    adp = new SqlDataAdapter(QueryString, myConnection);
                    adp.Fill(ds);
                    ddlName.Items.Clear();
                    ddlName.DataSource = ds.Tables[0];
                    ddlName.DataTextField = dataTextField;
                    ddlName.DataValueField = dataValueField;
                    ddlName.DataBind();
                    ddlName.Items.Insert(0, new ListItem("-Select-", "0"));
                    if (!string.IsNullOrEmpty(selectedValue) && selectedValue != "-1")
                    {
                        //ddlName.SelectedItem.Text = selectedValue;
                        ddlName.Items.FindByText(selectedValue).Selected = true;
                    }
                }
            }
            else
            {
                ddlName.Items.Insert(0, new ListItem("Active", "1"));
                ddlName.Items.Insert(1, new ListItem("Inactive", "0"));
                //ddlName.SelectedItem.Text = selectedValue;
                ddlName.Items.FindByText(selectedValue).Selected = true;
            }
        }

    }

    protected void grvUserAccess_RowEditing(object sender, GridViewEditEventArgs e)
    {
        grvUserAccess.EditIndex = e.NewEditIndex;
        BindGridData();

    }
    protected void grvUserAccess_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        grvUserAccess.EditIndex = -1;
        BindGridData();
    }
    protected void grvUserAccess_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        int status = 0;
        lblMessage.Text = string.Empty;
        try
        {
            using (myConnection = new SqlConnection(connectionString))
            {

                DropDownList userId = (DropDownList)grvUserAccess.Rows[e.RowIndex].FindControl("ddlUserNameEdit");
                DropDownList functionId = (DropDownList)grvUserAccess.Rows[e.RowIndex].FindControl("ddlFunctionNameEdit");
                DropDownList cityID = (DropDownList)grvUserAccess.Rows[e.RowIndex].FindControl("ddlCityNameEdit");
                DropDownList active = (DropDownList)grvUserAccess.Rows[e.RowIndex].FindControl("ddlStatusEdit");
                int cityWiseID = (int)grvUserAccess.DataKeys[e.RowIndex].Value;

                myConnection.Open();
                cmd = new SqlCommand();
                cmd.CommandText = "Prc_UpdateCityWiseAccess";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@CityWiseID", cityWiseID);
                cmd.Parameters.AddWithValue("@FunctionId", Convert.ToInt32(functionId.SelectedValue.ToString()));
                cmd.Parameters.AddWithValue("@Active", active.SelectedValue);
                cmd.Parameters.AddWithValue("@CityID", cityID.SelectedValue);
                cmd.Parameters.AddWithValue("@SysUserID", Session["UserID"]);
                cmd.Connection = myConnection;
                status = cmd.ExecuteNonQuery();

                if (status >= 1)
                {
                    lblMessage.Visible = true;
                    lblMessage.Text = "Update successfully";
                    lblMessage.ForeColor = System.Drawing.Color.Red;
                    BindGridData();
                }
                else
                {
                    lblMessage.Visible = true;
                    lblMessage.Text = "Update failed.";
                    lblMessage.ForeColor = System.Drawing.Color.Red;
                }
            }

        }
        catch (Exception EX)
        {
            lblMessage.Visible = true;
            lblMessage.Text = EX.Message;
            lblMessage.ForeColor = System.Drawing.Color.Red;
        }


    }
    protected void grvUserAccess_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        SqlParameter regIdentity;
        int status = 0;
        lblMessage.Text = string.Empty;
        try
        {

            if (e.CommandName == "AddNew")
            {
                using (myConnection = new SqlConnection(connectionString))
                {
                    DropDownList userId = (DropDownList)grvUserAccess.FooterRow.FindControl("ddlUserNameAdd");
                    DropDownList functionId = (DropDownList)grvUserAccess.FooterRow.FindControl("ddlFunctionNameAdd");
                    DropDownList cityID = (DropDownList)grvUserAccess.FooterRow.FindControl("ddlCityNameAdd");
                    DropDownList active = (DropDownList)grvUserAccess.FooterRow.FindControl("ddlStatusAdd");

                    myConnection.Open();
                    cmd = new SqlCommand();
                    cmd.CommandText = "Prc_AddCityWiseAccess";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@UserID", userId.SelectedValue);
                    cmd.Parameters.AddWithValue("@FunctionId", functionId.SelectedValue);
                    cmd.Parameters.AddWithValue("@CityID", cityID.SelectedValue);
                    cmd.Parameters.AddWithValue("@SysUserID", Session["UserID"]);
                    regIdentity = cmd.Parameters.AddWithValue("@StatusID", SqlDbType.Int);
                    regIdentity.Direction = ParameterDirection.Output;
                    cmd.Connection = myConnection;
                    cmd.ExecuteNonQuery();
                    status = Convert.ToInt32(cmd.Parameters["@StatusID"].Value);
                    if (status == 420)
                    {
                        lblMessage.Visible = true;
                        lblMessage.Text = "All ready registered.";
                        lblMessage.ForeColor = System.Drawing.Color.Red;
                    }
                    else if (status > 0)
                    {
                        lblMessage.Visible = true;
                        lblMessage.Text = "Registration successfully";
                        lblMessage.ForeColor = System.Drawing.Color.Red;
                        
                    }
                    else
                    {
                        lblMessage.Visible = true;
                        lblMessage.Text = "Registration failed.";
                        lblMessage.ForeColor = System.Drawing.Color.Red;

                    }

                }
            }
        }
        catch (Exception EX)
        {

            lblMessage.Visible = true;
            lblMessage.Text = EX.Message;
            lblMessage.ForeColor = System.Drawing.Color.Red;
        }
    }
}