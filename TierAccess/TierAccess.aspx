﻿<%@ Page Title="2&3 Tier Access Rights" Language="C#" MasterPageFile="~/Salespage.master" AutoEventWireup="true" CodeFile="TierAccess.aspx.cs" Inherits="TierAccess_TierAccess" EnableEventValidation="false" %>
<asp:Content ID="cntPlaceholder" ContentPlaceHolderID="ContentPlaceHolder_Page" runat="Server">
    <center>
        <table border="0" cellpadding="2" cellspacing="0">
            <tr>
                <td colspan="5" style="text-align: center;"><b>2 and 3 Tier City Access </b></td>
            </tr>
            <tr>
                <td colspan="5">
                    <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label></td>
            </tr>
            <tr>
                <td>User Name </td>
                <td>
                    <asp:DropDownList ID="ddlUserName" runat="server"></asp:DropDownList>&nbsp;
                    <asp:RequiredFieldValidator ID="rfvUserName" runat="server" ControlToValidate="ddlUserName" ErrorMessage="*" InitialValue="0" Display="Dynamic" ForeColor="Red" SetFocusOnError="true"></asp:RequiredFieldValidator>
                </td>
                <td>City Name</td>
                <td>
                    <asp:DropDownList ID="ddlCityName" runat="server"></asp:DropDownList>&nbsp;
                </td>
                <td>
                    <asp:Button ID="btnGet" runat="server" Text="Get IT" OnClick="btnGet_Click" />
                </td>
            </tr>
            <%--<tr>
                <td>
                    Module Name
                </td>
                <td colspan="2">
                    <asp:DropDownList ID="ddlModuleName" runat ="server"></asp:DropDownList>
                </td>
                <td colspan="2">
                    <asp:Button ID ="btnAdd" runat ="server" Text="Add" />
                </td>
            </tr>--%>

            <tr>
                <asp:GridView ID="grvUserAccess" runat="server" AutoGenerateColumns="false" ShowFooter="True" OnRowDataBound="grvUserAccess_RowDataBound" OnRowCancelingEdit="grvUserAccess_RowCancelingEdit"
                    OnRowEditing="grvUserAccess_RowEditing" OnRowUpdating="grvUserAccess_RowUpdating" OnRowCommand="grvUserAccess_RowCommand" DataKeyNames="citywiseID">
                    <Columns>
                        <asp:TemplateField HeaderText="UserName" SortExpression="UserName">
                            <ItemTemplate>
                                <asp:Label ID="lblUserName" runat="server" Text='<%#Eval("UserName") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Label ID="lblUserNameEdit" runat="server" Text='<%#Eval("UserName") %>' Visible="false"></asp:Label>
                                <asp:DropDownList ID="ddlUserNameEdit" runat="server">
                                </asp:DropDownList>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:DropDownList ID="ddlUserNameAdd" runat="server">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="rfvddlUserNameA" ValidationGroup="Add" runat="server" ControlToValidate="ddlUserNameAdd" ErrorMessage="*" InitialValue="0" Display="Dynamic" ForeColor="Red" SetFocusOnError="true"></asp:RequiredFieldValidator>
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="FunctionName" SortExpression="UserName">
                            <ItemTemplate>
                                <asp:Label ID="lblFunctionName" runat="server" Text='<%#Eval("FunctionName") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Label ID="lblFunctionNameEdit" runat="server" Text='<%#Eval("FunctionName") %>' Visible="false"></asp:Label>
                                <asp:DropDownList ID="ddlFunctionNameEdit" runat="server">
                                </asp:DropDownList>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:DropDownList ID="ddlFunctionNameAdd" runat="server">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="rfvddlFunctionNameAdd" runat="server" ValidationGroup="Add" ControlToValidate="ddlFunctionNameAdd" ErrorMessage="*" InitialValue="0" Display="Dynamic" ForeColor="Red" SetFocusOnError="true"></asp:RequiredFieldValidator>
                            </FooterTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="CityName" SortExpression="UserName">
                            <ItemTemplate>
                                <asp:Label ID="lblCityName" runat="server" Text='<%#Eval("CityName") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Label ID="lblCityNameEdit" runat="server" Text='<%#Eval("CityName") %>' Visible="false"></asp:Label>
                                <asp:DropDownList ID="ddlCityNameEdit" runat="server">
                                </asp:DropDownList>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:DropDownList ID="ddlCityNameAdd" runat="server">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="rfvddlCityNameAdd" ValidationGroup="Add" runat="server" ControlToValidate="ddlCityNameAdd" ErrorMessage="*" InitialValue="0" Display="Dynamic" ForeColor="Red" SetFocusOnError="true"></asp:RequiredFieldValidator>
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Status" SortExpression="UserName">
                            <ItemTemplate>
                                <asp:Label ID="lblStatus" runat="server" Text='<%#Eval("Status") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Label ID="lblStatusEdit" runat="server" Text='<%#Eval("Status") %>' Visible="false"></asp:Label>
                                <asp:DropDownList ID="ddlStatusEdit" runat="server">
                                </asp:DropDownList>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:DropDownList ID="ddlStatusAdd" runat="server">
                                    <asp:ListItem Value="1" Text="Active"></asp:ListItem>
                                    <asp:ListItem Value="0" Text="Inactive"></asp:ListItem>
                                </asp:DropDownList>
                            </FooterTemplate>
                        </asp:TemplateField>


                        <asp:TemplateField HeaderText="Edit" ShowHeader="False">
                            <EditItemTemplate>
                                <asp:LinkButton ID="lnkUpdate" runat="server" CommandName="Update"
                                    Text="Update"></asp:LinkButton>
                                <asp:LinkButton ID="lnkCancel" runat="server" CommandName="Cancel"
                                    Text="Cancel"></asp:LinkButton>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:LinkButton ID="lnkAddNew" runat="server" ValidationGroup="Add" CommandName="AddNew"
                                    Text="Add New"></asp:LinkButton>
                            </FooterTemplate>
                            <ItemTemplate>
                                <asp:LinkButton ID="LnkEdit" runat="server" CommandName="Edit"
                                    Text="Edit"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </tr>
        </table>
    </center>

</asp:Content>
